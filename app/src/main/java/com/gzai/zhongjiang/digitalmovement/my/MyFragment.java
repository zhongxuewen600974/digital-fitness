package com.gzai.zhongjiang.digitalmovement.my;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.MyStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.coach.BookingManagementActivity;
import com.gzai.zhongjiang.digitalmovement.coach.CoachQrcodeActivity;
import com.gzai.zhongjiang.digitalmovement.coach.PrivateStudentsActivity;
import com.gzai.zhongjiang.digitalmovement.coach.StudyOrderCommentActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.step.ISportStepInterface;
import com.gzai.zhongjiang.digitalmovement.util.step.TodayStepManager;
import com.gzai.zhongjiang.digitalmovement.util.step.TodayStepService;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.SimpleDateFormat;
import java.util.Date;


public class MyFragment extends Fragment implements View.OnClickListener {
    private static final int REFRESH_STEP_WHAT = 0;
    private long TIME_INTERVAL_REFRESH = 3000;
    private Handler mDelayHandler = new Handler(new TodayStepCounterCall());
    private int mStepSum;
    private ISportStepInterface iSportStepInterface;
    private TextView stepTextView, my_name, my_mobile, follow_count, befollow_count, post_count,my_rank;
    LinearLayout follow_linear, follow_linear1, to_my_page,to_fitdata,to_mypage,to_step,setting_linear,to_vip_linear,
            to_trainer,to_motin,to_feed,to_students,to_teaching,to_management,vip_role,sijiao_role,jiaolian_role,coach_linear;
    CircleImageView head_image;
    ImageView to_setting;
    SmartRefreshLayout smartRefreshLayout;
    private String user_id,  datedate;
    int step = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        smartRefreshLayout = view.findViewById(R.id.smartRefreshLayout);
        head_image = view.findViewById(R.id.head_image);
        my_name = view.findViewById(R.id.my_name);
        my_mobile = view.findViewById(R.id.my_mobile);
        follow_count = view.findViewById(R.id.follow_count);
        befollow_count = view.findViewById(R.id.befollow_count);
        post_count = view.findViewById(R.id.post_count);
        follow_linear = view.findViewById(R.id.follow_linear);
        follow_linear1 = view.findViewById(R.id.follow_linear1);
        setting_linear=view.findViewById(R.id.setting_linear);
        to_vip_linear=view.findViewById(R.id.to_vip_linear);
        to_trainer=view.findViewById(R.id.to_trainer);
        to_motin=view.findViewById(R.id.to_motin);
        my_rank=view.findViewById(R.id.my_rank);
        to_feed=view.findViewById(R.id.to_feed);
        vip_role=view.findViewById(R.id.vip_role);
        sijiao_role=view.findViewById(R.id.sijiao_role);
        jiaolian_role=view.findViewById(R.id.jiaolian_role);
        to_students=view.findViewById(R.id.to_students);
        to_teaching=view.findViewById(R.id.to_teaching);
        to_management=view.findViewById(R.id.to_management);
        coach_linear=view.findViewById(R.id.coach_linear);
        to_management.setOnClickListener(this);
        to_teaching.setOnClickListener(this);
        to_students.setOnClickListener(this);
        to_feed.setOnClickListener(this);
        to_motin.setOnClickListener(this);
        to_trainer.setOnClickListener(this);
        to_vip_linear.setOnClickListener(this);
        setting_linear.setOnClickListener(this);
        to_step=view.findViewById(R.id.to_step);
        to_step.setOnClickListener(this);
        to_setting = view.findViewById(R.id.to_setting);
        to_my_page = view.findViewById(R.id.to_my_page);
        to_fitdata=view.findViewById(R.id.to_fitdata);
        to_mypage=view.findViewById(R.id.to_mypage);
        to_mypage.setOnClickListener(this);
        to_fitdata.setOnClickListener(this);
        to_my_page.setOnClickListener(this);
        to_setting.setOnClickListener(this);
        follow_linear.setOnClickListener(this);
        follow_linear1.setOnClickListener(this);
        stepTextView = (TextView) view.findViewById(R.id.stepTextView);
        //初始化计步模块
        TodayStepManager.startTodayStepService(getActivity().getApplication());
        //开启计步Service，同时绑定Activity进行aidl通信
        Intent intent = new Intent(getActivity(), TodayStepService.class);
        getActivity().startService(intent);
        getActivity().bindService(intent, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                //Activity和Service通过aidl进行通信
                iSportStepInterface = ISportStepInterface.Stub.asInterface(service);
                try {
                    mStepSum = iSportStepInterface.getCurrentTimeSportStep();
                    updateStepCount();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                mDelayHandler.sendEmptyMessageDelayed(REFRESH_STEP_WHAT, TIME_INTERVAL_REFRESH);

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        }, Context.BIND_AUTO_CREATE);

        //计时器
        mhandmhandlele.post(timeRunable);
        intView();

        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            intView();
                            getMyStepRank();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setEnableLoadMore(false);
        }


        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        datedate = sdf.format(new Date());
        getMyStepRank();
        return view;
    }

    private void getMyStepRank() {
        RequestUtils.getMyStepRank(SharePreUtil.getString(getContext(), "token", ""),  new MyObserver<MyStepBean>(getContext()) {
            @Override
            public void onSuccess(MyStepBean result) {
                if(stepTextView.getText().toString().equals("0")){
                    stepTextView.setText(result.getStep_count());
                }
                my_rank.setText(result.getRownum());
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }



    private void intView() {
        RequestUtils.getUserInfo(SharePreUtil.getString(getContext(), "token", ""), new MyObserver<MyUserInfo>(getContext()) {
            @Override
            public void onSuccess(MyUserInfo result) {
                try {
                    if (result.getAvatar().length() > 0) {
                        Glide.with(getContext()).load(result.getAvatar()).into(head_image);
                    } else {
                        head_image.setImageResource(R.drawable.mine_head_icon);
                    }
                    user_id = result.getId();
                    my_name.setText(result.getNick_name());
                    my_mobile.setText(result.getMobile());
                    SharePreUtil.putString(getContext(),"mobile",result.getMobile());
                    follow_count.setText(result.getFollow_count());
                    befollow_count.setText(result.getBefollow_count());
                    post_count.setText(result.getPost_count());
                    if(result.getRole().equals("2")){
                        jiaolian_role.setVisibility(View.VISIBLE);
                        coach_linear.setVisibility(View.VISIBLE);
                    }else {
                        jiaolian_role.setVisibility(View.GONE);
                        coach_linear.setVisibility(View.GONE);
                    }
                    if(result.getVip().equals("1")){
                        vip_role.setVisibility(View.VISIBLE);
                    }else {
                        vip_role.setVisibility(View.GONE);
                    }
                    if (result.getPer_vip().equals("1")){
                        sijiao_role.setVisibility(View.VISIBLE);

                    }else {
                        sijiao_role.setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    @Override
    public void onDestroyView() {
        if(step>0){
            stepReport(datedate,step+"");
        }
        super.onDestroyView();
    }
    private void stepReport(String record_date, String step_count) {
        RequestUtils.stepReport(SharePreUtil.getString(getContext(), "token", ""), record_date, step_count,new NollDataMyObserver<NullData>(getContext()) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }



    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.follow_linear:
                Intent intent = new Intent(getActivity(), FollowActivity.class);
                intent.putExtra("CurrentItem","0");
                startActivity(intent);
                break;
            case R.id.follow_linear1:
                Intent intent1 = new Intent(getActivity(), FollowActivity.class);
                intent1.putExtra("CurrentItem","1");
                startActivity(intent1);
                break;
            case R.id.to_setting:
                Intent intent2 = new Intent(getActivity(), ModInfoActivity.class);
                startActivity(intent2);
                break;
            case R.id.to_my_page:
                Intent intent3 = new Intent(getActivity(), OthersPageActivity.class);
                intent3.putExtra("user_id", user_id);
                startActivity(intent3);
                break;
            case R.id.to_fitdata:
                Intent intent4 = new Intent(getActivity(), FitnessDataActivity.class);
                startActivity(intent4);
                break;
            case R.id.to_mypage:
                Intent intent5 = new Intent(getActivity(), OthersPageActivity.class);
                intent5.putExtra("user_id", user_id);
                startActivity(intent5);
                break;
            case R.id.to_step:
                if(step>0){
                    stepReport(datedate,step+"");
                }
                Intent intent6 = new Intent(getActivity(), StepsActivity.class);
                startActivity(intent6);
                break;
            case R.id.setting_linear:
                Intent intent7 = new Intent(getActivity(), SettingActivity.class);
                 startActivity(intent7);
                break;
            case R.id.to_vip_linear:
                Intent intent8 = new Intent(getActivity(), VipCenterActivity.class);
                startActivity(intent8);
                break;
            case R.id.to_trainer:
                Intent intent9 = new Intent(getActivity(), PersonTActivity.class);
                intent9.putExtra("user_id",user_id);
                startActivity(intent9);
                break;
            case R.id.to_motin:
                ToastUtils.show("暂未开通");
//                Intent intent10 = new Intent(getActivity(), StudyOrderCommentActivity.class);
//                startActivity(intent10);
                break;
            case R.id.to_feed:
                Intent intent11 = new Intent(getActivity(), MyFeedbackActivity.class);
                startActivity(intent11);
                break;
            case R.id.to_students:
                Intent intent12 = new Intent(getActivity(), PrivateStudentsActivity.class);
                startActivity(intent12);
                break;
            case R.id.to_teaching:
                Intent intent13 = new Intent(getActivity(), CoachQrcodeActivity.class);
                startActivity(intent13);
                break;
            case R.id.to_management:
                Intent intent14 = new Intent(getActivity(), BookingManagementActivity.class);
                intent14.putExtra("user_id",user_id);
                startActivity(intent14);
                break;
        }
    }

    class TodayStepCounterCall implements Handler.Callback {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_STEP_WHAT: {
                    //每隔500毫秒获取一次计步数据刷新UI
                    if (null != iSportStepInterface) {

                        try {
                            step = iSportStepInterface.getCurrentTimeSportStep();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        if (mStepSum != step) {
                            mStepSum = step;
                            updateStepCount();
                        }
                    }
                    mDelayHandler.sendEmptyMessageDelayed(REFRESH_STEP_WHAT, TIME_INTERVAL_REFRESH);

                    break;
                }
            }
            return false;
        }
    }

    private void updateStepCount() {
        stepTextView.setText(mStepSum + "");
    }

    /*****************计时器*******************/
    private Runnable timeRunable = new Runnable() {
        @Override
        public void run() {
            currentSecond = currentSecond + 1000;
            if (!isPause) {
                //递归调用本runable对象，实现每隔一秒一次执行任务
                mhandmhandlele.postDelayed(this, 1000);
            }
        }
    };
    //计时器
    private Handler mhandmhandlele = new Handler();
    private boolean isPause = false;//是否暂停
    private long currentSecond = 0;//当前毫秒数
/*****************计时器*******************/

    /**
     * 根据毫秒返回时分秒
     *
     * @param time
     * @return
     */
    public static String getFormatHMS(long time) {
        time = time / 1000;//总秒数
        int s = (int) (time % 60);//秒
        int m = (int) (time / 60);//分
        int h = (int) (time / 3600);//秒
        return String.format("%02d:%02d:%02d", h, m, s);
    }





}