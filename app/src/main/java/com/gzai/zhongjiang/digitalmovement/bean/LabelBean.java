package com.gzai.zhongjiang.digitalmovement.bean;

import java.util.List;

public class LabelBean {
    private String id;
    private String type_name;
    List<LabelTextBean> label_list;
    public LabelBean(String id,String type_name,List<LabelTextBean> label_list){
        this.id=id;
        this.type_name=type_name;
        this.label_list=label_list;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public List<LabelTextBean> getLabel_list() {
        return label_list;
    }

    public void setLabel_list(List<LabelTextBean> label_list) {
        this.label_list = label_list;
    }
}
