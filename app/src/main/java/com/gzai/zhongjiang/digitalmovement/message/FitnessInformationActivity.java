package com.gzai.zhongjiang.digitalmovement.message;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.NewAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.PraiseAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.NewsListBean;
import com.gzai.zhongjiang.digitalmovement.bean.PraiseNoticeListBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FitnessInformationActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private int page_total, page = 1;
    NewsListBean dyBean;
    List<NewsListBean> beanList = new ArrayList<>();
    NewAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_information);
        ButterKnife.bind(this);
        actionBarView.setTitle("健身资讯");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                 loadMore(page);
                            } else {
                             //   ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
         intView();
    }


    private void loadMore(int page) {
        RequestUtils.getNewsList(SharePreUtil.getString(this, "token", ""),page,10, new ListMyObserver<ListBean<NewsListBean>>(this) {
            @Override
            public void onSuccess(ListBean<NewsListBean> result) {
                ListBean<NewsListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String title = list.getList().get(i).getTitle();
                        String image = list.getList().get(i).getImage();
                        String intro = list.getList().get(i).getIntro();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new NewsListBean(id, title, image, intro, update_time, create_time);
                        beanList.add(dyBean);

                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }


    private void intView() {
        beanList.clear();
        RequestUtils.getNewsList(SharePreUtil.getString(this, "token", ""),1,10, new ListMyObserver<ListBean<NewsListBean>>(this) {
            @Override
            public void onSuccess(ListBean<NewsListBean> result) {
                ListBean<NewsListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String title = list.getList().get(i).getTitle();
                        String image = list.getList().get(i).getImage();
                        String intro = list.getList().get(i).getIntro();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new NewsListBean(id, title, image, intro, update_time, create_time);
                        beanList.add(dyBean);

                    }
                    myAdapter = new NewAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);

                } else {
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }
}