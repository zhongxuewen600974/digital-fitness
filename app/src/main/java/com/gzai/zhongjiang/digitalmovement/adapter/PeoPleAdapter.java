package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentChild;
import com.gzai.zhongjiang.digitalmovement.bean.GymRegionBean;

import java.util.List;

public class PeoPleAdapter extends RecyclerView.Adapter<PeoPleAdapter.ViewHolder> {
    private Context mContext;
    private List<GymRegionBean> dataBean;


    public PeoPleAdapter(List<GymRegionBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gympeo, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.number.setText(dataBean.get(position).getMembers() + "");
            holder.gym_name.setText(dataBean.get(position).getRegion_name());


            if (position == 0) {
                holder.bg.setBackgroundResource(R.drawable.bg_1);
            } else if (position == 1) {
                holder.bg.setBackgroundResource(R.drawable.bg_2);
            } else if (position == 2) {
                holder.bg.setBackgroundResource(R.drawable.bg_3);
            } else if (position == 3) {
                holder.bg.setBackgroundResource(R.drawable.bg_4);
            } else if (position == 4) {
                holder.bg.setBackgroundResource(R.drawable.bg_5);
            } else if (position == 5) {
                holder.bg.setBackgroundResource(R.drawable.bg_9);
            } else if (position == 6) {
                holder.bg.setBackgroundResource(R.drawable.bg_6);
            } else if (position == 7) {
                holder.bg.setBackgroundResource(R.drawable.bg_7);
            } else if (position == 8) {
                holder.bg.setBackgroundResource(R.drawable.bg_8);
            } else {
                holder.bg.setBackgroundResource(R.drawable.bg_3);
            }

        } catch (Exception e) {
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout bg;
        TextView number, peo_ren, gym_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            bg = (LinearLayout) itemView.findViewById(R.id.peo_linear);
            number = (TextView) itemView.findViewById(R.id.number);
            peo_ren = (TextView) itemView.findViewById(R.id.peo_ren);
            gym_name = (TextView) itemView.findViewById(R.id.gym_name);
        }
    }

}
