package com.gzai.zhongjiang.digitalmovement.release;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.LabelAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.LabelBean;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.UploadImageBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.RetrofitUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.LookImageviewActivity;
import com.gzai.zhongjiang.digitalmovement.view.MyDialogOpen;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.GlideImageLoader;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.ImagePickerLoader;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGirdImageContainer;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridBean;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridView;
import com.hjq.toast.ToastUtils;
import com.lwkandroid.imagepicker.ImagePicker;
import com.lwkandroid.imagepicker.data.ImageBean;
import com.lwkandroid.imagepicker.data.ImagePickType;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.FullScreenPopupView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ReleaseActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, NineGridView.onItemClickListener, View.OnClickListener, MyDialogOpen.OnBottomMenuItemClickListener {
    private NineGridView mNineGridView;
    private MyDialogOpen bottomDialog;
    private final int REQUEST_CODE_PICKER = 100;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.release)
    TextView release;
    @BindView(R.id.input_release)
    EditText input_release;
    @BindView(R.id.release_number)
    TextView release_number;
    @BindView(R.id.add_lable)
    LinearLayout add_lable;
    @BindView(R.id.release_label)
    TextView release_label;
    @BindView(R.id.release_status)
    LinearLayout release_status;
    @BindView(R.id.release_status_text)
    TextView release_status_text;
    @BindView(R.id.release_status_image)
    ImageView release_status_image;

    private int imageNumber = 0;
    ArrayList<String> choosePic = new ArrayList<>();
    ArrayList<String> backimageUrl = new ArrayList<>();
    String is_open = "1";
    Gson gson = new Gson();
    private RecyclerView recyclerView;

    private String lab_id = "", circle_id = "", type = "";

    LabelBean myBean;
    List<LabelBean> beanList = new ArrayList<>();
    LabelAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_release);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        try {
            Intent intent = getIntent();
            circle_id = intent.getStringExtra("circle_id");
            type = intent.getStringExtra("type");

        } catch (Exception e) {
        }

        back.setOnClickListener(this);
        release.setOnClickListener(this);
        add_lable.setOnClickListener(this);
        release_status.setOnClickListener(this);
        mNineGridView = (NineGridView) findViewById(R.id.ninegridview);
        //设置图片加载器，这个是必须的，不然图片无法显示
        mNineGridView.setImageLoader(new GlideImageLoader());
        //设置显示列数，默认3列
        mNineGridView.setColumnCount(3);
        //设置是否为编辑模式，默认为false
        mNineGridView.setIsEditMode(true);
        //设置单张图片显示时的尺寸，默认100dp
        mNineGridView.setSingleImageSize(150);
        //设置单张图片显示时的宽高比，默认1.0f
        mNineGridView.setSingleImageRatio(0.8f);
        //设置最大显示数量，默认9张
        mNineGridView.setMaxNum(9);
        //设置图片显示间隔大小，默认3dp
        mNineGridView.setSpcaeSize(3);
        //设置删除图片
        //        mNineGridView.setIcDeleteResId(R.drawable.ic_block_black_24dp);
        //设置删除图片与父视图的大小比例，默认0.25f
        mNineGridView.setRatioOfDeleteIcon(0.3f);
        //设置“+”号的图片
        mNineGridView.setIcAddMoreResId(R.drawable.ic_ngv_add_pic);
        //设置各类点击监听
        mNineGridView.setOnItemClickListener(this);


        input_release.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                release_number.setText(s.length() + "/" + "300");
                if (s.length() >= 300) {
                    release_number.setTextColor(Color.parseColor("#E91E63"));
                } else {
                    release_number.setTextColor(Color.parseColor("#333333"));

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        bottomDialog = new MyDialogOpen(this, R.layout.dailog_open, new int[]{R.id.only_i_look, R.id.open, R.id.diamiss});
        bottomDialog.setOnBottomMenuItemClickListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mNineGridView.setIsEditMode(b);
    }

    @Override
    public void onNineGirdAddMoreClick(int cha) {
        //编辑模式下，图片展示数量尚未达到最大数量时，会显示一个“+”号，点击后回调这里
        new ImagePicker()
                .cachePath(Environment.getExternalStorageDirectory().getAbsolutePath())
                .pickType(ImagePickType.MULTI)
                .displayer(new ImagePickerLoader())
                .maxNum(cha)
                .start(this, REQUEST_CODE_PICKER);
    }

    @Override
    public void onNineGirdItemClick(int position, NineGridBean gridBean, NineGirdImageContainer imageContainer) {
        String uid = gridBean.getOriginUrl().toString();
        Intent intent = new Intent(ReleaseActivity.this, LookImageviewActivity.class);
        intent.putExtra("image_url", uid);
        startActivity(intent);
    }


    @Override
    public void onNineGirdItemDeleted(int position, NineGridBean gridBean, NineGirdImageContainer imageContainer) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            List<ImageBean> list = data.getParcelableArrayListExtra(ImagePicker.INTENT_RESULT_DATA);
            List<NineGridBean> resultList = new ArrayList<>();
            for (ImageBean imageBean : list) {
                choosePic.add(imageBean.getImagePath());
                NineGridBean nineGirdData = new NineGridBean(imageBean.getImagePath());
                resultList.add(nineGirdData);
            }
            mNineGridView.addDataList(resultList);
            imageNumber = resultList.size();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.release:
                if (input_release.getText().toString().length() > 0) {
                    if (imageNumber > 0) {
                        showuploadProgressDialog();
                        uploadImage(choosePic.get(0));
                    } else {
                        addPost(input_release.getText().toString(), "");
                    }
                } else {
                    ToastUtils.show("请输入内容");
                }

                break;
            case R.id.add_lable:
                new XPopup.Builder(this)
                        .hasStatusBarShadow(true)
                        .autoOpenSoftInput(true)
                        .asCustom(new CustomFullScreenPopup(this))
                        .show();
                break;
            case R.id.release_status:
                bottomDialog.show();
                release_status_image.setRotation(180);
                break;
        }
    }


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 1://代表继续上传
                    String uploadPath = (String) msg.obj;
                    uploadImage(uploadPath);
                    break;
            }
        }
    };

    private void uploadImage(String imagePath) {
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg/png"), file);
        MultipartBody.Part body = null;
        try {
            body = MultipartBody.Part.createFormData("file", URLEncoder.encode(file.getName(), "UTF-8"), requestBody);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RequestUtils.uploadImage(SharePreUtil.getString(this, "token", ""), RetrofitUtils.BaseUrl + "api/Upload/image", body, new MyObserver<UploadImageBean>(this) {
            @Override
            public void onSuccess(UploadImageBean result) {
                backimageUrl.add(result.getUrl());
                choosePic.remove(0);
                if (choosePic.size() > 0) {
                    Message message = new Message();
                    message.what = 1;
                    message.obj = choosePic.get(0);
                    mHandler.sendMessage(message);
                } else {
                    String image_list = gson.toJson(backimageUrl);
                    addPost(input_release.getText().toString(), image_list);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();

            }
        });
    }

    private void addPost(String content, String image_list) {
        RequestUtils.addPost(SharePreUtil.getString(this, "token", ""), type, circle_id, "", content, image_list, lab_id, "1", is_open, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                EventBus.getDefault().post(new MessageEventBus("refresh", ""));
                dismissProgressDialog();
                ToastUtils.show("动态发布成功！");
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
    }

    @Override
    public void onBottomMenuItemClick(MyDialogOpen dialog, View view) {
        switch (view.getId()) {
            case R.id.only_i_look:
                release_status_image.setRotation(360);
                release_status_text.setText("仅自己可见");
                is_open = "2";
                break;
            case R.id.open:
                release_status_image.setRotation(360);
                release_status_text.setText("公开");
                is_open = "1";
                break;
            case R.id.diamiss:
                bottomDialog.dismiss();
                release_status_image.setRotation(360);
                break;
        }
    }


    public class CustomFullScreenPopup extends FullScreenPopupView {
        public CustomFullScreenPopup(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_label;
        }

        @Override
        protected void onCreate() {
            super.onCreate();

            findViewById(R.id.back).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (SharePreUtil.getString(getContext(), "label_name", "").length() == 0) {
                            release_label.setText("添加标签");
                            release_label.setBackgroundResource(R.drawable.white_bg);
                            release_label.setTextColor(Color.parseColor("#111111"));
                        } else {
                            release_label.setText(SharePreUtil.getString(getContext(), "label_name", ""));
                            release_label.setBackgroundResource(R.drawable.textview_3);
                            release_label.setTextColor(Color.parseColor("#FCB02A"));
                            lab_id = SharePreUtil.getString(getContext(), "label_id", "");
                        }
                    } catch (Exception e) {

                    }
                    dismiss();
                }
            });
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            getLabel();
        }

        @Override
        protected void onShow() {
            SharePreUtil.deleShare(getContext(), "label_name");
            SharePreUtil.deleShare(getContext(), "label_id");
            super.onShow();
        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }


    private void getLabel() {
        beanList.clear();
        RequestUtils.getLabelList(SharePreUtil.getString(this, "token", ""), new MyObserver<List<LabelBean>>(this) {
            @Override
            public void onSuccess(List<LabelBean> result) {
                List<LabelBean> list = result;
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        String id = list.get(i).getId();
                        String name = list.get(i).getType_name();
                        List label_list = list.get(i).getLabel_list();
                        myBean = new LabelBean(id, name, label_list);
                        beanList.add(myBean);
                    }
                    myAdapter = new LabelAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);


                } else {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }

}