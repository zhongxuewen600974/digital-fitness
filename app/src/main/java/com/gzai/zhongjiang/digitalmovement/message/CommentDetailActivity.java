package com.gzai.zhongjiang.digitalmovement.message;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.DailogComAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.ComDasilogBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommDetailBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommDetailBean1;
import com.gzai.zhongjiang.digitalmovement.bean.MyStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.CommListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentDetailActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarRoot;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.content)
    TextView content;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.praises_image)
    ImageView praises_image;
    @BindView(R.id.praises)
    TextView praises;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private int page_total1, page1 = 1;
    ComDasilogBean dyBean1;
    List<ComDasilogBean> ComDasilogBeanList = new ArrayList<>();
    DailogComAdapter dailogComAdapter;
    String id_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_detail);
        ButterKnife.bind(this);
        actionBarRoot.setTitle("评论详情");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        Intent intent = getIntent();
        id_1 = intent.getStringExtra("id_1");
        getCommentInfo();

    }

    private void getChildCommentList(String post_id, String parent_id) {
        ComDasilogBeanList.clear();
        RequestUtils.getChildCommentList(SharePreUtil.getString(this, "token", ""), 1, 100, post_id, parent_id, new ListMyObserver<CommListBean<ComDasilogBean>>(this) {
            @Override
            public void onSuccess(CommListBean<ComDasilogBean> result) {
                CommListBean<ComDasilogBean> list = result;
                page_total1 = result.getPage_info().getPage_total();
                if (list.getComment_list().size() > 0) {

                    for (int i = 0; i < list.getComment_list().size(); i++) {
                        String nick_name = list.getComment_list().get(i).getNick_name();
                        String avatar = list.getComment_list().get(i).getAvatar();
                        String id = list.getComment_list().get(i).getId();
                        String post_id = list.getComment_list().get(i).getUser_id();
                        String create_time = list.getComment_list().get(i).getCreate_time();
                        String user_id = list.getComment_list().get(i).getUser_id();
                        String to_user_id = list.getComment_list().get(i).getTo_user_id();
                        String comment_nick_name = list.getComment_list().get(i).getComment_nick_name();
                        String comment_avatar = list.getComment_list().get(i).getComment_avatar();
                        String content = list.getComment_list().get(i).getContent();
                        String comments = list.getComment_list().get(i).getComments();
                        String praises = list.getComment_list().get(i).getPraises();
                        String ispraise = list.getComment_list().get(i).getIspraise();
                        dyBean1 = new ComDasilogBean(nick_name, avatar, id, post_id, create_time, user_id, to_user_id, comment_nick_name, comment_avatar, content, comments, praises, ispraise);
                        ComDasilogBeanList.add(dyBean1);
                    }
                    dailogComAdapter = new DailogComAdapter(ComDasilogBeanList);
                    recyclerView.setAdapter(dailogComAdapter);

                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void getCommentInfo() {
        RequestUtils.getCommentInfo(SharePreUtil.getString(this, "token", ""), id_1, new MyObserver<CommDetailBean>(this) {
            @Override
            public void onSuccess(CommDetailBean result) {
                Glide.with(CommentDetailActivity.this).load(result.getSend_avatar()).into(circleImageView);
                name.setText(result.getSend_name());
                content.setText(result.getContent());
                time.setText(stampToDate(result.getCreate_time()));
                praises.setText(result.getPraises());
                if (result.getIspraise().equals("0")) {
                    praises_image.setImageResource(R.drawable.dianzan_fase);
                    praises_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addPraise(result.getPost_id(), result.getId());
                        }
                    });
                } else if (result.getIspraise().equals("1")) {
                    praises_image.setImageResource(R.drawable.home_dianzan);
                    praises_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            cancelPraise(result.getPost_id(), result.getId());
                        }
                    });
                }
                getChildCommentList(result.getPost_id(), result.getId());
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    private void addPraise(String post_id, String id) {
        RequestUtils.addPraise(SharePreUtil.getString(this, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                getCommentInfo();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelPraise(String post_id, String id) {
        RequestUtils.cancelPraise(SharePreUtil.getString(this, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                getCommentInfo();

            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

}