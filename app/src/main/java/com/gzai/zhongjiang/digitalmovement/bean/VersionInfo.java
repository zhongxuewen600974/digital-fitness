package com.gzai.zhongjiang.digitalmovement.bean;

public class VersionInfo {
    AppVersion info;

    public AppVersion getInfo() {
        return info;
    }

    public void setInfo(AppVersion info) {
        this.info = info;
    }
}
