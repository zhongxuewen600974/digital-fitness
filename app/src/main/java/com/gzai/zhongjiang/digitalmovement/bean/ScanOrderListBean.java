package com.gzai.zhongjiang.digitalmovement.bean;

public class ScanOrderListBean {
    private String id;
    private String user_id;
    private String coach_id;
    private String coach_name;
    private String order_id;
    private String truename;
    private String status;
    private String is_comment;
    private String record_date;
    private String update_time;
    private String create_time;
    public ScanOrderListBean(String id,String user_id,String coach_id,String coach_name,String order_id,String truename,String status,String is_comment,String record_date,String update_time,String create_time){
        this.id=id;
        this.user_id=user_id;
        this.coach_id=coach_id;
        this.coach_name=coach_name;
        this.order_id=order_id;
        this.truename=truename;
        this.status=status;
        this.is_comment=is_comment;
        this.record_date=record_date;
        this.update_time=update_time;
        this.create_time=create_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(String coach_id) {
        this.coach_id = coach_id;
    }

    public String getCoach_name() {
        return coach_name;
    }

    public void setCoach_name(String coach_name) {
        this.coach_name = coach_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_comment() {
        return is_comment;
    }

    public void setIs_comment(String is_comment) {
        this.is_comment = is_comment;
    }

    public String getRecord_date() {
        return record_date;
    }

    public void setRecord_date(String record_date) {
        this.record_date = record_date;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
