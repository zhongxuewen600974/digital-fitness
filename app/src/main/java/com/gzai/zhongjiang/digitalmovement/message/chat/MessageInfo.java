package com.gzai.zhongjiang.digitalmovement.message.chat;

public class MessageInfo {
    private String msg;
    public  MessageInfo(String msg){
        this.msg=msg;
    }
    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
