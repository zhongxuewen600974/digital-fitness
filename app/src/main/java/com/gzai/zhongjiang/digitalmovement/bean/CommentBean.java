package com.gzai.zhongjiang.digitalmovement.bean;

public class CommentBean {
    private String id;
    private String user_id;
    private String to_user_id;
    private String parent_id;
    private String post_id;
    private String content;
    private String images;
    private String praises;
    private String comments;
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String recom;
    private String status;
    private String update_time;
    private String create_time;
    private String nick_name;
    private String avatar;
    private String ispraise;
    ChildList child_list;

    public CommentBean(String id, String user_id, String to_user_id, String parent_id, String post_id, String content, String images, String praises, String comments,String type,
                       String recom, String status, String update_time, String create_time, String nick_name, String avatar, String ispraise,ChildList child_list){
        this.id=id;
        this.user_id=user_id;
        this.to_user_id=to_user_id;
        this.parent_id=parent_id;
        this.post_id=post_id;
        this.content=content;
        this.images=images;
        this.praises=praises;
        this.comments=comments;
        this.type=type;
        this.recom=recom;
        this.status=status;
        this.update_time=update_time;
        this.create_time=create_time;
        this.nick_name=nick_name;
        this.avatar=avatar;
        this.ispraise=ispraise;
        this.child_list=child_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getPraises() {
        return praises;
    }

    public void setPraises(String praises) {
        this.praises = praises;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getRecom() {
        return recom;
    }

    public void setRecom(String recom) {
        this.recom = recom;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getIspraise() {
        return ispraise;
    }

    public void setIspraise(String ispraise) {
        this.ispraise = ispraise;
    }

    public ChildList getChild_list() {
        return child_list;
    }

    public void setChild_list(ChildList child_list) {
        this.child_list = child_list;
    }
}
