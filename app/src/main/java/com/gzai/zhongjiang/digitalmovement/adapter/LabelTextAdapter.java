package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.LabelTextBean;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.wetchimage.JZCImageUtil;

import java.util.List;

public class LabelTextAdapter extends RecyclerView.Adapter<LabelTextAdapter.ViewHolder> {
    private Context mContext;
    private List<LabelTextBean> dataBean;
    private OnItemClickListener onItemClickListener;
    int resourceId = -1;


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;

    }

    public interface OnItemClickListener {
        void onItemClickListener(int position, String id, String lab_name);
    }

    public LabelTextAdapter(List<LabelTextBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_label_text, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.icon.setText(dataBean.get(position).getLabel_name());
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resourceId = position;
                SharePreUtil.putString(mContext, "label_id", dataBean.get(position).getLabel_id());
                SharePreUtil.putString(mContext, "label_name", dataBean.get(position).getLabel_name());
                notifyDataSetChanged();
            }
        });

        if (position == resourceId) {
            holder.icon.setBackgroundResource(R.drawable.textview_1);
            holder.icon.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.icon.setBackgroundResource(R.drawable.textview_2);
            holder.icon.setTextColor(Color.parseColor("#999999"));
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView icon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = (TextView) itemView.findViewById(R.id.label_name);
        }
    }

}
