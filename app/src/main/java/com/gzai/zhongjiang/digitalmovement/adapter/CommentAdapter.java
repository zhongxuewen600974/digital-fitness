package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    private Context mContext;
    private List<CommentBean> dataBean;

    private OnItemClickListener onItemClickListener;
    private OnItemClickListener onItemClickListener1;
    private OnItemClickListener onItemClickListener2;
    private OnItemClickListener onItemClickListener3;

    private OnItemClickListener onItemClickListener4;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.onItemClickListener1 = onItemClickListener;
        this.onItemClickListener2 = onItemClickListener;
        this.onItemClickListener3 = onItemClickListener;
        this.onItemClickListener4 = onItemClickListener;
    }

    public CommentAdapter(List<CommentBean> list) {
        this.dataBean = list;
    }

    public interface OnItemClickListener {
        void onItemClickListener(String parent_id, String to_user_id);

        void OnItemClickListener1(String post_id, String id,String avatar,String name,String content,String time,String praises,String comments,String to_user_id,String ispraise);

        void OnItemClickListener2(int position, int id, int num);

        void OnItemClickListener3(int position, int id);

        void OnItemClickListener4(int position, int id);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_parent_comment, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            holder.name.setText(dataBean.get(position).getNick_name());
            holder.time.setText(stampToDate(dataBean.get(position).getCreate_time()));
            holder.content.setText(dataBean.get(position).getContent());
            holder.praises.setText(dataBean.get(position).getPraises());
            holder.comment.setText(dataBean.get(position).getComments());
            if (dataBean.get(position).getChild_list().getComment_list() == null) {
                holder.recyclerView.setVisibility(View.GONE);
            } else {
                holder.recyclerView.setVisibility(View.VISIBLE);
                holder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            }

            if (dataBean.get(position).getIspraise().equals("0")) {
                holder.praises_image.setImageResource(R.drawable.dianzan_fase);
            } else {
                holder.praises_image.setImageResource(R.drawable.home_dianzan);
            }


            if (dataBean.get(position).getChild_list().getComment_list().size() == 0) {
                holder.child_linear.setVisibility(View.GONE);
            } else {
                holder.child_linear.setVisibility(View.VISIBLE);
            }
            if (dataBean.get(position).getChild_list().getComment_list().size() >= 3) {
                holder.counts.setText("共" + dataBean.get(position).getComments() + "条回复 >");
                holder.counts.setVisibility(View.VISIBLE);
            } else {
                holder.counts.setVisibility(View.GONE);
            }

            ChildCommentAdapter pAdapter = new ChildCommentAdapter(dataBean.get(position).getChild_list().getComment_list());
            holder.recyclerView.setAdapter(pAdapter);
        } catch (Exception e) {
        }


        holder.praises_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataBean.get(position).getIspraise().equals("0")) {
                    addPraise(dataBean.get(position).getPost_id(), dataBean.get(position).getId());
                    dataBean.get(position).setIspraise("1");
                    dataBean.get(position).setPraises(Integer.parseInt(dataBean.get(position).getPraises()) + 1 + "");
                    notifyItemChanged(position);
                } else {
                    cancelPraise(dataBean.get(position).getPost_id(), dataBean.get(position).getId());
                    dataBean.get(position).setIspraise("0");
                    dataBean.get(position).setPraises(Integer.parseInt(dataBean.get(position).getPraises()) - 1 + "");
                    notifyItemChanged(position);
                }
            }
        });


        holder.comment_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClickListener(dataBean.get(position).getId(), dataBean.get(position).getUser_id());
            }
        });


        holder.circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });


        holder.counts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnItemClickListener1(dataBean.get(position).getPost_id(),
                        dataBean.get(position).getId(),dataBean.get(position).getAvatar(),dataBean.get(position).getNick_name(),dataBean.get(position).getContent(),
                        dataBean.get(position).getPraises(),holder.time.getText().toString(),dataBean.get(position).getComments(),dataBean.get(position).getUser_id(),dataBean.get(position).getIspraise());
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.OnItemClickListener1(dataBean.get(position).getPost_id(),
                        dataBean.get(position).getId(),dataBean.get(position).getAvatar(),dataBean.get(position).getNick_name(),dataBean.get(position).getContent(),
                        dataBean.get(position).getPraises(),holder.time.getText().toString(),dataBean.get(position).getComments(),dataBean.get(position).getUser_id(),dataBean.get(position).getIspraise());
            }
        });


    }

    private void addPraise(String post_id, String id) {
        RequestUtils.addPraise(SharePreUtil.getString(mContext, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelPraise(String post_id, String id) {
        RequestUtils.cancelPraise(SharePreUtil.getString(mContext, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        ImageView praises_image, comment_image;
        TextView name, time, content, counts, praises, comment;
        RecyclerView recyclerView;
        LinearLayout child_linear;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            child_linear = (LinearLayout) itemView.findViewById(R.id.child_linear);
            counts = (TextView) itemView.findViewById(R.id.counts);
            content = (TextView) itemView.findViewById(R.id.content);
            praises_image = (ImageView) itemView.findViewById(R.id.praises_image);
            praises = (TextView) itemView.findViewById(R.id.praises);
            comment_image = (ImageView) itemView.findViewById(R.id.comment_image);
            comment = (TextView) itemView.findViewById(R.id.comment);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerView);
        }
    }
}
