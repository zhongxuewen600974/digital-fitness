package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentChild;
import com.gzai.zhongjiang.digitalmovement.bean.NoticeListBean;
import com.gzai.zhongjiang.digitalmovement.message.FitnessInformationActivity;
import com.gzai.zhongjiang.digitalmovement.message.chat.ChatActivity;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.ViewHolder> {
    private Context mContext;
    private List<NoticeListBean> dataBean;


    public NoticeAdapter(List<NoticeListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notice, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getSend_avatar()==null || dataBean.get(position).getSend_avatar().length()==0) {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            } else {
                Glide.with(mContext).load(dataBean.get(position).getSend_avatar()).into(holder.circleImageView);
            }
            holder.conent.setText(dataBean.get(position).getMsg_info().substring(8, dataBean.get(position).getMsg_info().length() - 2));
            if (dataBean.get(position).getRefer_type().equals("news")) {
                holder.name.setText("健身资讯");

            } else if (dataBean.get(position).getRefer_type().equals("chat")) {
                holder.name.setText(dataBean.get(position).getSend_nickname());
            }




            if (dataBean.get(position).getUnread().equals("0")) {
                holder.unread.setVisibility(View.GONE);
            } else {
                holder.unread.setVisibility(View.VISIBLE);
                holder.unread.setText(dataBean.get(position).getUnread());
            }
            holder.time.setText(stampToDate(dataBean.get(position).getUpdate_time()));

        } catch (Exception e) {
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBean.get(position).setUnread("0");
                notifyDataSetChanged();
                if (dataBean.get(position).getRefer_type().equals("news")) {
                    Intent intent = new Intent(mContext, FitnessInformationActivity.class);
                    mContext.startActivity(intent);
                } else if (dataBean.get(position).getRefer_type().equals("chat")) {
                    Intent intent = new Intent(mContext, ChatActivity.class);
                    intent.putExtra("send_name", dataBean.get(position).getSend_nickname());
                    intent.putExtra("send_id", dataBean.get(position).getSend_uid());
                    mContext.startActivity(intent);
                }
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, time, conent, unread;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            conent = (TextView) itemView.findViewById(R.id.conent);
            unread = (TextView) itemView.findViewById(R.id.unread);
        }
    }

}
