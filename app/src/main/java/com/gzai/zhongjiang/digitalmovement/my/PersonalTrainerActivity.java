package com.gzai.zhongjiang.digitalmovement.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.GoodAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.MyCourseAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.MyDyAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.StepTextAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.StepListBean;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.gzai.zhongjiang.digitalmovement.view.LookImageviewActivity;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.ImagePickerLoader;
import com.hjq.toast.ToastUtils;
import com.lwkandroid.imagepicker.ImagePicker;
import com.lwkandroid.imagepicker.data.ImagePickType;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.interfaces.SimpleCallback;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.security.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonalTrainerActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.week_1)
    TextView week_1;
    @BindView(R.id.week_2)
    TextView week_2;
    @BindView(R.id.week_3)
    TextView week_3;
    @BindView(R.id.week_4)
    TextView week_4;
    @BindView(R.id.week_5)
    TextView week_5;
    @BindView(R.id.week_6)
    TextView week_6;
    @BindView(R.id.week_7)
    TextView week_7;
    @BindView(R.id.day_1)
    TextView day_1;
    @BindView(R.id.day_2)
    TextView day_2;
    @BindView(R.id.day_3)
    TextView day_3;
    @BindView(R.id.day_4)
    TextView day_4;
    @BindView(R.id.day_5)
    TextView day_5;
    @BindView(R.id.day_6)
    TextView day_6;
    @BindView(R.id.day_7)
    TextView day_7;
    @BindView(R.id.coash_1)
    TextView coash_1;
    @BindView(R.id.coash_2)
    TextView coash_2;
    @BindView(R.id.coash_3)
    TextView coash_3;
    @BindView(R.id.coash_4)
    TextView coash_4;
    @BindView(R.id.coash_5)
    TextView coash_5;
    @BindView(R.id.coash_6)
    TextView coash_6;
    @BindView(R.id.coash_7)
    TextView coash_7;
    @BindView(R.id.to_appointment)
    TextView to_appointment;
    @BindView(R.id.yeas_mouth)
    TextView yeas_mouth;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.tel)
    ImageView tel;
    @BindView(R.id.good_recyclerView)
    RecyclerView good_recyclerView;
    @BindView(R.id.nocourse_linear)
    LinearLayout nocourse_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    List<String> GoodList = new ArrayList<>();
    GoodAdapter goodAdapter;

    private Calendar calendar = Calendar.getInstance();
    Date date1;
    SimpleDateFormat simpleDateFormat;
    String coach_id;

    MyCourse dyBean;
    List<MyCourse> beanList = new ArrayList<>();
    MyCourseAdapter myAdapter;
    ImageView image_slot_1, image_slot_2, image_slot_3, image_slot_4, image_slot_5, image_slot_6,
            image_slot_7, image_slot_8, image_slot_9, image_slot_10, image_slot_11, image_slot_12,image_slot_13;
    TextView time_slot_1, time_slot_2, time_slot_3, time_slot_4, time_slot_5, time_slot_6,
            time_slot_7, time_slot_8, time_slot_9, time_slot_10, time_slot_11, time_slot_12,time_slot_13;
    TextView d_week_1, d_week_2, d_week_3, d_week_4, d_week_5, d_week_6, d_week_7;
    private String today = getLastDayOfWeek(0);
    private String newtoday = getLastDayOfWeek(0);
    private int cb = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_trainer);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        coach_id = intent.getStringExtra("coach_id");
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        back.setOnClickListener(this);
        day_1.setOnClickListener(this);
        day_2.setOnClickListener(this);
        day_3.setOnClickListener(this);
        day_4.setOnClickListener(this);
        day_5.setOnClickListener(this);
        day_6.setOnClickListener(this);
        day_7.setOnClickListener(this);
        tel.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
        to_appointment.setOnClickListener(this);
        intRound();
        calendar.add(Calendar.DATE, 0);
        Date date = calendar.getTime();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        date1 = calendar.getTime();
        yeas_mouth.setText(simpleDateFormat.format(date).substring(0, 4) + "年" + simpleDateFormat.format(date).substring(5, 7) + "月");

        day_1.setText(simpleDateFormat.format(date).substring(8, 10));
        day_2.setText(getLastDayOfWeek(1).substring(8, 10));
        day_3.setText(getLastDayOfWeek(2).substring(8, 10));
        day_4.setText(getLastDayOfWeek(3).substring(8, 10));
        day_5.setText(getLastDayOfWeek(4).substring(8, 10));
        day_6.setText(getLastDayOfWeek(5).substring(8, 10));
        day_7.setText(getLastDayOfWeek(6).substring(8, 10));

        week_2.setText(getDayofweek(getLastDayOfWeek(0)));
        week_3.setText(getDayofweek(getLastDayOfWeek(1)));
        week_4.setText(getDayofweek(getLastDayOfWeek(2)));
        week_5.setText(getDayofweek(getLastDayOfWeek(3)));
        week_6.setText(getDayofweek(getLastDayOfWeek(4)));
        week_7.setText(getDayofweek(getLastDayOfWeek(5)));

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        good_recyclerView.setLayoutManager(layoutManager);

        intView(coach_id);
        getMySubCourse(getLastDayOfWeek(0), coach_id);


        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getMySubCourse(today, coach_id);
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setEnableLoadMore(false);
        }
    }


    private void intRound() {
        RequestUtils.getMySubCourse(SharePreUtil.getString(this, "token", ""), "", coach_id, new MyObserver<List<MyCourse>>(this) {
            @Override
            public void onSuccess(List<MyCourse> result) {
                List<MyCourse> list = result;
                if (list.size() > 0) {

                    for (int i = 0; i < list.size(); i++) {

                        String record_date = list.get(i).getRecord_date();
                        if (record_date.substring(8, 10).equals(day_1.getText().toString())) {
                            coash_1.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_2.getText().toString())) {
                            coash_2.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_3.getText().toString())) {
                            coash_3.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_4.getText().toString())) {
                            coash_4.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_5.getText().toString())) {
                            coash_5.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_6.getText().toString())) {
                            coash_6.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_7.getText().toString())) {
                            coash_7.setVisibility(View.VISIBLE);
                        }

                    }

                } else {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void getMySubCourse(String record_date, String coach_id) {
        beanList.clear();
        RequestUtils.getMySubCourse(SharePreUtil.getString(this, "token", ""), record_date, coach_id, new MyObserver<List<MyCourse>>(this) {
            @Override
            public void onSuccess(List<MyCourse> result) {
                List<MyCourse> list = result;
                if (list.size() > 0) {
                    nocourse_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.size(); i++) {
                        String id = list.get(i).getId();
                        String coach_id = list.get(i).getCoach_id();
                        String user_id = list.get(i).getUser_id();
                        String start_time = list.get(i).getStart_time();
                        String end_time = list.get(i).getEnd_time();
                        String record_date = list.get(i).getRecord_date();
                        if (record_date.substring(8, 10).equals(day_1.getText().toString())) {
                            coash_1.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_2.getText().toString())) {
                            coash_2.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_3.getText().toString())) {
                            coash_3.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_4.getText().toString())) {
                            coash_4.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_5.getText().toString())) {
                            coash_5.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_6.getText().toString())) {
                            coash_6.setVisibility(View.VISIBLE);
                        }
                        if (record_date.substring(8, 10).equals(day_7.getText().toString())) {
                            coash_7.setVisibility(View.VISIBLE);
                        }
                        String status = list.get(i).getStatus();
                        String remarks = list.get(i).getRemarks();
                        String deal_uid = list.get(i).getDeal_uid();
                        String update_time = list.get(i).getUpdate_time();
                        String create_time = list.get(i).getCreate_time();
                        dyBean = new MyCourse(id, coach_id, user_id, start_time, end_time, record_date, status, remarks, deal_uid, update_time, create_time);
                        beanList.add(dyBean);
                    }
                    myAdapter = new MyCourseAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    nocourse_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void intView(String user_id) {
        GoodList.clear();
        RequestUtils.getCoachInfo(SharePreUtil.getString(this, "token", ""), user_id, new MyObserver<CoachDetailInfo>(this) {
            @Override
            public void onSuccess(CoachDetailInfo result) {
                try {
                    if (result.getInfo().getAvatar().length() > 0) {
                        Glide.with(getBaseContext()).load(result.getInfo().getAvatar()).into(circleImageView);
                    } else {
                        circleImageView.setImageResource(R.drawable.mine_head_icon);
                    }
                    name.setText(result.getInfo().getTruename());
                    phone.setText(result.getInfo().getMobile());
                    String strcids = result.getInfo().getGood_at();
                    String[] strs = strcids.split(",");
                    for (String str : strs) {
                        GoodList.add(str);
                    }
                    goodAdapter = new GoodAdapter(GoodList);
                    good_recyclerView.setAdapter(goodAdapter);
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    public static String getDayofweek(String date) {
        Calendar cal = Calendar.getInstance();
        if (date.equals("")) {
            cal.setTime(new Date(System.currentTimeMillis()));
        } else {
            cal.setTime(new Date(getDateByStr2(date).getTime()));
        }

        if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
            return "一";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 2) {
            return "二";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 3) {
            return "三";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 4) {
            return "四";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 5) {
            return "五";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 6) {
            return "六";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
            return "日";
        }
        return "";
    }

    public static Date getDateByStr2(String dd) {

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = sd.parse(dd);
        } catch (ParseException e) {
            date = null;
            e.printStackTrace();
        }
        return date;
    }


    ShopShareShadowPopupView popupView;

    private void showPartShadow(String newtoday, int cb) {
        if (popupView == null) {
            popupView = (ShopShareShadowPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new ShopShareShadowPopupView(this, newtoday, cb));
        }
        popupView.show();
    }

    public class ShopShareShadowPopupView extends BottomPopupView {
        Boolean select1 = false, select2 = false, select3 = false, select4 = false, select5 = false, select6 = false,
                select7 = false, select8 = false, select9 = false, select10 = false, select11 = false, select12 = false,select13 = false;

        private String start_time = "", end_time = "", newtoday;


        public ShopShareShadowPopupView(@NonNull Context context, String newtoday, int cb) {
            super(context);
            this.newtoday = newtoday;
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_appointment;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            time_slot_1 = findViewById(R.id.time_slot_1);
            time_slot_2 = findViewById(R.id.time_slot_2);
            time_slot_3 = findViewById(R.id.time_slot_3);
            time_slot_4 = findViewById(R.id.time_slot_4);
            time_slot_5 = findViewById(R.id.time_slot_5);
            time_slot_6 = findViewById(R.id.time_slot_6);
            time_slot_7 = findViewById(R.id.time_slot_7);
            time_slot_8 = findViewById(R.id.time_slot_8);
            time_slot_9 = findViewById(R.id.time_slot_9);
            time_slot_10 = findViewById(R.id.time_slot_10);
            time_slot_11 = findViewById(R.id.time_slot_11);
            time_slot_12 = findViewById(R.id.time_slot_12);
            time_slot_13 = findViewById(R.id.time_slot_13);
            image_slot_1 = findViewById(R.id.image_slot_1);
            image_slot_2 = findViewById(R.id.image_slot_2);
            image_slot_3 = findViewById(R.id.image_slot_3);
            image_slot_4 = findViewById(R.id.image_slot_4);
            image_slot_5 = findViewById(R.id.image_slot_5);
            image_slot_6 = findViewById(R.id.image_slot_6);
            image_slot_7 = findViewById(R.id.image_slot_7);
            image_slot_8 = findViewById(R.id.image_slot_8);
            image_slot_9 = findViewById(R.id.image_slot_9);
            image_slot_10 = findViewById(R.id.image_slot_10);
            image_slot_11 = findViewById(R.id.image_slot_11);
            image_slot_12 = findViewById(R.id.image_slot_12);
            image_slot_13 = findViewById(R.id.image_slot_13);
            d_week_1 = findViewById(R.id.d_week_1);
            d_week_2 = findViewById(R.id.d_week_2);
            d_week_3 = findViewById(R.id.d_week_3);
            d_week_4 = findViewById(R.id.d_week_4);
            d_week_5 = findViewById(R.id.d_week_5);
            d_week_6 = findViewById(R.id.d_week_6);
            d_week_7 = findViewById(R.id.d_week_7);
            d_week_1.setText("今天(周" + TodayWeek(0) + ")");
            d_week_2.setText("明天(周" + TodayWeek(1) + ")");
            d_week_3.setText(getLastDayOfWeek(2).substring(5, 10) + "(周" + TodayWeek(2) + ")");
            d_week_4.setText(getLastDayOfWeek(3).substring(5, 10) + "(周" + TodayWeek(3) + ")");
            d_week_5.setText(getLastDayOfWeek(4).substring(5, 10) + "(周" + TodayWeek(4) + ")");
            d_week_6.setText(getLastDayOfWeek(5).substring(5, 10) + "(周" + TodayWeek(5) + ")");
            d_week_7.setText(getLastDayOfWeek(6).substring(5, 10) + "(周" + TodayWeek(6) + ")");
            d_week_1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    d_week_1.setTextColor(Color.parseColor("#01D66A"));
                    d_week_1.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    today = getLastDayOfWeek(0);
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(0));
                }
            });

            d_week_2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(1);
                    d_week_2.setTextColor(Color.parseColor("#01D66A"));
                    d_week_2.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(1));
                }
            });

            d_week_3.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(2);
                    d_week_3.setTextColor(Color.parseColor("#01D66A"));
                    d_week_3.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(2));
                }
            });

            d_week_4.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(3);
                    d_week_4.setTextColor(Color.parseColor("#01D66A"));
                    d_week_4.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(3));
                }
            });
            d_week_5.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(4);
                    d_week_5.setTextColor(Color.parseColor("#01D66A"));
                    d_week_5.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(4));
                }
            });

            d_week_6.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(5);
                    d_week_6.setTextColor(Color.parseColor("#01D66A"));
                    d_week_6.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(5));
                }
            });

            d_week_7.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(6);
                    d_week_7.setTextColor(Color.parseColor("#01D66A"));
                    d_week_7.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(6));
                }
            });

            time_slot_1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select1) {
                        start_time = "";
                        end_time = "";
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 09:00:00";
                        end_time = today + " 10:00:00";
                        select1 = true;
                        time_slot_1.setSelected(true);
                        time_slot_1.setTextColor(Color.parseColor("#FFFFFFFF"));

                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select2) {
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 10:00:00";
                        end_time = today + " 11:00:00";
                        select2 = true;
                        time_slot_2.setSelected(true);
                        time_slot_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_3.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select3) {
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 11:00:00";
                        end_time = today + " 12:00:00";
                        select3 = true;
                        time_slot_3.setSelected(true);
                        time_slot_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_4.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select4) {
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select4 = true;
                        time_slot_4.setSelected(true);
                        time_slot_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                        start_time = today + " 14:00:00";
                        end_time = today + " 15:00:00";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_5.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select5) {
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 15:00:00";
                        end_time = today + " 16:00:00";
                        select5 = true;
                        time_slot_5.setSelected(true);
                        time_slot_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_6.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select6) {
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        start_time = "";
                        end_time = "";

                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 16:00:00";
                        end_time = today + " 17:00:00";
                        select6 = true;
                        time_slot_6.setSelected(true);
                        time_slot_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_7.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select7) {
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select7 = true;
                        start_time = today + " 17:00:00";
                        end_time = today + " 18:00:00";
                        time_slot_7.setSelected(true);
                        time_slot_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_8.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select8) {
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select8 = true;
                        start_time = today + " 18:00:00";
                        end_time = today + " 19:00:00";
                        time_slot_8.setSelected(true);
                        time_slot_8.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_9.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select9) {
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select9 = true;
                        start_time = today + " 19:00:00";
                        end_time = today + " 20:00:00";
                        time_slot_9.setSelected(true);
                        time_slot_9.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_10.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select10) {
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 20:00:00";
                        end_time = today + " 21:00:00";
                        select10 = true;
                        time_slot_10.setSelected(true);
                        time_slot_10.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_11.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select11) {
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 21:00:00";
                        end_time = today + " 22:00:00";
                        select11 = true;
                        time_slot_11.setSelected(true);
                        time_slot_11.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_12.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select12) {
                        start_time = "";
                        end_time = "";
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select12 = true;
                        start_time = today + " 22:00:00";
                        end_time = today + " 23:00:00";
                        time_slot_12.setSelected(true);
                        time_slot_12.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_13.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select13) {
                        start_time = "";
                        end_time = "";
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select13 = true;
                        start_time = today + " 23:00:00";
                        end_time = today + " 24:00:00";
                        time_slot_13.setSelected(true);
                        time_slot_13.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                    }
                }
            });


            findViewById(R.id.back).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (start_time.length() > 0 && end_time.length() > 0) {
                        subscribeCourse(start_time, end_time);
                    }
                    dismiss();
                }
            });
        }


        @Override
        protected void onShow() {
            super.onShow();
            select1 = false;
            select2 = false;
            select3 = false;
            select4 = false;
            select5 = false;
            select6 = false;
            select7 = false;
            select8 = false;
            select9 = false;
            select10 = false;
            select11 = false;
            select12 = false;
            select13 = false;
            if (cb == 1) {
                d_week_1.setTextColor(Color.parseColor("#01D66A"));
                d_week_1.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                today = getLastDayOfWeek(0);
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(0));
            } else if (cb == 2) {
                today = getLastDayOfWeek(1);
                d_week_2.setTextColor(Color.parseColor("#01D66A"));
                d_week_2.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(1));
            } else if (cb == 3) {
                today = getLastDayOfWeek(2);
                d_week_3.setTextColor(Color.parseColor("#01D66A"));
                d_week_3.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(2));
            } else if (cb == 4) {
                today = getLastDayOfWeek(3);
                d_week_4.setTextColor(Color.parseColor("#01D66A"));
                d_week_4.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(3));
            } else if (cb == 5) {
                today = getLastDayOfWeek(4);
                d_week_5.setTextColor(Color.parseColor("#01D66A"));
                d_week_5.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(4));
            } else if (cb == 6) {
                today = getLastDayOfWeek(5);
                d_week_6.setTextColor(Color.parseColor("#01D66A"));
                d_week_6.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(5));
            } else if (cb == 7) {
                today = getLastDayOfWeek(6);
                d_week_7.setTextColor(Color.parseColor("#01D66A"));
                d_week_7.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(6));
            }
        }

        @Override
        protected void onDismiss() {
            super.onDismiss();
        }
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    private void intViewDailog(String record_date) {
        image_slot_1.setVisibility(View.GONE);
        time_slot_1.setEnabled(true);
        image_slot_2.setVisibility(View.GONE);
        time_slot_2.setEnabled(true);
        image_slot_3.setVisibility(View.GONE);
        time_slot_3.setEnabled(true);
        image_slot_4.setVisibility(View.GONE);
        time_slot_4.setEnabled(true);
        image_slot_5.setVisibility(View.GONE);
        time_slot_5.setEnabled(true);
        image_slot_6.setVisibility(View.GONE);
        time_slot_6.setEnabled(true);
        image_slot_7.setVisibility(View.GONE);
        time_slot_7.setEnabled(true);
        image_slot_8.setVisibility(View.GONE);
        time_slot_8.setEnabled(true);
        image_slot_9.setVisibility(View.GONE);
        time_slot_9.setEnabled(true);
        image_slot_10.setVisibility(View.GONE);
        time_slot_10.setEnabled(true);
        image_slot_11.setVisibility(View.GONE);
        time_slot_11.setEnabled(true);
        image_slot_12.setVisibility(View.GONE);
        time_slot_12.setEnabled(true);
        image_slot_13.setVisibility(View.GONE);
        time_slot_13.setEnabled(true);
        RequestUtils.getMySubCourse(SharePreUtil.getString(this, "token", ""), record_date, coach_id, new MyObserver<List<MyCourse>>(this) {
            @Override
            public void onSuccess(List<MyCourse> result) {
                List<MyCourse> list = result;
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        String start_time = list.get(i).getStart_time();
                        if (stampToDate(start_time).equals("09:00")) {
                            image_slot_1.setVisibility(View.VISIBLE);
                            time_slot_1.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("10:00")) {
                            image_slot_2.setVisibility(View.VISIBLE);
                            time_slot_2.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("11:00")) {
                            image_slot_3.setVisibility(View.VISIBLE);
                            time_slot_3.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("14:00")) {
                            image_slot_4.setVisibility(View.VISIBLE);
                            time_slot_4.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("15:00")) {
                            image_slot_5.setVisibility(View.VISIBLE);
                            time_slot_5.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("16:00")) {
                            image_slot_6.setVisibility(View.VISIBLE);
                            time_slot_6.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("17:00")) {
                            image_slot_7.setVisibility(View.VISIBLE);
                            time_slot_7.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("18:00")) {
                            image_slot_8.setVisibility(View.VISIBLE);
                            time_slot_8.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("19:00")) {
                            image_slot_9.setVisibility(View.VISIBLE);
                            time_slot_9.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("20:00")) {
                            image_slot_10.setVisibility(View.VISIBLE);
                            time_slot_10.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("21:00")) {
                            image_slot_11.setVisibility(View.VISIBLE);
                            time_slot_11.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("22:00")) {
                            image_slot_12.setVisibility(View.VISIBLE);
                            time_slot_12.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("23:00")) {
                            image_slot_13.setVisibility(View.VISIBLE);
                            time_slot_13.setEnabled(false);
                        }
                    }
                } else {

                }

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void subscribeCourse(String start_time, String end_time) {
        RequestUtils.subscribeCourse(SharePreUtil.getString(this, "token", ""), start_time, end_time, coach_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {

                showsexShadow();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    SexPopupView sexpopupView;

    private void showsexShadow() {
        if (sexpopupView == null) {
            sexpopupView = (SexPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new SexPopupView(this));
        }
        sexpopupView.show();
    }

    public class SexPopupView extends CenterPopupView {


        public SexPopupView(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_ok;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.iknow).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMySubCourse(today, coach_id);
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.circleImageView:
                Intent Stingintent = new Intent(PersonalTrainerActivity.this, OthersPageActivity.class);
                Stingintent.putExtra("user_id", coach_id);
                startActivity(Stingintent);
                break;
            case R.id.tel:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + phone.getText().toString());
                intent.setData(data);
                startActivity(intent);
                break;
            case R.id.to_appointment:
                showPartShadow(newtoday, cb);
                break;
            case R.id.back:
                finish();
                break;
            case R.id.day_1:
                day_1.setBackgroundResource(R.drawable.date_sign);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_1.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(0), coach_id);
                newtoday = getLastDayOfWeek(0);
                today = getLastDayOfWeek(0);
                cb = 1;
                break;
            case R.id.day_2:
                day_2.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(1), coach_id);
                newtoday = getLastDayOfWeek(1);
                today = getLastDayOfWeek(1);
                cb = 2;
                break;
            case R.id.day_3:
                day_3.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(2), coach_id);
                newtoday = getLastDayOfWeek(2);
                today = getLastDayOfWeek(2);
                cb = 3;
                break;
            case R.id.day_4:
                day_4.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(3), coach_id);
                newtoday = getLastDayOfWeek(3);
                today = getLastDayOfWeek(3);
                cb = 4;
                break;
            case R.id.day_5:
                day_5.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(4), coach_id);
                newtoday = getLastDayOfWeek(4);
                today = getLastDayOfWeek(4);
                cb = 5;
                break;
            case R.id.day_6:
                day_6.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(5), coach_id);
                newtoday = getLastDayOfWeek(5);
                today = getLastDayOfWeek(5);
                cb = 6;
                break;
            case R.id.day_7:
                day_7.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                getMySubCourse(getLastDayOfWeek(6), coach_id);
                newtoday = getLastDayOfWeek(6);
                today = getLastDayOfWeek(6);
                cb = 7;
                break;
        }
    }

    public static String getLastDayOfWeek(int day) {
        Date dat = null;
        Calendar cd = Calendar.getInstance();
        cd.add(Calendar.DATE, day);
        dat = cd.getTime();
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");
        return dformat.format(dat);
    }


    private String TodayWeek(int time) {
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        int weekday = c.get(Calendar.DAY_OF_WEEK) - 1 + time;


        if (weekday % 7 == 1) {
            return "一";
        } else if (weekday % 7 == 2) {
            return "二";
        } else if (weekday % 7 == 3) {
            return "三";
        } else if (weekday % 7 == 4) {
            return "四";
        } else if (weekday % 7 == 5) {
            return "五";
        } else if (weekday % 7 == 6) {
            return "六";
        } else if (weekday % 7 == 0) {
            return "日";
        }
        return "";
    }


}