package com.gzai.zhongjiang.digitalmovement.bean;

public class FeedBean {
    private String id;
    private String user_id;
    private String content;
    private String image_list;
    private String update_time;
    private String create_time;
    public FeedBean(String id,String user_id,String content,String image_list,String update_time,String create_time){
        this.id=id;
        this.user_id=user_id;
        this.content=content;
        this.image_list=image_list;
        this.update_time=update_time;
        this.create_time=create_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage_list() {
        return image_list;
    }

    public void setImage_list(String image_list) {
        this.image_list = image_list;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
