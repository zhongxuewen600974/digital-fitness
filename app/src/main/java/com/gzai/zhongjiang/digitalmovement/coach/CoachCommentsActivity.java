package com.gzai.zhongjiang.digitalmovement.coach;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachCommentBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoachCommentsActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.commen)
    TextView commen;
    @BindView(R.id.back)
    TextView back;
    String order_id,user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_comments);
        ButterKnife.bind(this);
        actionBarView.setTitle("教练点评");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        try {
            Intent intent=getIntent();
            order_id=intent.getStringExtra("order_id");
            user_id=intent.getStringExtra("user_id");
            intView(order_id,user_id);
        }catch (Exception e){

        }

    }

    private void intView(String order_id,String user_id) {
        RequestUtils.getCoachComment(SharePreUtil.getString(this, "token", ""), order_id, user_id, new MyObserver<CoachCommentBean>(this) {
            @Override
            public void onSuccess(CoachCommentBean result) {
                if (result.getContent().length() > 0) {
                    commen.setText(result.getContent());
                }else {
                    commen.setText("教练暂未评论");
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}