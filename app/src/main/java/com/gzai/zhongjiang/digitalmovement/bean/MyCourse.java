package com.gzai.zhongjiang.digitalmovement.bean;

public class MyCourse {
    private String id;
    private String coach_id;
    private String user_id;
    private String start_time;
    private String end_time;
    private String record_date;
    private String status;
    private String remarks;
    private String deal_uid;
    private String update_time;
    private String create_time;
    private String truename;
    private String avatar;
    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public MyCourse(String id, String coach_id, String user_id, String start_time, String end_time, String record_date, String status, String remarks,
                    String deal_uid, String update_time, String create_time){
        this.id=id;
        this.coach_id=coach_id;
        this.user_id=user_id;
        this.start_time=start_time;
        this.end_time=end_time;
        this.record_date=record_date;
        this.status=status;
        this.remarks=remarks;
        this.deal_uid=deal_uid;
        this.update_time=update_time;
        this.create_time=create_time;
    }

    public MyCourse(String id, String coach_id, String user_id, String start_time, String end_time, String record_date, String status, String remarks,
                    String deal_uid, String update_time, String create_time,String truename,String avatar){
        this.id=id;
        this.coach_id=coach_id;
        this.user_id=user_id;
        this.start_time=start_time;
        this.end_time=end_time;
        this.record_date=record_date;
        this.status=status;
        this.remarks=remarks;
        this.deal_uid=deal_uid;
        this.update_time=update_time;
        this.create_time=create_time;
        this.truename=truename;
        this.avatar=avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(String coach_id) {
        this.coach_id = coach_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getRecord_date() {
        return record_date;
    }

    public void setRecord_date(String record_date) {
        this.record_date = record_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getDeal_uid() {
        return deal_uid;
    }

    public void setDeal_uid(String deal_uid) {
        this.deal_uid = deal_uid;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
