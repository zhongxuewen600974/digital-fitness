package com.gzai.zhongjiang.digitalmovement;


import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.alibaba.fastjson.JSON;
import com.azhon.appupdate.config.UpdateConfiguration;
import com.azhon.appupdate.manager.DownloadManager;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.VersionInfo;
import com.gzai.zhongjiang.digitalmovement.message.MessageFragment;
import com.gzai.zhongjiang.digitalmovement.gym.GymFragment;
import com.gzai.zhongjiang.digitalmovement.home.HomeFragment;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.message.chat.LoginBean;
import com.gzai.zhongjiang.digitalmovement.my.MyFragment;
import com.gzai.zhongjiang.digitalmovement.release.ReleaseActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallbackWithBeforeParam;
import com.permissionx.guolindev.callback.ForwardToSettingsCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.permissionx.guolindev.request.ForwardScope;
import com.zhangke.websocket.SimpleListener;
import com.zhangke.websocket.SocketListener;
import com.zhangke.websocket.WebSocketHandler;
import com.zhangke.websocket.response.ErrorResponse;

import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    private static int isExit = 0;
    private static final int TIME_EXIT = 2000;
    private ArrayList fragments;
    @BindView(R.id.main_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.btn_home)
    LinearLayout homeLinear;
    @BindView(R.id.btn_gym)
    LinearLayout gymLinear;
    @BindView(R.id.btn_release)
    LinearLayout releaseLinear;
    @BindView(R.id.btn_coach)
    LinearLayout coachLinear;
    @BindView(R.id.btn_my)
    LinearLayout myLinear;
    @BindView(R.id.tv_home)
    TextView tv_home;
    @BindView(R.id.tv_my)
    TextView tv_my;
    @BindView(R.id.tv_gym)
    TextView tv_gym;
    @BindView(R.id.tv_release)
    TextView tv_release;
    @BindView(R.id.tv_coach)
    TextView tv_coach;
    @BindView(R.id.main_imageView)
    ImageView main_imageView;
    @BindView(R.id.tab_home_icon)
    ImageView tab_home_icon;
    @BindView(R.id.tab_jsf_icon)
    ImageView tab_jsf_icon;
    @BindView(R.id.tab_xx_icon)
    ImageView tab_xx_icon;
    @BindView(R.id.tab_mine_icon)
    ImageView tab_mine_icon;
    private String version;
    private DownloadManager manager;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    private static final int NO_1 = 0x1;
    int num = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.hardware.sensor.stepcounter",
            "feature:android.hardware.sensor.stepdetector",
            "android.permission.ACTIVITY_RECOGNITION",
            "Manifest.permission.ACCESS_COARSE_LOCATION"
    };

    public static void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.ACTIVITY_RECOGNITION");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermission();
        ButterKnife.bind(this);
        myLinear.setOnClickListener(this);
        homeLinear.setOnClickListener(this);
        gymLinear.setOnClickListener(this);
        releaseLinear.setOnClickListener(this);
        coachLinear.setOnClickListener(this);
        main_imageView.setOnClickListener(this);
        fragments = new ArrayList<Fragment>();
        Fragment home = new HomeFragment();
        Fragment coach = new MessageFragment();
        Fragment gym = new GymFragment();
        Fragment my = new MyFragment();
        fragments.add(home);
        fragments.add(gym);
        fragments.add(coach);
        fragments.add(my);
        mViewPager.setAdapter(new MyFragmentAdapter(getSupportFragmentManager(), fragments));
        mViewPager.setOnPageChangeListener(new myOnPageChangeListener());
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(4);
        version = getVersionCode(this);
        cherVersion(version);
        LoginBean loginBean = new LoginBean("login", SharePreUtil.getString(this, "token", ""));
        WebSocketHandler.getDefault().send(JSON.toJSONString(loginBean));
        Log.e("token", SharePreUtil.getString(this, "token", ""));
        verifyStoragePermissions(this);

        WebSocketHandler.getDefault().addListener(socketListener);


    }

    private void checkPermission() {
        PermissionX.init(this)
                .permissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.CHANGE_WIFI_STATE,
                        Manifest.permission.CHANGE_NETWORK_STATE,
                        Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS
                )
                .onExplainRequestReason(new ExplainReasonCallbackWithBeforeParam() {
                    @Override
                    public void onExplainReason(ExplainScope scope, List<String> deniedList, boolean beforeRequest) {
                        // scope.showRequestReasonDialog(deniedList, "即将申请的权限是程序必须依赖的权限", "我已明白");
                    }
                })
                .onForwardToSettings(new ForwardToSettingsCallback() {
                    @Override
                    public void onForwardToSettings(ForwardScope scope, List<String> deniedList) {
                        // scope.showForwardToSettingsDialog(deniedList, "您需要去应用程序设置当中手动开启权限", "我已明白");
                    }
                })
                .request(new RequestCallback() {
                    @Override
                    public void onResult(boolean allGranted, List<String> grantedList, List<String> deniedList) {
                        if (allGranted) {
                            // Toast.makeText(MainActivity.this, "所有申请的权限都已通过", Toast.LENGTH_SHORT).show();
                        } else {
                            //  Toast.makeText(MainActivity.this, "您拒绝了如下权限：" + deniedList, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            //申请权限
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 222);//自定义的code
//        }
    }









    public void showNotification(String name, String msg) {
        NotificationManagerCompat manager = NotificationManagerCompat.from(MainActivity.this);

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel("channelId", name, NotificationManager.IMPORTANCE_LOW);
            NotificationManager notificationManager =
                    (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Notification notification = new NotificationCompat.Builder(MainActivity.this, "channelId")
                .setContentTitle(name+"给你发了条消息")
                .setContentText(msg.substring(8, msg.length() - 2))
                .setWhen(System.currentTimeMillis())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this,MainActivity.class), 0))
                .build();
        manager.notify(0, notification);
    }

    private SocketListener socketListener = new SimpleListener() {
        @Override
        public void onConnected() {
            appendMsgDisplay("onConnected");
        }

        @Override
        public void onConnectFailed(Throwable e) {
            if (e != null) {
                appendMsgDisplay("onConnectFailed:" + e.toString());
            } else {
                appendMsgDisplay("onConnectFailed:null");
            }
        }

        @Override
        public void onDisconnect() {
            appendMsgDisplay("onDisconnect");
        }

        @Override
        public void onSendDataError(ErrorResponse errorResponse) {
            appendMsgDisplay("onSendDataError:" + errorResponse.toString());
            errorResponse.release();
        }

        @Override
        public <T> void onMessage(String message, T data) {
            appendMsgDisplay("onMessage(String, T):" + message);
            try {
                JSONObject jsonObject = new JSONObject(message);
                String error_code = jsonObject.optString("error_code", null);
                if (error_code.equals("0")) {
                    String mydata = jsonObject.optString("data", null);
                    JSONObject dataObject = new JSONObject(mydata);
                    String magdata = dataObject.optString("data", null);
                    JSONObject MsgObject = new JSONObject(magdata);
                    String send_uid = MsgObject.optString("send_uid", null);
                    String msg_id = MsgObject.optString("msg_id", null);
                    String msg_info = MsgObject.optString("msg_info", null);
                    String msg_type = MsgObject.optString("msg_type", null);
                    String create_time = MsgObject.optString("create_time", null);
                    String send_nickname = MsgObject.optString("send_nickname", null);
                    String send_avatar = MsgObject.optString("send_avatar", null);
                    String receive_uid = MsgObject.optString("receive_uid", null);
                    String receive_nickname = MsgObject.optString("receive_nickname", null);
                    String receive_avatar = MsgObject.optString("receive_avatar", null);
                    showNotification(send_nickname, msg_info);

                }
            } catch (Exception e) {
            }

        }

        @Override
        public <T> void onMessage(ByteBuffer bytes, T data) {
            appendMsgDisplay("onMessage(ByteBuffer, T):" + bytes);
        }
    };

    private void appendMsgDisplay(String msg) {
        StringBuilder textBuilder = new StringBuilder();
        textBuilder.append(msg);
        textBuilder.append("\n");
      //  Log.e("123456", textBuilder.toString());
    }


    private void cherVersion(String version) {
        RequestUtils.getAppVersion(new MyObserver<VersionInfo>(this) {
            @Override
            public void onSuccess(VersionInfo result) {
                if (version.equals(result.getInfo().getApp_ver())) {
                } else {
                    startUpdate(result.getInfo().getApp_file(), result.getInfo().getApp_ver(), result.getInfo().getApp_desc());
                    Log.e("123456", result.getInfo().getApp_file());
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void startUpdate(String app_file, String app_ver, String app_desc) {
        UpdateConfiguration configuration = new UpdateConfiguration()
                .setEnableLog(true)
                .setJumpInstallPage(true)
                .setDialogButtonTextColor(Color.WHITE)
                .setShowNotification(true)
                .setShowBgdToast(false)
                .setForcedUpgrade(false);
        manager = DownloadManager.getInstance(this);
        manager.setApkName("数字运动.apk")
                .setApkUrl(app_file)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setShowNewerToast(true)
                .setConfiguration(configuration)
                .setApkVersionCode(2)
                .setApkVersionName(app_ver)
                .setApkSize("62")
                .setApkDescription(app_desc)
                .download();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.btn_home:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.btn_gym:
                mViewPager.setCurrentItem(1);
                break;
            case R.id.btn_release:
                Intent Singintent = new Intent(MainActivity.this, ReleaseActivity.class);
                Singintent.putExtra("type", "1");
                startActivity(Singintent);
                break;
            case R.id.btn_coach:
                mViewPager.setCurrentItem(2);
                break;
            case R.id.btn_my:
                mViewPager.setCurrentItem(3);
                break;
            case R.id.main_imageView:
                Intent Stingintent = new Intent(MainActivity.this, ReleaseActivity.class);
                Stingintent.putExtra("type", "1");
                startActivity(Stingintent);
                break;
        }

    }

    public class myOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onPageSelected(int arg0) {
            if (arg0 == 0) {
                tab_home_icon.setImageResource(R.drawable.tab_home_icon_true);
                tv_home.setTextColor(Color.parseColor("#01BD5D"));
                tab_jsf_icon.setImageResource(R.drawable.tab_jsf_icon_fase);
                tv_gym.setTextColor(Color.parseColor("#999999"));
                tab_xx_icon.setImageResource(R.drawable.tab_xx_icon_fase);
                tv_coach.setTextColor(Color.parseColor("#999999"));
                tab_mine_icon.setImageResource(R.drawable.tab_mine_icon_fase);
                tv_my.setTextColor(Color.parseColor("#999999"));
            }
            if (arg0 == 1) {
                tab_jsf_icon.setImageResource(R.drawable.tab_jsf_icon_true);
                tv_gym.setTextColor(Color.parseColor("#01BD5D"));
                tab_home_icon.setImageResource(R.drawable.tab_home_icon_fase);
                tv_home.setTextColor(Color.parseColor("#999999"));
                tab_xx_icon.setImageResource(R.drawable.tab_xx_icon_fase);
                tv_coach.setTextColor(Color.parseColor("#999999"));
                tab_mine_icon.setImageResource(R.drawable.tab_mine_icon_fase);
                tv_my.setTextColor(Color.parseColor("#999999"));
            }
            if (arg0 == 2) {
                tab_xx_icon.setImageResource(R.drawable.tab_xx_icon_true);
                tv_coach.setTextColor(Color.parseColor("#01BD5D"));
                tab_home_icon.setImageResource(R.drawable.tab_home_icon_fase);
                tv_home.setTextColor(Color.parseColor("#999999"));
                tab_jsf_icon.setImageResource(R.drawable.tab_jsf_icon_fase);
                tv_gym.setTextColor(Color.parseColor("#999999"));
                tab_mine_icon.setImageResource(R.drawable.tab_mine_icon_fase);
                tv_my.setTextColor(Color.parseColor("#999999"));
            }
            if (arg0 == 3) {
                tab_mine_icon.setImageResource(R.drawable.tab_mine_icon_true);
                tv_my.setTextColor(Color.parseColor("#01BD5D"));
                tab_jsf_icon.setImageResource(R.drawable.tab_jsf_icon_fase);
                tv_gym.setTextColor(Color.parseColor("#999999"));
                tab_xx_icon.setImageResource(R.drawable.tab_xx_icon_fase);
                tv_coach.setTextColor(Color.parseColor("#999999"));
                tab_home_icon.setImageResource(R.drawable.tab_home_icon_fase);
                tv_home.setTextColor(Color.parseColor("#999999"));
            }
        }
    }


    private class MyFragmentAdapter extends FragmentPagerAdapter {
        ArrayList<Fragment> list;

        public MyFragmentAdapter(FragmentManager fm, ArrayList<Fragment> list) {
            super(fm);
            this.list = list;
        }

        @Override
        public Fragment getItem(int arg0) {
            // TODO Auto-generated method stub
            return list.get(arg0);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return list.size();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
        }
        return false;
    }


    //设置主页延迟推出
    private void exit() {
        if (isExit < 1) {
            Toast.makeText(getApplicationContext(), "再点击一次返回退出程序", Toast.LENGTH_SHORT).show();
            mCountDownTimer.start();
        } else {
            isExit = 0;
            Toast.makeText(getApplicationContext(), "退出程序", Toast.LENGTH_SHORT).show();
            mCountDownTimer.cancel();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            System.exit(0);
        }
    }

    private CountDownTimer mCountDownTimer = new CountDownTimer(TIME_EXIT, 1000) {
        @Override
        public void onTick(long l) {
            isExit++;
        }

        @Override
        public void onFinish() {
            isExit = 0;
        }
    };

    public static synchronized String getVersionCode(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}