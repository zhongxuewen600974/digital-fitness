package com.gzai.zhongjiang.digitalmovement.coach;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.BookManageAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.CountListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookingManagementActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.week_1)
    TextView week_1;
    @BindView(R.id.week_2)
    TextView week_2;
    @BindView(R.id.week_3)
    TextView week_3;
    @BindView(R.id.week_4)
    TextView week_4;
    @BindView(R.id.week_5)
    TextView week_5;
    @BindView(R.id.week_6)
    TextView week_6;
    @BindView(R.id.week_7)
    TextView week_7;
    @BindView(R.id.day_1)
    TextView day_1;
    @BindView(R.id.day_2)
    TextView day_2;
    @BindView(R.id.day_3)
    TextView day_3;
    @BindView(R.id.day_4)
    TextView day_4;
    @BindView(R.id.day_5)
    TextView day_5;
    @BindView(R.id.day_6)
    TextView day_6;
    @BindView(R.id.day_7)
    TextView day_7;
    @BindView(R.id.coash_1)
    TextView coash_1;
    @BindView(R.id.coash_2)
    TextView coash_2;
    @BindView(R.id.coash_3)
    TextView coash_3;
    @BindView(R.id.coash_4)
    TextView coash_4;
    @BindView(R.id.coash_5)
    TextView coash_5;
    @BindView(R.id.coash_6)
    TextView coash_6;
    @BindView(R.id.coash_7)
    TextView coash_7;
    @BindView(R.id.yeas_mouth)
    TextView yeas_mouth;
    @BindView(R.id.to_appointment)
    TextView to_appointment;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.nocourse_linear)
    LinearLayout nocourse_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;


    private Calendar calendar = Calendar.getInstance();
    Date date1;
    SimpleDateFormat simpleDateFormat;
    String coach_id;

    MyCourse dyBean;
    List<MyCourse> beanList = new ArrayList<>();
    BookManageAdapter myAdapter;
    private int cb_1 = 0, cb_2 = 0, cb_3 = 0, cb_4 = 0, cb_5 = 0, cb_6 = 0, cb_7 = 0;
    String today = getLastDayOfWeek(0);
    private int cb=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_management);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        back.setOnClickListener(this);
        day_1.setOnClickListener(this);
        day_2.setOnClickListener(this);
        day_3.setOnClickListener(this);
        day_4.setOnClickListener(this);
        day_5.setOnClickListener(this);
        day_6.setOnClickListener(this);
        day_7.setOnClickListener(this);
        to_appointment.setOnClickListener(this);

        calendar.add(Calendar.DATE, 0);
        Date date = calendar.getTime();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        date1 = calendar.getTime();
        yeas_mouth.setText(simpleDateFormat.format(date).substring(0, 4) + "年" + simpleDateFormat.format(date).substring(5, 7) + "月");

        day_1.setText(simpleDateFormat.format(date).substring(8, 10));
        day_2.setText(getLastDayOfWeek(1).substring(8, 10));
        day_3.setText(getLastDayOfWeek(2).substring(8, 10));
        day_4.setText(getLastDayOfWeek(3).substring(8, 10));
        day_5.setText(getLastDayOfWeek(4).substring(8, 10));
        day_6.setText(getLastDayOfWeek(5).substring(8, 10));
        day_7.setText(getLastDayOfWeek(6).substring(8, 10));

        week_2.setText(getDayofweek(getLastDayOfWeek(0)));
        week_3.setText(getDayofweek(getLastDayOfWeek(1)));
        week_4.setText(getDayofweek(getLastDayOfWeek(2)));
        week_5.setText(getDayofweek(getLastDayOfWeek(3)));
        week_6.setText(getDayofweek(getLastDayOfWeek(4)));
        week_7.setText(getDayofweek(getLastDayOfWeek(5)));

        try {
            Intent intent = getIntent();
            coach_id = intent.getStringExtra("user_id");
            intView(getLastDayOfWeek(0));
        } catch (Exception e) {

        }
        intRound();
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            intView(today);
                            intRound();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setEnableLoadMore(false);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
        switch (messageEvent.getMessageType()) {
            case "refresh_BookingManagemen":
                intView(today);
                intRound();
                break;
        }
    }


    private void intRound() {
        cb_1 = 0;
        cb_2 = 0;
        cb_3 = 0;
        cb_4 = 0;
        cb_5 = 0;
        cb_6 = 0;
        cb_7 = 0;
        coash_1.setVisibility(View.GONE);
        coash_2.setVisibility(View.GONE);
        coash_3.setVisibility(View.GONE);
        coash_4.setVisibility(View.GONE);
        coash_5.setVisibility(View.GONE);
        coash_6.setVisibility(View.GONE);
        coash_7.setVisibility(View.GONE);
        RequestUtils.getSubCourseList(SharePreUtil.getString(this, "token", ""), "", coach_id, "", new ListMyObserver<CountListBean<MyCourse>>(this) {
            @Override
            public void onSuccess(CountListBean<MyCourse> result) {
                CountListBean<MyCourse> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String record_date = list.getList().get(i).getRecord_date();
                        if (record_date.equals(getLastDayOfWeek(0))) {
                            cb_1++;
                        }
                        if (record_date.equals(getLastDayOfWeek(1))) {
                            cb_2++;
                        }
                        if (record_date.equals(getLastDayOfWeek(2))) {
                            cb_3++;
                        }
                        if (record_date.equals(getLastDayOfWeek(3))) {
                            cb_4++;
                        }
                        if (record_date.equals(getLastDayOfWeek(4))) {
                            cb_5++;
                        }
                        if (record_date.equals(getLastDayOfWeek(5))) {
                            cb_6++;
                        }
                        if (record_date.equals(getLastDayOfWeek(6))) {
                            cb_7++;
                        }
                    }
                    if (cb_1 > 0) {
                        coash_1.setVisibility(View.VISIBLE);
                        coash_1.setText(cb_1 + "");
                    }
                    if (cb_2 > 0) {
                        coash_2.setVisibility(View.VISIBLE);
                        coash_2.setText(cb_2 + "");
                    }
                    if (cb_3 > 0) {
                        coash_3.setVisibility(View.VISIBLE);
                        coash_3.setText(cb_3 + "");
                    }
                    if (cb_4 > 0) {
                        coash_4.setVisibility(View.VISIBLE);
                        coash_4.setText(cb_4 + "");
                    }
                    if (cb_5 > 0) {
                        coash_5.setVisibility(View.VISIBLE);
                        coash_5.setText(cb_5 + "");
                    }
                    if (cb_6 > 0) {
                        coash_6.setVisibility(View.VISIBLE);
                        coash_6.setText(cb_6 + "");
                    }
                    if (cb_7 > 0) {
                        coash_7.setVisibility(View.VISIBLE);
                        coash_7.setText(cb_7 + "");
                    }
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void intView(String record_date) {
        beanList.clear();
        RequestUtils.getSubCourseList(SharePreUtil.getString(this, "token", ""), record_date, coach_id, "", new ListMyObserver<CountListBean<MyCourse>>(this) {
            @Override
            public void onSuccess(CountListBean<MyCourse> result) {
                CountListBean<MyCourse> list = result;
                if (list.getList().size() > 0) {
                    nocourse_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String user_id = list.getList().get(i).getUser_id();
                        String start_time = list.getList().get(i).getStart_time();
                        String end_time = list.getList().get(i).getEnd_time();
                        String record_date = list.getList().get(i).getRecord_date();
                        String status = list.getList().get(i).getStatus();
                        String remarks = list.getList().get(i).getRemarks();
                        String deal_uid = list.getList().get(i).getDeal_uid();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String truename = list.getList().get(i).getTruename();
                        String avatar = list.getList().get(i).getAvatar();
                        dyBean = new MyCourse(id, coach_id, user_id, start_time, end_time, record_date, status, remarks, deal_uid, update_time, create_time, truename, avatar);
                        beanList.add(dyBean);
                    }
                    myAdapter = new BookManageAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    nocourse_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    public static String getLastDayOfWeek(int day) {
        Date dat = null;
        Calendar cd = Calendar.getInstance();
        cd.add(Calendar.DATE, day);
        dat = cd.getTime();
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");
        return dformat.format(dat);
    }

    public static String getDayofweek(String date) {
        Calendar cal = Calendar.getInstance();
        if (date.equals("")) {
            cal.setTime(new Date(System.currentTimeMillis()));
        } else {
            cal.setTime(new Date(getDateByStr2(date).getTime()));
        }

        if (cal.get(Calendar.DAY_OF_WEEK) == 1) {
            return "一";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 2) {
            return "二";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 3) {
            return "三";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 4) {
            return "四";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 5) {
            return "五";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 6) {
            return "六";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == 7) {
            return "日";
        }
        return "";
    }

    public static Date getDateByStr2(String dd) {

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = sd.parse(dd);
        } catch (ParseException e) {
            date = null;
            e.printStackTrace();
        }
        return date;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.to_appointment:
                Intent intent = new Intent(BookingManagementActivity.this, AppointmentActivity.class);
                intent.putExtra("coach_id", coach_id);
                intent.putExtra("cb",cb);
                startActivity(intent);
                break;
            case R.id.back:
                finish();
                break;
            case R.id.day_1:
                day_1.setBackgroundResource(R.drawable.date_sign);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_1.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(0));
                today = getLastDayOfWeek(0);
                cb=1;
                break;
            case R.id.day_2:
                day_2.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(1));
                today = getLastDayOfWeek(1);
                cb=2;
                break;
            case R.id.day_3:
                day_3.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(2));
                today = getLastDayOfWeek(2);
                cb=3;
                break;
            case R.id.day_4:
                day_4.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(3));
                today = getLastDayOfWeek(3);
                cb=4;
                break;
            case R.id.day_5:
                day_5.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(4));
                today = getLastDayOfWeek(4);
                cb=5;
                break;
            case R.id.day_6:
                day_6.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(5));
                today = getLastDayOfWeek(5);
                cb=6;
                break;
            case R.id.day_7:
                day_7.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                intView(getLastDayOfWeek(6));
                today = getLastDayOfWeek(6);
                cb=7;
                break;
        }
    }
}