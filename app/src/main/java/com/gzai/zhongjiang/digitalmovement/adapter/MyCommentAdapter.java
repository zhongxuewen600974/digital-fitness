package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentChild;
import com.gzai.zhongjiang.digitalmovement.bean.CommentInfo;
import com.gzai.zhongjiang.digitalmovement.bean.CommentListBean;
import com.gzai.zhongjiang.digitalmovement.home.DynamicDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.message.CommentDetailActivity;
import com.gzai.zhongjiang.digitalmovement.message.ReplyActivity;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyCommentAdapter extends RecyclerView.Adapter<MyCommentAdapter.ViewHolder> {
    private Context mContext;
    private List<CommentListBean> dataBean;
    CommentInfo commentInfo;

    public MyCommentAdapter(List<CommentListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_comment, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            holder.name.setText(dataBean.get(position).getNick_name());
            holder.time.setText(stampToDate(dataBean.get(position).getCreate_time()));
            holder.content.setText(dataBean.get(position).getComment_content());



            if(dataBean.get(position).getType().equals("post")){
                holder.my_name.setText(dataBean.get(position).getPost_nick_name());
                holder.comment_content.setText( dataBean.get(position).getContent());
                holder.collback.setVisibility(View.GONE);
                holder.child_name.setText("");
                holder.child_comment.setText("");
            }else {
                holder.my_name.setText(dataBean.get(position).getNick_name());
                holder.collback.setVisibility(View.VISIBLE);
                holder.child_name.setText(dataBean.get(position).getComment_info().getSend_name());
                holder.comment_content.setText( dataBean.get(position).getComment_info().getContent());
                holder.child_comment.setText("");
            }

        } catch (Exception e) {
        }


        holder.reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, ReplyActivity.class);
                Stingintent.putExtra("post_id", dataBean.get(position).getPost_id());
                Stingintent.putExtra("parent_id", dataBean.get(position).getComment_id());
                Stingintent.putExtra("to_user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });

        holder.circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataBean.get(position).getType().equals("post")) {
                    Intent Stingintent = new Intent(mContext, DynamicDetailsActivity.class);
                    Stingintent.putExtra("post_id", dataBean.get(position).getPost_id());
                    mContext.startActivity(Stingintent);
                }else if(dataBean.get(position).getType().equals("comment")){
                    Intent Stingintent = new Intent(mContext, CommentDetailActivity.class);
                    Stingintent.putExtra("id", dataBean.get(position).getComment_id());
                    Stingintent.putExtra("id_1", dataBean.get(position).getComment_info().getId());
                    mContext.startActivity(Stingintent);
                }
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, time, reply, content, my_name, collback, child_name, child_comment, comment_content;
        LinearLayout child_linear;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            reply = (TextView) itemView.findViewById(R.id.reply);
            content = (TextView) itemView.findViewById(R.id.content);
            my_name = (TextView) itemView.findViewById(R.id.my_name);
            collback = (TextView) itemView.findViewById(R.id.collback);
            child_name = (TextView) itemView.findViewById(R.id.child_name);
            comment_content = (TextView) itemView.findViewById(R.id.comment_content);
            child_linear = (LinearLayout) itemView.findViewById(R.id.child_linear);
            child_comment = (TextView) itemView.findViewById(R.id.child_comment);
        }
    }

}
