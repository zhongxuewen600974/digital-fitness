package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CircleInfo;
import com.gzai.zhongjiang.digitalmovement.bean.HealthInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FitnessDataActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.weight)
    EditText weight;
    @BindView(R.id.height)
    EditText height;
    @BindView(R.id.bmi)
    EditText bmi;
    @BindView(R.id.body_fat_percentage)
    EditText body_fat_percentage;
    @BindView(R.id.body_fat)
    EditText body_fat;
    @BindView(R.id.metabolic_rate)
    EditText metabolic_rate;
    @BindView(R.id.waist_hip_ratio)
    EditText waist_hip_ratio;
    @BindView(R.id.moisture_content)
    EditText moisture_content;
    @BindView(R.id.muscle_content)
    EditText muscle_content;
    @BindView(R.id.arm_lift)
    EditText arm_lift;
    @BindView(R.id.arm_right)
    EditText arm_right;
    @BindView(R.id.hipline)
    EditText hipline;
    @BindView(R.id.bust)
    EditText bust;
    @BindView(R.id.the_waist)
    EditText the_waist;
    @BindView(R.id.thigh_lift)
    EditText thigh_lift;
    @BindView(R.id.thigh_right)
    EditText thigh_right;
    @BindView(R.id.calf_lift)
    EditText calf_lift;
    @BindView(R.id.calf_right)
    EditText calf_right;
    @BindView(R.id.determine)
    TextView determine;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_data);
        ButterKnife.bind(this);
        actionBarView.setTitle("健康数据");
        determine.setOnClickListener(this);
        intView();
    }

    private void intView() {
        RequestUtils.getHealthInfo(SharePreUtil.getString(this, "token", ""), new MyObserver<HealthInfo>(this) {
            @Override
            public void onSuccess(HealthInfo result) {
                if(result.getInfo()==null){
                    ToastUtils.show("你还未输入健康信息");
                }else {
                    weight.setText(result.getInfo().getWeight());
                    height.setText(result.getInfo().getHeight());
                    bmi.setText(result.getInfo().getBmi());
                    body_fat_percentage.setText(result.getInfo().getFat_rate());
                    body_fat.setText(result.getInfo().getFat());
                    metabolic_rate.setText(result.getInfo().getMetabolic_rate());
                    waist_hip_ratio.setText(result.getInfo().getWhr_rate());
                    moisture_content.setText(result.getInfo().getHumidity_rate());
                    muscle_content.setText(result.getInfo().getMuscle_rate());
                    arm_lift.setText(result.getInfo().getUpperarm_left());
                    arm_right.setText(result.getInfo().getUpperarm_right());
                    hipline.setText(result.getInfo().getHipline());
                    bust.setText(result.getInfo().getBust());
                    the_waist.setText(result.getInfo().getWaist());
                    thigh_lift.setText(result.getInfo().getThigh_left());
                    thigh_right.setText(result.getInfo().getThigh_right());
                    calf_lift.setText(result.getInfo().getShank_left());
                    calf_right.setText(result.getInfo().getShank_right());
                }

            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {
               // ToastUtils.show(errorMsg);
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.determine:
                   if(weight.getText().toString().isEmpty()){
                       ToastUtils.show("请输入体重");
                       return;
                   }else if(height.getText().toString().isEmpty()){
                       ToastUtils.show("请输入身高");
                       return;
                   }else {
                       showuploadProgressDialog();
                       saveHealth(weight.getText().toString(),height.getText().toString(),bmi.getText().toString(),body_fat_percentage.getText().toString(),body_fat.getText().toString(),
                               metabolic_rate.getText().toString(),waist_hip_ratio.getText().toString(),moisture_content.getText().toString(),muscle_content.getText().toString(),arm_lift.getText().toString(),
                               arm_right.getText().toString(),hipline.getText().toString(),bust.getText().toString(),the_waist.getText().toString(),thigh_lift.getText().toString(),
                               thigh_right.getText().toString(),calf_lift.getText().toString(),calf_right.getText().toString());
                   }
                break;
        }
    }
    private void saveHealth(String weight, String height, String bmi, String fat_rate, String fat,
                            String metabolic_rate, String whr_rate, String humidity_rate, String muscle_rate, String upperarm_left, String upperarm_right,
                            String hipline, String bust, String waist, String thigh_left, String thigh_right,
                            String shank_left, String shank_right) {
        RequestUtils.saveHealth(SharePreUtil.getString(this, "token", ""),"0",SharePreUtil.getString(this, "user_id", ""), weight, height, bmi, fat_rate, fat, metabolic_rate, whr_rate,
                humidity_rate, muscle_rate, upperarm_left, upperarm_right, hipline, bust, waist, thigh_left, thigh_right, shank_left, shank_right, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("修改成功");
                dismissProgressDialog();
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();

            }
        });
    }



}