package com.gzai.zhongjiang.digitalmovement.message.chat;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.ChatAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.SoftHideKeyBoardUtil;
import com.hjq.toast.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.zhangke.websocket.SimpleListener;
import com.zhangke.websocket.SocketListener;
import com.zhangke.websocket.WebSocketHandler;
import com.zhangke.websocket.response.ErrorResponse;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.input_msg)
    EditText input_msg;
    @BindView(R.id.put_msg)
    TextView put_msg;
    String send_id;

    private int page_total, page = 1;
    MessageBean dyBean;
    List<MessageBean> beanList = new ArrayList<>();
    ChatAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ButterKnife.bind(this);
        Intent intent = getIntent();
        String send_name = intent.getStringExtra("send_name");
        send_id = intent.getStringExtra("send_id");
        actionBarView.setTitle(send_name);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        // layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page <= page_total) {
                                page++;
                                loadMore(page);
                            } else {
                            }
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setEnableLoadMore(false);

        }
        intView();
        put_msg.setOnClickListener(this);
        input_msg.setOnClickListener(this);
        WebSocketHandler.getDefault().addListener(socketListener);
        SoftHideKeyBoardUtil.assistActivity(this);
    }

    private void loadMore(int page) {
        beanList.clear();
        RequestUtils.getMyMsgList(SharePreUtil.getString(this, "token", ""), 1, 10 * page, send_id, "", new ListMyObserver<ListBean<MessageBean>>(this) {
            @Override
            public void onSuccess(ListBean<MessageBean> result) {
                ListBean<MessageBean> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String send_uid = list.getList().get(i).getSend_uid();
                        String msg_id = list.getList().get(i).getMsg_id();
                        String msg_info = list.getList().get(i).getMsg_info();
                        String msg_type = list.getList().get(i).getMsg_type();
                        String create_time = list.getList().get(i).getCreate_time();
                        String send_nickname = list.getList().get(i).getSend_nickname();
                        String send_avatar = list.getList().get(i).getSend_avatar();
                        String receive_uid = list.getList().get(i).getReceive_uid();
                        String receive_nickname = list.getList().get(i).getReceive_nickname();
                        String receive_avatar = list.getList().get(i).getReceive_avatar();
                        dyBean = new MessageBean(send_uid, msg_id, msg_info, msg_type, create_time, send_nickname, send_avatar, receive_uid, receive_nickname, receive_avatar);
                        beanList.add(dyBean);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void intView() {
        beanList.clear();
        RequestUtils.getMyMsgList(SharePreUtil.getString(this, "token", ""), 1, 10, send_id, "", new ListMyObserver<ListBean<MessageBean>>(this) {
            @Override
            public void onSuccess(ListBean<MessageBean> result) {
                ListBean<MessageBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String send_uid = list.getList().get(i).getSend_uid();
                        String msg_id = list.getList().get(i).getMsg_id();
                        String msg_info = list.getList().get(i).getMsg_info();
                        String msg_type = list.getList().get(i).getMsg_type();
                        String create_time = list.getList().get(i).getCreate_time();
                        String send_nickname = list.getList().get(i).getSend_nickname();
                        String send_avatar = list.getList().get(i).getSend_avatar();
                        String receive_uid = list.getList().get(i).getReceive_uid();
                        String receive_nickname = list.getList().get(i).getReceive_nickname();
                        String receive_avatar = list.getList().get(i).getReceive_avatar();
                        dyBean = new MessageBean(send_uid, msg_id, msg_info, msg_type, create_time, send_nickname, send_avatar, receive_uid, receive_nickname, receive_avatar);
                        beanList.add(dyBean);
                    }
                    myAdapter = new ChatAdapter(beanList);
                    if (beanList.size() > 5) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        layoutManager.setStackFromEnd(true);
                        recyclerView.setLayoutManager(layoutManager);
                    }
                    recyclerView.setAdapter(myAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.put_msg:
                if (input_msg.getText().toString().length() > 0) {
                    sendMag(input_msg.getText().toString());

                } else {
                    ToastUtils.show("请输入内容");
                }
                break;
            case R.id.input_msg:
                recyclerView.canScrollVertically(1);
                break;
        }
    }

    private void sendMag(String msg) {
        MessageInfo messageInfo = new MessageInfo(msg);
        SndMsgBean sndMsgBean = new SndMsgBean(0, JSON.toJSONString(messageInfo), "0", send_id);
        MessageData messageData = new MessageData("msg", SharePreUtil.getString(this, "token", ""), sndMsgBean);
        WebSocketHandler.getDefault().send(JSON.toJSONString(messageData));

    }

    private SocketListener socketListener = new SimpleListener() {
        @Override
        public void onConnected() {
            appendMsgDisplay("onConnected");
        }

        @Override
        public void onConnectFailed(Throwable e) {
            if (e != null) {
                appendMsgDisplay("onConnectFailed:" + e.toString());
            } else {
                appendMsgDisplay("onConnectFailed:null");
            }
        }

        @Override
        public void onDisconnect() {
            appendMsgDisplay("onDisconnect");
        }

        @Override
        public void onSendDataError(ErrorResponse errorResponse) {
            appendMsgDisplay("onSendDataError:" + errorResponse.toString());
            errorResponse.release();
        }

        @Override
        public <T> void onMessage(String message, T data) {
            appendMsgDisplay("onMessage(String, T):" + message);
            intView();
            input_msg.setText("");
            recyclerView.canScrollVertically(1);
//            try {
 //               JSONObject jsonObject = new JSONObject(message);
//                String error_code = jsonObject.optString("error_code", null);
//                if (error_code.equals("0")) {
//                    String mydata = jsonObject.optString("data", null);
//                    JSONObject dataObject = new JSONObject(mydata);
//                    String magdata=dataObject.optString("data", null);
//                    JSONObject MsgObject = new JSONObject(magdata);
//                    String send_uid = MsgObject.optString("send_uid", null);
//                    String msg_id = MsgObject.optString("msg_id", null);
//                    String msg_info = MsgObject.optString("msg_info", null);
//                    String msg_type = MsgObject.optString("msg_type", null);
//                    String create_time = MsgObject.optString("create_time", null);
//                    String send_nickname = MsgObject.optString("send_nickname", null);
//                    String send_avatar = MsgObject.optString("send_avatar", null);
//                    String receive_uid = MsgObject.optString("receive_uid", null);
//                    String receive_nickname = MsgObject.optString("receive_nickname", null);
//                    String receive_avatar = MsgObject.optString("receive_avatar", null);
//                    dyBean = new MessageBean(send_uid, msg_id, msg_info, msg_type, create_time, send_nickname, send_avatar, receive_uid, receive_nickname, receive_avatar);
//                    beanList.add(dyBean);
//                    myAdapter.notifyDataSetChanged();
//                    input_msg.setText("");
//                    recyclerView.canScrollVertically(1);
//                }
//            }catch (Exception e){}
//        }
        }

        @Override
        public <T> void onMessage(ByteBuffer bytes, T data) {
            appendMsgDisplay("onMessage(ByteBuffer, T):" + bytes);
        }
    };

    private void appendMsgDisplay(String msg) {
        StringBuilder textBuilder = new StringBuilder();
        textBuilder.append(msg);
        textBuilder.append("\n");
      //  Log.e("123456", textBuilder.toString());
    }
}