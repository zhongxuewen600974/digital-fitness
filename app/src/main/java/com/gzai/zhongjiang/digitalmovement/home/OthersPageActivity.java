package com.gzai.zhongjiang.digitalmovement.home;

import androidx.annotation.ArrayRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.UserDyAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.message.PraiseActivity;
import com.gzai.zhongjiang.digitalmovement.message.chat.ChatActivity;
import com.gzai.zhongjiang.digitalmovement.my.FollowActivity;
import com.gzai.zhongjiang.digitalmovement.my.ModInfoActivity;
 import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.flowlayout.FlowTagAdapter;
import com.gzai.zhongjiang.digitalmovement.util.flowlayout.FlowTagLayout;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.gzai.zhongjiang.digitalmovement.view.DialogOpen;
import com.gzai.zhongjiang.digitalmovement.view.LookImageviewActivity;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.FullScreenPopupView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OthersPageActivity extends BaseActivity implements View.OnClickListener, DialogOpen.OnBottomMenuItemClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.sex_image)
    ImageView sex_image;
    @BindView(R.id.intro)
    TextView intro;
    @BindView(R.id.follow_count)
    TextView follow_count;
    @BindView(R.id.befollow_count)
    TextView befollow_count;
    @BindView(R.id.praise_count)
    TextView praise_count;
    @BindView(R.id.avatar)
    CircleImageView avatar;
    @BindView(R.id.new_linear)
    LinearLayout new_linear;
    @BindView(R.id.news_icon)
    ImageView news_icon;
    @BindView(R.id.isfollow_linear)
    LinearLayout isfollow_linear;
    @BindView(R.id.isfollow_icon)
    ImageView isfollow_icon;
    @BindView(R.id.isfollow)
    TextView isfollow;
    @BindView(R.id.mod_info)
    TextView mod_info;
    @BindView(R.id.post_count)
    TextView post_count;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.follow_Linear)
    LinearLayout follow_Linear;
    @BindView(R.id.befollow_linear)
    LinearLayout befollow_linear;
    @BindView(R.id.praise_linear)
    LinearLayout praise_linear;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private int page_total, page = 1;
    DyBean dyBean;
    List<DyBean> beanList = new ArrayList<>();
    UserDyAdapter myAdapter;
    private String is_follow = "", user_id, send_name;
    private DialogOpen bottomDialog;
    String report_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_others_page);
        ButterKnife.bind(this);
        try {
            Intent intent = getIntent();
            user_id = intent.getStringExtra("user_id");
            intView(user_id);
            if (user_id.equals(SharePreUtil.getString(this, "user_id", ""))) {
                title.setText("我的主页");
                new_linear.setVisibility(View.GONE);
                isfollow_linear.setVisibility(View.GONE);
                mod_info.setVisibility(View.VISIBLE);
                follow_Linear.setOnClickListener(this);
                befollow_linear.setOnClickListener(this);
                praise_linear.setOnClickListener(this);
            } else {
                title.setText("TA的主页");
                new_linear.setVisibility(View.VISIBLE);
                isfollow_linear.setVisibility(View.VISIBLE);
                mod_info.setVisibility(View.GONE);
            }
        }catch (Exception e){

        }

        mod_info.setOnClickListener(this);
        news_icon.setOnClickListener(this);
        back.setOnClickListener(this);
        isfollow.setOnClickListener(this);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            intViewPost();
                            intView(user_id);
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                             //   ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intViewPost();
        bottomDialog = new DialogOpen(this, R.layout.dailog_report, new int[]{R.id.report, R.id.diamiss});
        bottomDialog.setOnBottomMenuItemClickListener(this);
    }

    private void intViewPost() {
        beanList.clear();
        RequestUtils.getMyPostList(SharePreUtil.getString(this, "token", ""), 1, 10, user_id, new ListMyObserver<ListBean<DyBean>>(this) {
            @Override
            public void onSuccess(ListBean<DyBean> result) {
                ListBean<DyBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        List<String> image_list = new ArrayList<>();
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String circle_id = list.getList().get(i).getCircle_id();
                        String type = list.getList().get(i).getType();
                        String title = list.getList().get(i).getTitle();
                        String content = list.getList().get(i).getContent();
                        String label_id = list.getList().get(i).getLabel_id();
                        String dy_sync = list.getList().get(i).getDy_sync();
                        String is_top = list.getList().get(i).getIs_top();
                        String praises = list.getList().get(i).getPraises();
                        String shares = list.getList().get(i).getShares();
                        String comments = list.getList().get(i).getComments();
                        String status = list.getList().get(i).getStatus();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String sex = list.getList().get(i).getSex();
                        String circle_name = list.getList().get(i).getCircle_name();
                        String label_name = list.getList().get(i).getLabel_name();
                        String isfollow = list.getList().get(i).getIsfollow();
                        String ispraise = list.getList().get(i).getIspraise();

                        if (list.getList().get(i).getImage_list() == null) {
                        } else {
                            image_list = list.getList().get(i).getImage_list();
                        }
                        dyBean = new DyBean(id, user_id, circle_id, type, title, content, label_id, dy_sync, is_top, praises, shares, comments, status, update_time, create_time,
                                nick_name, avatar, sex, circle_name, label_name, isfollow, ispraise, image_list);
                        beanList.add(dyBean);
                    }
                    myAdapter = new UserDyAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                    myAdapter.setOnItemClickListener(new UserDyAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClickListener(String id) {
                            report_id = id;
                            bottomDialog.show();
                        }

                        @Override
                        public void OnItemClickListener1(int i, int position, int id) {

                        }

                        @Override
                        public void OnItemClickListener2(int position, int id, int num) {

                        }

                        @Override
                        public void OnItemClickListener3(int position, int id) {

                        }

                        @Override
                        public void OnItemClickListener4(int position, int id) {

                        }
                    });
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(int page) {
        RequestUtils.getMyPostList(SharePreUtil.getString(this, "token", ""), page, 10, user_id, new ListMyObserver<ListBean<DyBean>>(this) {
            @Override
            public void onSuccess(ListBean<DyBean> result) {
                ListBean<DyBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        List<String> image_list = new ArrayList<>();
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String circle_id = list.getList().get(i).getCircle_id();
                        String type = list.getList().get(i).getType();
                        String title = list.getList().get(i).getTitle();
                        String content = list.getList().get(i).getContent();
                        String label_id = list.getList().get(i).getLabel_id();
                        String dy_sync = list.getList().get(i).getDy_sync();
                        String is_top = list.getList().get(i).getIs_top();
                        String praises = list.getList().get(i).getPraises();
                        String shares = list.getList().get(i).getShares();
                        String comments = list.getList().get(i).getComments();
                        String status = list.getList().get(i).getStatus();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String sex = list.getList().get(i).getSex();
                        String circle_name = list.getList().get(i).getCircle_name();
                        String label_name = list.getList().get(i).getLabel_name();
                        String isfollow = list.getList().get(i).getIsfollow();
                        String ispraise = list.getList().get(i).getIspraise();

                        if (list.getList().get(i).getImage_list() == null) {
                        } else {
                            image_list = list.getList().get(i).getImage_list();
                        }
                        dyBean = new DyBean(id, user_id, circle_id, type, title, content, label_id, dy_sync, is_top, praises, shares, comments, status, update_time, create_time,
                                nick_name, avatar, sex, circle_name, label_name, isfollow, ispraise, image_list);
                        beanList.add(dyBean);
                    }
                    myAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void intView(String user_id) {
        RequestUtils.getOtherUserInfo(SharePreUtil.getString(this, "token", ""), user_id, new MyObserver<MyUserInfo>(this) {
            @Override
            public void onSuccess(MyUserInfo result) {
                try {
                    if (result.getAvatar().length() > 0) {
                        Glide.with(OthersPageActivity.this).load(result.getAvatar()).into(avatar);
                        avatar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(OthersPageActivity.this, LookImageviewActivity.class);
                                intent.putExtra("image_url", result.getAvatar());
                                startActivity(intent);
                            }
                        });
                    } else {
                        avatar.setImageResource(R.drawable.mine_head_icon);
                    }
                    name.setText(result.getNick_name());
                    send_name = result.getNick_name();
                    if (result.getSex().equals("0")) {
                        sex_image.setVisibility(View.GONE);
                    } else if (result.getSex().equals("1")) {
                        sex_image.setVisibility(View.VISIBLE);
                        sex_image.setImageResource(R.drawable.name_right_icon);
                    } else {
                        sex_image.setVisibility(View.VISIBLE);
                        sex_image.setImageResource(R.drawable.home_wm_icon);
                    }
                    if (result.getIntro() != null) {
                        intro.setText(result.getIntro());
                    } else {
                        intro.setText("暂未添加简介");
                    }
                    follow_count.setText(result.getFollow_count());
                    befollow_count.setText(result.getBefollow_count());
                    praise_count.setText(result.getPraise_count());
                    post_count.setText("全部动态（" + result.getPost_count() + "）");
                    if (result.getIs_follow().equals("0")) {
                        isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                        isfollow_icon.setVisibility(View.VISIBLE);
                        isfollow.setText("关注");
                        isfollow.setTextColor(Color.parseColor("#FFFFFFFF"));
                    } else {
                        isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                        isfollow_icon.setVisibility(View.GONE);
                        isfollow.setText("已关注");
                        isfollow.setTextColor(Color.parseColor("#999999"));
                    }
                    is_follow = result.getIs_follow();

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void addFollow(String to_user_id) {
        RequestUtils.addFollow(SharePreUtil.getString(this, "token", ""), to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                is_follow = "1";
                isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                isfollow_icon.setVisibility(View.GONE);
                isfollow.setText("已关注");
                isfollow.setTextColor(Color.parseColor("#999999"));

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelFollow(String to_user_id) {
        RequestUtils.cancelFollow(SharePreUtil.getString(this, "token", ""), to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                is_follow = "0";
                isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                isfollow_icon.setVisibility(View.VISIBLE);
                isfollow.setText("关注");
                isfollow.setTextColor(Color.parseColor("#FFFFFFFF"));

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
    @Override
    public void onBottomMenuItemClick(DialogOpen dialog, View view) {
        switch (view.getId()) {
            case R.id.report:
                bottomDialog.dismiss();
                ShowDailg();
                break;
            case R.id.diamiss:
                bottomDialog.dismiss();
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.news_icon:
                Intent intent = new Intent(OthersPageActivity.this, ChatActivity.class);
                intent.putExtra("send_name", send_name);
                intent.putExtra("send_id", user_id);
                startActivity(intent);
                break;
            case R.id.mod_info:
                Intent Stingintent = new Intent(OthersPageActivity.this, ModInfoActivity.class);
                startActivity(Stingintent);
                break;
            case R.id.isfollow:
                if (is_follow.equals("0")) {
                    addFollow(user_id);
                } else {
                    cancelFollow(user_id);
                }
                break;
            case R.id.follow_Linear:
                Intent intent2 = new Intent(OthersPageActivity.this, FollowActivity.class);
                intent2.putExtra("CurrentItem","0");
                startActivity(intent2);
                break;
            case R.id.befollow_linear:
                Intent intent1 = new Intent(OthersPageActivity.this, FollowActivity.class);
                intent1.putExtra("CurrentItem","1");
                startActivity(intent1);
                break;
            case R.id.praise_linear:
                Intent Stingintent1 = new Intent(OthersPageActivity.this, PraiseActivity.class);
                startActivity(Stingintent1);
                break;
        }
    }

    private String getSelectedText(FlowTagLayout parent, List<Integer> selectedList) {
        StringBuilder sb = new StringBuilder("举报类型：\n");
        for (int index : selectedList) {
            sb.append(parent.getAdapter().getItem(index));
            sb.append(";");
        }
        return sb.toString();
    }
    public String[] getStringArray(@ArrayRes int resId) {
        return    getResources().getStringArray(resId);
    }
    private void ShowDailg() {
        new XPopup.Builder(this)
                .hasStatusBarShadow(true)
                .autoOpenSoftInput(true)
                .asCustom(new ToCommentPopup(this))
                .show();
    }
    public class ToCommentPopup extends FullScreenPopupView {
        public ToCommentPopup(@NonNull Context context) {
            super(context);
        }
        String repot_type="";
        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_to_repot;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            EditText input_comm;
            TextView number;
            FlowTagLayout mMultiFlowTagLayout;
            mMultiFlowTagLayout=findViewById(R.id.flowlayout_multi_select);
            FlowTagAdapter tagAdapter = new FlowTagAdapter(getContext());
            mMultiFlowTagLayout.setAdapter(tagAdapter);
            mMultiFlowTagLayout.setTagCheckedMode(FlowTagLayout.FLOW_TAG_CHECKED_MULTI);
            mMultiFlowTagLayout.setOnTagSelectListener(new FlowTagLayout.OnTagSelectListener() {
                @Override
                public void onItemSelect(FlowTagLayout parent, int position, List<Integer> selectedList) {
                    repot_type= getSelectedText(parent, selectedList);
                }
            });
            tagAdapter.addTags(getStringArray(R.array.tags_values));
            number = findViewById(R.id.comment_number);
            input_comm = findViewById(R.id.input_repot);
            input_comm.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    number.setText(s.length() + "/" + "240");
                    if (s.length() >= 240) {
                        number.setTextColor(Color.parseColor("#E91E63"));
                    } else {
                        number.setTextColor(Color.parseColor("#333333"));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (repot_type.length() > 0) {
                        accusation(report_id, input_comm.getText().toString(),repot_type);
                        dismiss();
                    } else {
                        ToastUtils.show("请选择举报内容");
                    }
                }
            });

            findViewById(R.id.back).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();
        }

        @Override
        protected void onDismiss() {
            super.onDismiss();
        }
    }


    private void accusation(String content_id, String memo,String repot_type) {
        RequestUtils.accusation(SharePreUtil.getString(this, "token", ""), "", content_id, "post",repot_type, memo, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("举报成功");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);

            }
        });
    }

}