package com.gzai.zhongjiang.digitalmovement.coach;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.MainActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudyOrderCommentActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarRoot;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.stars_1)
    ImageView stars_1;
    @BindView(R.id.stars_2)
    ImageView stars_2;
    @BindView(R.id.stars_3)
    ImageView stars_3;
    @BindView(R.id.input_release)
    EditText input_release;
    @BindView(R.id.post)
    TextView post;
    @BindView(R.id.stars_tv1)
    TextView stars_tv1;
    @BindView(R.id.stars_tv2)
    TextView stars_tv2;
    @BindView(R.id.stars_tv3)
    TextView stars_tv3;

    @BindView(R.id.cj)
    LinearLayout cj;
    @BindView(R.id.cj_ok)
    LinearLayout cj_ok;

    String order_id, stars;
    boolean s1 = false, s2 = false, s3 = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_order_comment);
        ButterKnife.bind(this);
        actionBarRoot.setTitle("私教评价");

        try {
            Intent intent = getIntent();
            order_id = intent.getStringExtra("order_id");
            String coachName = intent.getStringExtra("name");
            String coach_id = intent.getStringExtra("coach_id");
            name.setText(coachName);
            String avatar = intent.getStringExtra("avatar");
            if (avatar.length() > 0) {
                Glide.with(getBaseContext()).load(avatar).into(circleImageView);
            } else {
                circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            if (coach_id.length()>0){
                intView(coach_id);
            }
        } catch (Exception e) {

        }
        stars_1.setOnClickListener(this);
        stars_2.setOnClickListener(this);
        stars_3.setOnClickListener(this);
        post.setOnClickListener(this);
        input_release.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    if (s1 == true || s2 == true || s3 == true) {
                        post.setEnabled(true);
                        post.setBackgroundResource(R.drawable.textview_1);
                    }
                } else {
                    post.setEnabled(false);
                    post.setBackgroundResource(R.drawable.textview_9);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void intView(String user_id) {
        RequestUtils.getCoachInfo(SharePreUtil.getString(this, "token", ""), user_id, new MyObserver<CoachDetailInfo>(this) {
            @Override
            public void onSuccess(CoachDetailInfo result) {
                try {
                    if (result.getInfo().getAvatar().length() > 0) {
                        Glide.with(getBaseContext()).load(result.getInfo().getAvatar()).into(circleImageView);
                    } else {
                        circleImageView.setImageResource(R.drawable.mine_head_icon);
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.stars_1:
                stars_1.setImageResource(R.drawable.star_1);
                stars = "1";
                s1 = true;
                stars_tv1.setTextColor(Color.parseColor("#A6A8BD"));
                stars_tv2.setTextColor(Color.parseColor("#999999"));
                stars_tv3.setTextColor(Color.parseColor("#999999"));
                stars_2.setImageResource(R.drawable.starts_2_false);
                stars_3.setImageResource(R.drawable.stars_3_false);
                if (input_release.getText().length() > 0) {
                    post.setEnabled(true);
                    post.setBackgroundResource(R.drawable.textview_1);
                } else {
                    post.setEnabled(false);
                    post.setBackgroundResource(R.drawable.textview_9);
                }
                break;
            case R.id.stars_2:
                stars_2.setImageResource(R.drawable.star_2);
                stars = "2";
                stars_1.setImageResource(R.drawable.stars_1_fase);
                stars_3.setImageResource(R.drawable.stars_3_false);
                stars_tv2.setTextColor(Color.parseColor("#FFA619"));
                stars_tv1.setTextColor(Color.parseColor("#999999"));
                stars_tv3.setTextColor(Color.parseColor("#999999"));
                s2 = true;
                if (input_release.getText().length() > 0) {
                    post.setEnabled(true);
                    post.setBackgroundResource(R.drawable.textview_1);
                } else {
                    post.setEnabled(false);
                    post.setBackgroundResource(R.drawable.textview_9);
                }
                break;
            case R.id.stars_3:
                stars_3.setImageResource(R.drawable.star_3);
                stars = "3";
                stars_1.setImageResource(R.drawable.stars_1_fase);
                stars_2.setImageResource(R.drawable.starts_2_false);
                stars_tv2.setTextColor(Color.parseColor("#999999"));
                stars_tv1.setTextColor(Color.parseColor("#999999"));
                stars_tv3.setTextColor(Color.parseColor("#FF7519"));
                s3 = true;
                if (input_release.getText().length() > 0) {
                    post.setEnabled(true);
                    post.setBackgroundResource(R.drawable.textview_1);
                } else {
                    post.setEnabled(false);
                    post.setBackgroundResource(R.drawable.textview_9);
                }
                break;
            case R.id.post:
                addOrderComment(input_release.getText().toString());
                break;
        }
    }


    private void addOrderComment(String content) {
        RequestUtils.addOrderComment(SharePreUtil.getString(this, "token", ""), order_id, stars, content, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                cj_ok.setVisibility(View.VISIBLE);
                cj.setVisibility(View.GONE);
                post.setText("返回");
                post.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent Stingintent = new Intent(StudyOrderCommentActivity.this, MainActivity.class);
                        startActivity(Stingintent);
                    }
                });
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }
}