package com.gzai.zhongjiang.digitalmovement.http.list;


import android.content.Context;
import android.content.Intent;

import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.http.RxExceptionUtil;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 数据返回统一处理  参考https://www.jianshu.com/p/ff619fea7e22
 *
 * @param <T>
 */
public abstract class ListBaseObserver<T> implements Observer<ListBaseResponse<T>> {
    private static final String TAG = "BaseObserver";
    Context context;

    @Override
    public void onNext(ListBaseResponse<T> response) {
        //在这边对 基础数据 进行统一处理  举个例子：
        if (response.getError_code() == 0) {
            onSuccess(response.getData());
        } else if (response.getError_code() == 1001 || response.getError_code() == 1002) {
            onFailure(null, response.getError_code() + "");
        } else {
            onFailure(null, response.getError_msg());
        }
    }

    @Override
    public void onError(Throwable e) {//服务器错误信息处理
        onFailure(e, RxExceptionUtil.exceptionHandler(e));
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onSubscribe(Disposable d) {
    }

    public abstract void onSuccess(T result);


    public abstract void onFailure(Throwable e, String errorMsg);

}
