package com.gzai.zhongjiang.digitalmovement.bean;

public class CoachListBean {
    private String  avatar;
    private String  user_id;
    private String  truename;
    private String  good_at;
    private String  price;
    private String  room_name;
   public CoachListBean(String  avatar,String  user_id,String  truename,String  good_at,String  price,String  room_name){
       this.avatar=avatar;
       this.user_id=user_id;
       this.truename=truename;
       this.good_at=good_at;
       this.price=price;
       this.room_name=room_name;
   }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getGood_at() {
        return good_at;
    }

    public void setGood_at(String good_at) {
        this.good_at = good_at;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }
}
