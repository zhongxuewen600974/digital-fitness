package com.gzai.zhongjiang.digitalmovement;


import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.UploadImageBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.RetrofitUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.ImagePickerLoader;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridBean;
import com.lwkandroid.imagepicker.ImagePicker;
import com.lwkandroid.imagepicker.data.ImageBean;
import com.lwkandroid.imagepicker.data.ImagePickType;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ImproveInformationActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.login_head_icon)
    ImageView login_head_icon;
    @BindView(R.id.next)
    TextView next;

    private final int REQUEST_CODE_PICKER = 100;
    private int cb = 1;
    ArrayList<String> backimageUrl = new ArrayList<>();
    Gson gson = new Gson();
    private String nick_name="", my_sex, avatar_url, imagePath="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_improve_information);
        ButterKnife.bind(this);
        login_head_icon.setOnClickListener(this);
        next.setOnClickListener(this);
        Intent intent=getIntent();
        nick_name=intent.getStringExtra("nick_name");
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.login_head_icon:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
                }
                new ImagePicker()
                        .cachePath(Environment.getExternalStorageDirectory().getAbsolutePath())
                        .pickType(ImagePickType.MULTI)
                        .displayer(new ImagePickerLoader())
                        .maxNum(1)
                        .start(ImproveInformationActivity.this, REQUEST_CODE_PICKER);
                break;
            case R.id.next:
                if(imagePath.length()>0 || input_name.getText().toString().length()>0){
                    if(imagePath.length()==0){
                        saveUserInfo(imagePath, "", input_name.getText().toString(), "",  "");
                    }else {
                        uploadImage(imagePath);
                    }
                }else {
                    Intent intent=new Intent(ImproveInformationActivity.this,ImproveActivity.class);
                    intent.putExtra("nick_name",nick_name);
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            List<ImageBean> list = data.getParcelableArrayListExtra(ImagePicker.INTENT_RESULT_DATA);
            List<NineGridBean> resultList = new ArrayList<>();
            for (ImageBean imageBean : list) {
                NineGridBean nineGirdData = new NineGridBean(imageBean.getImagePath());
                resultList.add(nineGirdData);
                cb = 2;
                imagePath = imageBean.getImagePath();
                Glide.with(ImproveInformationActivity.this).load(imageBean.getImagePath()).into(login_head_icon);
            }

        }
    }

    private void uploadImage(String imagePath) {
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg/png"), file);
        MultipartBody.Part body = null;
        try {
            body = MultipartBody.Part.createFormData("file", URLEncoder.encode(file.getName(), "UTF-8"), requestBody);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RequestUtils.uploadImage(SharePreUtil.getString(this, "token", ""), RetrofitUtils.BaseUrl + "api/Upload/image", body, new MyObserver<UploadImageBean>(this) {
            @Override
            public void onSuccess(UploadImageBean result) {
                backimageUrl.add(result.getUrl());
                String image_list = result.getUrl();
                if(input_name.getText().toString().length()>0){
                    nick_name=input_name.getText().toString();
                }
                saveUserInfo(image_list, my_sex, nick_name, "",  "");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {


            }
        });
    }
    private void saveUserInfo(String avatar, String sex, String nick_name, String birthday, String intro) {
        RequestUtils.saveUserInfo(SharePreUtil.getString(this, "token", ""), avatar, sex, nick_name, birthday, intro, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                Intent intent=new Intent(ImproveInformationActivity.this,ImproveActivity.class);
                intent.putExtra("imagePath",imagePath);
                intent.putExtra("nick_name",nick_name);
                startActivity(intent);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}