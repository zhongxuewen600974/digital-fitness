package com.gzai.zhongjiang.digitalmovement.gym;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.home.DynamicDetailsActivity;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.ExplainReasonCallbackWithBeforeParam;
import com.permissionx.guolindev.callback.ForwardToSettingsCallback;
import com.permissionx.guolindev.callback.RequestCallback;
import com.permissionx.guolindev.request.ExplainScope;
import com.permissionx.guolindev.request.ForwardScope;
import com.tencent.map.geolocation.TencentLocation;
import com.tencent.map.geolocation.TencentLocationListener;
import com.tencent.map.geolocation.TencentLocationManager;
import com.tencent.map.geolocation.TencentLocationRequest;
import com.tencent.tencentmap.mapsdk.maps.LocationSource;
import com.tencent.tencentmap.mapsdk.maps.MapView;
import com.tencent.tencentmap.mapsdk.maps.TencentMap;


import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapActivity extends BaseActivity implements LocationSource, TencentLocationListener, View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.gym_name)
    TextView gym_name_tv;
    @BindView(R.id.gym_address)
    TextView gym_address_tv;
    @BindView(R.id.select_map_type_image)
    ImageView select_map_type_image;
    private LocationSource.OnLocationChangedListener locationChangedListener;
    private TencentLocationManager locationManager;
    private TencentLocationRequest locationRequest;
    TencentMap tencentMap;
    MapView mapView;
    String lng, lat, dym_name, dym_address;
    int cb = 1;
    private LocationManager mylocationManager;


    private void getLocation() {
        ArrayList<String> permissions = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (permissions.size() == 0) {//有权限，直接获取定位
            initLocation();
        } else {//没有权限，获取定位权限
            checkPermission();
           //requestPermissions(permissions.toArray(new String[permissions.size()]), 2);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);
        actionBarView.setTitle("位置信息");
        Intent intent = getIntent();
        lng = intent.getStringExtra("lng");
        lat = intent.getStringExtra("lat");
        dym_name = intent.getStringExtra("dym_name");
        dym_address = intent.getStringExtra("dym_address");
        gym_name_tv.setText(dym_name);
        gym_address_tv.setText(dym_address);

        mapView = findViewById(R.id.task_page_map_view);
        tencentMap = mapView.getMap();
        // initLocation();
        select_map_type_image.setOnClickListener(this);
        mylocationManager = (LocationManager) getBaseContext().getSystemService(Context.LOCATION_SERVICE);
        if (mylocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            getLocation();
            //gps已打开
        } else {
//            EasyPermissions.requestPermissions(this, "请允许定位权限",
//                    100, Manifest.permission.ACCESS_FINE_LOCATION);
            checkPermission();
            // Toast.makeText(getBaseContext().getApplicationContext(), "定位系统没有开启", Toast.LENGTH_SHORT).show();
        }


    }

    private void checkPermission() {
        PermissionX.init(this)
                .permissions(

                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION

                )
                .onExplainRequestReason(new ExplainReasonCallbackWithBeforeParam() {
                    @Override
                    public void onExplainReason(ExplainScope scope, List<String> deniedList, boolean beforeRequest) {
                       // scope.showRequestReasonDialog(deniedList, "即将申请的权限是程序必须依赖的权限", "我已明白");
                    }
                })
                .onForwardToSettings(new ForwardToSettingsCallback() {
                    @Override
                    public void onForwardToSettings(ForwardScope scope, List<String> deniedList) {
                       // scope.showForwardToSettingsDialog(deniedList, "您需要去应用程序设置当中手动开启权限", "我已明白");
                    }
                })
                .request(new RequestCallback() {
                    @Override
                    public void onResult(boolean allGranted, List<String> grantedList, List<String> deniedList) {
                        if (allGranted) {
                            // Toast.makeText(MainActivity.this, "所有申请的权限都已通过", Toast.LENGTH_SHORT).show();
                        } else {
                            //  Toast.makeText(MainActivity.this, "您拒绝了如下权限：" + deniedList, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            //申请权限
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 222);//自定义的code
//        }
    }

    private void initLocation() {
        locationManager = TencentLocationManager.getInstance(this);
        locationRequest = TencentLocationRequest.create();
        locationRequest.setInterval(30 * 1000);
        locationManager.requestLocationUpdates(locationRequest, this, Looper.getMainLooper());
        tencentMap.setLocationSource(this);
        tencentMap.setMyLocationEnabled(true);
    }

    @Override
    public void onLocationChanged(TencentLocation tencentLocation, int i, String s) {
        if (i == TencentLocation.ERROR_OK && tencentLocation != null) {
            //设置经纬度
            if (cb == 1) {
                Location location = new Location(tencentLocation.getProvider());
                location.setLatitude(Double.parseDouble(lat));
                location.setLongitude(Double.parseDouble(lng));
                //设置精度，这个值会被设置为定位点上表示精度的圆形半径
                location.setAccuracy(tencentLocation.getAccuracy());
                //设置定位标的旋转角度，注意 tencentLocation.getBearing() 只有在 gps 时才有可能获取
                location.setBearing(tencentLocation.getBearing());
                //将位置信息返回给地图
                locationChangedListener.onLocationChanged(location);
            }
            cb++;
        }
    }

    @Override
    public void onStatusUpdate(String s, int i, String s1) {

    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        locationChangedListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {

    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        mapView.onStop();
    }


    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        mapView.onDestroy();

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.select_map_type_image:
                ShowDailg();
                break;
        }
    }


    private void ShowDailg() {
        new XPopup.Builder(this)
                .hasStatusBarShadow(true)
                .autoOpenSoftInput(true)
                .asCustom(new ToCommentPopup(this))
                .show();
    }

    public class ToCommentPopup extends BottomPopupView {
        public ToCommentPopup(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_select_map;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.tv_cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });


            findViewById(R.id.baidu_map).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAvilible(getBaseContext(), "com.baidu.BaiduMap")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(
                                Uri.parse("qqmap://map/routeplan?type=bus&from=我的位置&fromcoord=0,0"
                                        + "&to=" + dym_name
                                        + "&tocoord=" + lat + "," + lng
                                        + "&policy=1&referer=myapp")
                        );
                        startActivity(intent);
                    } else {
                        ToastUtils.show("没有安装百度地图");
                    }
                }
            });


            findViewById(R.id.tv_gaode).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAvilible(getBaseContext(), "com.autonavi.minimap")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("androidamap://route?sourceApplication=" + R.string.app_name
                                + "&sname=我的位置&dlat=" + lat
                                + "&dlon=" + lng
                                + "&dname=" + dym_name
                                + "&dev=0&m=0&t=1"));
                        startActivity(intent);
                    } else {
                        ToastUtils.show("高德地图未安装");
                    }
                }
            });


            findViewById(R.id.tv_tengxun).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAvilible(getBaseContext(), "com.tencent.map")) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(
                                Uri.parse("qqmap://map/routeplan?type=bus&from=我的位置&fromcoord=0,0"
                                        + "&to=" + dym_name
                                        + "&tocoord=" + lat + "," + lng
                                        + "&policy=1&referer=myapp")
                        );
                        startActivity(intent);
                    } else {
                        ToastUtils.show("腾讯地图未安装");
                    }
                }
            });
        }

        @Override
        protected void onShow() {

            super.onShow();
        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }


    private boolean isAvilible(Context context, String packageName) {
        //获取packagemanager
        final PackageManager packageManager = context.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        //用于存储所有已安装程序的包名
        List<String> packageNames = new ArrayList<>();
        //从pinfo中将包名字逐一取出，压入pName list中
        if (packageInfos != null) {
            for (int i = 0; i < packageInfos.size(); i++) {
                String packName = packageInfos.get(i).packageName;
                packageNames.add(packName);
            }
        }
        //判断packageNames中是否有目标程序的包名，有TRUE，没有FALSE
        return packageNames.contains(packageName);
    }
}