package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
 import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
 import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
 import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
 import com.gzai.zhongjiang.digitalmovement.message.chat.MessageBean;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private Context mContext;
    private List<MessageBean> dataBean;


    public ChatAdapter(List<MessageBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_other, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getSend_uid().equals(SharePreUtil.getString(mContext, "user_id", ""))) {
                holder.chat_linear_right.setVisibility(View.VISIBLE);
                holder.chat_linear.setVisibility(View.GONE);
                holder.msg_gight.setText(dataBean.get(position).getMsg_info().substring(8, dataBean.get(position).getMsg_info().length() - 2));
                if (dataBean.get(position).getSend_avatar().length() > 0) {
                    Glide.with(mContext).load(dataBean.get(position).getSend_avatar()).into(holder.me_circleImageView);
                } else {
                    holder.me_circleImageView.setImageResource(R.drawable.mine_head_icon);
                }
            } else {
                holder.chat_linear_right.setVisibility(View.GONE);
                holder.chat_linear.setVisibility(View.VISIBLE);
                holder.msg.setText(dataBean.get(position).getMsg_info().substring(8, dataBean.get(position).getMsg_info().length() - 2));
                if (dataBean.get(position).getSend_avatar().length() > 0) {
                    Glide.with(mContext).load(dataBean.get(position).getSend_avatar()).into(holder.circleImageView);
                } else {
                    holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
                }
            }
            holder.time.setText(stampToDate(dataBean.get(position).getCreate_time()));


        } catch (Exception e) {

        }
        holder.circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getSend_uid());
                mContext.startActivity(Stingintent);
            }
        });


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView, me_circleImageView;
        TextView msg, time, msg_gight;
        ImageView imageView_lift, imageView_right;
        LinearLayout chat_linear, chat_linear_right;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            me_circleImageView = (CircleImageView) itemView.findViewById(R.id.me_circleImageView);
            msg = (TextView) itemView.findViewById(R.id.msg_left);
            msg_gight = (TextView) itemView.findViewById(R.id.msg_gight);
            time = (TextView) itemView.findViewById(R.id.time);
            imageView_lift = (ImageView) itemView.findViewById(R.id.imageView_lift);
            imageView_right = (ImageView) itemView.findViewById(R.id.imageView_right);
            chat_linear = (LinearLayout) itemView.findViewById(R.id.chat_linear);
            chat_linear_right = (LinearLayout) itemView.findViewById(R.id.chat_linear_right);
        }
    }

}
