package com.gzai.zhongjiang.digitalmovement.gym;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.CoachListAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachListBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoachListActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.nodata_linear)
    LinearLayout onData;
    @BindView(R.id.data_linear)
    LinearLayout haveData;
    private int page_total, page = 1;

    private String dym_id = "";

    CoachListBean myBean;
    List<CoachListBean> beanList = new ArrayList<>();
    CoachListAdapter myAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_list);
        ButterKnife.bind(this);
        actionBarView.setTitle("教练");
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        Intent intent = getIntent();
        dym_id = intent.getStringExtra("dym_id");
        intView(dym_id);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page=1;
                            intView(dym_id);
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                 loadMore(dym_id,page);
                            } else {
                               // ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
    }

    private void intView(String dym_id) {
        beanList.clear();
        RequestUtils.getCoachList(SharePreUtil.getString(this, "token", ""), 1, 10, dym_id, new ListMyObserver<ListBean<CoachListBean>>(this) {
            @Override
            public void onSuccess(ListBean<CoachListBean> result) {
                ListBean<CoachListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String avatar = list.getList().get(i).getAvatar();
                        String user_id = list.getList().get(i).getUser_id();
                        String truename = list.getList().get(i).getTruename();
                        String good_at = list.getList().get(i).getGood_at();
                        String price = list.getList().get(i).getPrice();
                        String room_name = list.getList().get(i).getRoom_name();
                        myBean = new CoachListBean(avatar, user_id, truename, good_at, price, room_name);
                        beanList.add(myBean);
                    }
                    myAdapter = new CoachListAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(String dym_id,int page) {
        RequestUtils.getCoachList(SharePreUtil.getString(this, "token", ""), page, 10, dym_id, new ListMyObserver<ListBean<CoachListBean>>(this) {
            @Override
            public void onSuccess(ListBean<CoachListBean> result) {
                ListBean<CoachListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String avatar = list.getList().get(i).getAvatar();
                        String user_id = list.getList().get(i).getUser_id();
                        String truename = list.getList().get(i).getTruename();
                        String good_at = list.getList().get(i).getGood_at();
                        String price = list.getList().get(i).getPrice();
                        String room_name = list.getList().get(i).getRoom_name();
                        myBean = new CoachListBean(avatar, user_id, truename, good_at, price, room_name);
                        beanList.add(myBean);
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


}