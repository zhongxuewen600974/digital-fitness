package com.gzai.zhongjiang.digitalmovement.view.wetchimage;

import android.content.Context;
import android.content.Intent;


import java.util.ArrayList;
import java.util.List;

public class JZCImageUtil {

    public static void imageBrower(Context context, int position, List<String> columns) {
        List<String> mAllImagePath = new ArrayList<>();
        for (int i = 0; i < columns.size(); i++){
            mAllImagePath.add(columns.get(i));
        }

        String[] urls = (String[]) mAllImagePath.toArray(new String[mAllImagePath.size()]);
        Intent intent = new Intent(context, ImagePagerActivity.class);
        // 图片url,为了演示这里使用常量，一般从数据库中或网络中获取
        intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_URLS, urls);
        intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_INDEX, position);
        context.startActivity(intent);
    }

}
