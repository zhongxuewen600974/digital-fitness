package com.gzai.zhongjiang.digitalmovement.message;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.CourseAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.MyCommentAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.PraiseAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CommentInfo;
import com.gzai.zhongjiang.digitalmovement.bean.CommentListBean;
import com.gzai.zhongjiang.digitalmovement.bean.CourseListBean;
import com.gzai.zhongjiang.digitalmovement.bean.PraiseNoticeListBean;
import com.gzai.zhongjiang.digitalmovement.gym.CourseWeekActivity;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.RetrofitUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class MyCommentActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private int page_total, page = 1;

    List<CommentListBean> beanList = new ArrayList<>();
    MyCommentAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_comment);
        ButterKnife.bind(this);
        actionBarView.setTitle("评论");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                              //  ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
    }

    private void loadMore(int page) {
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("page", page + "")
                .addFormDataPart("page_size", 10 + "")
                .build();
        Request request = new Request.Builder().url(RetrofitUtils.BaseUrl + "api/app/getCommentNoticeList").addHeader("token", SharePreUtil.getString(this, "token", "")).post(requestBody).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                String result = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String error_code = jsonObject.optString("error_code", null);
                    if (error_code.equals("0")) {
                        String data = jsonObject.optString("data", null);
                        JSONObject dataobject = new JSONObject(data);
                        String page_info = dataobject.optString("page_info", null);
                        JSONObject page_infoobject = new JSONObject(page_info);
                        int counts = page_infoobject.optInt("counts", 0);
                        if (counts > 0) {
                            JSONArray list = dataobject.getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                CommentListBean dyBean=new CommentListBean();
                                JSONObject value = list.getJSONObject(i);
                                String comment_id = value.getString("comment_id");
                                dyBean.setComment_id(comment_id);
                                String user_id = value.getString("user_id");
                                dyBean.setUser_id(user_id);
                                String to_user_id = value.getString("to_user_id");
                                dyBean.setTo_user_id(to_user_id);
                                String parent_id = value.getString("parent_id");
                                dyBean.setParent_id(parent_id);
                                String post_id = value.getString("post_id");
                                dyBean.setPost_id(post_id);
                                String type = value.getString("type");
                                dyBean.setType(type);
                                String comment_content = value.getString("comment_content");
                                dyBean.setComment_content(comment_content);
                                String title = value.getString("title");
                                dyBean.setTitle(title);
                                String content = value.getString("content");
                                dyBean.setContent(content);
                                String nick_name = value.getString("nick_name");
                                dyBean.setNick_name(nick_name);
                                String avatar = value.getString("avatar");
                                dyBean.setAvatar(avatar);
                                String create_time = value.getString("create_time");
                                dyBean.setCreate_time(create_time);
                                String post_nick_name = value.getString("post_nick_name");
                                dyBean.setPost_nick_name(post_nick_name);
                                String post_avatar = value.getString("post_avatar");
                                dyBean.setPost_avatar(post_avatar);
                                String mycomment_info = value.getString("comment_info");
                                if (mycomment_info.length() > 10) {
                                    JSONObject infoobject = new JSONObject(mycomment_info);
                                    CommentInfo commentInfo = new CommentInfo();
                                    commentInfo.setId(infoobject.getString("id"));
                                    commentInfo.setPost_id(infoobject.getString("post_id"));
                                    commentInfo.setContent(infoobject.getString("content"));
                                    commentInfo.setSend_name(infoobject.getString("send_name"));
                                    commentInfo.setSend_uid(infoobject.getString("send_uid"));
                                    commentInfo.setRecive_uid(infoobject.getString("recive_uid"));
                                    commentInfo.setRecive_name(infoobject.getString("recive_name"));
                                    commentInfo.setCreate_time(infoobject.getString("create_time"));
                                    dyBean.setComment_info(commentInfo);
                                }
                                beanList.add(dyBean);
                            }
                            new RequestThread1().start();
                        }
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void intView() {
        beanList.clear();
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("page", 1 + "")
                .addFormDataPart("page_size", 10 + "")
                .build();
        Request request = new Request.Builder().url(RetrofitUtils.BaseUrl + "api/app/getCommentNoticeList").addHeader("token", SharePreUtil.getString(this, "token", "")).post(requestBody).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                String result = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String error_code = jsonObject.optString("error_code", null);
                    if (error_code.equals("0")) {
                        String data = jsonObject.optString("data", null);
                        JSONObject dataobject = new JSONObject(data);
                        String page_info = dataobject.optString("page_info", null);
                        JSONObject page_infoobject = new JSONObject(page_info);
                        page_total = page_infoobject.optInt("page_total", 0);
                        int counts = page_infoobject.optInt("counts", 0);
                        if (counts > 0) {
                            JSONArray list = dataobject.getJSONArray("list");
                            for (int i = 0; i < list.length(); i++) {
                                CommentListBean dyBean=new CommentListBean();
                                JSONObject value = list.getJSONObject(i);
                                String comment_id = value.getString("comment_id");
                                dyBean.setComment_id(comment_id);
                                String user_id = value.getString("user_id");
                                dyBean.setUser_id(user_id);
                                String to_user_id = value.getString("to_user_id");
                                dyBean.setTo_user_id(to_user_id);
                                String parent_id = value.getString("parent_id");
                                dyBean.setParent_id(parent_id);
                                String post_id = value.getString("post_id");
                                dyBean.setPost_id(post_id);
                                String type = value.getString("type");
                                dyBean.setType(type);
                                String comment_content = value.getString("comment_content");
                                dyBean.setComment_content(comment_content);
                                String title = value.getString("title");
                                dyBean.setTitle(title);
                                String content = value.getString("content");
                                dyBean.setContent(content);
                                String nick_name = value.getString("nick_name");
                                dyBean.setNick_name(nick_name);
                                String avatar = value.getString("avatar");
                                dyBean.setAvatar(avatar);
                                String create_time = value.getString("create_time");
                                dyBean.setCreate_time(create_time);

                                String post_nick_name = value.getString("post_nick_name");
                                dyBean.setPost_nick_name(post_nick_name);
                                String post_avatar = value.getString("post_avatar");
                                dyBean.setPost_avatar(post_avatar);


                                String mycomment_info = value.getString("comment_info");
                                if (mycomment_info.length() > 10) {
                                    JSONObject infoobject = new JSONObject(mycomment_info);
                                    CommentInfo commentInfo = new CommentInfo();
                                    commentInfo.setId(infoobject.getString("id"));
                                    commentInfo.setPost_id(infoobject.getString("post_id"));
                                    commentInfo.setContent(infoobject.getString("content"));
                                    commentInfo.setSend_name(infoobject.getString("send_name"));
                                    commentInfo.setSend_uid(infoobject.getString("send_uid"));
                                    commentInfo.setRecive_uid(infoobject.getString("recive_uid"));
                                    commentInfo.setRecive_name(infoobject.getString("recive_name"));
                                    commentInfo.setCreate_time(infoobject.getString("create_time"));
                                    dyBean.setComment_info(commentInfo);
                                }
                                beanList.add(dyBean);
                            }
                            myAdapter = new MyCommentAdapter(beanList);
                            new RequestThread().start();
                        }
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private class RequestThread extends Thread {
        @Override
        public void run() {
            Message msg = new Message();
            msg.what = 1;
            handler.sendMessage(msg);
        }
    }

    private class RequestThread1 extends Thread {
        @Override
        public void run() {
            Message msg = new Message();
            msg.what = 2;
            handler.sendMessage(msg);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                nodata_linear.setVisibility(View.GONE);
                data_linear.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(myAdapter);
            } else if (msg.what == 2) {
                myAdapter.notifyDataSetChanged();
            }
        }
    };
}