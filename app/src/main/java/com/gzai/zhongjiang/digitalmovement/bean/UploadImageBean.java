package com.gzai.zhongjiang.digitalmovement.bean;

public class UploadImageBean {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
