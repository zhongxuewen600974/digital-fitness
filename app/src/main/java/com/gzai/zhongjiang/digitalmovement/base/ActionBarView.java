package com.gzai.zhongjiang.digitalmovement.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;


public class ActionBarView extends RelativeLayout {
    public RelativeLayout rl_left;
    public RelativeLayout rl_right;
    public TextView tv_left;
    public TextView tv_right;
    public TextView tv_title;
    public ImageView iv_left;
    public ImageView iv_right;


    public ActionBarView(Context context) {
        super(context);
    }

    public ActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ActionBarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    protected void init() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View v = inflater.inflate(R.layout.communal_head, this);
        rl_left = (RelativeLayout) v.findViewById(R.id.rl_left);
        rl_right = (RelativeLayout) v.findViewById(R.id.rl_right);
        tv_left = (TextView) v.findViewById(R.id.tv_left);
        tv_right = (TextView) v.findViewById(R.id.tv_right);
        iv_left = (ImageView) v.findViewById(R.id.iv_left);
        iv_right = (ImageView) v.findViewById(R.id.iv_right);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_right.setVisibility(GONE);
        iv_right.setVisibility(GONE);
        tv_left.setVisibility(GONE);
        rl_left.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity activity = (Activity) getContext();
                activity.finish();
            }
        });
    }

    public void setTitle(String title) {
        tv_title.setText(title);
    }

    public void setLeftText(String content) {
        iv_left.setVisibility(GONE);
        tv_left.setVisibility(VISIBLE);
        tv_left.setText(content);
    }

    public void settitleColor(String content) {
        tv_title.setTextColor(Color.parseColor(content));
    }


    public View getLeftMenu() {
        return rl_left;
    }

    public void setRightText(String content){
        tv_right.setVisibility(VISIBLE);
        iv_right.setVisibility(GONE);
        tv_right.setText(content);
    }

    public void setRightTextSize(float size, int color){
        tv_right.setTextSize(size);
        tv_right.setTextColor(color);
    }
    public void setRightImg(int res){
        tv_right.setVisibility(GONE);
        iv_right.setVisibility(VISIBLE);
        iv_right.setImageResource(res);
    }
    public void setLiefImg(int res){
        iv_left.setVisibility(VISIBLE);
        tv_left.setVisibility(GONE);
        iv_left.setImageResource(res);
    }
    public View getRightMenu() {
        return rl_right;
    }
}
