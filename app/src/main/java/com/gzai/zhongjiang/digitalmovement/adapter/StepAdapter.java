package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentChild;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.StepBean;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;

import java.util.List;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.ViewHolder> {
    private Context mContext;
    private List<StepBean> dataBean;


    public StepAdapter(List<StepBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_setp_rank, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.name.setText(dataBean.get(position).getNick_name());
            holder.step.setText(dataBean.get(position).getStep_count());
            if(position == 0){
                holder.step.setTextColor(Color.parseColor("#00D669"));
            }else {
                holder.step.setTextColor(Color.parseColor("#666666"));
            }

            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            if (position == 0) {
                holder.imageView.setVisibility(View.VISIBLE);
                holder.imageView.setImageResource(R.drawable.mine_pm_1_icon);
                holder.rank.setVisibility(View.GONE);
            } else if (position == 1) {
                holder.imageView.setVisibility(View.VISIBLE);
                holder.imageView.setImageResource(R.drawable.mine_pm_2_icon);
                holder.rank.setVisibility(View.GONE);
            } else if (position == 2) {
                holder.imageView.setVisibility(View.VISIBLE);
                holder.imageView.setImageResource(R.drawable.mine_pm_3_icon);
                holder.rank.setVisibility(View.GONE);
            } else {
                holder.imageView.setVisibility(View.GONE);
                holder.rank.setVisibility(View.VISIBLE);
                holder.rank.setText(position +1+ "");
            }
            holder.pairse.setText(dataBean.get(position).getPraises());
            if (dataBean.get(position).getIspraise().equals("0")) {
                holder.praises_image.setImageResource(R.drawable.dianzan_fase);
            } else {
                holder.praises_image.setImageResource(R.drawable.home_dianzan);
            }
        } catch (Exception e) {
        }

        holder.praises_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataBean.get(position).getIspraise().equals("0")) {
                    addStepPraise(dataBean.get(position).getId());
                    holder.praises_image.setImageResource(R.drawable.home_dianzan);
                    holder.pairse.setText((Integer.parseInt(dataBean.get(position).getPraises())+1)+"");
                }
            }
        });

        holder.circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });


    }

    private void addStepPraise(String id) {
        RequestUtils.addStepPraise(SharePreUtil.getString(mContext, "token", ""), id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, praises_image;
        TextView rank, name, step, pairse;
        CircleImageView circleImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.imageView_rank);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            rank = (TextView) itemView.findViewById(R.id.rank);
            name = (TextView) itemView.findViewById(R.id.name);
            step = (TextView) itemView.findViewById(R.id.step);
            pairse = (TextView) itemView.findViewById(R.id.pairse);
            praises_image = (ImageView) itemView.findViewById(R.id.praises_image);
        }
    }

}
