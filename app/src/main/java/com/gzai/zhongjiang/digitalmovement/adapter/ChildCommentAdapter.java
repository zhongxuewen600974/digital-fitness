package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentChild;

import java.util.List;

public class ChildCommentAdapter extends RecyclerView.Adapter<ChildCommentAdapter.ViewHolder> {
    private Context mContext;
    private List<CommentChild> dataBean;


    public ChildCommentAdapter(List<CommentChild> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_child_comment, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getNick_name().length() > 0) {
                holder.parent_name.setText(dataBean.get(position).getNick_name() + "");
                holder.parent_name.setVisibility(View.VISIBLE);
                holder.collback.setVisibility(View.VISIBLE);
                holder.child_comment.setVisibility(View.VISIBLE);
            }
            holder.child_name.setText(dataBean.get(position).getComment_nick_name() + ":");
            holder.child_comment.setText(dataBean.get(position).getContent());
        } catch (Exception e) {
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView parent_name, collback, child_name, child_comment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            parent_name = (TextView) itemView.findViewById(R.id.parent_name);
            collback = (TextView) itemView.findViewById(R.id.collback);
            child_name = (TextView) itemView.findViewById(R.id.child_name);
            child_comment = (TextView) itemView.findViewById(R.id.child_comment);
        }
    }

}
