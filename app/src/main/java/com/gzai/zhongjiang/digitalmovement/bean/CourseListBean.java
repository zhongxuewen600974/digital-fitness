package com.gzai.zhongjiang.digitalmovement.bean;

public class CourseListBean {
    private String id;
    private String course_name;
    private String course_date;
    private String start_time;
    private String end_time;
    public CourseListBean(String id,String course_name,String course_date,String start_time,String end_time){
        this.id=id;
        this.course_name=course_name;
        this.course_date=course_date;
        this.start_time=start_time;
        this.end_time=end_time;
    }
    public CourseListBean(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getCourse_date() {
        return course_date;
    }

    public void setCourse_date(String course_date) {
        this.course_date = course_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
