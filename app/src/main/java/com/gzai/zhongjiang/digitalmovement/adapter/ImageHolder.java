package com.gzai.zhongjiang.digitalmovement.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.view.RxRoundProgressBar;

public class ImageHolder extends RecyclerView.ViewHolder{
    TextView room_name, truename, card_id, expire_time,courses;
    RxRoundProgressBar rxRoundProgressBar;

    public ImageHolder(@NonNull View itemView) {
        super(itemView);
        room_name = (TextView) itemView.findViewById(R.id.room_name);
        truename = (TextView) itemView.findViewById(R.id.truename);
        card_id = (TextView) itemView.findViewById(R.id.card_id);
        expire_time = (TextView) itemView.findViewById(R.id.expire_time);
        courses = (TextView) itemView.findViewById(R.id.courses);
        rxRoundProgressBar = (RxRoundProgressBar) itemView.findViewById(R.id.rxRoundProgressBar);
    }
}
