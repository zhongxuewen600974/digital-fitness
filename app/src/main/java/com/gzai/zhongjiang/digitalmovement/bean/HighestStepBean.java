package com.gzai.zhongjiang.digitalmovement.bean;

public class HighestStepBean {
    private String step_count;
    private String record_date;

    public String getStep_count() {
        return step_count;
    }

    public void setStep_count(String step_count) {
        this.step_count = step_count;
    }

    public String getRecord_date() {
        return record_date;
    }

    public void setRecord_date(String record_date) {
        this.record_date = record_date;
    }
}
