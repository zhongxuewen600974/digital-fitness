package com.gzai.zhongjiang.digitalmovement.http.list;

import com.gzai.zhongjiang.digitalmovement.bean.PageInfo;

import java.util.List;

public class CountListBean<T> {
    private List<T> list;
    private String counts;

    public String getCounts() {
        return counts;
    }

    public void setCounts(String counts) {
        this.counts = counts;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }


}
