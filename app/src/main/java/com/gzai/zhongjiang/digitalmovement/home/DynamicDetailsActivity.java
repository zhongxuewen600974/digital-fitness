package com.gzai.zhongjiang.digitalmovement.home;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.CommentAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.DailogComAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.MyImageAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.ChildList;
import com.gzai.zhongjiang.digitalmovement.bean.ComDasilogBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.CommListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DynamicDetailsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.my_left)
    ImageView my_left;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.sex_image)
    ImageView sex_image;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.isfollow_linear)
    LinearLayout isfollow_linear;
    @BindView(R.id.isfollow_icon)
    ImageView isfollow_icon;
    @BindView(R.id.isfollow)
    TextView isfollow;
    @BindView(R.id.content)
    TextView content;
    @BindView(R.id.image_recyclerView)
    RecyclerView image_recyclerView;
    @BindView(R.id.label_linear)
    LinearLayout label_linear;
    @BindView(R.id.label)
    TextView label;
    @BindView(R.id.comments)
    TextView comments;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.to_comment)
    TextView to_comment;
    @BindView(R.id.praises_image)
    ImageView praises_image;
    @BindView(R.id.praises)
    TextView praises;
    @BindView(R.id.comment_image)
    ImageView comment_image;
    @BindView(R.id.comment)
    TextView comment;
    @BindView(R.id.share_image)
    ImageView share_image;
    @BindView(R.id.share)
    TextView share;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;

    SmartRefreshLayout smartRefreshLayout1;
    RecyclerView recyclerView1;
    LinearLayout nodata_linear1;
    LinearLayout data_linear1;


    private String post_id, label_name = "", is_follow, user_id, to_comm = "", ispraises = "", mypraises;
    List<String> beanList = new ArrayList<>();
    MyImageAdapter myAdapter;
    private int cb = 1;

    private int page_total, page = 1;
    CommentBean dyBean;
    List<CommentBean> commentbeanList = new ArrayList<>();
    CommentAdapter commentAdapter;
    String parent_id = "", to_user_id = "";


    private int page_total1, page1 = 1;
    ComDasilogBean dyBean1;
    List<ComDasilogBean> ComDasilogBeanList = new ArrayList<>();
    DailogComAdapter dailogComAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic_details);
        ButterKnife.bind(this);
        isfollow.setOnClickListener(this);
        try {
            Intent intent = getIntent();
            post_id = intent.getStringExtra("post_id");
            label_name = intent.getStringExtra("label_name");
            to_comm = intent.getStringExtra("to_comm");
            intView(post_id);
            intComment();
            if (to_comm.length() > 0) {
                new XPopup.Builder(this)
                        .hasStatusBarShadow(true)
                        .autoOpenSoftInput(true)
                        .asCustom(new ToCommentPopup(this))
                        .show();
            }
        } catch (Exception e) {
        }
        to_comment.setOnClickListener(this);
        praises_image.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
        comment_image.setOnClickListener(this);
        my_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);


        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            intComment();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                               // ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
    }


    private void intComment() {
        commentbeanList.clear();
        RequestUtils.getPostComment(SharePreUtil.getString(this, "token", ""), post_id, 1, 10, new ListMyObserver<ListBean<CommentBean>>(this) {
            @Override
            public void onSuccess(ListBean<CommentBean> result) {
                ListBean<CommentBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        ChildList child_list;
                        nodata_linear.setVisibility(View.GONE);
                        data_linear.setVisibility(View.VISIBLE);
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String to_user_id = list.getList().get(i).getTo_user_id();
                        String parent_id = list.getList().get(i).getParent_id();
                        String post_id = list.getList().get(i).getPost_id();
                        String content = list.getList().get(i).getContent();
                        String images = list.getList().get(i).getImages();
                        String praises = list.getList().get(i).getPraises();
                        String comments = list.getList().get(i).getComments();
                        String type = list.getList().get(i).getType();
                        String recom = list.getList().get(i).getRecom();
                        String status = list.getList().get(i).getStatus();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String ispraise = list.getList().get(i).getIspraise();
                        child_list = list.getList().get(i).getChild_list();
                        dyBean = new CommentBean(id, user_id, to_user_id, parent_id, post_id, content, images, praises, comments, type, recom, status, update_time, create_time,
                                nick_name, avatar, ispraise, child_list);
                        commentbeanList.add(dyBean);

                    }
                    commentAdapter = new CommentAdapter(commentbeanList);
                    recyclerView.setAdapter(commentAdapter);
                    commentAdapter.setOnItemClickListener(new CommentAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClickListener(String myparent_id, String myto_user_id) {
                            parent_id = myparent_id;
                            to_user_id = myto_user_id;
                            ShowDailg();
                        }

                        @Override
                        public void OnItemClickListener1(String post_id, String id, String avatar, String name, String content, String praises, String time, String comments, String to_user_id,String ispraise) {
                            ShowCommDailg(post_id, id, avatar, name, content, praises, time, comments, to_user_id,ispraise);
                        }

                        @Override
                        public void OnItemClickListener2(int position, int id, int num) {

                        }

                        @Override
                        public void OnItemClickListener3(int position, int id) {

                        }

                        @Override
                        public void OnItemClickListener4(int position, int id) {

                        }
                    });
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(int page) {

        RequestUtils.getPostComment(SharePreUtil.getString(this, "token", ""), post_id, page, 10, new ListMyObserver<ListBean<CommentBean>>(this) {
            @Override
            public void onSuccess(ListBean<CommentBean> result) {
                ListBean<CommentBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        ChildList child_list;
                        nodata_linear.setVisibility(View.GONE);
                        data_linear.setVisibility(View.VISIBLE);
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String to_user_id = list.getList().get(i).getTo_user_id();
                        String parent_id = list.getList().get(i).getParent_id();
                        String post_id = list.getList().get(i).getPost_id();
                        String content = list.getList().get(i).getContent();
                        String images = list.getList().get(i).getImages();
                        String praises = list.getList().get(i).getPraises();
                        String comments = list.getList().get(i).getComments();
                        String type = list.getList().get(i).getType();
                        String recom = list.getList().get(i).getRecom();
                        String status = list.getList().get(i).getStatus();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String ispraise = list.getList().get(i).getIspraise();
                        child_list = list.getList().get(i).getChild_list();
                        dyBean = new CommentBean(id, user_id, to_user_id, parent_id, post_id, content, images, praises, comments, type, recom, status, update_time, create_time,
                                nick_name, avatar, ispraise, child_list);
                        commentbeanList.add(dyBean);

                    }
                    commentAdapter.notifyDataSetChanged();
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    private void intView(String post_id) {
        beanList.clear();
        RequestUtils.getPostInfo(SharePreUtil.getString(this, "token", ""), post_id, new MyObserver<DyBean>(this) {
            @Override
            public void onSuccess(DyBean result) {
                if (result.getAvatar().toString().length() > 0) {
                    Glide.with(getBaseContext()).load(result.getAvatar()).into(circleImageView);
                } else {
                    circleImageView.setImageResource(R.drawable.mine_head_icon);
                }
                circleImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent Stingintent = new Intent(DynamicDetailsActivity.this, OthersPageActivity.class);
                        Stingintent.putExtra("user_id", result.getUser_id());
                        startActivity(Stingintent);
                    }
                });
                name.setText(result.getNick_name());
                if (result.getSex().equals("1")) {
                    sex_image.setImageResource(R.drawable.name_right_icon);
                } else {
                    sex_image.setImageResource(R.drawable.home_wm_icon);
                }
                time.setText(stampToDate(result.getCreate_time()));
                is_follow = result.getIsfollow();
                user_id = result.getUser_id();
                if (SharePreUtil.getString(DynamicDetailsActivity.this, "user_id", "").equals(result.getUser_id())) {
                    isfollow_linear.setVisibility(View.GONE);
                } else {
                    if (result.getIsfollow().equals("0")) {
                        isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                        isfollow_icon.setVisibility(View.VISIBLE);
                        isfollow.setText("关注");
                        isfollow.setTextColor(Color.parseColor("#FFFFFFFF"));
                    } else {
                        isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                        isfollow_icon.setVisibility(View.GONE);
                        isfollow.setText("已关注");
                        isfollow.setTextColor(Color.parseColor("#999999"));
                    }
                }

                content.setText(result.getContent());
                try {
                    if (label_name.length() > 0) {
                        label_linear.setVisibility(View.VISIBLE);
                        label.setText(  label_name  );
                    } else {
                        label_linear.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                }
                ispraises = result.getIspraise();
                if (result.getIspraise().equals("0")) {
                    praises_image.setImageResource(R.drawable.dianzan_fase);
                    cb = 1;
                } else {
                    cb = 2;
                    praises_image.setImageResource(R.drawable.home_dianzan);
                }

                comments.setText("评论: " + result.getComments());

                mypraises = result.getPraises();
                praises.setText(result.getPraises());
                comment.setText(result.getComments());
                share.setText(result.getShares());
                try {
                    for (int i = 0; i < result.getImage_list().size(); i++) {
                        beanList.add(result.getImage_list().get(i).toString());
                    }
                    if (result.getImage_list().size() == 1) {
                        image_recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(), 1));
                    } else {
                        image_recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(), 3));
                    }
                    myAdapter = new MyImageAdapter(beanList);
                    image_recyclerView.setAdapter(myAdapter);
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }

    private void addFollow(String to_user_id) {
        RequestUtils.addFollow(SharePreUtil.getString(this, "token", ""), to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                is_follow = "1";
                isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                isfollow_icon.setVisibility(View.GONE);
                isfollow.setText("已关注");
                isfollow.setTextColor(Color.parseColor("#999999"));

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelFollow(String to_user_id) {
        RequestUtils.cancelFollow(SharePreUtil.getString(this, "token", ""), to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                is_follow = "0";
                isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                isfollow_icon.setVisibility(View.VISIBLE);
                isfollow.setText("关注");
                isfollow.setTextColor(Color.parseColor("#FFFFFFFF"));

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.isfollow:
                if (is_follow.equals("0")) {
                    addFollow(user_id);
                } else {
                    cancelFollow(user_id);
                }
                break;
            case R.id.to_comment:
                new XPopup.Builder(this)
                        .hasStatusBarShadow(true)
                        .autoOpenSoftInput(true)
                        .asCustom(new ToCommentPopup(this))
                        .show();
                break;
            case R.id.comment_image:
                new XPopup.Builder(this)
                        .hasStatusBarShadow(true)
                        .autoOpenSoftInput(true)
                        .asCustom(new ToCommentPopup(this))
                        .show();
                break;
            case R.id.praises_image:
                if (ispraises.equals("0")) {
                    addPraise(post_id);
                } else {
                    cancelPraise(post_id);
                }
                break;

        }
    }

    private void addPraise(String post_id) {
        RequestUtils.addPraise(SharePreUtil.getString(this, "token", ""), post_id, "post", "", new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ispraises = "1";
                if (cb == 1) {
                    praises.setText((Integer.parseInt(mypraises) + 1) + "");
                } else {
                    praises.setText((Integer.parseInt(mypraises)) + "");
                }
                praises_image.setImageResource(R.drawable.home_dianzan);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelPraise(String post_id) {
        RequestUtils.cancelPraise(SharePreUtil.getString(this, "token", ""), post_id, "post", "", new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ispraises = "0";
                if (cb == 1) {
                    praises.setText((Integer.parseInt(mypraises)) + "");
                } else {
                    praises.setText((Integer.parseInt(mypraises) - 1) + "");
                }
                praises_image.setImageResource(R.drawable.dianzan_fase);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void ShowDailg() {
        new XPopup.Builder(this)
                .hasStatusBarShadow(true)
                .autoOpenSoftInput(true)
                .asCustom(new ToCommentPopup(this))
                .show();
    }

    public class ToCommentPopup extends BottomPopupView {
        public ToCommentPopup(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_to_comment;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            EditText input_comm;
            TextView number;
            number = findViewById(R.id.comment_number);
            input_comm = findViewById(R.id.input_comment);
            input_comm.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    number.setText(s.length() + "/" + "240");
                    if (s.length() >= 240) {
                        number.setTextColor(Color.parseColor("#E91E63"));
                    } else {
                        number.setTextColor(Color.parseColor("#333333"));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            findViewById(R.id.comment).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (input_comm.getText().toString().length() > 0) {
                        addPostComment(input_comm.getText().toString());
                        dismiss();
                    } else {
                        ToastUtils.show("请输入评论");
                    }
                }
            });
        }

        @Override
        protected void onShow() {

            super.onShow();
        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }

    private void addPostComment(String content) {
        RequestUtils.addPostComment(SharePreUtil.getString(this, "token", ""), post_id, content, parent_id, to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
               // intView(post_id);
                intComment();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void ShowCommDailg(String post_id, String id, String avatar, String name, String content, String praises, String time, String comments, String to_user_id,String ispraise) {
        new XPopup.Builder(this)
                .hasStatusBarShadow(true)
                .autoOpenSoftInput(true)
                .asCustom(new CommentPopup(this, post_id, id, avatar, name, content, praises, time, comments, to_user_id,ispraise))
                .show();
    }

    public class CommentPopup extends BottomPopupView {
        String Mypost_id, Myid, avatar, name, content, time, praises, comments, myto_user_id,ispraise;

        public CommentPopup(@NonNull Context context, String post_id, String id, String avatar, String name, String content, String praises, String time, String comments, String to_user_id,String ispraise) {
            super(context);
            this.Mypost_id = post_id;
            this.Myid = id;
            this.avatar = avatar;
            this.name = name;
            this.content = content;
            this.time = time;
            this.praises = praises;
            this.comments = comments;
            this.myto_user_id = to_user_id;
            this.ispraise=ispraise;
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_childcomment;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            CircleImageView circleImageView;
            ImageView praises_image1;
            EditText input_comment;
            TextView can_input;
            can_input = findViewById(R.id.can_input);
            input_comment = findViewById(R.id.input_comment);
            can_input.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    can_input.setVisibility(GONE);
                    input_comment.setVisibility(VISIBLE);
                    input_comment.setFocusable(true);
                    input_comment.setFocusableInTouchMode(true);
                    input_comment.requestFocus();
                    InputMethodManager imm = (InputMethodManager) input_comment.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(input_comment, 0);

                }
            });
            smartRefreshLayout1 = findViewById(R.id.smartRefreshLayout);
            recyclerView1 = findViewById(R.id.recyclerView);
            LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
            layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerView1.setLayoutManager(layoutManager1);
            nodata_linear1 = findViewById(R.id.nodata_linear);
            data_linear1 = findViewById(R.id.data_linear);
            circleImageView = findViewById(R.id.circleImageView);
            praises_image1 = findViewById(R.id.praises_image);
            if(ispraise.equals("0")){

            }else {

            }
            if (avatar.length() > 0) {
                Glide.with(getContext()).load(avatar).into(circleImageView);
            } else {
                circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            TextView t_name, t_content, t_time, t_praises, t_comments, put_comment;
            t_time = findViewById(R.id.time);
            t_name = findViewById(R.id.name);
            t_comments = findViewById(R.id.comments);
            t_content = findViewById(R.id.content);
            t_praises = findViewById(R.id.praises);
            put_comment = findViewById(R.id.put_comment);

            t_content.setText(content);
            t_name.setText(name);
            t_time.setText(time);
            put_comment.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (input_comment.getText().toString().length() > 0) {
                        addPostComment1(post_id, input_comment.getText().toString(), Myid, myto_user_id);
                        int c = Integer.parseInt(comments);
                        c++;
                        t_comments.setText("回复 " + c);
                        input_comment.setText("");
                    } else {
                        ToastUtils.show("");
                    }
                }
            });

            findViewById(R.id.back).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            getChildCommentList(Mypost_id, Myid);
            t_comments.setText("回复 " + comments);
            if (smartRefreshLayout1 != null) {
                smartRefreshLayout1.setEnableLoadMoreWhenContentNotFull(true);
                smartRefreshLayout1.setOnRefreshListener(new OnRefreshListener() {
                    @Override
                    public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                        refreshLayout.getLayout().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                page1 = 1;
                                getChildCommentList(Mypost_id, Myid);
                                refreshLayout.finishRefresh();
                            }
                        }, 500);
                    }
                });
                smartRefreshLayout1.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                        refreshLayout.getLayout().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (page1 < page_total1) {
                                    page1++;
                                    getChildCommentListMore(Mypost_id, Myid, page1);
                                } else {
                                   // ToastUtils.show("已全部加载");
                                }
                                refreshLayout.finishLoadMore();
                            }
                        }, 500);
                    }
                });
            }
        }



        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }

    private void addPraise(String post_id, String id) {
        RequestUtils.addPraise(SharePreUtil.getString(this, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelPraise(String post_id, String id) {
        RequestUtils.cancelPraise(SharePreUtil.getString(this, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {


            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }




    private void addPostComment1(String post_id, String content, String parent_id, String to_user_id) {
        RequestUtils.addPostComment(SharePreUtil.getString(this, "token", ""), post_id, content, parent_id, to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                getChildCommentList(post_id, parent_id);
                ToastUtils.show("评论成功");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void getChildCommentList(String post_id, String parent_id) {
        ComDasilogBeanList.clear();
        RequestUtils.getChildCommentList(SharePreUtil.getString(this, "token", ""), 1, 10, post_id, parent_id, new ListMyObserver<CommListBean<ComDasilogBean>>(this) {
            @Override
            public void onSuccess(CommListBean<ComDasilogBean> result) {
                CommListBean<ComDasilogBean> list = result;
                page_total1 = result.getPage_info().getPage_total();
                if (list.getComment_list().size() > 0) {
                    nodata_linear1.setVisibility(View.GONE);
                    data_linear1.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getComment_list().size(); i++) {
                        String nick_name = list.getComment_list().get(i).getNick_name();
                        String avatar = list.getComment_list().get(i).getAvatar();
                        String id = list.getComment_list().get(i).getId();
                        String post_id = list.getComment_list().get(i).getUser_id();
                        String create_time = list.getComment_list().get(i).getCreate_time();
                        String user_id = list.getComment_list().get(i).getUser_id();
                        String to_user_id = list.getComment_list().get(i).getTo_user_id();
                        String comment_nick_name = list.getComment_list().get(i).getComment_nick_name();
                        String comment_avatar = list.getComment_list().get(i).getComment_avatar();
                        String content = list.getComment_list().get(i).getContent();
                        String comments = list.getComment_list().get(i).getComments();
                        String praises = list.getComment_list().get(i).getPraises();
                        String ispraise = list.getComment_list().get(i).getIspraise();
                        dyBean1 = new ComDasilogBean(nick_name, avatar, id, post_id, create_time, user_id, to_user_id, comment_nick_name, comment_avatar, content, comments, praises, ispraise);
                        ComDasilogBeanList.add(dyBean1);
                    }
                    dailogComAdapter = new DailogComAdapter(ComDasilogBeanList);
                    recyclerView1.setAdapter(dailogComAdapter);

                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void getChildCommentListMore(String post_id, String parent_id, int page1) {
        ComDasilogBeanList.clear();
        RequestUtils.getChildCommentList(SharePreUtil.getString(this, "token", ""), page1, 10, post_id, parent_id, new ListMyObserver<CommListBean<ComDasilogBean>>(this) {
            @Override
            public void onSuccess(CommListBean<ComDasilogBean> result) {
                CommListBean<ComDasilogBean> list = result;
                page_total1 = result.getPage_info().getPage_total();
                if (list.getComment_list().size() > 0) {
                    nodata_linear1.setVisibility(View.GONE);
                    data_linear1.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getComment_list().size(); i++) {
                        String nick_name = list.getComment_list().get(i).getNick_name();
                        String avatar = list.getComment_list().get(i).getAvatar();
                        String id = list.getComment_list().get(i).getId();
                        String post_id = list.getComment_list().get(i).getUser_id();
                        String create_time = list.getComment_list().get(i).getCreate_time();
                        String user_id = list.getComment_list().get(i).getUser_id();
                        String to_user_id = list.getComment_list().get(i).getTo_user_id();
                        String comment_nick_name = list.getComment_list().get(i).getComment_nick_name();
                        String comment_avatar = list.getComment_list().get(i).getComment_avatar();
                        String content = list.getComment_list().get(i).getContent();
                        String comments = list.getComment_list().get(i).getComments();
                        String praises = list.getComment_list().get(i).getPraises();
                        String ispraise = list.getComment_list().get(i).getIspraise();
                        dyBean1 = new ComDasilogBean(nick_name, avatar, id, post_id, create_time, user_id, to_user_id, comment_nick_name, comment_avatar, content, comments, praises, ispraise);
                        ComDasilogBeanList.add(dyBean1);
                    }
                    dailogComAdapter.notifyDataSetChanged();
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


}