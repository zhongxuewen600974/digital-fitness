package com.gzai.zhongjiang.digitalmovement.bean;

public class StudioBean {
    private String id;
    private String card_id;
    private String user_id;
    private String last_time;
    private String truename;
    private String avatar;
    private String times;
    private String totals;
    private String expire_time;
    private String start_time;
    private String end_time;
    private String coach_id;
    public StudioBean(String id,String card_id,String user_id,String last_time,String truename,String avatar,String times,String totals,String expire_time){
        this.id=id;
        this.card_id=card_id;
        this.user_id=user_id;
        this.last_time=last_time;
        this.truename=truename;
        this.avatar=avatar;
        this.times=times;
        this.totals=totals;
        this.expire_time=expire_time;
    }
    public StudioBean(String id,String card_id,String user_id,String last_time,String truename,String avatar,String times,String totals,String expire_time,String start_time,String end_time,String coach_id){
        this.id=id;
        this.card_id=card_id;
        this.user_id=user_id;
        this.last_time=last_time;
        this.truename=truename;
        this.avatar=avatar;
        this.times=times;
        this.totals=totals;
        this.expire_time=expire_time;
        this.start_time=start_time;
        this.end_time=end_time;
        this.coach_id=coach_id;
    }

    public String getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(String coach_id) {
        this.coach_id = coach_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(String expire_time) {
        this.expire_time = expire_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLast_time() {
        return last_time;
    }

    public void setLast_time(String last_time) {
        this.last_time = last_time;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getTotals() {
        return totals;
    }

    public void setTotals(String totals) {
        this.totals = totals;
    }
}
