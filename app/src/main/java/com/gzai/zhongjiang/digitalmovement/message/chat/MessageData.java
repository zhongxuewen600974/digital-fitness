package com.gzai.zhongjiang.digitalmovement.message.chat;

public class MessageData {
    private String token;
    private String action;
    SndMsgBean data;

    public MessageData(String action, String token, SndMsgBean data) {
        this.action = action;
        this.token = token;
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public SndMsgBean getData() {
        return data;
    }

    public void setData(SndMsgBean data) {
        this.data = data;
    }
}
