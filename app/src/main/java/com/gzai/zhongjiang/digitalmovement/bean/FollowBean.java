package com.gzai.zhongjiang.digitalmovement.bean;

public class FollowBean {
    private String nick_name;
    private String avatar;
    private String user_id;
    private String ismutual;
    private String create_time;
    public FollowBean(String nick_name,String avatar,String user_id,String ismutual,String create_time){
        this.nick_name=nick_name;
        this.avatar=avatar;
        this.user_id=user_id;
        this.ismutual=ismutual;
        this.create_time=create_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getIsmutual() {
        return ismutual;
    }

    public void setIsmutual(String ismutual) {
        this.ismutual = ismutual;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
