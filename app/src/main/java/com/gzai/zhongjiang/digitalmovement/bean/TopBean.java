package com.gzai.zhongjiang.digitalmovement.bean;

public class TopBean {
    private String id;
    private String title;
    private String create_time;
    public TopBean(String id,String title,String create_time){
        this.id=id;
        this.title=title;
        this.create_time=create_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
