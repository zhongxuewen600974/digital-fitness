package com.gzai.zhongjiang.digitalmovement.bean;

public class HealthInfoBean {
    private String id;
    private String user_id;
    private String weight;
    private String height;
    private String bmi;
    private String fat_rate;
    private String fat;
    private String metabolic_rate;
    private String whr_rate;
    private String humidity_rate;
    private String muscle_rate;
    private String upperarm_left;
    private String upperarm_right;
    private String hipline;
    private String bust;
    private String waist;
    private String thigh_left;
    private String thigh_right;
    private String shank_left;
    private String shank_right;
    private String update_time;
    private String create_time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getFat_rate() {
        return fat_rate;
    }

    public void setFat_rate(String fat_rate) {
        this.fat_rate = fat_rate;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getMetabolic_rate() {
        return metabolic_rate;
    }

    public void setMetabolic_rate(String metabolic_rate) {
        this.metabolic_rate = metabolic_rate;
    }

    public String getWhr_rate() {
        return whr_rate;
    }

    public void setWhr_rate(String whr_rate) {
        this.whr_rate = whr_rate;
    }

    public String getHumidity_rate() {
        return humidity_rate;
    }

    public void setHumidity_rate(String humidity_rate) {
        this.humidity_rate = humidity_rate;
    }

    public String getMuscle_rate() {
        return muscle_rate;
    }

    public void setMuscle_rate(String muscle_rate) {
        this.muscle_rate = muscle_rate;
    }

    public String getUpperarm_left() {
        return upperarm_left;
    }

    public void setUpperarm_left(String upperarm_left) {
        this.upperarm_left = upperarm_left;
    }

    public String getUpperarm_right() {
        return upperarm_right;
    }

    public void setUpperarm_right(String upperarm_right) {
        this.upperarm_right = upperarm_right;
    }

    public String getHipline() {
        return hipline;
    }

    public void setHipline(String hipline) {
        this.hipline = hipline;
    }

    public String getBust() {
        return bust;
    }

    public void setBust(String bust) {
        this.bust = bust;
    }

    public String getWaist() {
        return waist;
    }

    public void setWaist(String waist) {
        this.waist = waist;
    }

    public String getThigh_left() {
        return thigh_left;
    }

    public void setThigh_left(String thigh_left) {
        this.thigh_left = thigh_left;
    }

    public String getThigh_right() {
        return thigh_right;
    }

    public void setThigh_right(String thigh_right) {
        this.thigh_right = thigh_right;
    }

    public String getShank_left() {
        return shank_left;
    }

    public void setShank_left(String shank_left) {
        this.shank_left = shank_left;
    }

    public String getShank_right() {
        return shank_right;
    }

    public void setShank_right(String shank_right) {
        this.shank_right = shank_right;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
