package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.gzai.zhongjiang.digitalmovement.view.wetchimage.JZCImageUtil;

import java.util.List;

public class GoodAdapter extends RecyclerView.Adapter<GoodAdapter.ViewHolder> {
    private Context mContext;
    private List<String> dataBean;
    private int number = 0;

    public GoodAdapter(List<String> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_good_at, parent, false);


        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.good_at.setText(dataBean.get(position).toString());

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView good_at;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            good_at = (TextView) itemView.findViewById(R.id.good_at);
        }
    }

}
