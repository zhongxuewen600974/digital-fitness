package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipInfoBean;
import com.gzai.zhongjiang.digitalmovement.bean.VipInfoBean;
import com.gzai.zhongjiang.digitalmovement.my.PersonTActivity;
import com.gzai.zhongjiang.digitalmovement.my.PersonalTrainerActivity;
import com.gzai.zhongjiang.digitalmovement.util.qrcode.ActivityScanerCode;
import com.youth.banner.adapter.BannerAdapter;
import com.youth.banner.util.BannerUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Personaladpter extends BannerAdapter<PersonalVipInfoBean, ImageHolder> {
    public Personaladpter(List<PersonalVipInfoBean> list) {
        super(list);
    }


    @Override
    public ImageHolder onCreateHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(BannerUtils.getView(parent, R.layout.item_personal));
    }

    @Override
    public void onBindView(ImageHolder holder, PersonalVipInfoBean data, int position, int size) {
        Thread downLoadRxRoundPdThread = new Thread();
        int mRxRoundProgress;
        holder.room_name.setText(data.getRoom_name() + "私教卡");
        holder.truename.setText("教练: " + data.getCoach_name());
        holder.courses.setText("剩余课程：" + (data.getTotals() - data.getTimes()) + "节"+"（总共"+data.getTotals()+"节)");
        mRxRoundProgress = data.getTimes();
        holder.rxRoundProgressBar.setMax(data.getTotals());
        Handler mRxRoundPdHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(msg.what == 101){
                    holder.rxRoundProgressBar.setProgress(mRxRoundProgress);
                }
            }
        };
        Thread finalDownLoadRxRoundPdThread = downLoadRxRoundPdThread;
        downLoadRxRoundPdThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (!finalDownLoadRxRoundPdThread.isInterrupted()) {
                        while (mRxRoundProgress < data.getTotals()) {
                            if (mRxRoundProgress < data.getTotals()) {
                                Message message = new Message();
                                message.what = 101;
                                mRxRoundPdHandler.sendMessage(message);
                            }
                            Thread.sleep(100);
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        downLoadRxRoundPdThread.start();

    }

}
