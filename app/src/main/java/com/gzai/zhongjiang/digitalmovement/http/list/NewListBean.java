package com.gzai.zhongjiang.digitalmovement.http.list;

import com.gzai.zhongjiang.digitalmovement.bean.PageInfo;

import java.util.List;

public class NewListBean<T> {
    private List<T> list;
    public List<T> getList() {
        return list;
    }
    public void setList(List<T> list) {
        this.list = list;
    }
}
