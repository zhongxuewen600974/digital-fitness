package com.gzai.zhongjiang.digitalmovement.home;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.ArrayRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.MyDyAdapter;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.flowlayout.FlowTagAdapter;
import com.gzai.zhongjiang.digitalmovement.util.flowlayout.FlowTagLayout;
import com.gzai.zhongjiang.digitalmovement.view.DialogOpen;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.impl.FullScreenPopupView;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class HotCircleFragment extends Fragment implements DialogOpen.OnBottomMenuItemClickListener{
    RecyclerView recyclerView;
    SmartRefreshLayout smartRefreshLayout;
    private int page_total, page = 1;
    LinearLayout onData, haveData;
    DyBean dyBean;
    List<DyBean> beanList = new ArrayList<>();
    MyDyAdapter myAdapter;
    private DialogOpen bottomDialog;
    String circle_id,report_id;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        circle_id= ((CircleDetailctivity) activity).getCircle_id();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_hot_circle, container, false);
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        recyclerView = view.findViewById(R.id.recyclerView);
        onData = view.findViewById(R.id.nodata_linear);
        haveData = view.findViewById(R.id.data_linear);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        smartRefreshLayout = view.findViewById(R.id.smartRefreshLayout);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page=1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                               // ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
        bottomDialog = new DialogOpen(getContext(), R.layout.dailog_report, new int[]{R.id.report, R.id.diamiss});
        bottomDialog.setOnBottomMenuItemClickListener(this);
        return view;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
        switch (messageEvent.getMessageType()) {
            case "refresh":
                intView();
                break;
        }
    }
    private void intView() {
        beanList.clear();
        RequestUtils.getPostList_cir(SharePreUtil.getString(getContext(), "token", ""), 1, 10, "circle",circle_id,"praisedesc", new ListMyObserver<ListBean<DyBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<DyBean> result) {
                ListBean<DyBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String isfollow = list.getList().get(i).getIsfollow();
                            onData.setVisibility(View.GONE);
                            haveData.setVisibility(View.VISIBLE);
                            String id = list.getList().get(i).getId();
                            String user_id = list.getList().get(i).getUser_id();
                            String circle_id = list.getList().get(i).getCircle_id();
                            String type = list.getList().get(i).getType();
                            String title = list.getList().get(i).getTitle();
                            String content = list.getList().get(i).getContent();
                            String label_id = list.getList().get(i).getLabel_id();
                            String dy_sync = list.getList().get(i).getDy_sync();
                            String is_top = list.getList().get(i).getIs_top();
                            String praises = list.getList().get(i).getPraises();
                            String shares = list.getList().get(i).getShares();
                            String comments = list.getList().get(i).getComments();
                            String status = list.getList().get(i).getStatus();
                            String update_time = list.getList().get(i).getUpdate_time();
                            String create_time = list.getList().get(i).getCreate_time();
                            String nick_name = list.getList().get(i).getNick_name();
                            String avatar = list.getList().get(i).getAvatar();
                            String sex = list.getList().get(i).getSex();
                            String circle_name = list.getList().get(i).getCircle_name();
                            String label_name = list.getList().get(i).getLabel_name();
                            List image_list = list.getList().get(i).getImage_list();
                        String ispraise = list.getList().get(i).getIspraise();
                        dyBean = new DyBean(id, user_id, circle_id, type, title, content, label_id, dy_sync, is_top, praises, shares, comments, status, update_time, create_time,
                                nick_name, avatar, sex, circle_name, label_name, isfollow, ispraise,image_list);
                            beanList.add(dyBean);

                    }
                    myAdapter = new MyDyAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                    myAdapter.setOnItemClickListener(new MyDyAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClickListener(String id) {
                            report_id=id;
                            bottomDialog.show();
                        }

                        @Override
                        public void OnItemClickListener1(int i, int position, int id) {

                        }

                        @Override
                        public void OnItemClickListener2(int position, int id, int num) {

                        }

                        @Override
                        public void OnItemClickListener3(int position, int id) {

                        }

                        @Override
                        public void OnItemClickListener4(int position, int id) {

                        }
                    });
                } else {
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }






    private void loadMore(int page) {
        RequestUtils.getPostList_cir(SharePreUtil.getString(getContext(), "token", ""), page, 10, "circle",circle_id,"praisedesc", new ListMyObserver<ListBean<DyBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<DyBean> result) {
                ListBean<DyBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String isfollow = list.getList().get(i).getIsfollow();

                            String id = list.getList().get(i).getId();
                            String user_id = list.getList().get(i).getUser_id();
                            String circle_id = list.getList().get(i).getCircle_id();
                            String type = list.getList().get(i).getType();
                            String title = list.getList().get(i).getTitle();
                            String content = list.getList().get(i).getContent();
                            String label_id = list.getList().get(i).getLabel_id();
                            String dy_sync = list.getList().get(i).getDy_sync();
                            String is_top = list.getList().get(i).getIs_top();
                            String praises = list.getList().get(i).getPraises();
                            String shares = list.getList().get(i).getShares();
                            String comments = list.getList().get(i).getComments();
                            String status = list.getList().get(i).getStatus();
                            String update_time = list.getList().get(i).getUpdate_time();
                            String create_time = list.getList().get(i).getCreate_time();
                            String nick_name = list.getList().get(i).getNick_name();
                            String avatar = list.getList().get(i).getAvatar();
                            String sex = list.getList().get(i).getSex();
                            String circle_name = list.getList().get(i).getCircle_name();
                            String label_name = list.getList().get(i).getLabel_name();

                            List image_list = list.getList().get(i).getImage_list();
                        String ispraise = list.getList().get(i).getIspraise();
                        dyBean = new DyBean(id, user_id, circle_id, type, title, content, label_id, dy_sync, is_top, praises, shares, comments, status, update_time, create_time,
                                nick_name, avatar, sex, circle_name, label_name, isfollow, ispraise,image_list);
                            beanList.add(dyBean);
                            myAdapter.notifyDataSetChanged();
                        }
                    }

            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    @Override
    public void onBottomMenuItemClick(DialogOpen dialog, View view) {
        switch (view.getId()) {
            case R.id.report:
                bottomDialog.dismiss();
                ShowDailg();
                break;
            case R.id.diamiss:
                bottomDialog.dismiss();
                break;
        }
    }
    private String getSelectedText(FlowTagLayout parent, List<Integer> selectedList) {
        StringBuilder sb = new StringBuilder("举报类型：\n");
        for (int index : selectedList) {
            sb.append(parent.getAdapter().getItem(index));
            sb.append(";");
        }
        return sb.toString();
    }
    public String[] getStringArray(@ArrayRes int resId) {
        return    getResources().getStringArray(resId);
    }
    private void ShowDailg() {
        new XPopup.Builder(getContext())
                .hasStatusBarShadow(true)
                .autoOpenSoftInput(true)
                .asCustom(new ToCommentPopup(getContext()))
                .show();
    }
    public class ToCommentPopup extends FullScreenPopupView {
        public ToCommentPopup(@NonNull Context context) {
            super(context);
        }
        String repot_type="";
        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_to_repot;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            EditText input_comm;
            TextView number;
            FlowTagLayout mMultiFlowTagLayout;
            mMultiFlowTagLayout=findViewById(R.id.flowlayout_multi_select);
            FlowTagAdapter tagAdapter = new FlowTagAdapter(getContext());
            mMultiFlowTagLayout.setAdapter(tagAdapter);
            mMultiFlowTagLayout.setTagCheckedMode(FlowTagLayout.FLOW_TAG_CHECKED_MULTI);
            mMultiFlowTagLayout.setOnTagSelectListener(new FlowTagLayout.OnTagSelectListener() {
                @Override
                public void onItemSelect(FlowTagLayout parent, int position, List<Integer> selectedList) {
                    repot_type= getSelectedText(parent, selectedList);
                }
            });
            tagAdapter.addTags(getStringArray(R.array.tags_values));
            number = findViewById(R.id.comment_number);
            input_comm = findViewById(R.id.input_repot);
            input_comm.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    number.setText(s.length() + "/" + "240");
                    if (s.length() >= 240) {
                        number.setTextColor(Color.parseColor("#E91E63"));
                    } else {
                        number.setTextColor(Color.parseColor("#333333"));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (repot_type.length() > 0) {
                        accusation(report_id, input_comm.getText().toString(),repot_type);
                        dismiss();
                    } else {
                        ToastUtils.show("请选择举报内容");
                    }
                }
            });

            findViewById(R.id.back).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();
        }

        @Override
        protected void onDismiss() {
            super.onDismiss();
        }
    }

    private void accusation(String content_id, String memo,String repot_type) {
        RequestUtils.accusation(SharePreUtil.getString(getContext(), "token", ""), "", content_id, "post",repot_type, memo, new NollDataMyObserver<NullData>(getContext()) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("举报成功");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);

            }
        });
    }
}