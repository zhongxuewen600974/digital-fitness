package com.gzai.zhongjiang.digitalmovement.my;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.BeFollowAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.FollowAdapter;
import com.gzai.zhongjiang.digitalmovement.bean.FollowBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;


public class FansFragment extends Fragment {
    RecyclerView recyclerView;
    SmartRefreshLayout smartRefreshLayout;
    private int page_total, page = 1;
    LinearLayout onData, haveData;
    FollowBean dyBean;
    List<FollowBean> beanList = new ArrayList<>();
    BeFollowAdapter myAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_fans, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        onData = view.findViewById(R.id.nodata_linear);
        haveData = view.findViewById(R.id.data_linear);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        smartRefreshLayout = view.findViewById(R.id.smartRefreshLayout);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page=1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                               // ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
        return view;
    }
    private void intView() {
        beanList.clear();
        RequestUtils.getFollowList(SharePreUtil.getString(getContext(), "token", ""), 1, 10,"befollow", new ListMyObserver<ListBean<FollowBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<FollowBean> result) {
                ListBean<FollowBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String user_id = list.getList().get(i).getUser_id();
                        String ismutual = list.getList().get(i).getIsmutual();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new FollowBean(nick_name,avatar,user_id,ismutual,create_time);
                        beanList.add(dyBean);

                    }
                    myAdapter = new BeFollowAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(int page) {
        RequestUtils.getFollowList(SharePreUtil.getString(getContext(), "token", ""), page, 10,"befollow", new ListMyObserver<ListBean<FollowBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<FollowBean> result) {
                ListBean<FollowBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String user_id = list.getList().get(i).getUser_id();
                        String ismutual = list.getList().get(i).getIsmutual();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new FollowBean(nick_name,avatar,user_id,ismutual,create_time);
                        beanList.add(dyBean);

                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}