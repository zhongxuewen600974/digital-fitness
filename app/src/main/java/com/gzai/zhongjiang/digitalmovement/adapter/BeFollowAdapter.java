package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
 import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.FollowBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.util.List;

public class BeFollowAdapter extends RecyclerView.Adapter<BeFollowAdapter.ViewHolder> {
    private Context mContext;
    private List<FollowBean> dataBean;

    public BeFollowAdapter(List<FollowBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_befollow, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            if (dataBean.get(position).getIsmutual().equals("0")) {
                holder.linearLayout.setBackgroundResource(R.drawable.textview_1);
                holder.imageView.setVisibility(View.VISIBLE);
                holder.isfollow.setText("关注");
                holder.isfollow.setTextColor(Color.parseColor("#FFFFFFFF"));
            } else {
                holder.linearLayout.setBackgroundResource(R.drawable.textview_2);
                holder.imageView.setVisibility(View.GONE);
                holder.isfollow.setTextColor(Color.parseColor("#999999"));
                holder.isfollow.setText("相互关注");
            }
            holder.name.setText(dataBean.get(position).getNick_name());
        } catch (Exception e) {
        }
        holder.isfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFollow(dataBean.get(position).getUser_id());
                dataBean.get(position).setIsmutual("1");
                notifyItemChanged(position);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });
    }


    private void addFollow(String to_user_id) {
        RequestUtils.addFollow(SharePreUtil.getString(mContext, "token", ""), to_user_id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }




    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, isfollow;
        LinearLayout linearLayout;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.isfollow_linear);
            imageView = (ImageView) itemView.findViewById(R.id.isfollow_icon);
            isfollow = (TextView) itemView.findViewById(R.id.isfollow);
        }
    }

}
