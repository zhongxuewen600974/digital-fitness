package com.gzai.zhongjiang.digitalmovement.bean;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class DyBean {
    private String id;
    private String user_id;
    private String circle_id;
    private String type;
    private String title;
    private String content;
    private String label_id;
    private String dy_sync;
    private String is_top;
    private String praises;
    private String shares;
    private String comments;
    private String status;
    private String update_time;
    private String create_time;
    private String nick_name;
    private String avatar;
    private String sex;
    private String circle_name;
    private String label_name;
    private String isfollow;
    private String ispraise;

    public String getIspraise() {
        return ispraise;
    }

    public void setIspraise(String ispraise) {
        this.ispraise = ispraise;
    }

    List<String> image_list;

    public String getIsfollow() {
        return isfollow;
    }

    public String getShares() {
        return shares;
    }

    public void setShares(String shares) {
        this.shares = shares;
    }

    public void setIsfollow(String isfollow) {
        this.isfollow = isfollow;
    }

    public DyBean(){}

    public DyBean(String id, String user_id, String circle_id, String type, String title, String content,
                  String label_id, String dy_sync, String is_top, String praises, String shares,String comments,
                  String status, String update_time, String create_time, String nick_name, String avatar,
                  String sex, String circle_name, String label_name, String isfollow, String ispraise,List<String> image_list ){
        this.id=id;
        this.user_id=user_id;
        this.circle_id=circle_id;
        this.type=type;
        this.title=title;
        this.content=content;
        this.label_id=label_id;
        this.dy_sync=dy_sync;
        this.is_top=is_top;
        this.praises=praises;
        this.shares=shares;
        this.comments=comments;
        this.status=status;
        this.update_time=update_time;
        this.create_time=create_time;
        this.nick_name=nick_name;
        this.avatar=avatar;
        this.sex=sex;
        this.circle_name=circle_name;
        this.label_name=label_name;
        this.isfollow=isfollow;
        this.ispraise=ispraise;
        this.image_list=image_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCircle_id() {
        return circle_id;
    }

    public void setCircle_id(String circle_id) {
        this.circle_id = circle_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLabel_id() {
        return label_id;
    }

    public void setLabel_id(String label_id) {
        this.label_id = label_id;
    }

    public String getDy_sync() {
        return dy_sync;
    }

    public void setDy_sync(String dy_sync) {
        this.dy_sync = dy_sync;
    }

    public String getIs_top() {
        return is_top;
    }

    public void setIs_top(String is_top) {
        this.is_top = is_top;
    }

    public String getPraises() {
        return praises;
    }

    public void setPraises(String praises) {
        this.praises = praises;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCircle_name() {
        return circle_name;
    }

    public void setCircle_name(String circle_name) {
        this.circle_name = circle_name;
    }

    public String getLabel_name() {
        return label_name;
    }

    public void setLabel_name(String label_name) {
        this.label_name = label_name;
    }

    public List<String> getImage_list() {
        return image_list;
    }

    public void setImage_list(List<String> image_list) {
        this.image_list = image_list;
    }





}
