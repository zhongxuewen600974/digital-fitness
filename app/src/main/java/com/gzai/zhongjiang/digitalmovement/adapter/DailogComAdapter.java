package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.ComDasilogBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DailogComAdapter extends RecyclerView.Adapter<DailogComAdapter.ViewHolder> {
    private Context mContext;
    private List<ComDasilogBean> dataBean;


    public DailogComAdapter(List<ComDasilogBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dailog_com, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            holder.name.setText(dataBean.get(position).getNick_name());
            holder.content.setText(dataBean.get(position).getContent());
            holder.time.setText(stampToDate(dataBean.get(position).getCreate_time()));
            holder.praises.setText(dataBean.get(position).getPraises());
            if (dataBean.get(position).getIspraise().equals("0")) {
                holder.imageView.setImageResource(R.drawable.dianzan_fase);
            } else {
                holder.imageView.setImageResource(R.drawable.home_dianzan);
            }
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dataBean.get(position).getIspraise().equals("0")) {
                        addPraise(dataBean.get(position).getPost_id(),dataBean.get(position).getId());
                        dataBean.get(position).setIspraise("1");
                        dataBean.get(position).setPraises(Integer.parseInt(dataBean.get(position).getPraises()) + 1 + "");
                        notifyItemChanged(position);
                    } else {
                        cancelPraise(dataBean.get(position).getPost_id(),dataBean.get(position).getId());
                        dataBean.get(position).setIspraise("0");
                        dataBean.get(position).setPraises(Integer.parseInt(dataBean.get(position).getPraises()) - 1 + "");
                        notifyItemChanged(position);
                    }
                }
            });

        } catch (Exception e) {
        }

    }

    private void addPraise(String post_id,String id) {
        RequestUtils.addPraise(SharePreUtil.getString(mContext, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelPraise(String post_id,String id) {
        RequestUtils.cancelPraise(SharePreUtil.getString(mContext, "token", ""), post_id, "comment", id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, content, time, praises;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.time);
            imageView = (ImageView) itemView.findViewById(R.id.praises_image);
            content = (TextView) itemView.findViewById(R.id.content);
            praises = (TextView) itemView.findViewById(R.id.praises);
        }
    }

}
