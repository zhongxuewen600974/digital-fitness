package com.gzai.zhongjiang.digitalmovement.gym;


import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CoachDetailsActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.imageView)
    NineGridImageView imageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.good_at)
    TextView good_at;
    @BindView(R.id.room_name)
    TextView room_name;
    @BindView(R.id.intro)
    TextView intro;



    String user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coach_details);
        ButterKnife.bind(this);
        actionBarView.setTitle("教练");
        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");
        intView(user_id);
    }

    private void intView(String user_id) {
        RequestUtils.getCoachInfo(SharePreUtil.getString(this,"token",""),user_id, new MyObserver<CoachDetailInfo>(this) {
            @Override
            public void onSuccess(CoachDetailInfo result) {
                try {

                    if (result.getInfo().getAvatar().length() > 0) {
                        Glide.with(getBaseContext()).load(result.getInfo().getAvatar()).into(imageView);
                    } else {
                        imageView.setImageResource(R.drawable.mine_head_icon);
                    }

                    name.setText(result.getInfo().getTruename());
                    price.setText("¥" + result.getInfo().getPrice() + "/节");
                    good_at.setText("擅长:" + result.getInfo().getGood_at());
                    room_name.setText("归属:" +result.getInfo().getRoom_name());
                    intro.setText(Html.fromHtml(result.getInfo().getResume()));
                }catch (Exception e){

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }
}