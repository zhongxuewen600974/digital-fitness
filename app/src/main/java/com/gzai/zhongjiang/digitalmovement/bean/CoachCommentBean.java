package com.gzai.zhongjiang.digitalmovement.bean;

public class CoachCommentBean {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
