package com.gzai.zhongjiang.digitalmovement.coach;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.ClassAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderListBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClassRecordActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    private int page_total, page = 1;

    ScanOrderListBean dyBean;
    List<ScanOrderListBean> beanList = new ArrayList<>();
    ClassAdapter myAdapter;
    String user_id,card_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_record);
        ButterKnife.bind(this);
        actionBarView.setTitle("消课记录");
        try {
            Intent intent=getIntent();
            user_id=intent.getStringExtra("user_id");
            card_id=intent.getStringExtra("card_id");
        }catch (Exception e){

        }
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            getScanOrderList(card_id);
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        getScanOrderList(card_id);
    }

    private void getScanOrderList(String card_id) {
        beanList.clear();
        RequestUtils.getScanOrderList(SharePreUtil.getString(this, "token", ""), 1, 10,user_id,card_id, new ListMyObserver<ListBean<ScanOrderListBean>>(this) {
            @Override
            public void onSuccess(ListBean<ScanOrderListBean> result) {
                ListBean<ScanOrderListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String coach_name = list.getList().get(i).getCoach_name();
                        String order_id = list.getList().get(i).getOrder_id();
                        String truename = list.getList().get(i).getTruename();
                        String status = list.getList().get(i).getStatus();
                        String is_comment = list.getList().get(i).getIs_comment();
                        String record_date = list.getList().get(i).getRecord_date();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new ScanOrderListBean(id,user_id,coach_id, coach_name, order_id,truename,status, is_comment, record_date,update_time,create_time);
                        beanList.add(dyBean);
                    }
                    myAdapter = new ClassAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(int page) {
        RequestUtils.getScanOrderList(SharePreUtil.getString(this, "token", ""), page, 10,user_id,card_id, new ListMyObserver<ListBean<ScanOrderListBean>>(this) {
            @Override
            public void onSuccess(ListBean<ScanOrderListBean> result) {
                ListBean<ScanOrderListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String coach_name = list.getList().get(i).getCoach_name();
                        String order_id = list.getList().get(i).getOrder_id();
                        String truename = list.getList().get(i).getTruename();
                        String status = list.getList().get(i).getStatus();
                        String is_comment = list.getList().get(i).getIs_comment();
                        String record_date = list.getList().get(i).getRecord_date();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new ScanOrderListBean(id,user_id,coach_id, coach_name, order_id,truename,status, is_comment, record_date,update_time,create_time);
                        beanList.add(dyBean);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


}