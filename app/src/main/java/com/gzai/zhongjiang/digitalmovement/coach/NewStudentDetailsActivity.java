package com.gzai.zhongjiang.digitalmovement.coach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.CountListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.message.chat.ChatActivity;
import com.gzai.zhongjiang.digitalmovement.my.PersonalTrainerActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.interfaces.SimpleCallback;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewStudentDetailsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.to_catch)
    LinearLayout to_catch;
    @BindView(R.id.to_phone)
    LinearLayout to_phone;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.appointment)
    TextView appointment;
    @BindView(R.id.cancel)
    TextView cancel;
    @BindView(R.id.stutas)
    TextView stutas;

    ImageView image_slot_1, image_slot_2, image_slot_3, image_slot_4, image_slot_5, image_slot_6,
            image_slot_7, image_slot_8, image_slot_9, image_slot_10, image_slot_11, image_slot_12, image_slot_13;
    TextView time_slot_1, time_slot_2, time_slot_3, time_slot_4, time_slot_5, time_slot_6,
            time_slot_7, time_slot_8, time_slot_9, time_slot_10, time_slot_11, time_slot_12, time_slot_13;
    TextView d_week_1, d_week_2, d_week_3, d_week_4, d_week_5, d_week_6, d_week_7;

    String user_id, phone, start_time, end_time, coach_id, id, mystatus;

    private String today = getLastDayOfWeek(0);
    private String newtoday = getLastDayOfWeek(0);
    int cb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_student_details);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        actionBarView.setTitle("学员详情");
        try {
            Intent intent = getIntent();
            user_id = intent.getStringExtra("user_id");
            start_time = intent.getStringExtra("start_time");
            end_time = intent.getStringExtra("end_time");
            coach_id = intent.getStringExtra("coach_id");
            id = intent.getStringExtra("id");
            if (start_time.length() > 0) {
                if (end_time.substring(11).equals("00:00:00")) {
                    time.setText(start_time + "-" + "24:00:00");
                } else {
                    time.setText(start_time + "-" + end_time.substring(11));
                }
            } else {
                time.setText("请选择时间");
            }
            intView(user_id);
            mystatus = intent.getStringExtra("status");
            stutas.setText(mystatus);
        } catch (Exception e) {

        }
        to_catch.setOnClickListener(this);
        to_phone.setOnClickListener(this);
        appointment.setOnClickListener(this);
        cancel.setOnClickListener(this);
        time.setOnClickListener(this);
        circleImageView.setOnClickListener(this);

    }

    public static Date stringToDate2(String time) throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = format.parse(time);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(date);
        Date newDate = formatter.parse(dateString);
        System.out.print(newDate);
        return newDate;
    }

    public static String getWeek(Date date) {
        String[] weeks = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int week_index = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (week_index < 0) {
            week_index = 0;
        }
        return weeks[week_index];
    }


    private void intView(String user_id) {
        RequestUtils.getOtherUserInfo(SharePreUtil.getString(this, "token", ""), user_id, new MyObserver<MyUserInfo>(this) {
            @Override
            public void onSuccess(MyUserInfo result) {
                try {
                    if (result.getAvatar().length() > 0) {
                        Glide.with(NewStudentDetailsActivity.this).load(result.getAvatar()).into(circleImageView);
                    } else {
                        circleImageView.setImageResource(R.drawable.mine_head_icon);
                    }
                    name.setText(result.getTruename());
                    phone = result.getMobile();
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.circleImageView:
                Intent Stingintent = new Intent(NewStudentDetailsActivity.this, OthersPageActivity.class);
                Stingintent.putExtra("user_id", user_id);
                startActivity(Stingintent);
                break;
            case R.id.to_catch:
                Intent intent = new Intent(NewStudentDetailsActivity.this, ChatActivity.class);
                intent.putExtra("send_name", name.getText().toString());
                intent.putExtra("send_id", user_id);
                startActivity(intent);
                break;
            case R.id.to_phone:
                Intent intent1 = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + phone);
                intent1.setData(data);
                startActivity(intent1);
                break;
            case R.id.cancel:
                if (mystatus.equals("预定中")) {
                    finish();
                } else {
                    showsexShadow1();
                }
                break;
            case R.id.appointment:
                if (start_time.length() > 0) {
                    showsexShadow();
                } else {
                    ToastUtils.show("请选择时间");
                }
                break;
            case R.id.time:
                showPartShadow();
                break;
        }
    }

    SexPopupView1 sexpopupView1;

    private void showsexShadow1() {
        if (sexpopupView1 == null) {
            sexpopupView1 = (SexPopupView1) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new SexPopupView1(this));
        }
        sexpopupView1.show();
    }

    public class SexPopupView1 extends CenterPopupView {


        public SexPopupView1(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_cancelsubcourse;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    setcancelSubCourse();
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }

    private void setcancelSubCourse() {
        RequestUtils.setSubCourse(SharePreUtil.getString(this, "token", ""), id, start_time, end_time, "2", new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                EventBus.getDefault().post(new MessageEventBus("refresh_BookingManagemen", ""));
                ToastUtils.show("取消成功");
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    SexPopupView sexpopupView;

    private void showsexShadow() {
        if (sexpopupView == null) {
            sexpopupView = (SexPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new SexPopupView(this));
        }
        sexpopupView.show();
    }

    public class SexPopupView extends CenterPopupView {


        public SexPopupView(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_addsubcourse;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mystatus.equals("预定中")) {
                        addSubCourse();
                    } else {
                        setSubCourse();
                    }
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }

    private void setSubCourse() {
        RequestUtils.setSubCourse(SharePreUtil.getString(this, "token", ""), id, start_time, end_time, "1", new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                EventBus.getDefault().post(new MessageEventBus("refresh_BookingManagemen", ""));
                ToastUtils.show("预约成功");
                mystatus = "已确认";
                stutas.setText("已确认");
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    private void addSubCourse() {
        RequestUtils.addSubCourse(SharePreUtil.getString(this, "token", ""), start_time, end_time, user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("预约成功");
                EventBus.getDefault().post(new MessageEventBus("refresh_BookingManagemen", ""));
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
    }


    public static String getLastDayOfWeek(int day) {
        Date dat = null;
        Calendar cd = Calendar.getInstance();
        cd.add(Calendar.DATE, day);
        dat = cd.getTime();
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");
        return dformat.format(dat);
    }


    private String TodayWeek(int time) {
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        int weekday = c.get(Calendar.DAY_OF_WEEK) - 1 + time;
        if (weekday % 7 == 1) {
            return "一";
        } else if (weekday % 7 == 2) {
            return "二";
        } else if (weekday % 7 == 3) {
            return "三";
        } else if (weekday % 7 == 4) {
            return "四";
        } else if (weekday % 7 == 5) {
            return "五";
        } else if (weekday % 7 == 6) {
            return "六";
        } else if (weekday % 7 == 0) {
            return "日";
        }
        return "";
    }


    ShopShareShadowPopupView popupView;

    private void showPartShadow() {
        if (popupView == null) {
            popupView = (ShopShareShadowPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new ShopShareShadowPopupView(this));
        }
        popupView.show();
    }

    public class ShopShareShadowPopupView extends BottomPopupView {
        Boolean select1 = false, select2 = false, select3 = false, select4 = false, select5 = false, select6 = false,
                select7 = false, select8 = false, select9 = false, select10 = false, select11 = false, select12 = false, select13 = false;


        public ShopShareShadowPopupView(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_appointment;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            time_slot_1 = findViewById(R.id.time_slot_1);
            time_slot_2 = findViewById(R.id.time_slot_2);
            time_slot_3 = findViewById(R.id.time_slot_3);
            time_slot_4 = findViewById(R.id.time_slot_4);
            time_slot_5 = findViewById(R.id.time_slot_5);
            time_slot_6 = findViewById(R.id.time_slot_6);
            time_slot_7 = findViewById(R.id.time_slot_7);
            time_slot_8 = findViewById(R.id.time_slot_8);
            time_slot_9 = findViewById(R.id.time_slot_9);
            time_slot_10 = findViewById(R.id.time_slot_10);
            time_slot_11 = findViewById(R.id.time_slot_11);
            time_slot_12 = findViewById(R.id.time_slot_12);
            time_slot_13 = findViewById(R.id.time_slot_13);
            image_slot_1 = findViewById(R.id.image_slot_1);
            image_slot_2 = findViewById(R.id.image_slot_2);
            image_slot_3 = findViewById(R.id.image_slot_3);
            image_slot_4 = findViewById(R.id.image_slot_4);
            image_slot_5 = findViewById(R.id.image_slot_5);
            image_slot_6 = findViewById(R.id.image_slot_6);
            image_slot_7 = findViewById(R.id.image_slot_7);
            image_slot_8 = findViewById(R.id.image_slot_8);
            image_slot_9 = findViewById(R.id.image_slot_9);
            image_slot_10 = findViewById(R.id.image_slot_10);
            image_slot_11 = findViewById(R.id.image_slot_11);
            image_slot_12 = findViewById(R.id.image_slot_12);
            image_slot_13 = findViewById(R.id.image_slot_13);
            d_week_1 = findViewById(R.id.d_week_1);
            d_week_2 = findViewById(R.id.d_week_2);
            d_week_3 = findViewById(R.id.d_week_3);
            d_week_4 = findViewById(R.id.d_week_4);
            d_week_5 = findViewById(R.id.d_week_5);
            d_week_6 = findViewById(R.id.d_week_6);
            d_week_7 = findViewById(R.id.d_week_7);
            d_week_1.setText("今天(周" + TodayWeek(0) + ")");
            d_week_2.setText("明天(周" + TodayWeek(1) + ")");
            d_week_3.setText(getLastDayOfWeek(2).substring(5, 10) + "(周" + TodayWeek(2) + ")");
            d_week_4.setText(getLastDayOfWeek(3).substring(5, 10) + "(周" + TodayWeek(3) + ")");
            d_week_5.setText(getLastDayOfWeek(4).substring(5, 10) + "(周" + TodayWeek(4) + ")");
            d_week_6.setText(getLastDayOfWeek(5).substring(5, 10) + "(周" + TodayWeek(5) + ")");
            d_week_7.setText(getLastDayOfWeek(6).substring(5, 10) + "(周" + TodayWeek(6) + ")");
            d_week_1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    d_week_1.setTextColor(Color.parseColor("#01D66A"));
                    d_week_1.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    today = getLastDayOfWeek(0);
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(0));
                }
            });

            d_week_2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(1);
                    d_week_2.setTextColor(Color.parseColor("#01D66A"));
                    d_week_2.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(1));
                }
            });

            d_week_3.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(2);
                    d_week_3.setTextColor(Color.parseColor("#01D66A"));
                    d_week_3.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(2));
                }
            });

            d_week_4.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(3);
                    d_week_4.setTextColor(Color.parseColor("#01D66A"));
                    d_week_4.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(3));
                }
            });
            d_week_5.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(4);
                    d_week_5.setTextColor(Color.parseColor("#01D66A"));
                    d_week_5.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(4));
                }
            });

            d_week_6.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(5);
                    d_week_6.setTextColor(Color.parseColor("#01D66A"));
                    d_week_6.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_7.setTextColor(Color.parseColor("#111111"));
                    d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(5));
                }
            });

            d_week_7.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    today = getLastDayOfWeek(6);
                    d_week_7.setTextColor(Color.parseColor("#01D66A"));
                    d_week_7.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                    d_week_1.setTextColor(Color.parseColor("#111111"));
                    d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_2.setTextColor(Color.parseColor("#111111"));
                    d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_3.setTextColor(Color.parseColor("#111111"));
                    d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_5.setTextColor(Color.parseColor("#111111"));
                    d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_4.setTextColor(Color.parseColor("#111111"));
                    d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    d_week_6.setTextColor(Color.parseColor("#111111"));
                    d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                    time_slot_1.setSelected(false);
                    time_slot_2.setSelected(false);
                    time_slot_3.setSelected(false);
                    time_slot_4.setSelected(false);
                    time_slot_5.setSelected(false);
                    time_slot_6.setSelected(false);
                    time_slot_7.setSelected(false);
                    time_slot_8.setSelected(false);
                    time_slot_9.setSelected(false);
                    time_slot_10.setSelected(false);
                    time_slot_11.setSelected(false);
                    time_slot_12.setSelected(false);
                    time_slot_13.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    select2 = false;
                    select3 = false;
                    select4 = false;
                    select5 = false;
                    select6 = false;
                    select7 = false;
                    select8 = false;
                    select9 = false;
                    select10 = false;
                    select11 = false;
                    select12 = false;
                    select13 = false;
                    start_time = "";
                    end_time = "";
                    intViewDailog(getLastDayOfWeek(6));
                }
            });

            time_slot_1.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select1) {
                        start_time = "";
                        end_time = "";
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 09:00:00";
                        end_time = today + " 10:00:00";
                        select1 = true;
                        time_slot_1.setSelected(true);
                        time_slot_1.setTextColor(Color.parseColor("#FFFFFFFF"));

                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_2.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select2) {
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 10:00:00";
                        end_time = today + " 11:00:00";
                        select2 = true;
                        time_slot_2.setSelected(true);
                        time_slot_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_3.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select3) {
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 11:00:00";
                        end_time = today + " 12:00:00";
                        select3 = true;
                        time_slot_3.setSelected(true);
                        time_slot_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_4.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select4) {
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select4 = true;
                        time_slot_4.setSelected(true);
                        time_slot_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                        start_time = today + " 14:00:00";
                        end_time = today + " 15:00:00";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_5.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select5) {
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 15:00:00";
                        end_time = today + " 16:00:00";
                        select5 = true;
                        time_slot_5.setSelected(true);
                        time_slot_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_6.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select6) {
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        start_time = "";
                        end_time = "";

                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 16:00:00";
                        end_time = today + " 17:00:00";
                        select6 = true;
                        time_slot_6.setSelected(true);
                        time_slot_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_7.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select7) {
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select7 = true;
                        start_time = today + " 17:00:00";
                        end_time = today + " 18:00:00";
                        time_slot_7.setSelected(true);
                        time_slot_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_8.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select8) {
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select8 = true;
                        start_time = today + " 18:00:00";
                        end_time = today + " 19:00:00";
                        time_slot_8.setSelected(true);
                        time_slot_8.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_9.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select9) {
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select9 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select9 = true;
                        start_time = today + " 19:00:00";
                        end_time = today + " 20:00:00";
                        time_slot_9.setSelected(true);
                        time_slot_9.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_10.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select10) {
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 20:00:00";
                        end_time = today + " 21:00:00";
                        select10 = true;
                        time_slot_10.setSelected(true);
                        time_slot_10.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_11.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select11) {
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select11 = false;
                        start_time = "";
                        end_time = "";
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        start_time = today + " 21:00:00";
                        end_time = today + " 22:00:00";
                        select11 = true;
                        time_slot_11.setSelected(true);
                        time_slot_11.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });
            time_slot_12.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select12) {
                        start_time = "";
                        end_time = "";
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select12 = true;
                        start_time = today + " 22:00:00";
                        end_time = today + " 23:00:00";
                        time_slot_12.setSelected(true);
                        time_slot_12.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    }
                }
            });

            time_slot_13.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (select13) {
                        start_time = "";
                        end_time = "";
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_13.setSelected(false);
                        time_slot_13.setTextColor(Color.parseColor("#111111"));
                        select13 = false;
                    } else {
                        select13 = true;
                        start_time = today + " 23:00:00";
                        end_time = today + " 24:00:00";
                        time_slot_13.setSelected(true);
                        time_slot_13.setTextColor(Color.parseColor("#FFFFFFFF"));
                        time_slot_3.setSelected(false);
                        time_slot_3.setTextColor(Color.parseColor("#111111"));
                        select3 = false;
                        time_slot_2.setSelected(false);
                        time_slot_2.setTextColor(Color.parseColor("#111111"));
                        select2 = false;
                        time_slot_1.setSelected(false);
                        time_slot_1.setTextColor(Color.parseColor("#111111"));
                        select1 = false;
                        time_slot_4.setSelected(false);
                        time_slot_4.setTextColor(Color.parseColor("#111111"));
                        select4 = false;
                        time_slot_5.setSelected(false);
                        time_slot_5.setTextColor(Color.parseColor("#111111"));
                        select5 = false;
                        time_slot_6.setSelected(false);
                        time_slot_6.setTextColor(Color.parseColor("#111111"));
                        select6 = false;
                        time_slot_7.setSelected(false);
                        time_slot_7.setTextColor(Color.parseColor("#111111"));
                        select7 = false;
                        time_slot_8.setSelected(false);
                        time_slot_8.setTextColor(Color.parseColor("#111111"));
                        select8 = false;
                        time_slot_9.setSelected(false);
                        time_slot_9.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_10.setSelected(false);
                        time_slot_10.setTextColor(Color.parseColor("#111111"));
                        select10 = false;
                        time_slot_11.setSelected(false);
                        time_slot_11.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                        time_slot_12.setSelected(false);
                        time_slot_12.setTextColor(Color.parseColor("#111111"));
                        select12 = false;
                    }
                }
            });

            findViewById(R.id.back).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (start_time.length() > 0 && end_time.length() > 0) {

                        time.setText(start_time + "-" + end_time.substring(11));
                    }
                    dismiss();
                }
            });
        }


        @Override
        protected void onShow() {
            super.onShow();
            select1 = false;
            select2 = false;
            select3 = false;
            select4 = false;
            select5 = false;
            select6 = false;
            select7 = false;
            select8 = false;
            select9 = false;
            select10 = false;
            select11 = false;
            select12 = false;
            select13 = false;
            try {
                if (getWeek(stringToDate2(start_time)).equals(d_week_1.getText().toString().substring(d_week_1.getText().toString().length() - 3, d_week_1.getText().toString().length() - 1))) {
                    cb = 1;
                } else if (getWeek(stringToDate2(start_time)).equals(d_week_2.getText().toString().substring(d_week_2.getText().toString().length() - 3, d_week_2.getText().toString().length() - 1))) {
                    cb = 2;
                } else if (getWeek(stringToDate2(start_time)).equals(d_week_3.getText().toString().substring(d_week_3.getText().toString().length() - 3, d_week_3.getText().toString().length() - 1))) {
                    cb = 3;
                } else if (getWeek(stringToDate2(start_time)).equals(d_week_4.getText().toString().substring(d_week_4.getText().toString().length() - 3, d_week_4.getText().toString().length() - 1))) {
                    cb = 4;
                } else if (getWeek(stringToDate2(start_time)).equals(d_week_5.getText().toString().substring(d_week_5.getText().toString().length() - 3, d_week_5.getText().toString().length() - 1))) {
                    cb = 5;
                } else if (getWeek(stringToDate2(start_time)).equals(d_week_6.getText().toString().substring(d_week_6.getText().toString().length() - 3, d_week_6.getText().toString().length() - 1))) {
                    cb = 6;
                } else if (getWeek(stringToDate2(start_time)).equals(d_week_7.getText().toString().substring(d_week_7.getText().toString().length() - 3, d_week_7.getText().toString().length() - 1))) {
                    cb = 7;
                }
                
            } catch (ParseException e) {
                e.printStackTrace();
            }


            if (cb == 1) {
                d_week_1.setTextColor(Color.parseColor("#01D66A"));
                d_week_1.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                today = getLastDayOfWeek(0);
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(0));
            } else if (cb == 2) {
                today = getLastDayOfWeek(1);
                d_week_2.setTextColor(Color.parseColor("#01D66A"));
                d_week_2.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(1));
            } else if (cb == 3) {
                today = getLastDayOfWeek(2);
                d_week_3.setTextColor(Color.parseColor("#01D66A"));
                d_week_3.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(2));
            } else if (cb == 4) {
                today = getLastDayOfWeek(3);
                d_week_4.setTextColor(Color.parseColor("#01D66A"));
                d_week_4.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(3));
            } else if (cb == 5) {
                today = getLastDayOfWeek(4);
                d_week_5.setTextColor(Color.parseColor("#01D66A"));
                d_week_5.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(4));
            } else if (cb == 6) {
                today = getLastDayOfWeek(5);
                d_week_6.setTextColor(Color.parseColor("#01D66A"));
                d_week_6.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(5));
            } else if (cb == 7) {
                today = getLastDayOfWeek(6);
                d_week_7.setTextColor(Color.parseColor("#01D66A"));
                d_week_7.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intViewDailog(getLastDayOfWeek(6));
            }
        }

        @Override
        protected void onDismiss() {
            super.onDismiss();
        }
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    private void intViewDailog(String record_date) {
        image_slot_1.setVisibility(View.GONE);
        time_slot_1.setEnabled(true);
        image_slot_2.setVisibility(View.GONE);
        time_slot_2.setEnabled(true);
        image_slot_3.setVisibility(View.GONE);
        time_slot_3.setEnabled(true);
        image_slot_4.setVisibility(View.GONE);
        time_slot_4.setEnabled(true);
        image_slot_5.setVisibility(View.GONE);
        time_slot_5.setEnabled(true);
        image_slot_6.setVisibility(View.GONE);
        time_slot_6.setEnabled(true);
        image_slot_7.setVisibility(View.GONE);
        time_slot_7.setEnabled(true);
        image_slot_8.setVisibility(View.GONE);
        time_slot_8.setEnabled(true);
        image_slot_9.setVisibility(View.GONE);
        time_slot_9.setEnabled(true);
        image_slot_10.setVisibility(View.GONE);
        time_slot_10.setEnabled(true);
        image_slot_11.setVisibility(View.GONE);
        time_slot_11.setEnabled(true);
        image_slot_12.setVisibility(View.GONE);
        time_slot_12.setEnabled(true);
        image_slot_13.setVisibility(View.GONE);
        time_slot_13.setEnabled(true);
        RequestUtils.getSubCourseList(SharePreUtil.getString(this, "token", ""), record_date, coach_id, "", new ListMyObserver<CountListBean<MyCourse>>(this) {
            @Override
            public void onSuccess(CountListBean<MyCourse> result) {
                CountListBean<MyCourse> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String start_time = list.getList().get(i).getStart_time();
                        if (stampToDate(start_time).equals("09:00")) {
                            image_slot_1.setVisibility(View.VISIBLE);
                            time_slot_1.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("10:00")) {
                            image_slot_2.setVisibility(View.VISIBLE);
                            time_slot_2.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("11:00")) {
                            image_slot_3.setVisibility(View.VISIBLE);
                            time_slot_3.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("14:00")) {
                            image_slot_4.setVisibility(View.VISIBLE);
                            time_slot_4.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("15:00")) {
                            image_slot_5.setVisibility(View.VISIBLE);
                            time_slot_5.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("16:00")) {
                            image_slot_6.setVisibility(View.VISIBLE);
                            time_slot_6.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("17:00")) {
                            image_slot_7.setVisibility(View.VISIBLE);
                            time_slot_7.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("18:00")) {
                            image_slot_8.setVisibility(View.VISIBLE);
                            time_slot_8.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("19:00")) {
                            image_slot_9.setVisibility(View.VISIBLE);
                            time_slot_9.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("20:00")) {
                            image_slot_10.setVisibility(View.VISIBLE);
                            time_slot_10.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("21:00")) {
                            image_slot_11.setVisibility(View.VISIBLE);
                            time_slot_11.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("22:00")) {
                            image_slot_12.setVisibility(View.VISIBLE);
                            time_slot_12.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("23:00")) {
                            image_slot_13.setVisibility(View.VISIBLE);
                            time_slot_13.setEnabled(false);
                        }
                    }
                } else {

                }

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

}