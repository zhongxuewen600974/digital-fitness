package com.gzai.zhongjiang.digitalmovement.bean;

public class NewsListBean {
    private String id;
    private String title;
    private String image;
    private String intro;
    private String update_time;
    private String create_time;
    public NewsListBean(String id,String title,String image,String intro,String update_time,String create_time){
        this.id=id;
        this.title=title;
        this.image=image;
        this.intro=intro;
        this.update_time=update_time;
        this.create_time=create_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
