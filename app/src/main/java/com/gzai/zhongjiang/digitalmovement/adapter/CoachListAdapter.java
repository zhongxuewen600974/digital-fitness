package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CoachListBean;
import com.gzai.zhongjiang.digitalmovement.gym.CoachDetailsActivity;

import java.util.List;

public class CoachListAdapter extends RecyclerView.Adapter<CoachListAdapter.ViewHolder> {
    private Context mContext;
    private List<CoachListBean> dataBean;


    public CoachListAdapter(List<CoachListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_coach, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.icon);
            } else {
                holder.icon.setImageResource(R.drawable.mine_head_icon);
            }

            holder.name.setText(dataBean.get(position).getTruename());
            holder.price.setText("¥" + dataBean.get(position).getPrice() + "/节");
            holder.good_at.setText("擅长:" + dataBean.get(position).getGood_at());
            holder.room_name.setText("归属:" + dataBean.get(position).getRoom_name());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Stingintent = new Intent(mContext, CoachDetailsActivity.class);
                    Stingintent.putExtra("user_id",dataBean.get(position).getUser_id());
                    mContext.startActivity(Stingintent);
                }
            });

        }catch (Exception e){

        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView name, price, good_at, room_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.imageView);
            name = (TextView) itemView.findViewById(R.id.name);
            price = (TextView) itemView.findViewById(R.id.price);
            good_at = (TextView) itemView.findViewById(R.id.good_at);
            room_name = (TextView) itemView.findViewById(R.id.room_name);
        }
    }

}
