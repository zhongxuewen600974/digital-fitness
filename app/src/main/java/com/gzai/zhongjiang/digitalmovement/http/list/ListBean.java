package com.gzai.zhongjiang.digitalmovement.http.list;

import com.gzai.zhongjiang.digitalmovement.bean.PageInfo;

import java.util.List;

public class ListBean<T> {
    private List<T> list;
    PageInfo page_info;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public PageInfo getPage_info() {
        return page_info;
    }

    public void setPage_info(PageInfo page_info) {
        this.page_info = page_info;
    }
}
