package com.gzai.zhongjiang.digitalmovement.bean;

public class CommentListBean {
    private String comment_id;
    private String user_id;
    private String to_user_id;
    private String parent_id;
    private String post_id;
    private String type;
    private String comment_content;
    private String title;
    private String content;
    private String nick_name;
    private String avatar;
    private String create_time;
    CommentInfo comment_info;

    private String post_nick_name;
    private String post_avatar;

    public String getPost_nick_name() {
        return post_nick_name;
    }

    public void setPost_nick_name(String post_nick_name) {
        this.post_nick_name = post_nick_name;
    }

    public String getPost_avatar() {
        return post_avatar;
    }

    public void setPost_avatar(String post_avatar) {
        this.post_avatar = post_avatar;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public CommentListBean(){}

    public CommentListBean(String user_id, String to_user_id, String parent_id, String post_id, String type, String comment_content, String title, String content,
                           String nick_name, String avatar, String create_time) {
        this.user_id = user_id;
        this.to_user_id = to_user_id;
        this.parent_id = parent_id;
        this.post_id = post_id;
        this.type = type;
        this.comment_content = comment_content;
        this.title = title;
        this.content = content;
        this.nick_name = nick_name;
        this.avatar = avatar;
        this.create_time = create_time;
    }
    public CommentListBean(String user_id, String to_user_id, String parent_id, String post_id, String type, String comment_content, String title, String content,
                           String nick_name, String avatar, String create_time, CommentInfo comment_info) {
        this.user_id = user_id;
        this.to_user_id = to_user_id;
        this.parent_id = parent_id;
        this.post_id = post_id;
        this.type = type;
        this.comment_content = comment_content;
        this.title = title;
        this.content = content;
        this.nick_name = nick_name;
        this.avatar = avatar;
        this.create_time = create_time;
        this.comment_info = comment_info;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public CommentInfo getComment_info() {
        return comment_info;
    }

    public void setComment_info(CommentInfo comment_info) {
        this.comment_info = comment_info;
    }
}
