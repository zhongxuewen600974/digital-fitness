package com.gzai.zhongjiang.digitalmovement.bean;

public class CommentInfo {
    private String id;
    private String post_id;
    private String content;
    private String send_name;
    private String send_uid;
    private String recive_uid;
    private String recive_name;
    private String create_time;
    public CommentInfo(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSend_name() {
        return send_name;
    }

    public void setSend_name(String send_name) {
        this.send_name = send_name;
    }

    public String getSend_uid() {
        return send_uid;
    }

    public void setSend_uid(String send_uid) {
        this.send_uid = send_uid;
    }

    public String getRecive_uid() {
        return recive_uid;
    }

    public void setRecive_uid(String recive_uid) {
        this.recive_uid = recive_uid;
    }

    public String getRecive_name() {
        return recive_name;
    }

    public void setRecive_name(String recive_name) {
        this.recive_name = recive_name;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
