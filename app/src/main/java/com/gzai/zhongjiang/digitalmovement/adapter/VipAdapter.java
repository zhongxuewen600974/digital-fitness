package com.gzai.zhongjiang.digitalmovement.adapter;

import android.view.ViewGroup;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.VipInfoBean;
import com.youth.banner.adapter.BannerAdapter;
import com.youth.banner.util.BannerUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class VipAdapter extends BannerAdapter<VipInfoBean, ImageHolder> {


    public VipAdapter(List<VipInfoBean> list) {
        super(list);
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public ImageHolder onCreateHolder(ViewGroup parent, int viewType) {
        return new ImageHolder(BannerUtils.getView(parent, R.layout.item_vipcard));
    }

    @Override
    public void onBindView(ImageHolder holder, VipInfoBean data, int position, int size) {
        holder.room_name.setText(data.getRoom_name() + "健身卡");
        holder.truename.setText("姓名: " + data.getTruename());
        holder.card_id.setText("卡号：" + data.getCard_id());
        holder.expire_time.setText("有效期至：" + stampToDate(data.getExpire_time()));
    }


}
