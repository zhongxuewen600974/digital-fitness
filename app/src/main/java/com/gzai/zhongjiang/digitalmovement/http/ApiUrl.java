package com.gzai.zhongjiang.digitalmovement.http;


import com.gzai.zhongjiang.digitalmovement.bean.CircleInfo;
import com.gzai.zhongjiang.digitalmovement.bean.CircleListBean;
import com.gzai.zhongjiang.digitalmovement.bean.CoachCommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.bean.CoachListBean;
import com.gzai.zhongjiang.digitalmovement.bean.CoachQrcodeBean;
import com.gzai.zhongjiang.digitalmovement.bean.ComDasilogBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommDetailBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommDetailBean1;
import com.gzai.zhongjiang.digitalmovement.bean.CommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommentListBean;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.FaceBean;
import com.gzai.zhongjiang.digitalmovement.bean.FeedBean;
import com.gzai.zhongjiang.digitalmovement.bean.FollowBean;
import com.gzai.zhongjiang.digitalmovement.bean.GymBean;
import com.gzai.zhongjiang.digitalmovement.bean.GymInfo;
import com.gzai.zhongjiang.digitalmovement.bean.GymRegionBean;
import com.gzai.zhongjiang.digitalmovement.bean.HealthInfo;
import com.gzai.zhongjiang.digitalmovement.bean.HighestStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.KeepRecordListBean;
import com.gzai.zhongjiang.digitalmovement.bean.LabelBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.bean.MyStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NewsInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NewsListBean;
import com.gzai.zhongjiang.digitalmovement.bean.NoticeListBean;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipBean;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipInfoBean;
import com.gzai.zhongjiang.digitalmovement.bean.PraiseNoticeListBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanCourseBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderByCoachBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderListBean;
import com.gzai.zhongjiang.digitalmovement.bean.StepBean;
import com.gzai.zhongjiang.digitalmovement.bean.StepListBean;
import com.gzai.zhongjiang.digitalmovement.bean.StudioBean;
import com.gzai.zhongjiang.digitalmovement.bean.TopBean;
import com.gzai.zhongjiang.digitalmovement.bean.UploadImageBean;
import com.gzai.zhongjiang.digitalmovement.bean.UserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.VersionInfo;
import com.gzai.zhongjiang.digitalmovement.bean.VipInfoBean;
import com.gzai.zhongjiang.digitalmovement.http.list.CommListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.CountListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBaseResponse;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.NewListBean;
import com.gzai.zhongjiang.digitalmovement.message.chat.MessageBean;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiUrl {
    //获取验证码
    @Headers("Accept:application/json")
    @POST("api/Auth/sms")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> getCode(@Field("mobile") String mobile,
                                             @Field("type") String type);


    //登陆
    @Headers("Accept:application/json")
    @POST("api/Auth/signup")
    @FormUrlEncoded
    Observable<BaseResponse<UserInfo>> login(@Field("user_name") String user_name,
                                             @Field("verification") String verification);

    //安卓更新信息获取
    @Headers("Accept:application/json")
    @POST("api/App/getAppVersion")
    @FormUrlEncoded
    Observable<BaseResponse<VersionInfo>> getAppVersion(@Field("") String version);


    //上传图片
    @Multipart
    @POST
    Observable<BaseResponse<UploadImageBean>> uploadImage(@Header("token") String token,
                                                          @Url String url,
                                                          @Part MultipartBody.Part file);

    //发布动态(圈子)
    @Headers("Accept:application/json")
    @POST("api/App/addPost")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addPost(@Header("token") String token,
                                             @Field("type") String type,
                                             @Field("circle_id") String circle_id,
                                             @Field("title") String title,
                                             @Field("content") String content,
                                             @Field("image_list") String image_list,
                                             @Field("label_id") String label_id,
                                             @Field("dy_sync") String dy_sync,
                                             @Field("is_open") String is_open);


    //广场/关注/圈子列表
    @Headers("Accept:application/json")
    @POST("api/App/getPostList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<DyBean>>> getPostList(@Header("token") String token, @Field("page") int page, @Field("page_size") int pageSize, @Field("post_type") String post_type);

    //广场/关注/圈子列表
    @Headers("Accept:application/json")
    @POST("api/App/getPostList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<DyBean>>> getPostList_cir(@Header("token") String token,
                                                                   @Field("page") int page,
                                                                   @Field("page_size") int pageSize,
                                                                   @Field("post_type") String post_type,
                                                                   @Field("circle_id") String circle_id,
                                                                   @Field("sortby") String sortby);


    //关注(用户)
    @Headers("Accept:application/json")
    @POST("api/App/addFollow")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addFollow(@Header("token") String token,
                                               @Field("to_user_id") String to_user_id);

    //取消关注(用户)
    @Headers("Accept:application/json")
    @POST("api/App/cancelFollow")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> cancelFollow(@Header("token") String token,
                                                  @Field("to_user_id") String to_user_id);


    //标签列表
    @Headers("Accept:application/json")
    @POST("api/App/getLabelList")
    @FormUrlEncoded
    Observable<BaseResponse<List<LabelBean>>> getLabelList(@Header("token") String token, @Field("") String s);


    //健身房列表
    @Headers("Accept:application/json")
    @POST("api/App/getGymList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<GymBean>>> getGymList(@Header("token") String token,
                                                               @Field("page") int page,
                                                               @Field("page_size") int pageSize,
                                                               @Field("lng") String lng,
                                                               @Field("lat") String lat);

    //健身房信息
    @Headers("Accept:application/json")
    @POST("api/App/getGymInfo")
    @FormUrlEncoded
    Observable<BaseResponse<GymInfo>> getGymInfo(@Field("id") String id);


    //教练列表
    @Headers("Accept:application/json")
    @POST("api/App/getCoachList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<CoachListBean>>> getCoachList(@Header("token") String token,
                                                                       @Field("page") int page,
                                                                       @Field("page_size") int pageSize,
                                                                       @Field("gym_id") String gym_id);


    //教练详情
    @Headers("Accept:application/json")
    @POST("api/App/getCoachInfo")
    @FormUrlEncoded
    Observable<BaseResponse<CoachDetailInfo>> getCoachInfo(@Header("token") String token, @Field("user_id") String user_id);


    //圈子列表
    @Headers("Accept:application/json")
    @POST("api/App/getCircleList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<CircleListBean>>> getCircleList(@Header("token") String token,
                                                                         @Field("page") int page,
                                                                         @Field("page_size") int pageSize);


    //加入圈子
    @Headers("Accept:application/json")
    @POST("api/App/joinCircle")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> joinCircle(@Header("token") String token,
                                                @Field("circle_id") String circle_id);

    //取消关注(用户)
    @Headers("Accept:application/json")
    @POST("api/App/cancelCircle")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> cancelCircle(@Header("token") String token,
                                                  @Field("circle_id") String circle_id);


    //动态/圈子详情
    @Headers("Accept:application/json")
    @POST("api/App/getPostInfo")
    @FormUrlEncoded
    Observable<BaseResponse<DyBean>> getPostInfo(@Header("token") String token, @Field("post_id") String post_id);


    //圈子详情
    @Headers("Accept:application/json")
    @POST("api/App/getCircleInfo")
    @FormUrlEncoded
    Observable<BaseResponse<CircleInfo>> getCircleInfo(@Header("token") String token, @Field("circle_id") String circle_id);


    //动态(圈子)评论
    @Headers("Accept:application/json")
    @POST("api/App/addPostComment")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addPostComment(@Header("token") String token,
                                                    @Field("post_id") String post_id,
                                                    @Field("content") String content,
                                                    @Field("parent_id") String parent_id,
                                                    @Field("to_user_id") String to_user_id,
                                                    @Field("images") String images);

    //动态评论列表
    @Headers("Accept:application/json")
    @POST("api/App/getPostComment")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<CommentBean>>> getPostComment(@Header("token") String token,
                                                                       @Field("post_id") String post_id,
                                                                       @Field("page") int page,
                                                                       @Field("page_size") int pageSize);


    //动态/评论点赞
    @Headers("Accept:application/json")
    @POST("api/App/addPraise")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addPraise(@Header("token") String token,
                                               @Field("post_id") String post_id,
                                               @Field("type") String type,
                                               @Field("comment_id") String comment_id);

    //动态/评论取消赞
    @Headers("Accept:application/json")
    @POST("api/App/cancelPraise")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> cancelPraise(@Header("token") String token,
                                                  @Field("post_id") String post_id,
                                                  @Field("type") String type,
                                                  @Field("comment_id") String comment_id);

    //我的点赞列表
    @Headers("Accept:application/json")
    @POST("api/App/getPraiseNoticeList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<PraiseNoticeListBean>>> getPraiseNoticeList(@Header("token") String token,
                                                                                     @Field("page") int page,
                                                                                     @Field("page_size") int pageSize);


    //关注/粉丝列表
    @Headers("Accept:application/json")
    @POST("api/App/getFollowList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<FollowBean>>> getFollowList(@Header("token") String token,
                                                                     @Field("page") int page,
                                                                     @Field("page_size") int pageSize,
                                                                     @Field("type") String type);

    //获取用户信息
    @Headers("Accept:application/json")
    @POST("api/Auth/getUserInfo")
    @FormUrlEncoded
    Observable<BaseResponse<MyUserInfo>> getUserInfo(@Header("token") String token,
                                                     @Field("") String c);


    //修改用户信息
    @Headers("Accept:application/json")
    @POST("api/Auth/saveUserInfo")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> saveUserInfo(@Header("token") String token,
                                                  @Field("avatar") String avatar,
                                                  @Field("sex") String sex,
                                                  @Field("nick_name") String nick_name,
                                                  @Field("birthday") String birthday,
                                                  @Field("intro") String intro);

    //获取用户信息
    @Headers("Accept:application/json")
    @POST("api/Auth/getUserInfo")
    @FormUrlEncoded
    Observable<BaseResponse<MyUserInfo>> getOtherUserInfo(@Header("token") String token,
                                                          @Field("user_id") String user_id);


    //广场/关注/圈子列表
    @Headers("Accept:application/json")
    @POST("api/App/getPostList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<DyBean>>> getMyPostList(@Header("token") String token,
                                                                 @Field("page") int page,
                                                                 @Field("page_size") int pageSize,
                                                                 @Field("user_id") String user_id,
                                                                 @Field("post_type") String post_type);


    //子评论列表
    @Headers("Accept:application/json")
    @POST("api/App/getChildCommentList")
    @FormUrlEncoded
    Observable<ListBaseResponse<CommListBean<ComDasilogBean>>> getChildCommentList(@Header("token") String token,
                                                                                   @Field("page") int page,
                                                                                   @Field("page_size") int pageSize,
                                                                                   @Field("post_id") String post_id,
                                                                                   @Field("parent_id") String parent_id);


    //举报内容（用户）
    @Headers("Accept:application/json")
    @POST("api/App/accusation")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> accusation(@Header("token") String token,
                                                @Field("to_user_id") String to_user_id,
                                                @Field("content_id") String content_id,
                                                @Field("type") String type,
                                                @Field("accusation_type") String accusation_type,
                                                @Field("memo") String memo);

    //获取健康数据
    @Headers("Accept:application/json")
    @POST("api/App/getHealthInfo")
    @FormUrlEncoded
    Observable<BaseResponse<HealthInfo>> getHealthInfo(@Header("token") String token, @Field("") String c);


    //保存健康数据
    @Headers("Accept:application/json")
    @POST("api/App/saveHealth")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> saveHealth(@Header("token") String token,
                                                @Field("is_coach") String is_coach,
                                                @Field("to_user_id") String to_user_id,
                                                @Field("weight") String weight,
                                                @Field("height") String height,
                                                @Field("bmi") String bmi,
                                                @Field("fat_rate") String fat_rate,
                                                @Field("fat") String fat,
                                                @Field("metabolic_rate") String metabolic_rate,
                                                @Field("whr_rate") String whr_rate,
                                                @Field("humidity_rate") String humidity_rate,
                                                @Field("muscle_rate") String muscle_rate,
                                                @Field("upperarm_left") String upperarm_left,
                                                @Field("upperarm_right") String upperarm_right,
                                                @Field("hipline") String hipline,
                                                @Field("bust") String bust,
                                                @Field("waist") String waist,
                                                @Field("thigh_left") String thigh_left,
                                                @Field("thigh_right") String thigh_right,
                                                @Field("shank_left") String shank_left,
                                                @Field("shank_right") String shank_right);


    //步数上报
    @Headers("Accept:application/json")
    @POST("api/App/stepReport")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> stepReport(@Header("token") String token,
                                                @Field("record_date") String record_date,
                                                @Field("step_count") String step_count);


    //健身房区域
    @Headers("Accept:application/json")
    @POST("api/App/getGymRegion")
    @FormUrlEncoded
    Observable<ListBaseResponse<NewListBean<GymRegionBean>>> getGymRegion(@Header("token") String token,
                                                                          @Field("gym_id") String gym_id);


    //好友步数排行
    @Headers("Accept:application/json")
    @POST("api/App/getStepRank")
    @FormUrlEncoded
    Observable<ListBaseResponse<NewListBean<StepBean>>> getStepRank(@Header("token") String token,
                                                                    @Field("record_date") String record_date);


    //单日最高步数
    @Headers("Accept:application/json")
    @POST("api/App/getHighestStep")
    @FormUrlEncoded
    Observable<BaseResponse<HighestStepBean>> getHighestStep(@Header("token") String token,
                                                             @Field("") String s);


    //消息通知
    @Headers("Accept:application/json")
    @POST("api/App/getNoticeList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<NoticeListBean>>> getNoticeList(@Header("token") String token,
                                                                         @Field("page") int page,
                                                                         @Field("page_size") int pageSize);


    //资讯列表
    @Headers("Accept:application/json")
    @POST("api/App/getNewsList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<NewsListBean>>> getNewsList(@Header("token") String token,
                                                                     @Field("page") int page,
                                                                     @Field("page_size") int pageSize);


    //资讯详情
    @Headers("Accept:application/json")
    @POST("api/App/getNewsInfo")
    @FormUrlEncoded
    Observable<BaseResponse<NewsInfo>> getNewsInfo(@Header("token") String token,
                                                   @Field("id") String id);

    //私聊-聊天记录
    @Headers("Accept:application/json")
    @POST("api/App/getMyMsgList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<MessageBean>>> getMyMsgList(@Header("token") String token,
                                                                     @Field("page") int page,
                                                                     @Field("page_size") int pageSize,
                                                                     @Field("send_uid") String send_uid,
                                                                     @Field("create_time") String create_time);


    //我的评论列表
    @Headers("Accept:application/json")
    @POST("api/App/getCommentNoticeList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<CommentListBean>>> getCommentNoticeList(@Header("token") String token,
                                                                                 @Field("page") int page,
                                                                                 @Field("page_size") int pageSize);


    //圈子置顶动态
    @Headers("Accept:application/json")
    @POST("api/App/getCircleTopPostList")
    @FormUrlEncoded
    Observable<ListBaseResponse<NewListBean<TopBean>>> getCircleTopPostList(@Header("token") String token,
                                                                            @Field("circle_id") String circle_id);


    //步数点赞
    @Headers("Accept:application/json")
    @POST("api/App/addStepPraise")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addStepPraise(@Header("token") String token,
                                                   @Field("step_id") String step_id);


    //步数曲线图
    @Headers("Accept:application/json")
    @POST("api/App/getStepList")
    @FormUrlEncoded
    Observable<BaseResponse<List<StepListBean>>> getStepList(@Header("token") String token,
                                                             @Field("") String s);


    //我的步数排名
    @Headers("Accept:application/json")
    @POST("api/App/getMyStepRank")
    @FormUrlEncoded
    Observable<BaseResponse<MyStepBean>> getMyStepRank(@Header("token") String token,
                                                       @Field("") String s);


    //评论详情
    @Headers("Accept:application/json")
    @POST("api/App/getCommentInfo")
    @FormUrlEncoded
    Observable<BaseResponse<CommDetailBean>> getCommentInfo(@Header("token") String token,
                                                            @Field("comment_id") String id);

    //评论详情
    @Headers("Accept:application/json")
    @POST("api/App/getCommentInfo")
    @FormUrlEncoded
    Observable<BaseResponse<CommDetailBean1>> getCommentInfo1(@Header("token") String token,
                                                              @Field("comment_id") String id);


    //会员卡信息
    @Headers("Accept:application/json")
    @POST("api/App/getVipInfo")
    @FormUrlEncoded
    Observable<BaseResponse<List<VipInfoBean>>> getVipInfo(@Header("token") String token,
                                                           @Field("") String s);


    //私教卡信息
    @Headers("Accept:application/json")
    @POST("api/App/getPersonalVipInfo")
    @FormUrlEncoded
    Observable<BaseResponse<List<PersonalVipInfoBean>>> getPersonalVipInfo(@Header("token") String token,
                                                                           @Field("") String s);


    //意见反馈
    @Headers("Accept:application/json")
    @POST("api/App/feedQuestion")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> feedQuestion(@Header("token") String token,
                                                  @Field("content") String content,
                                                  @Field("image_list") String image_list);


    //意见反馈记录
    @Headers("Accept:application/json")
    @POST("api/App/getFeedQuestionList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<FeedBean>>> getFeedQuestionList(@Header("token") String token,
                                                                         @Field("page") int page,
                                                                         @Field("page_size") int pageSize);


    //会员-我的预约课程记录
    @Headers("Accept:application/json")
    @POST("api/App/getMySubCourse")
    @FormUrlEncoded
    Observable<BaseResponse<List<MyCourse>>> getMySubCourse(@Header("token") String token,
                                                            @Field("record_date") String record_date,
                                                            @Field("coach_id") String coach_id);


    //私教课程预约
    @Headers("Accept:application/json")
    @POST("api/App/subscribeCourse")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> subscribeCourse(@Header("token") String token,
                                                     @Field("start_time") String start_time,
                                                     @Field("end_time") String end_time,
                                                     @Field("coach_id") String coach_id);

    //教练-获取销课二维码信息
    @Headers("Accept:application/json")
    @POST("api/App/getCoachQrcode")
    @FormUrlEncoded
    Observable<BaseResponse<CoachQrcodeBean>> getCoachQrcode(@Header("token") String token,
                                                             @Field("") String s);


    //教练-当前私教对应学员列表
    @Headers("Accept:application/json")
    @POST("api/App/getPersonalList")
    @FormUrlEncoded
    Observable<BaseResponse<List<StudioBean>>> getPersonalList(@Header("token") String token,
                                                               @Field("search_name") String search_name);


    //该教练已被预约课程列表
    @Headers("Accept:application/json")
    @POST("api/App/getSubCourseList")
    @FormUrlEncoded
    Observable<ListBaseResponse<CountListBean<MyCourse>>> getSubCourseList(@Header("token") String token,
                                                                           @Field("record_date") String record_date,
                                                                           @Field("coach_id") String coach_id,
                                                                           @Field("status") String status);


    //会员-健身记录
    @Headers("Accept:application/json")
    @POST("api/App/getKeepRecordList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<KeepRecordListBean>>> getKeepRecordList(@Header("token") String token,
                                                                                 @Field("page") int page,
                                                                                 @Field("page_size") int pageSize);


    //会员-健身记录
    @Headers("Accept:application/json")
    @POST("api/App/getKeepRecordList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<KeepRecordListBean>>> getNewKeepRecordList(@Header("token") String token,
                                                                                    @Field("page") int page,
                                                                                    @Field("page_size") int pageSize,
                                                                                    @Field("user_id") String user_id);


    //教练-获取销课记录
    @Headers("Accept:application/json")
    @POST("api/App/getScanOrderByCoach")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<ScanOrderByCoachBean>>> getScanOrderByCoach(@Header("token") String token,
                                                                                     @Field("month") String month,
                                                                                     @Field("page") int page,
                                                                                     @Field("page_size") int pageSize);


    //教练-销课点评内容获取
    @Headers("Accept:application/json")
    @POST("api/App/getCoachComment")
    @FormUrlEncoded
    Observable<BaseResponse<CoachCommentBean>> getCoachComment(@Header("token") String token,
                                                               @Field("order_id") String order_id,
                                                               @Field("user_id") String user_id);


    //教练-对私教课点评
    @Headers("Accept:application/json")
    @POST("api/App/addCoachComment")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addCoachComment(@Header("token") String token,
                                                     @Field("order_id") String order_id,
                                                     @Field("user_id") String user_id,
                                                     @Field("content") String content);

    //会员-扫码销课
    @Headers("Accept:application/json")
    @POST("api/App/scanCourse")
    @FormUrlEncoded
    Observable<BaseResponse<ScanCourseBean>> scanCourse(@Header("token") String token,
                                                        @Field("qr_data") String qr_data);


    //会员-私教卡列表
    @Headers("Accept:application/json")
    @POST("api/App/getMyPersonalVip")
    @FormUrlEncoded
    Observable<BaseResponse<List<PersonalVipBean>>> getMyPersonalVip(@Header("token") String token,
                                                                     @Field("") String s);


    //会员-确认销课
    @Headers("Accept:application/json")
    @POST("api/App/updateScanOrder")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> updateScanOrder(@Header("token") String token,
                                                     @Field("order_id") String order_id,
                                                     @Field("card_id") String card_id);

    //会员-订单销课确认评价
    @Headers("Accept:application/json")
    @POST("api/App/addOrderComment")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addOrderComment(@Header("token") String token,
                                                     @Field("order_id") String order_id,
                                                     @Field("stars") String stars,
                                                     @Field("content") String content);


    //会员-销课记录
    @Headers("Accept:application/json")
    @POST("api/App/getScanOrderList")
    @FormUrlEncoded
    Observable<ListBaseResponse<ListBean<ScanOrderListBean>>> getScanOrderList(@Header("token") String token,
                                                                               @Field("page") int page,
                                                                               @Field("page_size") int pageSize,
                                                                               @Field("user_id") String user_id,
                                                                               @Field("card_id") String card_id);


    //教练-添加预约课程
    @Headers("Accept:application/json")
    @POST("api/App/addSubCourse")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> addSubCourse(@Header("token") String token,
                                                  @Field("start_time") String start_time,
                                                  @Field("end_time") String end_time,
                                                  @Field("user_id") String user_id);


    //教练-确认/取消预约课
    @Headers("Accept:application/json")
    @POST("api/App/setSubCourse")
    @FormUrlEncoded
    Observable<NollDataBaseResponse> setSubCourse(@Header("token") String token,
                                                  @Field("id") String id,
                                                  @Field("start_time") String start_time,
                                                  @Field("end_time") String end_time,
                                                  @Field("status") String status);


    //用户-人脸录入(注册)
    @Headers("Accept:application/json")
    @POST("api/App/faceAdd")
    @FormUrlEncoded
    Observable<BaseResponse<FaceBean>> faceAdd(@Header("token") String token,
                                               @Field("image") String image,
                                               @Field("image_type") String image_type);
}

