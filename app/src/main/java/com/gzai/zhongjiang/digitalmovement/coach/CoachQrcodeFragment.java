package com.gzai.zhongjiang.digitalmovement.coach;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CoachQrcodeBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.qrcode.RxQRCode;


public class CoachQrcodeFragment extends Fragment {
   ImageView iv_qr_code;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_coach_qrcode, container, false);
        iv_qr_code=view.findViewById(R.id.iv_qr_code);
        intView();
        return view;
    }

    private void intView() {
        RequestUtils.getCoachQrcode(SharePreUtil.getString(getContext(), "token", ""), new MyObserver<CoachQrcodeBean>(getContext()) {
            @Override
            public void onSuccess(CoachQrcodeBean result) {
                RxQRCode.builder(result.getQr_data()).
                        backColor(0xFFFFFFFF).
                        codeColor(0xFF000000).
                        codeSide(600).
                        codeBorder(1).
                        into(iv_qr_code);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}