package com.gzai.zhongjiang.digitalmovement.bean;

import java.util.List;

public class CircleListBean {
    private String id;
    private String circle_name;
    private String circle_image;
    private String members;
    private String create_time;
    private String isjoin;

    public String getCircle_intro() {
        return circle_intro;
    }

    public void setCircle_intro(String circle_intro) {
        this.circle_intro = circle_intro;
    }

    private String circle_intro;
    List<JoinList> join_list;

    public CircleListBean(String id, String circle_name, String circle_image, String members, String create_time, String isjoin, List<JoinList> join_list) {
        this.id=id;
        this.circle_name=circle_name;
        this.circle_image=circle_image;
        this.members=members;
        this.create_time=create_time;
        this.isjoin=isjoin;
        this.join_list=join_list;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCircle_name() {
        return circle_name;
    }

    public void setCircle_name(String circle_name) {
        this.circle_name = circle_name;
    }

    public String getCircle_image() {
        return circle_image;
    }

    public void setCircle_image(String circle_image) {
        this.circle_image = circle_image;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getIsjoin() {
        return isjoin;
    }

    public void setIsjoin(String isjoin) {
        this.isjoin = isjoin;
    }

    public List<JoinList> getJoin_list() {
        return join_list;
    }

    public void setJoin_list(List<JoinList> join_list) {
        this.join_list = join_list;
    }
}
