package com.gzai.zhongjiang.digitalmovement.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.UploadImageBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.RetrofitUtils;
import com.gzai.zhongjiang.digitalmovement.release.ReleaseActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.gzai.zhongjiang.digitalmovement.view.LookImageviewActivity;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.ImagePickerLoader;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridBean;
import com.hjq.toast.ToastUtils;
import com.lwkandroid.imagepicker.ImagePicker;
import com.lwkandroid.imagepicker.data.ImageBean;
import com.lwkandroid.imagepicker.data.ImagePickType;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.interfaces.SimpleCallback;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ModInfoActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.determine)
    TextView determine;
    @BindView(R.id.head_image)
    CircleImageView head_image;
    @BindView(R.id.pd_name)
    TextView pd_name;
    @BindView(R.id.sex)
    TextView sex;
    @BindView(R.id.birthday)
    TextView birthday;
    @BindView(R.id.input_intro)
    EditText input_intro;
    Calendar calendar = Calendar.getInstance(Locale.CHINA);
    ArrayList<String> backimageUrl = new ArrayList<>();
    Gson gson = new Gson();
    private String nick_name, my_sex, avatar_url, imagePath,dayTime;
    private final int REQUEST_CODE_PICKER = 100;
    private int cb = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_info);
        ButterKnife.bind(this);
        back.setOnClickListener(this);
        determine.setOnClickListener(this);
        pd_name.setOnClickListener(this);
        sex.setOnClickListener(this);
        birthday.setOnClickListener(this);
        head_image.setOnClickListener(this);
        intView();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        dayTime = simpleDateFormat.format(date);
    }

    private void intView() {
        RequestUtils.getUserInfo(SharePreUtil.getString(this, "token", ""), new MyObserver<MyUserInfo>(this) {
            @Override
            public void onSuccess(MyUserInfo result) {
                try {
                    if (result.getAvatar().length() > 0) {
                        avatar_url = result.getAvatar();
                        Glide.with(ModInfoActivity.this).load(avatar_url).into(head_image);
                    } else {
                        avatar_url = "";
                        head_image.setImageResource(R.drawable.mine_head_icon);
                    }
                    nick_name = result.getNick_name();
                    pd_name.setText(result.getNick_name());
                    if (result.getSex().equals("0")) {
                        my_sex = "0";
                        sex.setText("请选择性别");
                    } else if (result.getSex().equals("1")) {
                        my_sex = result.getSex();
                        sex.setText("男");
                    } else {
                        my_sex = result.getSex();
                        sex.setText("女");
                    }
                    if (result.getBirthday() != null) {
                        birthday.setText(result.getBirthday());
                    } else {
                        birthday.setText("请填写您的生日");
                    }
                    if (result.getIntro().length() > 0) {
                        input_intro.setText(result.getIntro());
                    } else {

                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    public static boolean isDateOneBigger(String str1, String str2) {
        boolean isBigger = false;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dt1 = null;
        Date dt2 = null;
        try {
            dt1 = sdf.parse(str1);
            dt2 = sdf.parse(str2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (dt1.getTime() >= dt2.getTime()) {
            isBigger = true;
        } else if (dt1.getTime() < dt2.getTime()) {
            isBigger = false;
        }
        return isBigger;
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.determine:
                if (avatar_url.length() == 0) {
                    ToastUtils.show("请选择头像");
                    return;
                } else if (pd_name.getText().toString().length() == 0) {
                    ToastUtils.show("请输入昵称");
                    return;
                } else if (my_sex.equals("0")) {
                    ToastUtils.show("请选择性别");
                    return;
                } else if (birthday.getText().toString().equals("请填写您的生日")) {
                    ToastUtils.show("请填写您的生日");
                    return;
                }else if(isDateOneBigger(birthday.getText().toString(),dayTime)){
                    ToastUtils.show("请正确填写日期");
                    return;
                } else if (input_intro.getText().toString().length() == 0) {
                    ToastUtils.show("请填写您的个人介绍");
                    return;
                } else {
                    if (cb == 2) {
                        uploadImage(imagePath);
                        showuploadProgressDialog();
                    } else {
                        saveUserInfo(avatar_url, my_sex, pd_name.getText().toString(), birthday.getText().toString(), input_intro.getText().toString());
                        showuploadProgressDialog();
                    }
                }
                break;
            case R.id.sex:
                showsexShadow();
                break;
            case R.id.birthday:
                showDatePickerDialog(ModInfoActivity.this, 4, birthday, calendar);
                break;
            case R.id.pd_name:
                showNameShadow();
                break;
            case R.id.head_image:
                showPartShadow();
                break;
        }
    }

    private void saveUserInfo(String avatar, String sex, String nick_name, String birthday, String intro) {
        RequestUtils.saveUserInfo(SharePreUtil.getString(this, "token", ""), avatar, sex, nick_name, birthday, intro, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("修改成功");
                dismissProgressDialog();
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();

            }
        });
    }

    private void uploadImage(String imagePath) {
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg/png"), file);
        MultipartBody.Part body = null;
        try {
            body = MultipartBody.Part.createFormData("file", URLEncoder.encode(file.getName(), "UTF-8"), requestBody);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RequestUtils.uploadImage(SharePreUtil.getString(this, "token", ""), RetrofitUtils.BaseUrl + "api/Upload/image", body, new MyObserver<UploadImageBean>(this) {
            @Override
            public void onSuccess(UploadImageBean result) {
                backimageUrl.add(result.getUrl());
                String image_list = result.getUrl();
                saveUserInfo(image_list, my_sex, pd_name.getText().toString(), birthday.getText().toString(), input_intro.getText().toString());
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();

            }
        });
    }


    MordNamePopupView mordNamepopupView;

    private void showNameShadow() {
        if (mordNamepopupView == null) {
            mordNamepopupView = (MordNamePopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new MordNamePopupView(this));
        }
        mordNamepopupView.show();
    }

    public class MordNamePopupView extends CenterPopupView {
        EditText editText;

        public MordNamePopupView(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_mord_name;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            editText = findViewById(R.id.edit_name);
            editText.setSelection(editText.getText().length());
            findViewById(R.id.no_exit).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.yes_exit).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (editText.getText().toString().length() > 0) {

                    } else {
                        ToastUtils.show("请输入修改昵称");
                    }
                    dismiss();
                }
            });

        }

        @Override
        protected void onShow() {
            super.onShow();
            editText.setText(nick_name);
            editText.setSelection(editText.getText().length());
        }

        @Override
        protected void onDismiss() {
            pd_name.setText(editText.getText().toString());
            super.onDismiss();
        }
    }


    SexPopupView sexpopupView;

    private void showsexShadow() {
        if (sexpopupView == null) {
            sexpopupView = (SexPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new SexPopupView(this));
        }
        sexpopupView.show();
    }

    public class SexPopupView extends CenterPopupView {


        public SexPopupView(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_sex;
        }

        @Override
        protected void onCreate() {
            super.onCreate();

            findViewById(R.id.sex_m).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    sex.setText("男");
                    my_sex = "1";
                    dismiss();
                }
            });
            findViewById(R.id.sex_f).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    sex.setText("女");
                    my_sex = "2";
                    dismiss();
                }
            });

        }

        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }


    public static void showDatePickerDialog(ModInfoActivity activity, int themeResId, final TextView tv, Calendar calendar) {
        new DatePickerDialog(activity, themeResId, new DatePickerDialog.OnDateSetListener() {
            // 绑定监听器(How the parent is notified that the date is set.)
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // 此处得到选择的时间，可以进行你想要的操作
                String month = monthOfYear + 1 + "", day;
                if (Integer.parseInt(month) < 10) {
                    month = "0" + month;
                } else {
                    month = month;
                }
                if (dayOfMonth < 10) {
                    day = "0" + dayOfMonth;
                } else {
                    day = dayOfMonth + "";
                }

                String time = year + "-" + month + "-" + day;

                tv.setText(time + "");
            }
        }
                // 设置初始日期
                , calendar.get(Calendar.YEAR)

                , calendar.get(Calendar.MONTH)

                , calendar.get(Calendar.DAY_OF_MONTH)).show();

    }


    ShopShareShadowPopupView popupView;

    private void showPartShadow() {
        if (popupView == null) {
            popupView = (ShopShareShadowPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new ShopShareShadowPopupView(this));
        }
        popupView.show();
    }

    public class ShopShareShadowPopupView extends BottomPopupView {
        public ShopShareShadowPopupView(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_head_setting;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
            }
            findViewById(R.id.select_mobile).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    new ImagePicker()
                            .cachePath(Environment.getExternalStorageDirectory().getAbsolutePath())
                            .pickType(ImagePickType.MULTI)
                            .displayer(new ImagePickerLoader())
                            .maxNum(1)
                            .start(ModInfoActivity.this, REQUEST_CODE_PICKER);
                    dismiss();
                }
            });

            findViewById(R.id.select_big).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ModInfoActivity.this, LookImageviewActivity.class);
                    intent.putExtra("image_url", avatar_url);
                    startActivity(intent);
                    dismiss();
                }
            });
            findViewById(R.id.select_cls).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();
        }

        @Override
        protected void onDismiss() {
            super.onDismiss();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            List<ImageBean> list = data.getParcelableArrayListExtra(ImagePicker.INTENT_RESULT_DATA);
            List<NineGridBean> resultList = new ArrayList<>();
            for (ImageBean imageBean : list) {
                NineGridBean nineGirdData = new NineGridBean(imageBean.getImagePath());
                resultList.add(nineGirdData);
                cb = 2;
                imagePath = imageBean.getImagePath();
                avatar_url = imageBean.getImagePath();
                Glide.with(ModInfoActivity.this).load(imageBean.getImagePath()).into(head_image);
            }

        }
    }
}