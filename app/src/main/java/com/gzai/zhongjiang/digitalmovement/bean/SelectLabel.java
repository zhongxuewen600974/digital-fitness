package com.gzai.zhongjiang.digitalmovement.bean;

public class SelectLabel {
    private String id;
    private String label_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel_name() {
        return label_name;
    }

    public void setLabel_name(String label_name) {
        this.label_name = label_name;
    }
}
