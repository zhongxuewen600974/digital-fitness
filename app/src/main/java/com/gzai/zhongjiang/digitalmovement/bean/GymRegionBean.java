package com.gzai.zhongjiang.digitalmovement.bean;

public class GymRegionBean {
    private String region_name;
    private int members;
    public GymRegionBean(int members,String region_name){
        this.region_name=region_name;
        this.members=members;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public int getMembers() {
        return members;
    }

    public void setMembers(int members) {
        this.members = members;
    }
}
