package com.gzai.zhongjiang.digitalmovement.message;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.NoticeAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseFragment;
import com.gzai.zhongjiang.digitalmovement.bean.NoticeListBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;


public class MessageFragment extends BaseFragment implements View.OnClickListener {
    LinearLayout praise_linear,comment_linear;
    RecyclerView recyclerView;
    SmartRefreshLayout smartRefreshLayout;
    private int page_total, page = 1;
    LinearLayout onData, haveData;
    NoticeListBean dyBean;
    List<NoticeListBean> beanList = new ArrayList<>();
    NoticeAdapter myAdapter;
    ImageView empty_image;
    TextView empty_text;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coach, container, false);
        praise_linear = view.findViewById(R.id.praise_linear);
        comment_linear=view.findViewById(R.id.comment_linear);
        empty_image=view.findViewById(R.id.empty_image);
        empty_text=view.findViewById(R.id.empty_text);
        comment_linear.setOnClickListener(this);
        praise_linear.setOnClickListener(this);
        recyclerView = view.findViewById(R.id.recyclerView);
        onData = view.findViewById(R.id.nodata_linear);
        haveData = view.findViewById(R.id.data_linear);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        smartRefreshLayout = view.findViewById(R.id.smartRefreshLayout);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                              //  ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
        if(isConnected(getContext())){
        }else {
            empty_image.setImageResource(R.drawable.not_net_png);
            empty_text.setText("目前没有网络，请检查网络设置");
        }
        return view;
    }

    private void intView() {
        beanList.clear();
        RequestUtils.getNoticeList(SharePreUtil.getString(getContext(), "token", ""), 1, 10, new ListMyObserver<ListBean<NoticeListBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<NoticeListBean> result) {
                ListBean<NoticeListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        onData.setVisibility(View.GONE);
                        haveData.setVisibility(View.VISIBLE);
                        String send_uid = list.getList().get(i).getSend_uid();
                        String msg_info = list.getList().get(i).getMsg_info();
                        String msg_type = list.getList().get(i).getMsg_type();
                        String unread = list.getList().get(i).getUnread();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String refer_id = list.getList().get(i).getRefer_id();
                        String refer_type = list.getList().get(i).getRefer_type();
                        String send_nickname = list.getList().get(i).getSend_nickname();
                        String send_avatar = list.getList().get(i).getSend_avatar();

                        dyBean = new NoticeListBean(send_uid, msg_info, msg_type, unread, update_time, refer_id, refer_type, send_nickname, send_avatar);
                        beanList.add(dyBean);

                    }
                    myAdapter = new NoticeAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);

                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void loadMore(int page) {
        RequestUtils.getNoticeList(SharePreUtil.getString(getContext(), "token", ""), page, 10, new ListMyObserver<ListBean<NoticeListBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<NoticeListBean> result) {
                ListBean<NoticeListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        onData.setVisibility(View.GONE);
                        haveData.setVisibility(View.VISIBLE);
                        String send_uid = list.getList().get(i).getSend_uid();
                        String msg_info = list.getList().get(i).getMsg_info();
                        String msg_type = list.getList().get(i).getMsg_type();
                        String unread = list.getList().get(i).getUnread();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String refer_id = list.getList().get(i).getRefer_id();
                        String refer_type = list.getList().get(i).getRefer_type();
                        String send_nickname = list.getList().get(i).getSend_nickname();
                        String send_avatar = list.getList().get(i).getSend_avatar();

                        dyBean = new NoticeListBean(send_uid, msg_info, msg_type, unread, update_time, refer_id, refer_type, send_nickname, send_avatar);
                        beanList.add(dyBean);

                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.praise_linear:
                Intent Stingintent = new Intent(getActivity(), PraiseActivity.class);
                startActivity(Stingintent);
                break;
            case R.id.comment_linear:
                Intent Stingintent1 = new Intent(getActivity(), MyCommentActivity.class);
                startActivity(Stingintent1);
                break;
        }
    }
}