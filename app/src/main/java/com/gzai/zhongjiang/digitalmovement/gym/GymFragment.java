package com.gzai.zhongjiang.digitalmovement.gym;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.GymAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseFragment;
import com.gzai.zhongjiang.digitalmovement.bean.GymBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;


public class GymFragment extends BaseFragment {
    RecyclerView recyclerView;
    SmartRefreshLayout smartRefreshLayout;
    LinearLayout onData, haveData;
    private int page_total, page = 1;
    GymBean myBean;
    List<GymBean> beanList = new ArrayList<>();
    GymAdapter myAdapter;
    ImageView empty_image;
    TextView empty_text;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gym, container, false);

        onData = view.findViewById(R.id.nodata_linear);
        haveData = view.findViewById(R.id.data_linear);
        empty_image = view.findViewById(R.id.empty_image);
        empty_text = view.findViewById(R.id.empty_text);
        recyclerView = view.findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        smartRefreshLayout = view.findViewById(R.id.gym_smartRefreshLayout);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                                //   ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
        if (isConnected(getContext())) {
        } else {
            empty_image.setImageResource(R.drawable.not_net_png);
            empty_text.setText("目前没有网络，请检查网络设置");
        }
        return view;

    }

    private void intView() {
        beanList.clear();
        RequestUtils.getGymList(SharePreUtil.getString(getContext(), "token", ""), 1, 10, "", "", new ListMyObserver<ListBean<GymBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<GymBean> result) {
                ListBean<GymBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String room_name = list.getList().get(i).getRoom_name();
                        String room_area = list.getList().get(i).getRoom_area();
                        String coachs = list.getList().get(i).getCoachs();
                        String address = list.getList().get(i).getAddress();
                        String image = list.getList().get(i).getImage();
                        String workings = list.getList().get(i).getWorkings();
                        String courses = list.getList().get(i).getCourses();
                        String lng = list.getList().get(i).getLng();
                        String lat = list.getList().get(i).getLat();
                        String sort = list.getList().get(i).getSort();
                        String status = list.getList().get(i).getStatus();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        myBean = new GymBean(id, room_name, room_area, coachs, address, image, workings, courses, lng, lat, sort, status, update_time, create_time);
                        beanList.add(myBean);
                    }
                    myAdapter = new GymAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void loadMore(int page) {
        RequestUtils.getGymList(SharePreUtil.getString(getContext(), "token", ""), page, 10, "", "", new ListMyObserver<ListBean<GymBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<GymBean> result) {
                ListBean<GymBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String room_name = list.getList().get(i).getRoom_name();
                        String room_area = list.getList().get(i).getRoom_area();
                        String coachs = list.getList().get(i).getCoachs();
                        String address = list.getList().get(i).getAddress();
                        String image = list.getList().get(i).getImage();
                        String workings = list.getList().get(i).getWorkings();
                        String courses = list.getList().get(i).getCourses();
                        String lng = list.getList().get(i).getLng();
                        String lat = list.getList().get(i).getLat();
                        String sort = list.getList().get(i).getSort();
                        String status = list.getList().get(i).getStatus();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        myBean = new GymBean(id, room_name, room_area, coachs, address, image, workings, courses, lng, lat, sort, status, update_time, create_time);
                        beanList.add(myBean);
                        myAdapter.notifyDataSetChanged();
                    }

                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}