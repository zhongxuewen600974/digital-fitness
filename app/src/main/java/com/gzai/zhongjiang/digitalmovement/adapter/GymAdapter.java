package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.GymBean;
import com.gzai.zhongjiang.digitalmovement.gym.GymActivity;
import com.gzai.zhongjiang.digitalmovement.gym.MapActivity;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;

import java.util.List;

public class GymAdapter extends RecyclerView.Adapter<GymAdapter.ViewHolder> {
    private Context mContext;
    private List<GymBean> dataBean;


    public GymAdapter(List<GymBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gym, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext).load(dataBean.get(position).getImage()).into(holder.icon);
        holder.name.setText(dataBean.get(position).getRoom_name());
        holder.room_area.setText("面积:" + dataBean.get(position).getRoom_area() + "㎡");
        holder.coachs.setText("教练:" + dataBean.get(position).getCoachs() + "人");
        holder.address.setText(dataBean.get(position).getAddress());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, GymActivity.class);
                Stingintent.putExtra("dym_id", dataBean.get(position).getId());
                mContext.startActivity(Stingintent);
            }
        });

        holder.address_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dataBean.get(position).getLng().length()>0 && dataBean.get(position).getLat().length()>0) {
                    Intent Stingintent = new Intent(mContext, MapActivity.class);
                    Stingintent.putExtra("lng", dataBean.get(position).getLng());
                    Stingintent.putExtra("lat", dataBean.get(position).getLat());
                    Stingintent.putExtra("dym_name", dataBean.get(position).getRoom_name());
                    Stingintent.putExtra("dym_address", dataBean.get(position).getAddress());
                    mContext.startActivity(Stingintent);
                }else {
                    Intent Stingintent = new Intent(mContext, GymActivity.class);
                    Stingintent.putExtra("dym_id", dataBean.get(position).getId());
                    mContext.startActivity(Stingintent);
                }
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        NineGridImageView icon;
        TextView name, room_area, coachs, address;
        LinearLayout address_linear;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = (NineGridImageView) itemView.findViewById(R.id.imageView);
            name = (TextView) itemView.findViewById(R.id.name);
            room_area = (TextView) itemView.findViewById(R.id.room_area);
            coachs = (TextView) itemView.findViewById(R.id.coachs);
            address = (TextView) itemView.findViewById(R.id.address);
            address_linear = (LinearLayout) itemView.findViewById(R.id.address_linear);
        }
    }

}
