package com.gzai.zhongjiang.digitalmovement.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LookImageviewActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.big_imageView)
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_look_imageview);
        ButterKnife.bind(this);
        actionBarView.setTitle("查看大图");
        try{
            Intent intent = getIntent();
            String avatar_url = intent.getStringExtra("image_url");
            if(avatar_url.length()>0){
                Glide.with(LookImageviewActivity.this).load(avatar_url).into(imageView);
            }
        }catch (Exception e){}

    }
}