package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ModPhoneActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.my_phone)
    TextView my_phone;
    @BindView(R.id.getcode)
    TextView getcode;
    @BindView(R.id.next)
    TextView next;
    @BindView(R.id.input_code)
    EditText input_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mod_phone);
        ButterKnife.bind(this);
        actionBarView.setTitle("更换手机号");
        getcode.setOnClickListener(this);
        next.setOnClickListener(this);
        String phone = SharePreUtil.getString(this, "mobile", "");
        my_phone.setText(phone);
    }
    private CountDownTimer mCountDownTimer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long l) {
            getcode.setEnabled(false);
            getcode.setText((l / 1000) + "秒后可重发");
        }

        @Override
        public void onFinish() {
            getcode.setEnabled(true);
            getcode.setText("获取验证码");
        }
    };
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.getcode:
                String phone = my_phone.getText().toString();
                mCountDownTimer.start();
               // getLoginCode(phone);
                break;
            case R.id.next:
                Intent intent=new Intent(ModPhoneActivity.this,BingPhoneActivity.class);
                startActivity(intent);
                break;
        }
    }
    private void getLoginCode(String myphone) {
        RequestUtils.getCode(myphone,"0", new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result ) {

            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }
}