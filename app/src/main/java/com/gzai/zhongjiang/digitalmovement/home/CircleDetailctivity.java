package com.gzai.zhongjiang.digitalmovement.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.TopAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CircleInfo;
import com.gzai.zhongjiang.digitalmovement.bean.JoinList;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.TopBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.list.NewListBean;
import com.gzai.zhongjiang.digitalmovement.release.ReleaseActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.hjq.toast.ToastUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CircleDetailctivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.imageView)
    NineGridImageView imageView;
    @BindView(R.id.circle_name)
    TextView circle_name;
    @BindView(R.id.avatar_1)
    CircleImageView avatar_1;
    @BindView(R.id.avatar_2)
    CircleImageView avatar_2;
    @BindView(R.id.avatar_3)
    CircleImageView avatar_3;
    @BindView(R.id.avatar_4)
    CircleImageView avatar_4;
    @BindView(R.id.avatar_m)
    ImageView avatar_m;
    @BindView(R.id.members)
    TextView members;
    @BindView(R.id.isfollow_linear)
    LinearLayout isfollow_linear;
    @BindView(R.id.isfollow_icon)
    ImageView isfollow_icon;
    @BindView(R.id.join)
    TextView join;
    @BindView(R.id.circle_intro)
    TextView circle_intro;
    @BindView(R.id.tab_fabu)
    ImageView tab_fabu;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;


    String circle_id, is_join, number;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    private List<Fragment> fragments = new ArrayList<>();

    TopBean dyBean;
    List<TopBean> beanList = new ArrayList<>();
    TopAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle_detailctivity);
        EventBus.getDefault().register(this);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        circle_id = intent.getStringExtra("circle_id");

        intView(circle_id);
        join.setOnClickListener(this);
        back.setOnClickListener(this);
        tab_fabu.setOnClickListener(this);
        tabLayout.setTabTextColors(Color.parseColor("#CCCCCC"), Color.parseColor("#333333"));
        fragments = new ArrayList<Fragment>();
        Fragment squareFragment = new NewCircleFragment();
        Fragment recommendFragment = new HotCircleFragment();
        fragments.add(squareFragment);
        fragments.add(recommendFragment);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        intTop();
    }


    private void intTop() {
        beanList.clear();
        RequestUtils.getCircleTopPostList(SharePreUtil.getString(this, "token", ""), circle_id, new ListMyObserver<NewListBean<TopBean>>(this) {
            @Override
            public void onSuccess(NewListBean<TopBean> result) {
                NewListBean<TopBean> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String title = list.getList().get(i).getTitle();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new TopBean(id, title, create_time);
                        beanList.add(dyBean);
                    }
                    myAdapter = new TopAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

                ToastUtils.show(errorMsg);

            }
        });
    }


    public String getCircle_id() {
        return circle_id;
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String t = "";
            if (position == 0) {
                t = "最新发布";
            } else if (position == 1) {
                t = "热门";
            }
            return t;
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
    }

    @Override
    public void onResume() {
        super.onResume();
        intView(circle_id);
    }

    private void intView(String circle_id) {
        RequestUtils.getCircleInfo(SharePreUtil.getString(this, "token", ""), circle_id, new MyObserver<CircleInfo>(this) {
            @Override
            public void onSuccess(CircleInfo result) {
                Glide.with(getBaseContext()).load(result.getInfo().getCircle_image()).into(imageView);
                circle_name.setText(result.getInfo().getCircle_name());
                circle_intro.setText(result.getInfo().getCircle_intro());
                number = result.getInfo().getMembers();
                members.setText(result.getInfo().getMembers() + "人已加入");
                is_join=result.getInfo().getIsjoin();
                if (result.getInfo().getIsjoin().equals("0")) {
                    isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                    isfollow_icon.setVisibility(View.VISIBLE);
                    join.setText("加入");
                    join.setTextColor(Color.parseColor("#FFFFFFFF"));
                } else {
                    isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                    isfollow_icon.setVisibility(View.GONE);
                    join.setText("已加入");
                    join.setTextColor(Color.parseColor("#999999"));
                }
                try {
                    List<JoinList> joinLists = result.getInfo().getJoin_list();
                    if (joinLists.size() == 1) {
                        if(joinLists.get(0).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(0).getAvatar()).into(avatar_1);
                        }else {
                            avatar_1.setImageResource(R.drawable.mine_head_icon);
                        }
                        avatar_1.setVisibility(View.VISIBLE);
                        avatar_2.setVisibility(View.GONE);
                        avatar_3.setVisibility(View.GONE);
                        avatar_4.setVisibility(View.GONE);
                        avatar_m.setVisibility(View.GONE);
                    } else if (joinLists.size() == 2) {
                        if(joinLists.get(0).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(0).getAvatar()).into(avatar_1);
                        }else {
                            avatar_1.setImageResource(R.drawable.mine_head_icon);
                        }
                        if(joinLists.get(1).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(1).getAvatar()).into(avatar_2);
                        }else {
                            avatar_2.setImageResource(R.drawable.mine_head_icon);
                        }
                        avatar_1.setVisibility(View.VISIBLE);
                        avatar_2.setVisibility(View.VISIBLE);
                        avatar_3.setVisibility(View.GONE);
                        avatar_4.setVisibility(View.GONE);
                        avatar_m.setVisibility(View.GONE);
                    } else if (joinLists.size() == 3) {
                        if(joinLists.get(0).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(0).getAvatar()).into(avatar_1);
                        }else {
                            avatar_1.setImageResource(R.drawable.mine_head_icon);
                        }
                        if(joinLists.get(1).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(1).getAvatar()).into(avatar_2);
                        }else {
                            avatar_2.setImageResource(R.drawable.mine_head_icon);
                        }
                        if(joinLists.get(2).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(2).getAvatar()).into(avatar_3);
                        }else {
                            avatar_3.setImageResource(R.drawable.mine_head_icon);
                        }

                        avatar_1.setVisibility(View.VISIBLE);
                        avatar_2.setVisibility(View.VISIBLE);
                        avatar_3.setVisibility(View.VISIBLE);
                        avatar_4.setVisibility(View.GONE);
                        avatar_m.setVisibility(View.GONE);
                    } else if (joinLists.size() >= 4) {
                        if(joinLists.get(0).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(0).getAvatar()).into(avatar_1);
                        }else {
                            avatar_1.setImageResource(R.drawable.mine_head_icon);
                        }
                        if(joinLists.get(1).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(1).getAvatar()).into(avatar_2);
                        }else {
                            avatar_2.setImageResource(R.drawable.mine_head_icon);
                        }
                        if(joinLists.get(2).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(2).getAvatar()).into(avatar_3);
                        }else {
                            avatar_3.setImageResource(R.drawable.mine_head_icon);
                        }
                        if(joinLists.get(3).getAvatar().length()>0){
                            Glide.with(getBaseContext()).load(joinLists.get(3).getAvatar()).into(avatar_4);
                        }else {
                            avatar_4.setImageResource(R.drawable.mine_head_icon);
                        }
                        avatar_1.setVisibility(View.VISIBLE);
                        avatar_2.setVisibility(View.VISIBLE);
                        avatar_3.setVisibility(View.VISIBLE);
                        avatar_4.setVisibility(View.VISIBLE);
                        avatar_m.setVisibility(View.GONE);
                        avatar_m.setImageResource(R.drawable.home_more_icon_qu);
                    }


                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.join:
                EventBus.getDefault().post(new MessageEventBus("refresh", ""));
                if (is_join.equals("0")) {
                    joinCircle(circle_id);
                } else {
                    cancelCircle(circle_id);
                }
                break;
            case R.id.tab_fabu:
                if (is_join.equals("0")) {
                    ToastUtils.show("请先加入圈子，再发布");
                } else {
                    Intent Stingintent = new Intent(CircleDetailctivity.this, ReleaseActivity.class);
                    Stingintent.putExtra("circle_id", circle_id);
                    Stingintent.putExtra("type", "2");
                    startActivity(Stingintent);
                }
                break;
        }
    }

    private void joinCircle(String circle_id) {
        RequestUtils.joinCircle(SharePreUtil.getString(this, "token", ""), circle_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                intView(circle_id);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelCircle(String circle_id) {
        RequestUtils.cancelCircle(SharePreUtil.getString(this, "token", ""), circle_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                intView(circle_id);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}