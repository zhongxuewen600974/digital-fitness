package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.KeepRecordListBean;
import com.gzai.zhongjiang.digitalmovement.bean.TopBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class KeepRecordAdapter extends RecyclerView.Adapter<KeepRecordAdapter.ViewHolder> {
    private Context mContext;
    private List<KeepRecordListBean> dataBean;

    public KeepRecordAdapter(List<KeepRecordListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_keeprecord, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.create_time.setText(stampToDate(dataBean.get(position).getCreate_time()));
            holder.duration.setText("健身时长："+dataBean.get(position).getDuration()+"分钟");
        } catch (Exception e) {
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView create_time,duration;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            create_time = (TextView) itemView.findViewById(R.id.create_time);
            duration = (TextView) itemView.findViewById(R.id.duration);
        }
    }

}
