package com.gzai.zhongjiang.digitalmovement.bean;

public class VipInfoBean {
    private String gym_id;
    private String card_id;
    private String expire_time;
    private String truename;
    private String times;
    private String room_name;
    private int viewType;
    public VipInfoBean(String gym_id,String card_id,String expire_time,String truename,String times,String room_name,int viewType){
        this.gym_id=gym_id;
        this.card_id=card_id;
        this.expire_time=expire_time;
        this.truename=truename;
        this.times=times;
        this.room_name=room_name;
        this.viewType=viewType;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(String expire_time) {
        this.expire_time = expire_time;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }
}
