package com.gzai.zhongjiang.digitalmovement.bean;

public class KeepRecordListBean {
    private String id;
    private String order_id;
    private String user_id;
    private String start_time;
    private String end_time;
    private String duration;
    private String record_date;
    private String update_time;
    private String create_time;
    public KeepRecordListBean(String id,String order_id,String user_id,String start_time,String end_time,String duration,String record_date,String update_time,String create_time){
        this.id=id;
        this.order_id=order_id;
        this.user_id=user_id;
        this.start_time=start_time;
        this.end_time=end_time;
        this.duration=duration;
        this.record_date=record_date;
        this.update_time=update_time;
        this.create_time=create_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRecord_date() {
        return record_date;
    }

    public void setRecord_date(String record_date) {
        this.record_date = record_date;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
