package com.gzai.zhongjiang.digitalmovement.util;

import android.content.Context;
import android.content.Intent;

import com.gzai.zhongjiang.digitalmovement.MainActivity;
import com.gzai.zhongjiang.digitalmovement.MyApplication;
import com.gzai.zhongjiang.digitalmovement.util.step.BaseClickBroadcast;


public class MyReceiver extends BaseClickBroadcast {

    private static final String TAG = "MyReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        MyApplication tsApplication = (MyApplication) context.getApplicationContext();
        if (!tsApplication.isForeground()) {
            Intent mainIntent = new Intent(context, MainActivity.class);
            mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mainIntent);
        } else {

        }
    }
}
