package com.gzai.zhongjiang.digitalmovement.coach;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.OnTimeSelectChangeListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.OrderByCoachAdapter;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderByCoachBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RecordFragment extends Fragment implements View.OnClickListener {
    RecyclerView recyclerView;
    SmartRefreshLayout smartRefreshLayout;
    private int page_total, page = 1;
    LinearLayout onData, haveData;
    ImageView imageView;
    TextView show_month,times;

    private TimePickerView pvTime;
    ScanOrderByCoachBean dyBean;
    List<ScanOrderByCoachBean> beanList = new ArrayList<>();
    OrderByCoachAdapter myAdapter;
    String month;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_record, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        onData = view.findViewById(R.id.nodata_linear);
        haveData = view.findViewById(R.id.data_linear);
        show_month=view.findViewById(R.id.show_month);
        imageView=view.findViewById(R.id.sjxk_drop_down);
        times=view.findViewById(R.id.times);
        show_month.setOnClickListener(this);

        Date date = new Date();
        String strDateFormat = "yyyy年MM月";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
        show_month.setText(sdf.format(date));

        String strDateFormat1 = "yyyy-MM";
        SimpleDateFormat sdf1 = new SimpleDateFormat(strDateFormat1);

        month=sdf1.format(date);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        smartRefreshLayout = view.findViewById(R.id.smartRefreshLayout);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page = 1;
                            getScanOrderByCoach(month);
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(month,page);
                            } else {
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        getScanOrderByCoach(sdf1.format(date));
        return view;
    }

    private void loadMore(String month,int page) {
        RequestUtils.getScanOrderByCoach(SharePreUtil.getString(getContext(), "token", ""), month,page, 10, new ListMyObserver<ListBean<ScanOrderByCoachBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<ScanOrderByCoachBean> result) {
                ListBean<ScanOrderByCoachBean> list = result;

                if (list.getList().size() > 0) {

                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String order_id = list.getList().get(i).getOrder_id();
                        String truename = list.getList().get(i).getTruename();
                        String status = list.getList().get(i).getStatus();
                        String is_comment = list.getList().get(i).getIs_comment();
                        String record_date = list.getList().get(i).getRecord_date();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String avatar = list.getList().get(i).getAvatar();
                        dyBean = new ScanOrderByCoachBean(id,user_id,coach_id, order_id, truename,status,is_comment,record_date, update_time, create_time,avatar);
                        beanList.add(dyBean);
                    }
                    myAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }



    private void getScanOrderByCoach(String month) {
        beanList.clear();
        RequestUtils.getScanOrderByCoach(SharePreUtil.getString(getContext(), "token", ""), month,1, 10, new ListMyObserver<ListBean<ScanOrderByCoachBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<ScanOrderByCoachBean> result) {
                ListBean<ScanOrderByCoachBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    times.setText("消课"+result.getPage_info().getCounts()+"次");
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String order_id = list.getList().get(i).getOrder_id();
                        String truename = list.getList().get(i).getTruename();
                        String status = list.getList().get(i).getStatus();
                        String is_comment = list.getList().get(i).getIs_comment();
                        String record_date = list.getList().get(i).getRecord_date();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        String avatar = list.getList().get(i).getAvatar();
                        dyBean = new ScanOrderByCoachBean(id,user_id,coach_id, order_id, truename,status,is_comment,record_date, update_time, create_time,avatar);
                        beanList.add(dyBean);
                    }
                    myAdapter = new OrderByCoachAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    times.setText("消课"+0+"次");
                    onData.setVisibility(View.VISIBLE);
                    haveData.setVisibility(View.GONE);
                }
            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }










    private void initTimePicker() {//Dialog 模式下，在底部弹出
        pvTime = new TimePickerBuilder(getContext(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {
                show_month.setText(getTime(date));
                imageView.setRotation(360);
                month=getMonth(date);
                getScanOrderByCoach(getMonth(date));
            }
        })
                .setTimeSelectChangeListener(new OnTimeSelectChangeListener() {
                    @Override
                    public void onTimeSelectChanged(Date date) {
                        Log.i("pvTime", "onTimeSelectChanged");
                    }
                })
                .setType(new boolean[]{true, true, false, false, false, false})
                .isDialog(true) //默认设置false ，内部实现将DecorView 作为它的父控件。
                .addOnCancelClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i("pvTime", "onCancelClickListener");
                    }
                })
                .setItemVisibleCount(5) //若设置偶数，实际值会加1（比如设置6，则最大可见条目为7）
                .setLineSpacingMultiplier(2.0f)
                .isAlphaGradient(true)
                .build();

        Dialog mDialog = pvTime.getDialog();
        if (mDialog != null) {

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    Gravity.BOTTOM);

            params.leftMargin = 0;
            params.rightMargin = 0;
            pvTime.getDialogContainerLayout().setLayoutParams(params);

            Window dialogWindow = mDialog.getWindow();
            if (dialogWindow != null) {
                dialogWindow.setWindowAnimations(com.bigkoo.pickerview.R.style.picker_view_slide_anim);//修改动画样式
                dialogWindow.setGravity(Gravity.BOTTOM);//改成Bottom,底部显示
                dialogWindow.setDimAmount(0.3f);
            }
        }
    }

    private String getTime(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月");
        return format.format(date);
    }


    private String getMonth(Date date) {//可根据需要自行截取数据显示
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM");
        return format.format(date);
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.show_month:
                initTimePicker();
                pvTime.show();
                imageView.setRotation(180);
                break;
        }
    }
}