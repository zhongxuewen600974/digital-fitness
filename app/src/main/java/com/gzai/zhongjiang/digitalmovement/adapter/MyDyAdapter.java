package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.gym.CoachDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.home.DynamicDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MyDyAdapter extends RecyclerView.Adapter<MyDyAdapter.ViewHolder> {
    private Context mContext;
    private List<DyBean> dataBean;


    private OnItemClickListener onItemClickListener;
    private OnItemClickListener onItemClickListener1;
    private OnItemClickListener onItemClickListener2;
    private OnItemClickListener onItemClickListener3;

    private OnItemClickListener onItemClickListener4;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.onItemClickListener1 = onItemClickListener;
        this.onItemClickListener2 = onItemClickListener;
        this.onItemClickListener3 = onItemClickListener;
        this.onItemClickListener4 = onItemClickListener;
    }


    public MyDyAdapter(List<DyBean> list) {
        this.dataBean = list;
    }

    public interface OnItemClickListener {
        void onItemClickListener(String id);

        void OnItemClickListener1(int i, int position, int id);

        void OnItemClickListener2(int position, int id, int num);

        void OnItemClickListener3(int position, int id);

        void OnItemClickListener4(int position, int id);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dy, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.iAvatar);
            } else {
                holder.iAvatar.setImageResource(R.drawable.mine_head_icon);
            }


            holder.name.setText(dataBean.get(position).getNick_name());
            holder.time.setText(stampToDate(dataBean.get(position).getCreate_time()));

            if (SharePreUtil.getString(mContext, "user_id", "").equals(dataBean.get(position).getUser_id())) {
                holder.isfollow_linear.setVisibility(View.GONE);
            } else {
                holder.isfollow_linear.setVisibility(View.VISIBLE);
                if (dataBean.get(position).getIsfollow().equals("0")) {
                    holder.isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                    holder.isfollow_icon.setVisibility(View.VISIBLE);
                    holder.isfollow.setText("关注");
                    holder.isfollow.setTextColor(Color.parseColor("#FFFFFFFF"));
                } else {
                    holder.isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                    holder.isfollow_icon.setVisibility(View.GONE);
                    holder.isfollow.setText("已关注");
                    holder.isfollow.setTextColor(Color.parseColor("#999999"));
                }
            }
            if (dataBean.get(position).getSex().equals("0")) {
                holder.isfollow_image.setVisibility(View.GONE);
            } else if (dataBean.get(position).getSex().equals("1")) {
                holder.isfollow_image.setVisibility(View.VISIBLE);
                holder.isfollow_image.setImageResource(R.drawable.name_right_icon);
            } else {
                holder.isfollow_image.setVisibility(View.VISIBLE);
                holder.isfollow_image.setImageResource(R.drawable.home_wm_icon);
            }
            holder.content.setText(dataBean.get(position).getContent());
            if (dataBean.get(position).getLabel_name() != null) {
                holder.label_linear.setVisibility(View.VISIBLE);
                holder.label.setText(  dataBean.get(position).getLabel_name() );
            } else {
                holder.label_linear.setVisibility(View.GONE);
            }

            if (dataBean.get(position).getIspraise().equals("0")) {
                holder.praises_image.setImageResource(R.drawable.dianzan_fase);
            } else {
                holder.praises_image.setImageResource(R.drawable.home_dianzan);
            }

            holder.praises.setText(dataBean.get(position).getPraises());
            holder.comment.setText(dataBean.get(position).getComments());
            holder.share.setText(dataBean.get(position).getShares());

            if (dataBean.get(position).getImage_list() == null) {
                holder.recyclerView.setVisibility(View.GONE);
            } else {
                holder.recyclerView.setVisibility(View.VISIBLE);
                if (dataBean.get(position).getImage_list().size() == 1) {
                    holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 1));
                } else if (dataBean.get(position).getImage_list().size() != 1) {
                    holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
                }
            }


            ImageAdapter pAdapter = new ImageAdapter(dataBean.get(position).getImage_list());
            holder.recyclerView.setAdapter(pAdapter);


        } catch (Exception e) {
        }
        holder.isfollow_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onItemClickListener.onItemClickListener(position,dataBean.get(position).getIsfollow(),dataBean.get(position).getUser_id());
                if (dataBean.get(position).getIsfollow().equals("0")) {
                    addFollow(dataBean.get(position).getUser_id());
                    dataBean.get(position).setIsfollow("1");
                    notifyItemChanged(position);
                } else {
                    cancelFollow(dataBean.get(position).getUser_id());
                    dataBean.get(position).setIsfollow("0");
                    notifyItemChanged(position);
                }
            }
        });

        holder.report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClickListener(dataBean.get(position).getId());
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, DynamicDetailsActivity.class);
                Stingintent.putExtra("post_id", dataBean.get(position).getId());
                Stingintent.putExtra("label_name", dataBean.get(position).getLabel_name());
                mContext.startActivity(Stingintent);
            }
        });


        holder.comment_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, DynamicDetailsActivity.class);
                Stingintent.putExtra("post_id", dataBean.get(position).getId());
                Stingintent.putExtra("label_name", dataBean.get(position).getLabel_name());
                Stingintent.putExtra("to_comm", "to_comment");
                mContext.startActivity(Stingintent);
            }
        });

        holder.praises_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataBean.get(position).getIspraise().equals("0")) {
                    addPraise(dataBean.get(position).getId());
                    dataBean.get(position).setIspraise("1");
                    dataBean.get(position).setPraises(Integer.parseInt(dataBean.get(position).getPraises()) + 1 + "");
                    notifyItemChanged(position);
                } else {
                    cancelPraise(dataBean.get(position).getId());
                    dataBean.get(position).setIspraise("0");
                    dataBean.get(position).setPraises(Integer.parseInt(dataBean.get(position).getPraises()) - 1 + "");
                    notifyItemChanged(position);
                }
            }
        });

        holder.iAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });

    }

    private void addPraise(String post_id) {
        RequestUtils.addPraise(SharePreUtil.getString(mContext, "token", ""), post_id, "post", "", new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelPraise(String post_id) {
        RequestUtils.cancelPraise(SharePreUtil.getString(mContext, "token", ""), post_id, "post", "", new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void addFollow(String to_user_id) {
        RequestUtils.addFollow(SharePreUtil.getString(mContext, "token", ""), to_user_id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelFollow(String to_user_id) {
        RequestUtils.cancelFollow(SharePreUtil.getString(mContext, "token", ""), to_user_id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView iAvatar;
        ImageView isfollow_image, isfollow_icon, praises_image, comment_image, share_image;
        TextView name, time, content, isfollow, label, praises, comment, share;
        RecyclerView recyclerView;
        LinearLayout isfollow_linear, label_linear, report;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iAvatar = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            isfollow_image = (ImageView) itemView.findViewById(R.id.isfollow_image);
            time = (TextView) itemView.findViewById(R.id.time);
            isfollow_linear = (LinearLayout) itemView.findViewById(R.id.isfollow_linear);
            isfollow_icon = (ImageView) itemView.findViewById(R.id.isfollow_icon);
            isfollow = (TextView) itemView.findViewById(R.id.isfollow);
            content = (TextView) itemView.findViewById(R.id.content);
            label_linear = (LinearLayout) itemView.findViewById(R.id.label_linear);
            label = (TextView) itemView.findViewById(R.id.label);
            praises_image = (ImageView) itemView.findViewById(R.id.praises_image);
            praises = (TextView) itemView.findViewById(R.id.praises);
            comment_image = (ImageView) itemView.findViewById(R.id.comment_image);
            comment = (TextView) itemView.findViewById(R.id.comment);
            share_image = (ImageView) itemView.findViewById(R.id.share_image);
            share = (TextView) itemView.findViewById(R.id.share);
            report = (LinearLayout) itemView.findViewById(R.id.report);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerView);
        }
    }
}
