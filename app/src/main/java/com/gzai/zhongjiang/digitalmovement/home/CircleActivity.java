package com.gzai.zhongjiang.digitalmovement.home;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CircleActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.circle_tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.circle_viewpager)
    ViewPager viewPager;
    private List<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle);
        ButterKnife.bind(this);
        actionBarView.setTitle("圈子");
        tabLayout.setTabTextColors(Color.parseColor("#CCCCCC"), Color.parseColor("#333333"));
        fragments = new ArrayList<Fragment>();
        Fragment squareFragment = new AllCircleFragment();
        Fragment recommendFragment = new MyCircleFragment();
        fragments.add(squareFragment);
        fragments.add(recommendFragment);
        viewPager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String t = "";
            if (position == 0) {
                t = "全部";
            } else if (position == 1) {
                t = "我加入的";
            }
            return t;
        }
    }
}