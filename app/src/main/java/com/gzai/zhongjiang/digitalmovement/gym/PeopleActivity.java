package com.gzai.zhongjiang.digitalmovement.gym;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.PeoPleAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.GymRegionBean;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.list.NewListBean;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;
import com.openxu.cview.chart.piechart.PieChartLayout;
import com.openxu.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PeopleActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.pieChart2)
    PieChartLayout pieChart2;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.no_people)
    LinearLayout no_people;


    List<GymRegionBean> datalist = new ArrayList<>();
    GymRegionBean gymRegionBean;
    private String dym_id="";

    PeoPleAdapter peoPleAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people);
        ButterKnife.bind(this);
        actionBarView.setTitle("健身房");
        pieChart2.setRingWidth(DensityUtil.dip2px(this, 20));
        pieChart2.setTagModul(PieChartLayout.TAG_MODUL.MODUL_LABLE);      //在lable后面显示tag
        pieChart2.setDebug(false);
        pieChart2.setLoading(true);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        Intent intent=getIntent();
        dym_id=intent.getStringExtra("dym_id");
        intView(dym_id);
    }


    private void intView(String dym_id) {
        datalist.clear();
        RequestUtils.getGymRegion(SharePreUtil.getString(this, "token", ""), dym_id, new ListMyObserver<NewListBean<GymRegionBean>>(this) {
            @Override
            public void onSuccess(NewListBean<GymRegionBean> result) {
                NewListBean<GymRegionBean> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        int members = list.getList().get(i).getMembers();
                        String region_name = list.getList().get(i).getRegion_name();
                        gymRegionBean = new GymRegionBean(members,region_name);
                        datalist.add(gymRegionBean);
                    }
                    pieChart2.setLoading(false);
                    pieChart2.setChartData(GymRegionBean.class, "members", "region_name",datalist ,null);
                    peoPleAdapter=new PeoPleAdapter(datalist);
                    recyclerView.setAdapter(peoPleAdapter);
                } else {
                    no_people.setVisibility(View.GONE);
                 ToastUtils.show("暂无数据");
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

                ToastUtils.show(errorMsg);

            }
        });
    }
}