package com.gzai.zhongjiang.digitalmovement.my;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.UploadImageBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.RetrofitUtils;
import com.gzai.zhongjiang.digitalmovement.release.ReleaseActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.LookImageviewActivity;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.GlideImageLoader;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.ImagePickerLoader;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGirdImageContainer;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridBean;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridView;
import com.hjq.toast.ToastUtils;
import com.lwkandroid.imagepicker.ImagePicker;
import com.lwkandroid.imagepicker.data.ImageBean;
import com.lwkandroid.imagepicker.data.ImagePickType;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class FeedBackActivity extends BaseActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, NineGridView.onItemClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.release)
    TextView release;
    @BindView(R.id.input_release)
    EditText input_release;
    @BindView(R.id.ninegridview)
    NineGridView mNineGridView;
    private final int REQUEST_CODE_PICKER = 100;
    ArrayList<String> choosePic = new ArrayList<>();
    ArrayList<String> backimageUrl = new ArrayList<>();
    private int imageNumber = 0;
    Gson gson = new Gson();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        back.setOnClickListener(this);
        release.setOnClickListener(this);
        mNineGridView.setImageLoader(new GlideImageLoader());
        //设置显示列数，默认3列
        mNineGridView.setColumnCount(3);
        //设置是否为编辑模式，默认为false
        mNineGridView.setIsEditMode(true);
        //设置单张图片显示时的尺寸，默认100dp
        mNineGridView.setSingleImageSize(150);
        //设置单张图片显示时的宽高比，默认1.0f
        mNineGridView.setSingleImageRatio(0.8f);
        //设置最大显示数量，默认9张
        mNineGridView.setMaxNum(9);
        //设置图片显示间隔大小，默认3dp
        mNineGridView.setSpcaeSize(3);
        //设置删除图片
        //        mNineGridView.setIcDeleteResId(R.drawable.ic_block_black_24dp);
        //设置删除图片与父视图的大小比例，默认0.25f
        mNineGridView.setRatioOfDeleteIcon(0.3f);
        //设置“+”号的图片
        mNineGridView.setIcAddMoreResId(R.drawable.ic_ngv_add_pic);
        //设置各类点击监听
        mNineGridView.setOnItemClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.release:
                if (input_release.getText().toString().length() > 0) {
                    if (imageNumber > 0) {
                        showuploadProgressDialog();
                        uploadImage(choosePic.get(0));
                    } else {
                        ToastUtils.show("请选择图片");
                    }
                } else {
                    ToastUtils.show("请输入内容");
                }
                break;
        }
    }
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 1://代表继续上传
                    String uploadPath = (String) msg.obj;
                    uploadImage(uploadPath);
                    break;
            }
        }
    };
    private void uploadImage(String imagePath) {
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/jpg/png"), file);
        MultipartBody.Part body = null;
        try {
            body = MultipartBody.Part.createFormData("file", URLEncoder.encode(file.getName(),"UTF-8"), requestBody);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        RequestUtils.uploadImage(SharePreUtil.getString(this, "token", ""), RetrofitUtils.BaseUrl + "api/Upload/image", body, new MyObserver<UploadImageBean>(this) {
            @Override
            public void onSuccess(UploadImageBean result) {
                backimageUrl.add(result.getUrl());
                choosePic.remove(0);
                if (choosePic.size() > 0) {
                    Message message = new Message();
                    message.what = 1;
                    message.obj = choosePic.get(0);
                    mHandler.sendMessage(message);
                } else {
                    String image_list = gson.toJson(backimageUrl);
                    feedQuestion(input_release.getText().toString(), image_list);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();

            }
        });
    }

    private void feedQuestion(String content, String image_list) {
        RequestUtils.feedQuestion(SharePreUtil.getString(this, "token", ""),  content, image_list, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                EventBus.getDefault().post(new MessageEventBus("refresh_feed", ""));
                dismissProgressDialog();
                ToastUtils.show("发布成功！");
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                dismissProgressDialog();
            }
        });
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
    }
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mNineGridView.setIsEditMode(b);
    }

    @Override
    public void onNineGirdAddMoreClick(int cha) {
        //编辑模式下，图片展示数量尚未达到最大数量时，会显示一个“+”号，点击后回调这里
        new ImagePicker()
                .cachePath(Environment.getExternalStorageDirectory().getAbsolutePath())
                .pickType(ImagePickType.MULTI)
                .displayer(new ImagePickerLoader())
                .maxNum(cha)
                .start(this, REQUEST_CODE_PICKER);
    }

    @Override
    public void onNineGirdItemClick(int position, NineGridBean gridBean, NineGirdImageContainer imageContainer) {
        String uid = gridBean.getOriginUrl().toString();
        Intent intent = new Intent(FeedBackActivity.this, LookImageviewActivity.class);
        intent.putExtra("image_url", uid);
        startActivity(intent);
    }


    @Override
    public void onNineGirdItemDeleted(int position, NineGridBean gridBean, NineGirdImageContainer imageContainer) {
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_PICKER && resultCode == RESULT_OK && data != null) {
            List<ImageBean> list = data.getParcelableArrayListExtra(ImagePicker.INTENT_RESULT_DATA);
            List<NineGridBean> resultList = new ArrayList<>();
            for (ImageBean imageBean : list) {
                choosePic.add(imageBean.getImagePath());
                NineGridBean nineGirdData = new NineGridBean(imageBean.getImagePath());
                resultList.add(nineGirdData);
            }
            mNineGridView.addDataList(resultList);
            imageNumber = resultList.size();
        }
    }

}