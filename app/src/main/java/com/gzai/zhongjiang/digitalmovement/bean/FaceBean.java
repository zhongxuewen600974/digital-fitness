package com.gzai.zhongjiang.digitalmovement.bean;

public class FaceBean {
    private String face_token;
    locationBean location;

    public String getFace_token() {
        return face_token;
    }

    public void setFace_token(String face_token) {
        this.face_token = face_token;
    }

    public locationBean getLocation() {
        return location;
    }

    public void setLocation(locationBean location) {
        this.location = location;
    }
}
