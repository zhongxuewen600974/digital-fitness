package com.gzai.zhongjiang.digitalmovement.message;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NewsInfo;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.hjq.toast.ToastUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewDetailActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.content)
    TextView content;
    @BindView(R.id.image)
    NineGridImageView nineGridImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_detail);
        ButterKnife.bind(this);
        actionBarView.setTitle("健身资讯");
        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        intView(id);
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    private void intView(String id) {
        RequestUtils.getNewsInfo(SharePreUtil.getString(this, "token", ""), id, new MyObserver<NewsInfo>(this) {
            @Override
            public void onSuccess(NewsInfo result) {
                Glide.with(NewDetailActivity.this).load(result.getInfo().getImage()).into(nineGridImageView);
                title.setText(result.getInfo().getTitle());
                time.setText(stampToDate(result.getInfo().getCreate_time()));
                content.setText(Html.fromHtml(result.getInfo().getContent()));
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }
}