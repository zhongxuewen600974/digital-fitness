package com.gzai.zhongjiang.digitalmovement.bean;

public class ComDasilogBean {
    private String nick_name;
    private String avatar;
    private String id;
    private String post_id;
    private String create_time;
    private String user_id;
    private String to_user_id;
    private String comment_nick_name;
    private String comment_avatar;
    private String content;
    private String comments;
    private String praises;
    private String ispraise;

    public ComDasilogBean(String nick_name, String avatar, String id, String post_id, String create_time, String user_id, String to_user_id, String comment_nick_name,
            String comment_avatar, String content, String comments, String praises, String ispraise) {
        this.nick_name=nick_name;
        this.avatar=avatar;
        this.id=id;
        this.post_id=post_id;
        this.create_time=create_time;
        this.user_id=user_id;
        this.to_user_id=to_user_id;
        this.comment_nick_name=comment_nick_name;
        this.comment_avatar=comment_avatar;
        this.content=content;
        this.comments=comments;
        this.praises=praises;
        this.ispraise=ispraise;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getComment_nick_name() {
        return comment_nick_name;
    }

    public void setComment_nick_name(String comment_nick_name) {
        this.comment_nick_name = comment_nick_name;
    }

    public String getComment_avatar() {
        return comment_avatar;
    }

    public void setComment_avatar(String comment_avatar) {
        this.comment_avatar = comment_avatar;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getPraises() {
        return praises;
    }

    public void setPraises(String praises) {
        this.praises = praises;
    }

    public String getIspraise() {
        return ispraise;
    }

    public void setIspraise(String ispraise) {
        this.ispraise = ispraise;
    }
}
