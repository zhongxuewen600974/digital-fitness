package com.gzai.zhongjiang.digitalmovement.base;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class FinishBroadcast extends BroadcastReceiver {
    public static String mAction = "finshAcyivity";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (mAction.equals(intent.getAction())) {
            ((Activity) context).finish();
        }
    }
}