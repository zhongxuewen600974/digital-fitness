package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.bean.TopBean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MyCourseAdapter extends RecyclerView.Adapter<MyCourseAdapter.ViewHolder> {
    private Context mContext;
    private List<MyCourse> dataBean;

    public MyCourseAdapter(List<MyCourse> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mycourse, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if(stampToDate(dataBean.get(position).getEnd_time()).equals("00:00")){
                holder.time_slot.setText(stampToDate(dataBean.get(position).getStart_time())+"-"+"24:00");
            }else {
                holder.time_slot.setText(stampToDate(dataBean.get(position).getStart_time())+"-"+stampToDate(dataBean.get(position).getEnd_time()));
            }


            holder.date.setText(dataBean.get(position).getRecord_date());
            if(dataBean.get(position).getStatus().equals("0")){
                holder.status.setBackgroundResource(R.drawable.textview_12);
                holder.status.setText("确认中");
            }else if(dataBean.get(position).getStatus().equals("1")){
                holder.status.setBackgroundResource(R.drawable.textview_13);
                holder.status.setText("已确认");
            }else {
                holder.status.setBackgroundResource(R.drawable.textview_14);
                holder.status.setText("已取消");
            }
        } catch (Exception e) {
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView time_slot,date,status;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            time_slot = (TextView) itemView.findViewById(R.id.time_slot);
            date = (TextView) itemView.findViewById(R.id.date);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }

}
