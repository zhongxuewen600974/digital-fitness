package com.gzai.zhongjiang.digitalmovement.gym;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.CourseAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CourseListBean;
import com.gzai.zhongjiang.digitalmovement.http.RetrofitUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class CourseWeekActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.week_1)
    TextView week_1;
    @BindView(R.id.week_2)
    TextView week_2;
    @BindView(R.id.week_3)
    TextView week_3;
    @BindView(R.id.week_4)
    TextView week_4;
    @BindView(R.id.week_5)
    TextView week_5;
    @BindView(R.id.week_6)
    TextView week_6;
    @BindView(R.id.week_7)
    TextView week_7;
    @BindView(R.id.day_1)
    TextView day_1;
    @BindView(R.id.day_2)
    TextView day_2;
    @BindView(R.id.day_3)
    TextView day_3;
    @BindView(R.id.day_4)
    TextView day_4;
    @BindView(R.id.day_5)
    TextView day_5;
    @BindView(R.id.day_6)
    TextView day_6;
    @BindView(R.id.day_7)
    TextView day_7;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private Calendar calendar = Calendar.getInstance();
    CourseListBean myBean;
    List<CourseListBean> beanList = new ArrayList<>();
    CourseAdapter myAdapter;
    private String dym_id = "";
    Date date1;
    SimpleDateFormat simpleDateFormat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_week);
        ButterKnife.bind(this);
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        actionBarView.setTitle("本周课程");
        day_1.setOnClickListener(this);
        day_2.setOnClickListener(this);
        day_3.setOnClickListener(this);
        day_4.setOnClickListener(this);
        day_5.setOnClickListener(this);
        day_6.setOnClickListener(this);
        day_7.setOnClickListener(this);
        printDayOfWeek();
        calendar.add(Calendar.DATE, 0);
        Date date = calendar.getTime();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        date1 = calendar.getTime();

        day_1.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 0)).substring(8, 10));
        day_2.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 1)).substring(8, 10));
        day_3.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 2)).substring(8, 10));
        day_4.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 3)).substring(8, 10));
        day_5.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 4)).substring(8, 10));
        day_6.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 5)).substring(8, 10));
        day_7.setText(simpleDateFormat.format(getLastDayOfWeek(date1, 6)).substring(8, 10));
        Intent intent = getIntent();
        dym_id = intent.getStringExtra("dym_id");


        // HH:mm:ss
        Date date2 = new Date(System.currentTimeMillis());
        intView(simpleDateFormat.format(date2));
    }

    public static Date getLastDayOfWeek(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_WEEK,
                calendar.getFirstDayOfWeek() + day); // Saturday
        return calendar.getTime();
    }


    private void printDayOfWeek() {
        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY:
                day_7.setBackgroundResource(R.drawable.date_sign);
                day_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_7.setText("今天");
                week_7.setTextColor(Color.parseColor("#01BD5D"));
                break;
            case Calendar.MONDAY:
                day_1.setBackgroundResource(R.drawable.date_sign);
                day_1.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_1.setText("今天");
                week_1.setTextColor(Color.parseColor("#01BD5D"));
                break;
            case Calendar.TUESDAY:
                day_2.setBackgroundResource(R.drawable.date_sign);
                day_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_2.setText("今天");
                week_2.setTextColor(Color.parseColor("#01BD5D"));
                break;
            case Calendar.WEDNESDAY:
                day_3.setBackgroundResource(R.drawable.date_sign);
                day_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_3.setText("今天");
                week_3.setTextColor(Color.parseColor("#01BD5D"));
                break;
            case Calendar.THURSDAY:
                day_4.setBackgroundResource(R.drawable.date_sign);
                day_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_4.setText("今天");
                week_4.setTextColor(Color.parseColor("#01BD5D"));
                break;
            case Calendar.FRIDAY:
                day_5.setBackgroundResource(R.drawable.date_sign);
                day_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_5.setText("今天");
                week_5.setTextColor(Color.parseColor("#01BD5D"));
                break;
            case Calendar.SATURDAY:
                day_6.setBackgroundResource(R.drawable.date_sign);
                day_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                week_6.setText("今天");
                week_6.setTextColor(Color.parseColor("#01BD5D"));
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.day_1:
                day_1.setBackgroundResource(R.drawable.date_sign);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_1.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 0)));
                break;
            case R.id.day_2:
                day_2.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 1)));
                break;
            case R.id.day_3:
                day_3.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 2)));
                break;
            case R.id.day_4:
                day_4.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 3)));
                break;
            case R.id.day_5:
                day_5.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 4)));
                break;
            case R.id.day_6:
                day_6.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                day_7.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 5)));
                break;
            case R.id.day_7:
                day_7.setBackgroundResource(R.drawable.date_sign);
                day_1.setBackgroundResource(R.drawable.white_bg_2);
                day_3.setBackgroundResource(R.drawable.white_bg_2);
                day_4.setBackgroundResource(R.drawable.white_bg_2);
                day_5.setBackgroundResource(R.drawable.white_bg_2);
                day_6.setBackgroundResource(R.drawable.white_bg_2);
                day_2.setBackgroundResource(R.drawable.white_bg_2);
                day_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                day_2.setTextColor(Color.parseColor("#333333"));
                day_3.setTextColor(Color.parseColor("#333333"));
                day_4.setTextColor(Color.parseColor("#333333"));
                day_5.setTextColor(Color.parseColor("#333333"));
                day_6.setTextColor(Color.parseColor("#333333"));
                day_1.setTextColor(Color.parseColor("#333333"));
                intView(simpleDateFormat.format(getLastDayOfWeek(date1, 6)));
                break;
        }
    }


    private void intView(String time) {
        beanList.clear();
        OkHttpClient okHttpClient = new OkHttpClient();
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("gym_id", dym_id)
                .build();
        Request request = new Request.Builder().url(RetrofitUtils.BaseUrl + "api/app/getCourseList").addHeader("token", SharePreUtil.getString(this, "token", "")).post(requestBody).build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
            }

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                String result = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String error_code = jsonObject.optString("error_code", null);
                    if (error_code.equals("0")) {
                        String data = jsonObject.optString("data", null);
                        JSONObject lisobject = new JSONObject(data);
                        Iterator<String> iterator = lisobject.keys();
                        while (iterator.hasNext()) {
                            String key = iterator.next();//获取key日期
                            if (key.equals(time)) {
                              //  Log.e("123456", key);
                              //  Log.e("123456", time);
                                JSONArray array = lisobject.getJSONArray(key);
                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject jsonObject1 = (JSONObject) array.get(i);
                                        String id = jsonObject1.optString("id");
                                        String course_name = jsonObject1.optString("course_name");
                                        String course_date = jsonObject1.optString("course_date");
                                        String start_time = jsonObject1.optString("start_time");
                                        String end_time = jsonObject1.optString("end_time");
                                        myBean = new CourseListBean(id, course_name, course_date, start_time, end_time);
                                        beanList.add(myBean);
                                    }
                                    myAdapter = new CourseAdapter(beanList);
                                    new RequestThread().start();
                                } else {
                                    new RequestThread1().start();
                                }
                            } else {

                            }
                        }
                    } else {
                        new RequestThread1().start();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private class RequestThread extends Thread {
        @Override
        public void run() {
            Message msg = new Message();
            msg.what = 1;
            handler.sendMessage(msg);
        }
    }

    private class RequestThread1 extends Thread {
        @Override
        public void run() {
            Message msg = new Message();
            msg.what = 2;
            handler.sendMessage(msg);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                nodata_linear.setVisibility(View.GONE);
                data_linear.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(myAdapter);
            } else if (msg.what == 2) {
                nodata_linear.setVisibility(View.VISIBLE);
                data_linear.setVisibility(View.GONE);
            }
        }
    };
}