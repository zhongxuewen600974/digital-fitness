package com.gzai.zhongjiang.digitalmovement.bean;

public class PersonalVipBean {
    private String id;
    private String gym_id;
    private String coach_id;
    private String user_id;
    private String card_id;
    private String expire_time;
    private int times;
    private int totals;
    private String status;
    private String last_time;
    private String update_time;
    private String create_time;
    private String room_name;
    private String coach_name;
    private boolean isChecked;

    public String getCoach_name() {
        return coach_name;
    }

    public void setCoach_name(String coach_name) {
        this.coach_name = coach_name;
    }

    public PersonalVipBean(String id, String gym_id, String coach_id, String user_id, String card_id, String expire_time, int times,
                           int totals, String status, String last_time, String update_time, String create_time, String room_name, String coach_name){
        this.id=id;
        this.gym_id=gym_id;
        this.coach_id=coach_id;
        this.user_id=user_id;
        this.card_id=card_id;
        this.expire_time=expire_time;
        this.times=times;
        this.totals=totals;
        this.status=status;
        this.last_time=last_time;
        this.update_time=update_time;
        this.create_time=create_time;
        this.room_name=room_name;
        this.coach_name=coach_name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(String coach_id) {
        this.coach_id = coach_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(String expire_time) {
        this.expire_time = expire_time;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public int getTotals() {
        return totals;
    }

    public void setTotals(int totals) {
        this.totals = totals;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLast_time() {
        return last_time;
    }

    public void setLast_time(String last_time) {
        this.last_time = last_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }
}
