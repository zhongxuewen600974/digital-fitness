package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipBean;
import com.gzai.zhongjiang.digitalmovement.bean.TopBean;

import java.util.List;

public class PersonalVipAdapter extends RecyclerView.Adapter<PersonalVipAdapter.ViewHolder> {
    private Context mContext;
    private List<PersonalVipBean> dataBean;
    private  OnItemClickListener onItemClickListener;
    public PersonalVipAdapter(List<PersonalVipBean> list) {
        this.dataBean = list;
    }
    int resourceId = -1;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    public interface OnItemClickListener {
        void onItemClickListener(String card_id);
    }


    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_personalvip, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.room_name.setText(dataBean.get(position).getRoom_name()+"私教卡");
            holder.coach_name.setText("教练："+dataBean.get(position).getCoach_name());
            holder.courses.setText("剩余课程："+(dataBean.get(position).getTotals()-dataBean.get(position).getTimes())+"节");
        } catch (Exception e) {
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onItemClickListener(dataBean.get(position).getCard_id());
                resourceId = position;
                notifyDataSetChanged();
            }
        });

        if (position == resourceId) {
            holder.select_image.setImageResource(R.drawable.cb_true);
        } else {
            holder.select_image.setImageResource(R.drawable.raund_white);
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView room_name,courses,coach_name;
        ImageView select_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            room_name = (TextView) itemView.findViewById(R.id.room_name);
            courses = (TextView) itemView.findViewById(R.id.courses);
            select_image = (ImageView) itemView.findViewById(R.id.select_image);
            coach_name = (TextView) itemView.findViewById(R.id.coach_name);
        }
    }

}
