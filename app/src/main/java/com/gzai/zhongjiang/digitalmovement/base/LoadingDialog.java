package com.gzai.zhongjiang.digitalmovement.base;

import android.app.Dialog;
import android.content.Context;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;


public class LoadingDialog extends Dialog {
    private final String mLoadingTip;
    Context mContext;
    private TextView mLoadingTv;

    public LoadingDialog(Context context) {
        super(context, R.style.loading_dialog);
        mContext = context;
        this.mLoadingTip = context.getResources().getString(R.string.loading);
        init();
    }

    private void init() {
        setContentView(R.layout.loading_dialog);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        mLoadingTv = (TextView) findViewById(R.id.tipTextView);
        mLoadingTv.setText(mLoadingTip);
    }

    public void showLoadDialog(int msg) {
        showLoadDialog(mContext.getString(msg));
    }

    public void showLoadDialog(String msg) {
        if (mLoadingTv != null) mLoadingTv.setText(msg);
        show();
    }
}

