package com.gzai.zhongjiang.digitalmovement.message.chat;

public class LoginBean {
    private String action;
    private String token;
    public LoginBean(String action,String token){
        this.action=action;
        this.token=token;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
