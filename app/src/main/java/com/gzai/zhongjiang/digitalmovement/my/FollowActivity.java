package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FollowActivity extends BaseActivity {
    private List<Fragment> fragments = new ArrayList<>();
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.follow_TabLayout)
    TabLayout tabLayout;
    @BindView(R.id.follow_viewpager)
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);
        ButterKnife.bind(this);
        Intent intent=getIntent();
        String currentItem=intent.getStringExtra("CurrentItem");
        actionBarView.setTitle("关注");
        tabLayout.setTabTextColors(Color.parseColor("#CCCCCC"), Color.parseColor("#333333"));
        fragments = new ArrayList<Fragment>();
        Fragment squareFragment = new FollwFragment();
        Fragment recommendFragment = new FansFragment();
        fragments.add(squareFragment);
        fragments.add(recommendFragment);
        viewPager.setAdapter(new  PagerAdapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(Integer.parseInt(currentItem));
        tabLayout.setupWithViewPager(viewPager);
    }
    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String t="";
            if(position==0){
                t= "关注";
            }else if(position==1){
                t= "粉丝";
            }
            return t;
        }
    }
}