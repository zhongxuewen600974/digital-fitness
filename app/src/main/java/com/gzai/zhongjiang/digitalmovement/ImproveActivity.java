package com.gzai.zhongjiang.digitalmovement;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.pickerview.DateTimePickerView;
import com.hjq.toast.ToastUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImproveActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.male_linear)
    LinearLayout male_linear;
    @BindView(R.id.male_image)
    ImageView male_image;
    @BindView(R.id.male_text)
    TextView male_text;
    @BindView(R.id.woman_linear)
    LinearLayout woman_linear;
    @BindView(R.id.woman_image)
    ImageView woman_image;
    @BindView(R.id.woman_text)
    TextView woman_text;
    @BindView(R.id.datePickerView)
    DateTimePickerView dateTimePickerView;
    @BindView(R.id.complete)
    TextView complete;
    String my_sex="", nick_name = "", imagePath, birthday = "",dayTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_improve);
        ButterKnife.bind(this);
        male_linear.setOnClickListener(this);
        woman_linear.setOnClickListener(this);
        complete.setOnClickListener(this);
        Intent intent = getIntent();
        nick_name = intent.getStringExtra("nick_name");
        imagePath = intent.getStringExtra("imagePath");
        dateTimePickerView.setStartDate(new GregorianCalendar(1980, 0, 1, 21, 30));
        dateTimePickerView.setBackgroundColor(Color.parseColor("#FFFFFFFF"));

        dateTimePickerView.setOnSelectedDateChangedListener(date -> {
            String dateString = getDateString(date);
            birthday = dateString;
        });


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        dayTime = simpleDateFormat.format(date);
    }

    private String getDateString(Calendar date) {
        int year = date.get(Calendar.YEAR);
        int month = date.get(Calendar.MONTH);
        int dayOfMonth = date.get(Calendar.DAY_OF_MONTH);
        int hour = date.get(Calendar.HOUR_OF_DAY);
        int minute = date.get(Calendar.MINUTE);
        return String.format(Locale.getDefault(), "%d-%02d-%02d", year, month + 1, dayOfMonth, hour, minute);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.male_linear:
                male_linear.setBackgroundResource(R.drawable.login_selected_man_btn);
                male_image.setImageResource(R.drawable.login_selected_man_icon);
                male_text.setTextColor(Color.parseColor("#FFFFFF"));
                my_sex = "1";


                woman_linear.setBackgroundResource(R.drawable.login_woman_btn);
                woman_image.setImageResource(R.drawable.login_woman_icon);
                woman_text.setTextColor(Color.parseColor("#FFB72E"));
                break;
            case R.id.woman_linear:
                woman_linear.setBackgroundResource(R.drawable.login_selected_woman_btn);
                woman_image.setImageResource(R.drawable.login_selected_woman_icon);
                woman_text.setTextColor(Color.parseColor("#FFFFFF"));
                my_sex = "2";


                male_linear.setBackgroundResource(R.drawable.login_sex_btn);
                male_image.setImageResource(R.drawable.login_sex_icon);
                male_text.setTextColor(Color.parseColor("#5599FC"));
                break;
            case R.id.complete:
                if(birthday.length()>0) {
                    if (isDateOneBigger(dayTime, birthday)) {
                        saveUserInfo(imagePath, my_sex, nick_name, birthday, "");
                    } else {
                        ToastUtils.show("请正确选择年龄");
                    }
                }else {
                    saveUserInfo(imagePath, my_sex, nick_name, birthday, "");
                }
                break;
        }
    }


    public static boolean isDateOneBigger(String str1, String str2) {
        boolean isBigger = false;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date dt1 = null;
        Date dt2 = null;
        try {
            dt1 = sdf.parse(str1);
            dt2 = sdf.parse(str2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (dt1.getTime() >= dt2.getTime()) {
            isBigger = true;
        } else if (dt1.getTime() < dt2.getTime()) {
            isBigger = false;
        }
        return isBigger;
    }

    private void saveUserInfo(String avatar, String sex, String nick_name, String birthday, String intro) {
        RequestUtils.saveUserInfo(SharePreUtil.getString(this, "token", ""), avatar, sex, nick_name, birthday, intro, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                Intent intent = new Intent(ImproveActivity.this, MainActivity.class);
                intent.putExtra("imagePath", imagePath);
                intent.putExtra("nick_name", nick_name);
                startActivity(intent);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}