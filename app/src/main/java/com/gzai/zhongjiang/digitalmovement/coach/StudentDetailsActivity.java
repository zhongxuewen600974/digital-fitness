package com.gzai.zhongjiang.digitalmovement.coach;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.message.chat.ChatActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentDetailsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.expire_time)
    TextView expire_time;
    @BindView(R.id.to_catch)
    LinearLayout to_catch;
    @BindView(R.id.to_phone)
    LinearLayout to_phone;
    @BindView(R.id.line1)
    LinearLayout line1;
    @BindView(R.id.line2)
    LinearLayout line2;
    @BindView(R.id.line3)
    LinearLayout line3;
    @BindView(R.id.line4)
    LinearLayout line4;

    String user_id, phone,card_id,coach_id;
    String truename,last_time,avatar="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_details);
        ButterKnife.bind(this);
        actionBarView.setTitle("学员详情");
        coach_id=SharePreUtil.getString(this, "user_id", "");
        try {
            Intent intent = getIntent();
            card_id=intent.getStringExtra("card_id");
            user_id = intent.getStringExtra("user_id");
            last_time=intent.getStringExtra("last_time");
            expire_time.setText("私教卡到期：" + stampToDate(intent.getStringExtra("expire_time")) + "     私教卡：" + intent.getStringExtra("times"));
            intView(user_id);

        } catch (Exception e) {

        }
        to_catch.setOnClickListener(this);
        to_phone.setOnClickListener(this);
        line1.setOnClickListener(this);
        line2.setOnClickListener(this);
        line3.setOnClickListener(this);
        line4.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }


    private void intView(String user_id) {
        RequestUtils.getOtherUserInfo(SharePreUtil.getString(this, "token", ""), user_id, new MyObserver<MyUserInfo>(this) {
            @Override
            public void onSuccess(MyUserInfo result) {
                try {
                    if (result.getAvatar().length() > 0) {
                        Glide.with(StudentDetailsActivity.this).load(result.getAvatar()).into(circleImageView);
                        avatar=result.getAvatar();
                    } else {
                        circleImageView.setImageResource(R.drawable.mine_head_icon);
                    }
                    name.setText(result.getTruename());
                    truename=result.getTruename();
                    phone = result.getMobile();
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.circleImageView:
                Intent Stingintent = new Intent(StudentDetailsActivity.this, OthersPageActivity.class);
                Stingintent.putExtra("user_id", user_id);
                startActivity(Stingintent);
                break;
            case R.id.to_catch:
                Intent intent = new Intent(StudentDetailsActivity.this, ChatActivity.class);
                intent.putExtra("send_name", name.getText().toString());
                intent.putExtra("send_id", user_id);
                startActivity(intent);
                break;
            case R.id.to_phone:
                Intent intent1 = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + phone);
                intent1.setData(data);
                startActivity(intent1);
                break;
            case R.id.line1:
                Intent intent4 = new Intent(StudentDetailsActivity.this, AddSubCourseActivity.class);
                intent4.putExtra("user_id", user_id);
                intent4.putExtra("truename", truename);
                intent4.putExtra("last_time", last_time);
                intent4.putExtra("avatar", avatar);
                intent4.putExtra("coach_id",coach_id);
                startActivity(intent4);
                break;
            case R.id.line2:
                Intent intent2 = new Intent(StudentDetailsActivity.this, KeepRecordActivity.class);
                intent2.putExtra("user_id", user_id);
                startActivity(intent2);
                break;
            case R.id.line3:
                Intent intent3 = new Intent(StudentDetailsActivity.this, ClassRecordActivity.class);
                intent3.putExtra("user_id", user_id);
                intent3.putExtra("card_id", card_id);
                startActivity(intent3);
                break;
            case R.id.line4:
                break;
        }
    }
}