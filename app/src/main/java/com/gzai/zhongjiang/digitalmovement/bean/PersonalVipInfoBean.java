package com.gzai.zhongjiang.digitalmovement.bean;

public class PersonalVipInfoBean {
    private String gym_id;
    private String card_id;
    private String coach_id;
    private String expire_time;
    private int times;
    private int totals;
    private String room_name;
    private String truename;
    private String coach_name;

    public PersonalVipInfoBean(String gym_id,String card_id,String coach_id,String expire_time, int times,int totals,String room_name,String truename,String coach_name){
        this.gym_id=gym_id;
        this.card_id=card_id;
        this.coach_id=coach_id;
        this.expire_time=expire_time;
        this.times=times;
        this.totals=totals;
        this.room_name=room_name;
        this.truename=truename;
        this.coach_name=coach_name;
    }

    public int getTimes() {
        return times;
    }

    public void setTimes(int times) {
        this.times = times;
    }

    public int getTotals() {
        return totals;
    }

    public void setTotals(int totals) {
        this.totals = totals;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public String getCard_id() {
        return card_id;
    }

    public void setCard_id(String card_id) {
        this.card_id = card_id;
    }

    public String getCoach_id() {
        return coach_id;
    }

    public void setCoach_id(String coach_id) {
        this.coach_id = coach_id;
    }

    public String getExpire_time() {
        return expire_time;
    }

    public void setExpire_time(String expire_time) {
        this.expire_time = expire_time;
    }


    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getTruename() {
        return truename;
    }

    public void setTruename(String truename) {
        this.truename = truename;
    }

    public String getCoach_name() {
        return coach_name;
    }

    public void setCoach_name(String coach_name) {
        this.coach_name = coach_name;
    }
}
