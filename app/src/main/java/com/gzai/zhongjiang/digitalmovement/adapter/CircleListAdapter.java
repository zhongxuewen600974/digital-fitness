package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CircleListBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.home.CircleDetailctivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.hjq.toast.ToastUtils;

import java.util.List;

public class CircleListAdapter extends RecyclerView.Adapter<CircleListAdapter.ViewHolder> {
    private Context mContext;
    private List<CircleListBean> dataBean;


    public CircleListAdapter(List<CircleListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {
        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_circle, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext).load(dataBean.get(position).getCircle_image()).into(holder.imageView);
        holder.circle_name.setText(dataBean.get(position).getCircle_name());
        holder.members.setText(dataBean.get(position).getMembers() + "人已加入");
        if (dataBean.get(position).getMembers() == "0") {
            holder.avatar_1.setVisibility(View.GONE);
            holder.avatar_2.setVisibility(View.GONE);
            holder.avatar_3.setVisibility(View.GONE);
            holder.avatar_4.setVisibility(View.GONE);
            holder.icon.setVisibility(View.GONE);
        }
        if (dataBean.get(position).getIsjoin().equals("0")) {
            holder.isfollow_linear.setBackgroundResource(R.drawable.textview_1);
            holder.isfollow_icon.setVisibility(View.VISIBLE);
            holder.join.setText("加入");
            holder.join.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.isfollow_linear.setBackgroundResource(R.drawable.textview_2);
            holder.isfollow_icon.setVisibility(View.GONE);
            holder.join.setText("已加入");
            holder.join.setTextColor(Color.parseColor("#999999"));
        }


        try {
            if (dataBean.get(position).getJoin_list().size() == 1) {
                if(dataBean.get(position).getJoin_list().get(0).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(0).getAvatar()).into(holder.avatar_1);
                }else {
                    holder.avatar_1.setImageResource(R.drawable.mine_head_icon);
                }
                holder.avatar_1.setVisibility(View.VISIBLE);
                holder.avatar_2.setVisibility(View.GONE);
                holder.avatar_3.setVisibility(View.GONE);
                holder.avatar_4.setVisibility(View.GONE);
                holder.icon.setVisibility(View.GONE);
            } else if (dataBean.get(position).getJoin_list().size() == 2) {
                if(dataBean.get(position).getJoin_list().get(0).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(0).getAvatar()).into(holder.avatar_1);
                }else {
                    holder.avatar_1.setImageResource(R.drawable.mine_head_icon);
                }
                if(dataBean.get(position).getJoin_list().get(1).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(1).getAvatar()).into(holder.avatar_2);
                }else {
                    holder.avatar_2.setImageResource(R.drawable.mine_head_icon);
                }
                holder.avatar_1.setVisibility(View.VISIBLE);
                holder.avatar_2.setVisibility(View.VISIBLE);
                holder.avatar_3.setVisibility(View.GONE);
                holder.avatar_4.setVisibility(View.GONE);
                holder.icon.setVisibility(View.GONE);
            } else if (dataBean.get(position).getJoin_list().size() == 3) {
                if(dataBean.get(position).getJoin_list().get(0).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(0).getAvatar()).into(holder.avatar_1);
                }else {
                    holder.avatar_1.setImageResource(R.drawable.mine_head_icon);
                }
                if(dataBean.get(position).getJoin_list().get(1).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(1).getAvatar()).into(holder.avatar_2);
                }else {
                    holder.avatar_2.setImageResource(R.drawable.mine_head_icon);
                }
                if(dataBean.get(position).getJoin_list().get(2).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(2).getAvatar()).into(holder.avatar_3);
                }else {
                    holder.avatar_3.setImageResource(R.drawable.mine_head_icon);
                }

                holder.avatar_1.setVisibility(View.VISIBLE);
                holder.avatar_2.setVisibility(View.VISIBLE);
                holder.avatar_3.setVisibility(View.VISIBLE);
                holder.avatar_4.setVisibility(View.GONE);
                holder.icon.setVisibility(View.GONE);
            } else if (dataBean.get(position).getJoin_list().size() >= 4) {
                if(dataBean.get(position).getJoin_list().get(0).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(0).getAvatar()).into(holder.avatar_1);
                }else {
                    holder.avatar_1.setImageResource(R.drawable.mine_head_icon);
                }
                if(dataBean.get(position).getJoin_list().get(1).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(1).getAvatar()).into(holder.avatar_2);
                }else {
                    holder.avatar_2.setImageResource(R.drawable.mine_head_icon);
                }
                if(dataBean.get(position).getJoin_list().get(2).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(2).getAvatar()).into(holder.avatar_3);
                }else {
                    holder.avatar_3.setImageResource(R.drawable.mine_head_icon);
                }
                if(dataBean.get(position).getJoin_list().get(3).getAvatar().length()>0){
                    Glide.with(mContext).load(dataBean.get(position).getJoin_list().get(3).getAvatar()).into(holder.avatar_4);
                }else {
                    holder.avatar_4.setImageResource(R.drawable.mine_head_icon);
                }
                holder.avatar_1.setVisibility(View.VISIBLE);
                holder.avatar_2.setVisibility(View.VISIBLE);
                holder.avatar_3.setVisibility(View.VISIBLE);
                holder.avatar_4.setVisibility(View.VISIBLE);
                holder.icon.setVisibility(View.GONE);
                holder.icon.setImageResource(R.drawable.home_more_icon_qu);
            }
        } catch (Exception e) {

        }
        int number = Integer.parseInt(dataBean.get(position).getMembers());
        holder.join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataBean.get(position).getIsjoin().equals("0")) {
                    holder.isfollow_linear.setBackgroundResource(R.drawable.textview_2);
                    holder.isfollow_icon.setVisibility(View.GONE);
                    holder.join.setText("已加入");
                    holder.join.setTextColor(Color.parseColor("#999999"));
                    holder.members.setText(number + 1 + "人已加入");
                    dataBean.get(position).setIsjoin("1");
                    dataBean.get(position).setMembers((number + 1) + "");
                    joinCircle(dataBean.get(position).getId());
                    notifyItemChanged(position);
                } else {
                    holder.isfollow_linear.setBackgroundResource(R.drawable.textview_1);
                    holder.isfollow_icon.setVisibility(View.VISIBLE);
                    holder.join.setText("加入");
                    holder.join.setTextColor(Color.parseColor("#FFFFFFFF"));
                    holder.members.setText(number - 1 + "人已加入");
                    dataBean.get(position).setIsjoin("0");
                    dataBean.get(position).setMembers((number - 1) + "");
                    cancelCircle(dataBean.get(position).getId());
                    notifyItemChanged(position);
                }
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, CircleDetailctivity.class);
                Stingintent.putExtra("circle_id", dataBean.get(position).getId());
                Stingintent.putExtra("is_join", dataBean.get(position).getIsjoin());
                mContext.startActivity(Stingintent);
            }
        });
    }


    private void joinCircle(String circle_id) {
        RequestUtils.joinCircle(SharePreUtil.getString(mContext, "token", ""), circle_id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("加入成功！");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void cancelCircle(String circle_id) {
        RequestUtils.cancelCircle(SharePreUtil.getString(mContext, "token", ""), circle_id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("取消圈子！");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        NineGridImageView imageView;
        ImageView icon, isfollow_icon;
        TextView circle_name, members, join;
        CircleImageView avatar_1, avatar_2, avatar_3, avatar_4;
        LinearLayout isfollow_linear;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = (NineGridImageView) itemView.findViewById(R.id.imageView);
            isfollow_linear = (LinearLayout) itemView.findViewById(R.id.isfollow_linear);
            icon = (ImageView) itemView.findViewById(R.id.avatar_m);
            isfollow_icon = (ImageView) itemView.findViewById(R.id.isfollow_icon);
            circle_name = (TextView) itemView.findViewById(R.id.circle_name);
            avatar_1 = (CircleImageView) itemView.findViewById(R.id.avatar_1);
            avatar_2 = (CircleImageView) itemView.findViewById(R.id.avatar_2);
            avatar_3 = (CircleImageView) itemView.findViewById(R.id.avatar_3);
            avatar_4 = (CircleImageView) itemView.findViewById(R.id.avatar_4);
            members = (TextView) itemView.findViewById(R.id.members);
            join = (TextView) itemView.findViewById(R.id.join);
        }
    }

}
