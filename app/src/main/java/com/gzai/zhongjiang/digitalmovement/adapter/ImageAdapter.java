package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.gzai.zhongjiang.digitalmovement.view.wetchimage.JZCImageUtil;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    private Context mContext;
    private List<String> dataBean;
    private int number=0;

    public ImageAdapter(List<String> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            if(dataBean.size()>=3){
                number=dataBean.size();
                s =3;
            }else {
                s = dataBean.size();
            }
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (dataBean.size() == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image1, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        }


        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
         Glide.with(mContext).load(dataBean.get(position)).skipMemoryCache(true).into(holder.icon);
         try {
             if (number == 0) {
                 holder.more_image_linear.setVisibility(View.GONE);
             } else if(number>3){
                 if (position == 2) {
                     holder.more_image_number.setText((number - 3) + "");
                     holder.more_image_linear.setVisibility(View.VISIBLE);
                 } else {
                     holder.more_image_linear.setVisibility(View.GONE);
                 }
             }
         }catch (Exception e){}

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JZCImageUtil.imageBrower(mContext, position, dataBean);
            }
        });

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        NineGridImageView icon;
        LinearLayout more_image_linear;
        TextView more_image_number;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = (NineGridImageView) itemView.findViewById(R.id.imageView);
            more_image_linear = (LinearLayout) itemView.findViewById(R.id.more_image_linear);
            more_image_number = (TextView) itemView.findViewById(R.id.more_image_number);
        }
    }

}
