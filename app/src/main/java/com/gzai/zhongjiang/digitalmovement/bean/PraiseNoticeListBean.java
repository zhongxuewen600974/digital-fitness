package com.gzai.zhongjiang.digitalmovement.bean;

public class PraiseNoticeListBean {
    private String id;
    private String post_id;
    private String user_id;
    private String to_user_id;
    private String comment_id;
    private String type;
    private String update_time;
    private String create_time;
    private String title;
    private String content;
    private String nick_name;
    private String avatar;
    private String comment_content;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getComment_content() {
        return comment_content;
    }

    public void setComment_content(String comment_content) {
        this.comment_content = comment_content;
    }

    public PraiseNoticeListBean(String id, String post_id, String user_id, String to_user_id, String comment_id, String type,
                                String update_time, String create_time, String title, String content, String nick_name, String avatar, String comment_content) {
        this.id = id;
        this.post_id = post_id;
        this.user_id = user_id;
        this.to_user_id = to_user_id;
        this.comment_id = comment_id;
        this.type = type;
        this.update_time = update_time;
        this.create_time = create_time;
        this.title = title;
        this.content = content;
        this.nick_name = nick_name;
        this.avatar = avatar;
        this.comment_content = comment_content;
    }

}
