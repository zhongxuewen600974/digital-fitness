package com.gzai.zhongjiang.digitalmovement.bean;

public class NoticeListBean {
    private String send_uid;
    private String msg_info;
    private String msg_type;
    private String unread;
    private String update_time;
    private String refer_id;
    private String refer_type;
    private String send_nickname;
    private String send_avatar;

    public String getSend_avatar() {
        return send_avatar;
    }

    public void setSend_avatar(String send_avatar) {
        this.send_avatar = send_avatar;
    }

    public NoticeListBean(String send_uid, String msg_info, String msg_type, String unread, String update_time, String refer_id, String refer_type, String send_nickname, String send_avatar){
        this.send_uid=send_uid;
        this.msg_info=msg_info;
        this.msg_type=msg_type;
        this.unread=unread;
        this.update_time=update_time;
        this.refer_id=refer_id;
        this.refer_type=refer_type;
        this.send_nickname=send_nickname;
        this.send_avatar=send_avatar;
    }

    public String getSend_uid() {
        return send_uid;
    }

    public void setSend_uid(String send_uid) {
        this.send_uid = send_uid;
    }

    public String getMsg_info() {
        return msg_info;
    }

    public void setMsg_info(String msg_info) {
        this.msg_info = msg_info;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public String getUnread() {
        return unread;
    }

    public void setUnread(String unread) {
        this.unread = unread;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getRefer_id() {
        return refer_id;
    }

    public void setRefer_id(String refer_id) {
        this.refer_id = refer_id;
    }

    public String getRefer_type() {
        return refer_type;
    }

    public void setRefer_type(String refer_type) {
        this.refer_type = refer_type;
    }

    public String getSend_nickname() {
        return send_nickname;
    }

    public void setSend_nickname(String send_nickname) {
        this.send_nickname = send_nickname;
    }
}
