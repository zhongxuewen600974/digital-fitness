package com.gzai.zhongjiang.digitalmovement.bean;

public class MessageEventBus {
    private Object message;
    private String messageType;

    public MessageEventBus(String messageType, Object message){
        this.message=message;
        this.messageType = messageType;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
}
