package com.gzai.zhongjiang.digitalmovement.bean;

public class LabelTextBean {
    private boolean onclick;
    private String label_id;
    private String label_name;
    private String sort;

    public boolean isOnclick() {
        return onclick;
    }

    public void setOnclick(boolean onclick) {
        this.onclick = onclick;
    }

    public String getLabel_id() {
        return label_id;
    }

    public void setLabel_id(String label_id) {
        this.label_id = label_id;
    }

    public String getLabel_name() {
        return label_name;
    }

    public void setLabel_name(String label_name) {
        this.label_name = label_name;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
