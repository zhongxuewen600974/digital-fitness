package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.StepListBean;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.util.List;

public class StepTextAdapter extends RecyclerView.Adapter<StepTextAdapter.ViewHolder> {
    private Context mContext;
    private List<StepListBean> dataBean;

    public StepTextAdapter(List<StepListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_step_text, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.step_day.setText(dataBean.get(position).getRecord_date().substring(5,7)+"/"+dataBean.get(position).getRecord_date().substring(8));
        } catch (Exception e) {
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView step_day;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            step_day = (TextView) itemView.findViewById(R.id.step_day);

        }
    }

}
