package com.gzai.zhongjiang.digitalmovement.home;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.CircleListAdapter;
import com.gzai.zhongjiang.digitalmovement.bean.CircleListBean;
import com.gzai.zhongjiang.digitalmovement.bean.MessageEventBus;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;


public class MyCircleFragment extends Fragment {
    RecyclerView recyclerView;
    SmartRefreshLayout smartRefreshLayout;
    private int page_total, page = 1;
    LinearLayout onData, haveData;
    CircleListBean myBean;
    List<CircleListBean> beanList = new ArrayList<>();
    CircleListAdapter myAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_my_circle, container, false);
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        onData = view.findViewById(R.id.nodata_linear);
        haveData = view.findViewById(R.id.data_linear);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        smartRefreshLayout = view.findViewById(R.id.smartRefreshLayout);
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            page=1;
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                             //   ToastUtils.show("已全部加载");
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void EventBusListen(MessageEventBus messageEvent) {
        switch (messageEvent.getMessageType()) {
            case "refresh":
                intView();
                break;
        }
    }
    private void intView() {
        beanList.clear();
        RequestUtils.getCircleList(SharePreUtil.getString(getContext(), "token", ""), 1, 10, new ListMyObserver<ListBean<CircleListBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<CircleListBean> result) {
                ListBean<CircleListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String isjoin = list.getList().get(i).getIsjoin();
                        if(isjoin.equals("1")) {
                            String id = list.getList().get(i).getId();
                            String circle_name = list.getList().get(i).getCircle_name();
                            String circle_image = list.getList().get(i).getCircle_image();
                            String members = list.getList().get(i).getMembers();
                            String create_time = list.getList().get(i).getCreate_time();
                            List join_list = list.getList().get(i).getJoin_list();
                            myBean = new CircleListBean(id, circle_name, circle_image, members, create_time, isjoin, join_list);
                            beanList.add(myBean);
                        }else {

                        }
                    }
                    myAdapter = new CircleListAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(int page) {
        RequestUtils.getCircleList(SharePreUtil.getString(getContext(), "token", ""), page, 10, new ListMyObserver<ListBean<CircleListBean>>(getContext()) {
            @Override
            public void onSuccess(ListBean<CircleListBean> result) {
                ListBean<CircleListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    onData.setVisibility(View.GONE);
                    haveData.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String isjoin = list.getList().get(i).getIsjoin();
                        if(isjoin.equals("1")) {
                            String id = list.getList().get(i).getId();
                            String circle_name = list.getList().get(i).getCircle_name();
                            String circle_image = list.getList().get(i).getCircle_image();
                            String members = list.getList().get(i).getMembers();
                            String create_time = list.getList().get(i).getCreate_time();
                            List join_list = list.getList().get(i).getJoin_list();
                            myBean = new CircleListBean(id, circle_name, circle_image, members, create_time, isjoin, join_list);
                            beanList.add(myBean);
                            myAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}