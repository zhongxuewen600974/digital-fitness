package com.gzai.zhongjiang.digitalmovement.coach;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.GoodAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.PersonalVipAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanCourseBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.interfaces.SimpleCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EliminateClassActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.tel)
    ImageView tel;
    @BindView(R.id.good_recyclerView)
    RecyclerView good_recyclerView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.post)
    TextView post;


    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;


    List<String> GoodList = new ArrayList<>();
    GoodAdapter goodAdapter;
    String qr_data, coach_id = "", order_id = "", card_id = "", avatar = "";
    private int cb = 1;


    PersonalVipBean dyBean;
    List<PersonalVipBean> beanList = new ArrayList<>();
    PersonalVipAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminate_class);
        ButterKnife.bind(this);
        back.setOnClickListener(this);
        post.setOnClickListener(this);
        tel.setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        good_recyclerView.setLayoutManager(layoutManager);
        try {
            Intent intent = getIntent();
            qr_data = intent.getStringExtra("qr_data");
            intView();
        } catch (Exception e) {

        }


    }

    private void getMyPersonalVip() {
        beanList.clear();
        RequestUtils.getMyPersonalVip(SharePreUtil.getString(this, "token", ""), new MyObserver<List<PersonalVipBean>>(this) {
            @Override
            public void onSuccess(List<PersonalVipBean> result) {
                List<PersonalVipBean> list = result;
                if (list.size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.size(); i++) {
                        String id = list.get(i).getId();
                        String gym_id = list.get(i).getGym_id();
                        String coach_id = list.get(i).getCoach_id();
                        String user_id = list.get(i).getUser_id();
                        String card_id = list.get(i).getCard_id();
                        String expire_time = list.get(i).getExpire_time();
                        int times = list.get(i).getTimes();
                        int totals = list.get(i).getTotals();
                        String status = list.get(i).getStatus();
                        String last_time = list.get(i).getLast_time();
                        String update_time = list.get(i).getUpdate_time();
                        String create_time = list.get(i).getCreate_time();
                        String room_name = list.get(i).getRoom_name();
                        String coach_name = list.get(i).getCoach_name();
                        dyBean = new PersonalVipBean(id, gym_id, coach_id, user_id, card_id, expire_time, times, totals, status, last_time, update_time, create_time, room_name, coach_name);
                        beanList.add(dyBean);
                    }
                    myAdapter = new PersonalVipAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                    myAdapter.setOnItemClickListener(new PersonalVipAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClickListener(String id) {
                            card_id = id;
                        }
                    });

                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    private void intView() {
        RequestUtils.scanCourse(SharePreUtil.getString(this, "token", ""), qr_data, new MyObserver<ScanCourseBean>(this) {
            @Override
            public void onSuccess(ScanCourseBean result) {
                try {
                    if (result.getOrder_id().equals("0")) {
                        cb = 2;
                        tel.setVisibility(View.GONE);
                        ToastUtils.show("请正确扫码");
                    } else {
                        tel.setVisibility(View.VISIBLE);
                        name.setText(result.getTruename());
                        coach_id = result.getCoach_id();
                        order_id = result.getOrder_id();
                        getCoachInfo(coach_id);
                        getMyPersonalVip();
                    }
                } catch (Exception e) {
                    ToastUtils.show("请正确扫码");
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void getCoachInfo(String user_id) {
        GoodList.clear();
        RequestUtils.getCoachInfo(SharePreUtil.getString(this, "token", ""), user_id, new MyObserver<CoachDetailInfo>(this) {
            @Override
            public void onSuccess(CoachDetailInfo result) {
                try {
                    if (result.getInfo().getAvatar().length() > 0) {
                        Glide.with(getBaseContext()).load(result.getInfo().getAvatar()).into(circleImageView);
                        avatar = result.getInfo().getAvatar();
                    } else {
                        circleImageView.setImageResource(R.drawable.mine_head_icon);
                    }
                    name.setText(result.getInfo().getTruename());
                    phone.setText(result.getInfo().getMobile());
                    String strcids = result.getInfo().getGood_at();
                    String[] strs = strcids.split(",");
                    for (String str : strs) {
                        GoodList.add(str);
                    }
                    goodAdapter = new GoodAdapter(GoodList);
                    good_recyclerView.setAdapter(goodAdapter);
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.post:
                if (cb == 1) {
                    if (order_id.length() > 0 && card_id.length() > 0) {
                        showsexShadow();
                    } else {
                        ToastUtils.show("请选择私教卡");
                    }
                } else {
                    ToastUtils.show("请重新正确扫码");
                }
                break;
            case R.id.tel:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + phone.getText().toString());
                intent.setData(data);
                startActivity(intent);
                break;
        }
    }


    private void updateScanOrder() {
        RequestUtils.updateScanOrder(SharePreUtil.getString(this, "token", ""), order_id, card_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                Intent Stingintent = new Intent(EliminateClassActivity.this, StudyOrderCommentActivity.class);
                Stingintent.putExtra("avatar", avatar);
                Stingintent.putExtra("coach_id", "");
                Stingintent.putExtra("name", name.getText().toString());
                Stingintent.putExtra("order_id", order_id);
                startActivity(Stingintent);
                ToastUtils.show("提交成功！");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }


    SexPopupView sexpopupView;

    private void showsexShadow() {
        if (sexpopupView == null) {
            sexpopupView = (SexPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new SexPopupView(this));
        }
        sexpopupView.show();
    }

    public class SexPopupView extends CenterPopupView {


        public SexPopupView(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_scanorder;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateScanOrder();
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }


}