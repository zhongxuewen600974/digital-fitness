package com.gzai.zhongjiang.digitalmovement.gym;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.GymInfo;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GymActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.room_area)
    TextView room_area;
    @BindView(R.id.coachs)
    TextView coachs;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.workings)
    TextView workings;
    @BindView(R.id.coachs1)
    TextView coachs1;
    @BindView(R.id.courses)
    TextView courses;
    @BindView(R.id.to_coach_text)
    TextView to_coach_text;
    @BindView(R.id.to_week_text)
    TextView to_week_text;
    @BindView(R.id.to_peo_text)
    TextView to_peo_text;
    @BindView(R.id.to_coach)
    LinearLayout to_coach;
    @BindView(R.id.to_peo_linear)
    LinearLayout to_peo_linear;
    @BindView(R.id.to_week_linear)
    LinearLayout to_week_linear;
    @BindView(R.id.call)
    ImageView call;


    private String dym_id = "", phone, lat = "", lng = "", room_name, room_adress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gym);
        ButterKnife.bind(this);
        actionBarView.setTitle("健身房");
        to_coach.setOnClickListener(this);
        to_coach_text.setOnClickListener(this);
        to_peo_linear.setOnClickListener(this);
        to_peo_text.setOnClickListener(this);
        to_week_linear.setOnClickListener(this);
        to_week_text.setOnClickListener(this);
        call.setOnClickListener(this);
        address.setOnClickListener(this);
        Intent intent = getIntent();
        dym_id = intent.getStringExtra("dym_id");
        intView(dym_id);
    }

    private void intView(String dym_id) {
        RequestUtils.getGymInfo(dym_id, new MyObserver<GymInfo>(this) {
            @Override
            public void onSuccess(GymInfo result) {
                Glide.with(getBaseContext()).load(result.getInfo().getImage()).into(imageView);
                name.setText(result.getInfo().getRoom_name());
                room_name = result.getInfo().getRoom_name();
                room_area.setText("面积:" + result.getInfo().getRoom_area() + "㎡");
                coachs.setText("教练:" + result.getInfo().getCoachs() + "人");
                address.setText(result.getInfo().getAddress());
                room_adress = result.getInfo().getAddress();
                workings.setText(result.getInfo().getWorkings());
                coachs1.setText(result.getInfo().getCoachs());
                courses.setText(result.getInfo().getCourses());
                phone = result.getInfo().getPhone();
                lat = result.getInfo().getLat();
                lng = result.getInfo().getLng();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.call:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                Uri data = Uri.parse("tel:" + phone);
                intent.setData(data);
                startActivity(intent);
                break;
            case R.id.to_coach:
                Intent Stingintent = new Intent(GymActivity.this, CoachListActivity.class);
                Stingintent.putExtra("dym_id", dym_id);
                startActivity(Stingintent);
                break;
            case R.id.to_coach_text:
                Intent Singintent = new Intent(GymActivity.this, CoachListActivity.class);
                Singintent.putExtra("dym_id", dym_id);
                startActivity(Singintent);
                break;
            case R.id.to_peo_linear:
                Intent Singintent1 = new Intent(GymActivity.this, PeopleActivity.class);
                Singintent1.putExtra("dym_id", dym_id);
                startActivity(Singintent1);
                break;
            case R.id.to_peo_text:
                Intent Singintent2 = new Intent(GymActivity.this, PeopleActivity.class);
                Singintent2.putExtra("dym_id", dym_id);
                startActivity(Singintent2);
                break;
            case R.id.to_week_text:
                Intent Singintent3 = new Intent(GymActivity.this, CourseWeekActivity.class);
                Singintent3.putExtra("dym_id", dym_id);
                startActivity(Singintent3);
                break;
            case R.id.to_week_linear:
                Intent Singintent4 = new Intent(GymActivity.this, CourseWeekActivity.class);
                Singintent4.putExtra("dym_id", dym_id);
                startActivity(Singintent4);
                break;
            case R.id.address:
                if (lat.length() > 0 && lng.length() > 0) {
                    Intent intent1 = new Intent(GymActivity.this, MapActivity.class);
                    intent1.putExtra("lng", lng);
                    intent1.putExtra("lat", lat);
                    intent1.putExtra("dym_name", room_name);
                    intent1.putExtra("dym_address", room_adress);
                    startActivity(intent1);
                }
                break;
        }
    }
}