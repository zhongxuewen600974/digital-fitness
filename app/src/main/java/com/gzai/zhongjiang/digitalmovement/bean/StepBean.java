package com.gzai.zhongjiang.digitalmovement.bean;

public class StepBean {
    private String id;
    private String nick_name;
    private String avatar;
    private String user_id;
    private String step_count;
    private String update_time;
    private String record_date;
    private String ispraise;
    private String praises;
    public StepBean(String id,String nick_name,String avatar,String user_id,String step_count,String update_time,String record_date,String ispraise,String praises){
        this.id=id;
        this.nick_name=nick_name;
        this.avatar=avatar;
        this.user_id=user_id;
        this.step_count=step_count;
        this.update_time=update_time;
        this.record_date=record_date;
        this.ispraise=ispraise;
        this.praises=praises;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIspraise() {
        return ispraise;
    }

    public void setIspraise(String ispraise) {
        this.ispraise = ispraise;
    }

    public String getPraises() {
        return praises;
    }

    public void setPraises(String praises) {
        this.praises = praises;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStep_count() {
        return step_count;
    }

    public void setStep_count(String step_count) {
        this.step_count = step_count;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getRecord_date() {
        return record_date;
    }

    public void setRecord_date(String record_date) {
        this.record_date = record_date;
    }
}
