package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.FollowBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.StudioBean;
import com.gzai.zhongjiang.digitalmovement.coach.StudentDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.CenterPopupView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class StudyAdapter extends RecyclerView.Adapter<StudyAdapter.ViewHolder> {
    private Context mContext;
    private List<StudioBean> dataBean;


    public StudyAdapter(List<StudioBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_students, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }
    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            holder.name.setText(dataBean.get(position).getTruename());
            holder.expire_time.setText("私教卡到期："+stampToDate(dataBean.get(position).getExpire_time()));
            holder.times.setText("剩余"+(Integer.parseInt(dataBean.get(position).getTotals())-Integer.parseInt(dataBean.get(position).getTimes()))+"次");
        } catch (Exception e) {
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, StudentDetailsActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                Stingintent.putExtra("expire_time", dataBean.get(position).getExpire_time());
                Stingintent.putExtra("card_id", dataBean.get(position).getCard_id());
                Stingintent.putExtra("last_time", dataBean.get(position).getLast_time());
                Stingintent.putExtra("times", Integer.parseInt(dataBean.get(position).getTotals())-Integer.parseInt(dataBean.get(position).getTimes())+"");
                mContext.startActivity(Stingintent);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, expire_time,times;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            expire_time = (TextView) itemView.findViewById(R.id.expire_time);
            times = (TextView) itemView.findViewById(R.id.times);
        }
    }

}
