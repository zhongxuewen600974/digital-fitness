package com.gzai.zhongjiang.digitalmovement.coach;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.StudyAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.StudioBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PrivateStudentsActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarRoot;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.input_name)
    EditText input_name;


    StudioBean dyBean;
    List<StudioBean> beanList = new ArrayList<>();
    StudyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_students);
        ButterKnife.bind(this);
        actionBarRoot.setTitle("私教学员");

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            intView();
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        intView();
        input_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                find(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void find(String search_name) {
        beanList.clear();
        RequestUtils.getPersonalList(SharePreUtil.getString(this, "token", ""),search_name, new MyObserver<List<StudioBean>>(this) {
            @Override
            public void onSuccess(List<StudioBean> result) {
                List<StudioBean> list = result;
                if (list.size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.size(); i++) {
                        String id = list.get(i).getId();
                        String card_id = list.get(i).getCard_id();
                        String user_id = list.get(i).getUser_id();
                        String last_time = list.get(i).getLast_time();
                        String truename = list.get(i).getTruename();
                        String avatar = list.get(i).getAvatar();
                        String times = list.get(i).getTimes();
                        String totals = list.get(i).getTotals();
                        String expire_time = list.get(i).getExpire_time();
                        dyBean = new StudioBean(id, card_id, user_id, last_time, truename, avatar, times, totals, expire_time);
                        beanList.add(dyBean);
                    }
                    myAdapter = new StudyAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }




    private void intView() {
        beanList.clear();
        RequestUtils.getPersonalList(SharePreUtil.getString(this, "token", ""),"", new MyObserver<List<StudioBean>>(this) {
            @Override
            public void onSuccess(List<StudioBean> result) {
                List<StudioBean> list = result;
                if (list.size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.size(); i++) {
                        String id = list.get(i).getId();
                        String card_id = list.get(i).getCard_id();
                        String user_id = list.get(i).getUser_id();
                        String last_time = list.get(i).getLast_time();
                        String truename = list.get(i).getTruename();
                        String avatar = list.get(i).getAvatar();
                        String times = list.get(i).getTimes();
                        String totals = list.get(i).getTotals();
                        String expire_time = list.get(i).getExpire_time();
                        dyBean = new StudioBean(id, card_id, user_id, last_time, truename, avatar, times, totals, expire_time);
                        beanList.add(dyBean);
                    }
                    myAdapter = new StudyAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


}