package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.coach.NewStudentDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BookManageAdapter extends RecyclerView.Adapter<BookManageAdapter.ViewHolder> {
    private Context mContext;
    private List<MyCourse> dataBean;

    public BookManageAdapter(List<MyCourse> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book_manage, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.iAvatar);
            } else {
                holder.iAvatar.setImageResource(R.drawable.mine_head_icon);
            }
            if(stampToDate(dataBean.get(position).getEnd_time()).equals("00:00")){
                holder.time_slot.setText(stampToDate(dataBean.get(position).getStart_time())+"-"+"24:00");
            }else {
                holder.time_slot.setText(stampToDate(dataBean.get(position).getStart_time())+"-"+stampToDate(dataBean.get(position).getEnd_time()));
            }


            holder.name.setText(dataBean.get(position).getTruename());
            if(dataBean.get(position).getStatus().equals("0")){
                holder.status.setBackgroundResource(R.drawable.textview_12);
                holder.status.setText("待确认");
            }else if(dataBean.get(position).getStatus().equals("1")){
                holder.status.setBackgroundResource(R.drawable.textview_13);
                holder.status.setText("已确认");
            }else {
                holder.status.setBackgroundResource(R.drawable.textview_14);
                holder.status.setText("已取消");
            }



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Stingintent = new Intent(mContext, NewStudentDetailsActivity.class);
                    Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                    Stingintent.putExtra("start_time",stampToDate1(dataBean.get(position).getStart_time()) );
                    Stingintent.putExtra("end_time",stampToDate1(dataBean.get(position).getEnd_time()));
                    Stingintent.putExtra("coach_id", dataBean.get(position).getCoach_id());
                    Stingintent.putExtra("id", dataBean.get(position).getId());
                    Stingintent.putExtra("status", holder.status.getText().toString());
                    mContext.startActivity(Stingintent);
                }
            });


        } catch (Exception e) {
        }
    }

    public static String stampToDate1(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }



    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView iAvatar;
        TextView time_slot,name,status;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iAvatar = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            time_slot = (TextView) itemView.findViewById(R.id.time_slot);
            name = (TextView) itemView.findViewById(R.id.name);
            status = (TextView) itemView.findViewById(R.id.status);
        }
    }

}
