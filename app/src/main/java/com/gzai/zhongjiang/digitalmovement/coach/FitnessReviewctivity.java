package com.gzai.zhongjiang.digitalmovement.coach;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.CoachCommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;


import butterknife.BindView;
import butterknife.ButterKnife;

public class FitnessReviewctivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.release)
    TextView release;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.input_release)
    EditText input_release;
    @BindView(R.id.release_number)
    TextView release_number;

    String user_id, order_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness_reviewctivity);
        ButterKnife.bind(this);
        back.setOnClickListener(this);
        release.setOnClickListener(this);
        input_release.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                release_number.setText(s.length() + "/" + "300");
                if (s.length() >= 300) {
                    release_number.setTextColor(Color.parseColor("#E91E63"));
                } else {
                    release_number.setTextColor(Color.parseColor("#333333"));

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        try {
            Intent intent = getIntent();
            String truename = intent.getStringExtra("truename");
            name.setText(truename + "学员健身点评");
            user_id = intent.getStringExtra("user_id");
            order_id = intent.getStringExtra("order_id");
            String avatar = intent.getStringExtra("avatar");
            if (avatar.length() > 0) {
                Glide.with(this).load(avatar).into(circleImageView);
            } else {
                circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
        } catch (Exception e) {

        }
        intView();

    }

    private void intView() {
        RequestUtils.getCoachComment(SharePreUtil.getString(this, "token", ""), order_id, user_id, new MyObserver<CoachCommentBean>(this) {
            @Override
            public void onSuccess(CoachCommentBean result) {
                if (result.getContent().length() > 0) {
                    input_release.setText(result.getContent());
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.release:
                if (input_release.getText().toString().length() > 0) {
                    addCoachComment(input_release.getText().toString());
                } else {
                    ToastUtils.show("请输入评论");
                }
                break;
        }
    }

    private void addCoachComment(String content) {
        RequestUtils.addCoachComment(SharePreUtil.getString(this, "token", ""), order_id, user_id, content, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("提交成功！");
                finish();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }
}