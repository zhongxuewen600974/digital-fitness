package com.gzai.zhongjiang.digitalmovement.bean;

import java.util.List;

public class ChildList {
    List<CommentChild> comment_list;
    PageInfo child_page_info;

    public List<CommentChild> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<CommentChild> comment_list) {
        this.comment_list = comment_list;
    }

    public PageInfo getChild_page_info() {
        return child_page_info;
    }

    public void setChild_page_info(PageInfo child_page_info) {
        this.child_page_info = child_page_info;
    }
}
