package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
 import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
 import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

 import com.gzai.zhongjiang.digitalmovement.R;
 import com.gzai.zhongjiang.digitalmovement.bean.LabelBean;

import java.text.SimpleDateFormat;
 import java.util.Date;
import java.util.List;

public class LabelAdapter extends RecyclerView.Adapter<LabelAdapter.ViewHolder> {
    private Context mContext;
    private List<LabelBean> dataBean;

    private OnItemClickListener onItemClickListener;
    private OnItemClickListener onItemClickListener1;
    private OnItemClickListener onItemClickListener2;
    private OnItemClickListener onItemClickListener3;
    private OnItemClickListener onItemClickListener4;



    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.onItemClickListener1 = onItemClickListener;
        this.onItemClickListener2 = onItemClickListener;
        this.onItemClickListener3 = onItemClickListener;
        this.onItemClickListener4 = onItemClickListener;
    }


    public LabelAdapter(List<LabelBean> list) {
        this.dataBean = list;

    }

    public interface OnItemClickListener {
        void onItemClickListener(int position, String isfollow, String id);

        void OnItemClickListener1(int i, int position, int id);

        void OnItemClickListener2(int position, int id, int num);

        void OnItemClickListener3(int position, int id);

        void OnItemClickListener4(int position, int id);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return dataBean.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_label, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (position == 0) {
                holder.imageView.setImageResource(R.drawable.label_1);
            } else if (position == 1) {
                holder.imageView.setImageResource(R.drawable.label_2);
            } else if (position == 2) {
                holder.imageView.setImageResource(R.drawable.label_3);
            } else if (position == 3) {
                holder.imageView.setImageResource(R.drawable.label_4);
            } else if (position == 4) {
                holder.imageView.setImageResource(R.drawable.label_5);
            } else if (position == 6){
                holder.imageView.setImageResource(R.drawable.label_6);
            }else if (position == 7){
                holder.imageView.setImageResource(R.drawable.label_1);
            }else if (position == 8) {
                holder.imageView.setImageResource(R.drawable.label_2);
            } else if (position == 9) {
                holder.imageView.setImageResource(R.drawable.label_3);
            } else if (position == 10) {
                holder.imageView.setImageResource(R.drawable.label_4);
            } else if (position == 11) {
                holder.imageView.setImageResource(R.drawable.label_5);
            } else if (position == 12){
                holder.imageView.setImageResource(R.drawable.label_6);
            }else if (position == 13) {
                holder.imageView.setImageResource(R.drawable.label_2);
            } else if (position == 14) {
                holder.imageView.setImageResource(R.drawable.label_3);
            } else if (position == 15) {
                holder.imageView.setImageResource(R.drawable.label_4);
            } else if (position == 16) {
                holder.imageView.setImageResource(R.drawable.label_5);
            } else {
                holder.imageView.setImageResource(R.drawable.label_6);
            }
            holder.label_name.setText(dataBean.get(position).getType_name());
            holder.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));


        } catch (Exception e) {
        }
//        pAdapterList.get(position).setOnItemClickListener(new LabelTextAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClickListener(int position, String id, String lab_name) {
//                SharePreUtil.putString(mContext, "label_id", id);
//                SharePreUtil.putString(mContext, "label_name", lab_name);
//            }
//        });
        LabelTextAdapter labelTextAdapter = new LabelTextAdapter(dataBean.get(position).getLabel_list());
        holder.recyclerView.setAdapter(labelTextAdapter);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView label_name;
        RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            label_name = (TextView) itemView.findViewById(R.id.label_name);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerView);
        }
    }
}
