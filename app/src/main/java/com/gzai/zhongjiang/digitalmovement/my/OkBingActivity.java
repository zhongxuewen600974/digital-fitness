package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OkBingActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok_bing);
        ButterKnife.bind(this);
        actionBarView.setTitle("绑定手机号");
    }
}