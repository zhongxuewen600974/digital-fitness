package com.gzai.zhongjiang.digitalmovement.bean;

public class GymBean {
    private String id;
    private String room_name;
    private String room_area;
    private String coachs;
    private String address;
    private String image;
    private String workings;
    private String courses;
    private String lng;
    private String lat;
    private String sort;
    private String status;
    private String update_time;
    private String create_time;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getRoom_area() {
        return room_area;
    }

    public void setRoom_area(String room_area) {
        this.room_area = room_area;
    }

    public String getCoachs() {
        return coachs;
    }

    public void setCoachs(String coachs) {
        this.coachs = coachs;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWorkings() {
        return workings;
    }

    public void setWorkings(String workings) {
        this.workings = workings;
    }

    public String getCourses() {
        return courses;
    }

    public void setCourses(String courses) {
        this.courses = courses;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public GymBean(String id, String room_name, String room_area, String coachs, String address, String image, String workings, String courses,
                   String lng, String lat, String sort, String status, String update_time, String create_time) {

        this.id = id;
        this.room_name = room_name;
        this.room_area = room_area;
        this.coachs = coachs;
        this.address = address;
        this.image = image;
        this.workings = workings;
        this.courses = courses;
        this.lng = lng;
        this.lat = lat;
        this.sort = sort;
        this.status = status;
        this.update_time = update_time;
        this.create_time = create_time;

    }
}
