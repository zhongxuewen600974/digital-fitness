package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.view.selectImage.NineGridImageView;
import com.gzai.zhongjiang.digitalmovement.view.wetchimage.JZCImageUtil;

import java.util.List;

public class MyImageAdapter extends RecyclerView.Adapter<MyImageAdapter.ViewHolder> {
    private Context mContext;
    private List<String> dataBean;


    public MyImageAdapter(List<String> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {

            s = dataBean.size();

        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        if (dataBean.size() == 1) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image1, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false);
        }


        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(mContext).load(dataBean.get(position)).skipMemoryCache(true).into(holder.icon);
        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JZCImageUtil.imageBrower(mContext, position, dataBean);
            }
        });

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        NineGridImageView icon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            icon = (NineGridImageView) itemView.findViewById(R.id.imageView);
        }
    }

}
