package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.StudioBean;
import com.gzai.zhongjiang.digitalmovement.coach.NewStudentDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DailogStudyAdapter extends RecyclerView.Adapter<DailogStudyAdapter.ViewHolder> {
    private Context mContext;
    private List<StudioBean> dataBean;


    public DailogStudyAdapter(List<StudioBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dailog_students, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }
    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            holder.name.setText(dataBean.get(position).getTruename());
            holder.last_time.setText("最近课程："+stampToDate(dataBean.get(position).getLast_time()));
        } catch (Exception e) {
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, NewStudentDetailsActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                Stingintent.putExtra("start_time", dataBean.get(position).getStart_time());
                Stingintent.putExtra("end_time", dataBean.get(position).getEnd_time());
                Stingintent.putExtra("coach_id", dataBean.get(position).getCoach_id());
                Stingintent.putExtra("id", dataBean.get(position).getId());
                Stingintent.putExtra("status","预定中");
                mContext.startActivity(Stingintent);
            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, last_time;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            last_time = (TextView) itemView.findViewById(R.id.last_time);
        }
    }

}
