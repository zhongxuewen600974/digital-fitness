package com.gzai.zhongjiang.digitalmovement.home;

import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.gzai.zhongjiang.digitalmovement.R;

import java.util.ArrayList;
import java.util.List;

import static com.gzai.zhongjiang.digitalmovement.MyApplication.context;


public class HomeFragment extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Fragment> fragments = new ArrayList<>();
    TextView toMyTextView, toMyTextView1, toMyTextView2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        tabLayout = (TabLayout) view.findViewById(R.id.home_TabLayout);
        tabLayout.setTabTextColors(Color.parseColor("#CCCCCC"), Color.parseColor("#333333"));
        viewPager = (ViewPager) view.findViewById(R.id.home_viewpager);
        fragments = new ArrayList<Fragment>();
        Fragment squareFragment = new SquareFragment();
        Fragment recommendFragment = new RecommendFragment();
        Fragment circleFragment = new CircleFragment();
        fragments.add(squareFragment);
        fragments.add(recommendFragment);
        fragments.add(circleFragment);
        viewPager.setAdapter(new PagerAdapter(getChildFragmentManager()));
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        changeTabTextView(tabLayout.getTabAt(0),true);
        changeTabTextView(tabLayout.getTabAt(1),false);
        changeTabTextView(tabLayout.getTabAt(2),false);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                changeTabTextView(tab, true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                changeTabTextView(tab, false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                changeTabTextView(tab, false);
            }
        });
        return view;
    }

    public void changeTabTextView(TabLayout.Tab tab, boolean isBold) {
        View view = tab.getCustomView();
        if (null == view) {
            tab.setCustomView(R.layout.tab_layout_text);
        }
        TextView textView = tab.getCustomView().findViewById(R.id.tab_textView);
        textView.setText(tab.getText());
        if (isBold) {
            textView.setTextAppearance(context, R.style.MyTabLayoutTextAppearance);
            textView.setTextColor(Color.parseColor("#333333"));
        } else {
            textView.setTextAppearance(context, R.style.MyTabLayoutTextAppearanceInverse);
            textView.setTextColor(Color.parseColor("#CCCCCC"));
        }
    }


    private class PagerAdapter extends FragmentPagerAdapter {

        public PagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {


            return fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String t = "";
            if (position == 0) {
                t = "广场";

            } else if (position == 1) {
                t = "关注";
            } else if (position == 2) {
                t = "圈子";
            }
            return t;
        }
    }

}