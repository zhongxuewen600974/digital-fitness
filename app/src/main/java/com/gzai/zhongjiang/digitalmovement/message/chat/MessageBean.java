package com.gzai.zhongjiang.digitalmovement.message.chat;


public class MessageBean  {
    private String msg_info;
    private String msg_type;
    private String msg_id;
    private String send_uid;
    private String send_nickname;
    private String send_avatar;
    private String receive_uid;
    private String receive_avatar;
    private String receive_nickname;
    private String create_time;

    public MessageBean(String send_uid,String msg_id,String msg_info,String msg_type,String create_time,String send_nickname,String send_avatar,String receive_uid,String receive_nickname,String receive_avatar){
        this.send_uid=send_uid;
        this.msg_id=msg_id;
        this.msg_info=msg_info;
        this.msg_type=msg_type;
        this.create_time=create_time;
        this.send_nickname=send_nickname;
        this.send_avatar=send_avatar;
        this.receive_uid=receive_uid;
        this.receive_nickname=receive_nickname;
        this.receive_avatar=receive_avatar;
    }

    public String getMsg_info() {
        return msg_info;
    }

    public void setMsg_info(String msg_info) {
        this.msg_info = msg_info;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public String getMsg_id() {
        return msg_id;
    }

    public void setMsg_id(String msg_id) {
        this.msg_id = msg_id;
    }

    public String getSend_uid() {
        return send_uid;
    }

    public void setSend_uid(String send_uid) {
        this.send_uid = send_uid;
    }

    public String getSend_nickname() {
        return send_nickname;
    }

    public void setSend_nickname(String send_nickname) {
        this.send_nickname = send_nickname;
    }

    public String getSend_avatar() {
        return send_avatar;
    }

    public void setSend_avatar(String send_avatar) {
        this.send_avatar = send_avatar;
    }

    public String getReceive_uid() {
        return receive_uid;
    }

    public void setReceive_uid(String receive_uid) {
        this.receive_uid = receive_uid;
    }

    public String getReceive_avatar() {
        return receive_avatar;
    }

    public void setReceive_avatar(String receive_avatar) {
        this.receive_avatar = receive_avatar;
    }

    public String getReceive_nickname() {
        return receive_nickname;
    }

    public void setReceive_nickname(String receive_nickname) {
        this.receive_nickname = receive_nickname;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

 }
