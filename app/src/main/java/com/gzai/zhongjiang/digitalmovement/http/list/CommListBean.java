package com.gzai.zhongjiang.digitalmovement.http.list;

import com.gzai.zhongjiang.digitalmovement.bean.PageInfo;

import java.util.List;

public class CommListBean<T> {
    private List<T> comment_list;
    PageInfo page_info;


    public List<T> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<T> comment_list) {
        this.comment_list = comment_list;
    }

    public PageInfo getPage_info() {
        return page_info;
    }

    public void setPage_info(PageInfo page_info) {
        this.page_info = page_info;
    }
}
