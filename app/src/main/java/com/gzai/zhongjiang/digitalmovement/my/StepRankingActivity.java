package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.PeoPleAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.StepAdapter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.GymRegionBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.StepBean;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.list.NewListBean;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepRankingActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.rank)
    TextView rank;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.step)
    TextView step;
    @BindView(R.id.pairse)
    TextView pairse;
    @BindView(R.id.praises_image)
    ImageView praises_image;
    @BindView(R.id.mystep_linear)
    LinearLayout mystep_linear;


    String datedate;
    List<StepBean> datalist = new ArrayList<>();
    StepBean stepBean;
    StepAdapter stepAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_step_ranking);
        ButterKnife.bind(this);
        actionBarView.setTitle("步数排名");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        datedate = sdf.format(new Date());
        intView(datedate);
    }

    private void intView(String datedate) {
        datalist.clear();
        RequestUtils.getStepRank(SharePreUtil.getString(this, "token", ""), datedate, new ListMyObserver<NewListBean<StepBean>>(this) {
            @Override
            public void onSuccess(NewListBean<StepBean> result) {
                NewListBean<StepBean> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String nick_name = list.getList().get(i).getNick_name();
                        String avatar = list.getList().get(i).getAvatar();
                        String user_id = list.getList().get(i).getUser_id();
                        String step_count = list.getList().get(i).getStep_count();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String record_date = list.getList().get(i).getRecord_date();
                        String ispraise = list.getList().get(i).getIspraise();
                        String praises = list.getList().get(i).getPraises();
                        if (user_id.equals(SharePreUtil.getString(getBaseContext(), "user_id", ""))) {
                            mystep_linear.setVisibility(View.VISIBLE);
                            rank.setText((i + 1) + "");
                            if(avatar.length()>0) {
                                Glide.with(getBaseContext()).load(avatar).into(circleImageView);
                            }else {
                                Glide.with(getBaseContext()).load(R.drawable.mine_head_icon).into(circleImageView);
                            }
                            name.setText(nick_name);
                            step.setText(step_count);
                            pairse.setText(praises);
                            if (ispraise.equals("0")) {
                                praises_image.setImageResource(R.drawable.dianzan_fase);
                            } else {
                                praises_image.setImageResource(R.drawable.home_dianzan);
                                praises_image.setEnabled(false);
                            }
                            praises_image.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    addStepPraise(id);
                                }
                            });

                        }
                        stepBean = new StepBean(id, nick_name, avatar, user_id, step_count, update_time, record_date, ispraise, praises);
                        datalist.add(stepBean);
                    }
                    stepAdapter = new StepAdapter(datalist);
                    recyclerView.setAdapter(stepAdapter);
                } else {
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

                ToastUtils.show(errorMsg);

            }
        });
    }

    private void addStepPraise(String id) {
        RequestUtils.addStepPraise(SharePreUtil.getString(this, "token", ""), id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                intView(datedate);
                praises_image.setEnabled(false);
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }

}