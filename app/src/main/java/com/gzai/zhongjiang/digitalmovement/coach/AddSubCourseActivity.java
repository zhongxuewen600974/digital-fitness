package com.gzai.zhongjiang.digitalmovement.coach;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.MainActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.CountListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.interfaces.SimpleCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddSubCourseActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarRoot;
    @BindView(R.id.circleImageView)
    CircleImageView circleImageView;
    @BindView(R.id.truename)
    TextView truename;
    @BindView(R.id.last_time)
    TextView last_time;
    @BindView(R.id.image_slot_1)
    ImageView image_slot_1;
    @BindView(R.id.image_slot_2)
    ImageView image_slot_2;
    @BindView(R.id.image_slot_3)
    ImageView image_slot_3;
    @BindView(R.id.image_slot_4)
    ImageView image_slot_4;
    @BindView(R.id.image_slot_5)
    ImageView image_slot_5;
    @BindView(R.id.image_slot_6)
    ImageView image_slot_6;
    @BindView(R.id.image_slot_7)
    ImageView image_slot_7;
    @BindView(R.id.image_slot_8)
    ImageView image_slot_8;
    @BindView(R.id.image_slot_9)
    ImageView image_slot_9;
    @BindView(R.id.image_slot_10)
    ImageView image_slot_10;
    @BindView(R.id.image_slot_11)
    ImageView image_slot_11;
    @BindView(R.id.image_slot_12)
    ImageView image_slot_12;
    @BindView(R.id.image_slot_13)
    ImageView image_slot_13;
    @BindView(R.id.time_slot_1)
    TextView time_slot_1;
    @BindView(R.id.time_slot_2)
    TextView time_slot_2;
    @BindView(R.id.time_slot_3)
    TextView time_slot_3;
    @BindView(R.id.time_slot_4)
    TextView time_slot_4;
    @BindView(R.id.time_slot_5)
    TextView time_slot_5;
    @BindView(R.id.time_slot_6)
    TextView time_slot_6;
    @BindView(R.id.time_slot_7)
    TextView time_slot_7;
    @BindView(R.id.time_slot_8)
    TextView time_slot_8;
    @BindView(R.id.time_slot_9)
    TextView time_slot_9;
    @BindView(R.id.time_slot_10)
    TextView time_slot_10;
    @BindView(R.id.time_slot_11)
    TextView time_slot_11;
    @BindView(R.id.time_slot_12)
    TextView time_slot_12;
    @BindView(R.id.time_slot_13)
    TextView time_slot_13;
    @BindView(R.id.d_week_1)
    TextView d_week_1;
    @BindView(R.id.d_week_2)
    TextView d_week_2;
    @BindView(R.id.d_week_3)
    TextView d_week_3;
    @BindView(R.id.d_week_4)
    TextView d_week_4;
    @BindView(R.id.d_week_5)
    TextView d_week_5;
    @BindView(R.id.d_week_6)
    TextView d_week_6;
    @BindView(R.id.d_week_7)
    TextView d_week_7;
    @BindView(R.id.determine)
    TextView determine;

    private String today = getLastDayOfWeek(0);
    private String start_time = "", end_time = "", coach_id;
    Boolean select1 = false, select2 = false, select3 = false, select4 = false, select5 = false, select6 = false,
            select7 = false, select8 = false, select9 = false, select10 = false, select11 = false, select12 = false,select13 = false;


    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sub_course);
        ButterKnife.bind(this);
        actionBarRoot.setTitle("添加预约");
        try {
            Intent intent = getIntent();
            user_id = intent.getStringExtra("user_id");
            truename.setText(intent.getStringExtra("truename"));
            last_time.setText(intent.getStringExtra("last_time"));
            String avatar = intent.getStringExtra("avatar");
            if (avatar.length() > 0) {
                Glide.with(AddSubCourseActivity.this).load(avatar).into(circleImageView);
            } else {
                circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            coach_id = intent.getStringExtra("coach_id");
            intView(getLastDayOfWeek(0));
        } catch (Exception e) {

        }

        d_week_1.setText("今天(周" + TodayWeek(0) + ")");
        d_week_2.setText("明天(周" + TodayWeek(1) + ")");
        d_week_3.setText(getLastDayOfWeek(2).substring(5, 10) + "(周" + TodayWeek(2) + ")");
        d_week_4.setText(getLastDayOfWeek(3).substring(5, 10) + "(周" + TodayWeek(3) + ")");
        d_week_5.setText(getLastDayOfWeek(4).substring(5, 10) + "(周" + TodayWeek(4) + ")");
        d_week_6.setText(getLastDayOfWeek(5).substring(5, 10) + "(周" + TodayWeek(5) + ")");
        d_week_7.setText(getLastDayOfWeek(6).substring(5, 10) + "(周" + TodayWeek(6) + ")");


        determine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (start_time.length() > 0 && end_time.length() > 0) {
                    showsexShadow();
                } else {
                    ToastUtils.show("请选择时间段");
                }
            }
        });

        d_week_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d_week_1.setTextColor(Color.parseColor("#01D66A"));
                d_week_1.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                today = getLastDayOfWeek(0);
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(0));
            }
        });

        d_week_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                today = getLastDayOfWeek(1);
                d_week_2.setTextColor(Color.parseColor("#01D66A"));
                d_week_2.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(1));
            }
        });

        d_week_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                today = getLastDayOfWeek(2);
                d_week_3.setTextColor(Color.parseColor("#01D66A"));
                d_week_3.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(2));
            }
        });

        d_week_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                today = getLastDayOfWeek(3);
                d_week_4.setTextColor(Color.parseColor("#01D66A"));
                d_week_4.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(3));
            }
        });
        d_week_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                today = getLastDayOfWeek(4);
                d_week_5.setTextColor(Color.parseColor("#01D66A"));
                d_week_5.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(4));
            }
        });

        d_week_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                today = getLastDayOfWeek(5);
                d_week_6.setTextColor(Color.parseColor("#01D66A"));
                d_week_6.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_7.setTextColor(Color.parseColor("#111111"));
                d_week_7.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(5));
            }
        });

        d_week_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                today = getLastDayOfWeek(6);
                d_week_7.setTextColor(Color.parseColor("#01D66A"));
                d_week_7.setBackgroundColor(Color.parseColor("#FFFFFFFF"));
                d_week_1.setTextColor(Color.parseColor("#111111"));
                d_week_1.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_2.setTextColor(Color.parseColor("#111111"));
                d_week_2.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_3.setTextColor(Color.parseColor("#111111"));
                d_week_3.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_5.setTextColor(Color.parseColor("#111111"));
                d_week_5.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_4.setTextColor(Color.parseColor("#111111"));
                d_week_4.setBackgroundColor(Color.parseColor("#F7F7F7"));
                d_week_6.setTextColor(Color.parseColor("#111111"));
                d_week_6.setBackgroundColor(Color.parseColor("#F7F7F7"));
                time_slot_1.setSelected(false);
                time_slot_2.setSelected(false);
                time_slot_3.setSelected(false);
                time_slot_4.setSelected(false);
                time_slot_5.setSelected(false);
                time_slot_6.setSelected(false);
                time_slot_7.setSelected(false);
                time_slot_8.setSelected(false);
                time_slot_9.setSelected(false);
                time_slot_10.setSelected(false);
                time_slot_11.setSelected(false);
                time_slot_12.setSelected(false);
                time_slot_13.setSelected(false);
                time_slot_1.setTextColor(Color.parseColor("#111111"));
                time_slot_2.setTextColor(Color.parseColor("#111111"));
                time_slot_3.setTextColor(Color.parseColor("#111111"));
                time_slot_4.setTextColor(Color.parseColor("#111111"));
                time_slot_5.setTextColor(Color.parseColor("#111111"));
                time_slot_6.setTextColor(Color.parseColor("#111111"));
                time_slot_7.setTextColor(Color.parseColor("#111111"));
                time_slot_8.setTextColor(Color.parseColor("#111111"));
                time_slot_9.setTextColor(Color.parseColor("#111111"));
                time_slot_10.setTextColor(Color.parseColor("#111111"));
                time_slot_11.setTextColor(Color.parseColor("#111111"));
                time_slot_12.setTextColor(Color.parseColor("#111111"));
                time_slot_13.setTextColor(Color.parseColor("#111111"));
                select1 = false;
                select2 = false;
                select3 = false;
                select4 = false;
                select5 = false;
                select6 = false;
                select7 = false;
                select8 = false;
                select9 = false;
                select10 = false;
                select11 = false;
                select12 = false;
                select13 = false;
                start_time = "";
                end_time = "";
                intView(getLastDayOfWeek(6));
            }
        });

        time_slot_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select1) {
                    start_time = "";
                    end_time = "";
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    start_time = today + " 09:00:00";
                    end_time = today + " 10:00:00";
                    select1 = true;
                    time_slot_1.setSelected(true);
                    time_slot_1.setTextColor(Color.parseColor("#FFFFFFFF"));

                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });

        time_slot_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select2) {
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    start_time = today + " 10:00:00";
                    end_time = today + " 11:00:00";
                    select2 = true;
                    time_slot_2.setSelected(true);
                    time_slot_2.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });

        time_slot_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select3) {
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    start_time = today + " 11:00:00";
                    end_time = today + " 12:00:00";
                    select3 = true;
                    time_slot_3.setSelected(true);
                    time_slot_3.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });

        time_slot_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select4) {
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;

                } else {
                    select4 = true;
                    time_slot_4.setSelected(true);
                    time_slot_4.setTextColor(Color.parseColor("#FFFFFFFF"));
                    start_time = today + " 14:00:00";
                    end_time = today + " 15:00:00";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select5) {
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;

                } else {
                    start_time = today + " 15:00:00";
                    end_time = today + " 16:00:00";
                    select5 = true;
                    time_slot_5.setSelected(true);
                    time_slot_5.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select6) {
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    start_time = "";
                    end_time = "";

                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    start_time = today + " 16:00:00";
                    end_time = today + " 17:00:00";
                    select6 = true;
                    time_slot_6.setSelected(true);
                    time_slot_6.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select7) {
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    select7 = true;
                    start_time = today + " 17:00:00";
                    end_time = today + " 18:00:00";
                    time_slot_7.setSelected(true);
                    time_slot_7.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select8) {
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    select8 = true;
                    start_time = today + " 18:00:00";
                    end_time = today + " 19:00:00";
                    time_slot_8.setSelected(true);
                    time_slot_8.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select9) {
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select9 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    select9 = true;
                    start_time = today + " 19:00:00";
                    end_time = today + " 20:00:00";
                    time_slot_9.setSelected(true);
                    time_slot_9.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select10) {
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    start_time = today + " 20:00:00";
                    end_time = today + " 21:00:00";
                    select10 = true;
                    time_slot_10.setSelected(true);
                    time_slot_10.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });

        time_slot_11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select11) {
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select11 = false;
                    start_time = "";
                    end_time = "";
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    start_time = today + " 21:00:00";
                    end_time = today + " 22:00:00";
                    select11 = true;
                    time_slot_11.setSelected(true);
                    time_slot_11.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select12) {
                    start_time = "";
                    end_time = "";
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    select12 = true;
                    start_time = today + " 22:00:00";
                    end_time = today + " 23:00:00";
                    time_slot_12.setSelected(true);
                    time_slot_12.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                }
            }
        });
        time_slot_13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (select13) {
                    start_time = "";
                    end_time = "";
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_13.setSelected(false);
                    time_slot_13.setTextColor(Color.parseColor("#111111"));
                    select13 = false;
                } else {
                    select13 = true;
                    start_time = today + " 23:00:00";
                    end_time = today + " 24:00:00";
                    time_slot_13.setSelected(true);
                    time_slot_13.setTextColor(Color.parseColor("#FFFFFFFF"));
                    time_slot_3.setSelected(false);
                    time_slot_3.setTextColor(Color.parseColor("#111111"));
                    select3 = false;
                    time_slot_2.setSelected(false);
                    time_slot_2.setTextColor(Color.parseColor("#111111"));
                    select2 = false;
                    time_slot_1.setSelected(false);
                    time_slot_1.setTextColor(Color.parseColor("#111111"));
                    select1 = false;
                    time_slot_4.setSelected(false);
                    time_slot_4.setTextColor(Color.parseColor("#111111"));
                    select4 = false;
                    time_slot_5.setSelected(false);
                    time_slot_5.setTextColor(Color.parseColor("#111111"));
                    select5 = false;
                    time_slot_6.setSelected(false);
                    time_slot_6.setTextColor(Color.parseColor("#111111"));
                    select6 = false;
                    time_slot_7.setSelected(false);
                    time_slot_7.setTextColor(Color.parseColor("#111111"));
                    select7 = false;
                    time_slot_8.setSelected(false);
                    time_slot_8.setTextColor(Color.parseColor("#111111"));
                    select8 = false;
                    time_slot_9.setSelected(false);
                    time_slot_9.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_10.setSelected(false);
                    time_slot_10.setTextColor(Color.parseColor("#111111"));
                    select10 = false;
                    time_slot_11.setSelected(false);
                    time_slot_11.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                    time_slot_12.setSelected(false);
                    time_slot_12.setTextColor(Color.parseColor("#111111"));
                    select12 = false;
                }
            }
        });

    }

    private void intView(String record_date) {
        image_slot_1.setVisibility(View.GONE);
        time_slot_1.setEnabled(true);
        image_slot_2.setVisibility(View.GONE);
        time_slot_2.setEnabled(true);
        image_slot_3.setVisibility(View.GONE);
        time_slot_3.setEnabled(true);
        image_slot_4.setVisibility(View.GONE);
        time_slot_4.setEnabled(true);
        image_slot_5.setVisibility(View.GONE);
        time_slot_5.setEnabled(true);
        image_slot_6.setVisibility(View.GONE);
        time_slot_6.setEnabled(true);
        image_slot_7.setVisibility(View.GONE);
        time_slot_7.setEnabled(true);
        image_slot_8.setVisibility(View.GONE);
        time_slot_8.setEnabled(true);
        image_slot_9.setVisibility(View.GONE);
        time_slot_9.setEnabled(true);
        image_slot_10.setVisibility(View.GONE);
        time_slot_10.setEnabled(true);
        image_slot_11.setVisibility(View.GONE);
        time_slot_11.setEnabled(true);
        image_slot_12.setVisibility(View.GONE);
        time_slot_12.setEnabled(true);
        image_slot_13.setVisibility(View.GONE);
        time_slot_13.setEnabled(true);
        RequestUtils.getSubCourseList(SharePreUtil.getString(this, "token", ""), record_date, coach_id, "", new ListMyObserver<CountListBean<MyCourse>>(this) {
            @Override
            public void onSuccess(CountListBean<MyCourse> result) {
                CountListBean<MyCourse> list = result;
                if (list.getList().size() > 0) {
                    for (int i = 0; i < list.getList().size(); i++) {
                        String start_time = list.getList().get(i).getStart_time();
                        if (stampToDate(start_time).equals("09:00")) {
                            image_slot_1.setVisibility(View.VISIBLE);
                            time_slot_1.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("10:00")) {
                            image_slot_2.setVisibility(View.VISIBLE);
                            time_slot_2.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("11:00")) {
                            image_slot_3.setVisibility(View.VISIBLE);
                            time_slot_3.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("14:00")) {
                            image_slot_4.setVisibility(View.VISIBLE);
                            time_slot_4.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("15:00")) {
                            image_slot_5.setVisibility(View.VISIBLE);
                            time_slot_5.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("16:00")) {
                            image_slot_6.setVisibility(View.VISIBLE);
                            time_slot_6.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("17:00")) {
                            image_slot_7.setVisibility(View.VISIBLE);
                            time_slot_7.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("18:00")) {
                            image_slot_8.setVisibility(View.VISIBLE);
                            time_slot_8.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("19:00")) {
                            image_slot_9.setVisibility(View.VISIBLE);
                            time_slot_9.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("20:00")) {
                            image_slot_10.setVisibility(View.VISIBLE);
                            time_slot_10.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("21:00")) {
                            image_slot_11.setVisibility(View.VISIBLE);
                            time_slot_11.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("22:00")) {
                            image_slot_12.setVisibility(View.VISIBLE);
                            time_slot_12.setEnabled(false);
                        }
                        if (stampToDate(start_time).equals("23:00")) {
                            image_slot_13.setVisibility(View.VISIBLE);
                            time_slot_13.setEnabled(false);
                        }
                    }
                } else {

                }

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }


    public static String getLastDayOfWeek(int day) {
        Date dat = null;
        Calendar cd = Calendar.getInstance();
        cd.add(Calendar.DATE, day);
        dat = cd.getTime();
        SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd");
        return dformat.format(dat);
    }

    private String TodayWeek(int time) {
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);
        int weekday = c.get(Calendar.DAY_OF_WEEK) - 1 + time;


        if (weekday % 7 == 1) {
            return "一";
        } else if (weekday % 7 == 2) {
            return "二";
        } else if (weekday % 7 == 3) {
            return "三";
        } else if (weekday % 7 == 4) {
            return "四";
        } else if (weekday % 7 == 5) {
            return "五";
        } else if (weekday % 7 == 6) {
            return "六";
        } else if (weekday % 7 == 0) {
            return "日";
        }
        return "";
    }


    SexPopupView sexpopupView;

    private void showsexShadow() {
        if (sexpopupView == null) {
            sexpopupView = (SexPopupView) new XPopup.Builder(this)
                    .setPopupCallback(new SimpleCallback() {
                        @Override
                        public void onShow() {

                        }

                        @Override
                        public void onDismiss() {

                        }
                    })
                    .asCustom(new SexPopupView(this));
        }
        sexpopupView.show();
    }

    public class SexPopupView extends CenterPopupView {


        public SexPopupView(@NonNull Context context) {
            super(context);

        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_addsubcourse;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    addSubCourse();
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();

        }

        @Override
        protected void onDismiss() {

            super.onDismiss();
        }
    }


    private void addSubCourse() {
        RequestUtils.addSubCourse(SharePreUtil.getString(this, "token", ""), start_time, end_time, user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("预约成功");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }
}