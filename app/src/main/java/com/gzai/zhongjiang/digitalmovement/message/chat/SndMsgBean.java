package com.gzai.zhongjiang.digitalmovement.message.chat;

public class SndMsgBean {
    private int  ope;
    private String msg_info;
    private String msg_type;
    private String receive_uid;

    public int getOpe() {
        return ope;
    }

    public void setOpe(int ope) {
        this.ope = ope;
    }

    public String getMsg_info() {
        return msg_info;
    }

    public void setMsg_info(String msg_info) {
        this.msg_info = msg_info;
    }

    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    public String getReceive_uid() {
        return receive_uid;
    }

    public void setReceive_uid(String receive_uid) {
        this.receive_uid = receive_uid;
    }

    public SndMsgBean(int ope, String msg_info, String msg_type, String receive_uid) {
        this.ope = ope;
        this.msg_info = msg_info;
        this.msg_type = msg_type;
        this.receive_uid = receive_uid;
    }
}
