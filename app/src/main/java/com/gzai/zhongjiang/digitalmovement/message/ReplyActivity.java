package com.gzai.zhongjiang.digitalmovement.message;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.home.DynamicDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReplyActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.cancel)
    TextView cancel;
    @BindView(R.id.send)
    TextView send;
    @BindView(R.id.input_reply)
    EditText input_reply;

    String post_id,parent_id,to_user_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);
        ButterKnife.bind(this);
        cancel.setOnClickListener(this);
        send.setOnClickListener(this);
        try {
            Intent intent=getIntent();
            post_id=intent.getStringExtra("post_id");
            parent_id=intent.getStringExtra("parent_id");
            to_user_id=intent.getStringExtra("to_user_id");
        }catch (Exception e){

        }
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.cancel:
                finish();
                break;
            case R.id.send:
                if(input_reply.getText().toString().length()>0){
                    addPostComment();
                }else {
                    ToastUtils.show("请输入回复内容");
                }
                break;
        }
    }

    private void addPostComment() {
        RequestUtils.addPostComment(SharePreUtil.getString(this, "token", ""), post_id, input_reply.getText().toString(), parent_id, to_user_id, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {
                ToastUtils.show("评论成功");
                input_reply.setText("");
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

}