package com.gzai.zhongjiang.digitalmovement.my;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.EducationAdapter;
import com.gzai.zhongjiang.digitalmovement.adapter.Personaladpter;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipInfoBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderListBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.qrcode.ActivityScanerCode;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.youth.banner.Banner;
import com.youth.banner.indicator.CircleIndicator;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.listener.OnPageChangeListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonTActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.scan_image)
    ImageView scan_image;
    @BindView(R.id.smartRefreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.nodata_linear)
    LinearLayout nodata_linear;
    @BindView(R.id.data_linear)
    LinearLayout data_linear;
    @BindView(R.id.scan_Linear)
    LinearLayout scan_Linear;
    @BindView(R.id.banner)
    Banner banner;
    @BindView(R.id.banner_linear)
    LinearLayout banner_linear;

    private int page_total, page = 1;


    ScanOrderListBean dyBean;
    List<ScanOrderListBean> beanList = new ArrayList<>();
    EducationAdapter myAdapter;

    String coach_id = "", user_id, card_id="";


    PersonalVipInfoBean vipInfoBean;
    List<PersonalVipInfoBean> vipInfoBeanList = new ArrayList<>();
    Personaladpter vipAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_t);
        ButterKnife.bind(this);
        actionBarView.setTitle("私人教练");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        Intent intent = getIntent();
        user_id = intent.getStringExtra("user_id");
        intView();


        if (smartRefreshLayout != null) {
            smartRefreshLayout.setEnableLoadMoreWhenContentNotFull(true);
            smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
                @Override
                public void onRefresh(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (card_id.length() > 0) {
                                page = 1;
                                getScanOrderList(card_id);
                            }
                            refreshLayout.finishRefresh();
                        }
                    }, 500);
                }
            });
            smartRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore(@NonNull final RefreshLayout refreshLayout) {
                    refreshLayout.getLayout().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (page < page_total) {
                                page++;
                                loadMore(page);
                            } else {
                            }
                            refreshLayout.finishLoadMore();
                        }
                    }, 500);
                }
            });
        }
        banner.setIndicator(new CircleIndicator(this));
    }


    private void getScanOrderList(String card_id) {
        beanList.clear();
        RequestUtils.getScanOrderList(SharePreUtil.getString(this, "token", ""), 1, 10, user_id, card_id, new ListMyObserver<ListBean<ScanOrderListBean>>(this) {
            @Override
            public void onSuccess(ListBean<ScanOrderListBean> result) {
                ListBean<ScanOrderListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String coach_name = list.getList().get(i).getCoach_name();
                        String order_id = list.getList().get(i).getOrder_id();
                        String truename = list.getList().get(i).getTruename();
                        String status = list.getList().get(i).getStatus();
                        String is_comment = list.getList().get(i).getIs_comment();
                        String record_date = list.getList().get(i).getRecord_date();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new ScanOrderListBean(id, user_id, coach_id, coach_name, order_id, truename, status, is_comment, record_date, update_time, create_time);
                        beanList.add(dyBean);
                    }
                    myAdapter = new EducationAdapter(beanList);
                    recyclerView.setAdapter(myAdapter);
                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void loadMore(int page) {
        RequestUtils.getScanOrderList(SharePreUtil.getString(this, "token", ""), page, 10, user_id, card_id, new ListMyObserver<ListBean<ScanOrderListBean>>(this) {
            @Override
            public void onSuccess(ListBean<ScanOrderListBean> result) {
                ListBean<ScanOrderListBean> list = result;
                page_total = result.getPage_info().getPage_total();
                if (list.getList().size() > 0) {
                    nodata_linear.setVisibility(View.GONE);
                    data_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.getList().size(); i++) {
                        String id = list.getList().get(i).getId();
                        String user_id = list.getList().get(i).getUser_id();
                        String coach_id = list.getList().get(i).getCoach_id();
                        String coach_name = list.getList().get(i).getCoach_name();
                        String order_id = list.getList().get(i).getOrder_id();
                        String truename = list.getList().get(i).getTruename();
                        String status = list.getList().get(i).getStatus();
                        String is_comment = list.getList().get(i).getIs_comment();
                        String record_date = list.getList().get(i).getRecord_date();
                        String update_time = list.getList().get(i).getUpdate_time();
                        String create_time = list.getList().get(i).getCreate_time();
                        dyBean = new ScanOrderListBean(id, user_id, coach_id, coach_name, order_id, truename, status, is_comment, record_date, update_time, create_time);
                        beanList.add(dyBean);
                    }
                    myAdapter.notifyDataSetChanged();
                } else {
                    nodata_linear.setVisibility(View.VISIBLE);
                    data_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    public void intView() {
        RequestUtils.getPersonalVipInfo(SharePreUtil.getString(this, "token", ""), new MyObserver<List<PersonalVipInfoBean>>(this) {
            @Override
            public void onSuccess(List<PersonalVipInfoBean> result) {
                List<PersonalVipInfoBean> list = result;
                if (list.size() > 0) {
                    scan_Linear.setVisibility(View.VISIBLE);
                    banner_linear.setVisibility(View.VISIBLE);
                    for (int i = 0; i < list.size(); i++) {
                        String gym_id = list.get(i).getGym_id();
                        String card_id = list.get(i).getCard_id();
                        String coach_id = list.get(i).getCoach_id();
                        String expire_time = list.get(i).getExpire_time();
                        int times = list.get(i).getTimes();
                        int totals = list.get(i).getTotals();
                        String room_name = list.get(i).getRoom_name();
                        String truename = list.get(i).getTruename();
                        String coach_name = list.get(i).getCoach_name();
                        vipInfoBean = new PersonalVipInfoBean(gym_id, card_id, coach_id, expire_time, times, totals, room_name, truename, coach_name);
                        vipInfoBeanList.add(vipInfoBean);
                    }
                    vipAdapter = new Personaladpter(vipInfoBeanList);
                    banner.setBannerRound(10f);
                    banner.isAutoLoop(false);
                    banner.setAdapter(new Personaladpter(vipInfoBeanList));
                    card_id = vipInfoBeanList.get(0).getCard_id();
                    getScanOrderList(card_id);
                    Log.e("123456",card_id);
                    scan_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(PersonTActivity.this, ActivityScanerCode.class);
                            startActivity(intent);
                        }
                    });
                    banner.addOnPageChangeListener(new OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            card_id = vipInfoBeanList.get(position).getCard_id();
                            coach_id = vipInfoBeanList.get(position).getCoach_id();
                            getScanOrderList(card_id);
                            Log.e("123456",card_id);
                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });
                    banner.setOnBannerListener(new OnBannerListener() {
                        @Override
                        public void OnBannerClick(Object data, int position) {
                            Intent intent = new Intent(PersonTActivity.this, PersonalTrainerActivity.class);
                            intent.putExtra("coach_id", vipInfoBeanList.get(position).getCoach_id());
                            startActivity(intent);
                        }
                    });
                } else {
                    scan_Linear.setVisibility(View.GONE);
                    banner_linear.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

                smartRefreshLayout.setEnableLoadMore(false);
            }
        });
    }
}