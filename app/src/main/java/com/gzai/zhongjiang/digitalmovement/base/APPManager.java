package com.gzai.zhongjiang.digitalmovement.base;

import android.app.Activity;

import java.util.Stack;

public class APPManager {

    private static Stack<Activity> activityStack;

    private static  APPManager instance;


    private APPManager(){

    }

    public static  APPManager getInstance(){

        if(instance==null){
            synchronized ( APPManager.class) {

                if(instance==null){

                    instance=new  APPManager();
                }
            }
        }
        return instance;
    }

    /**
     * 添加Activity到Activity队列中
     * @param activity
     */
    public void addActivity(Activity activity){

        if(activityStack==null){
            activityStack=new Stack<>();
        }
        activityStack.add(activity);
    }

    public void removeActivity(Activity activity){
        if (activity!=null){
            activityStack.remove(activity);
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity(){

        for (int i = 0; i < activityStack.size(); i++) {

            if(activityStack.get(i)!=null){

                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }



}

