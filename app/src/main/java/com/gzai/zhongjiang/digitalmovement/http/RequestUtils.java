package com.gzai.zhongjiang.digitalmovement.http;


import com.gzai.zhongjiang.digitalmovement.bean.CircleInfo;
import com.gzai.zhongjiang.digitalmovement.bean.CircleListBean;
import com.gzai.zhongjiang.digitalmovement.bean.CoachCommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.CoachDetailInfo;
import com.gzai.zhongjiang.digitalmovement.bean.CoachListBean;
import com.gzai.zhongjiang.digitalmovement.bean.CoachQrcodeBean;
import com.gzai.zhongjiang.digitalmovement.bean.ComDasilogBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommDetailBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommDetailBean1;
import com.gzai.zhongjiang.digitalmovement.bean.CommentBean;
import com.gzai.zhongjiang.digitalmovement.bean.CommentListBean;
import com.gzai.zhongjiang.digitalmovement.bean.DyBean;
import com.gzai.zhongjiang.digitalmovement.bean.FaceBean;
import com.gzai.zhongjiang.digitalmovement.bean.FeedBean;
import com.gzai.zhongjiang.digitalmovement.bean.FollowBean;
import com.gzai.zhongjiang.digitalmovement.bean.GymBean;
import com.gzai.zhongjiang.digitalmovement.bean.GymInfo;
import com.gzai.zhongjiang.digitalmovement.bean.GymRegionBean;
import com.gzai.zhongjiang.digitalmovement.bean.HealthInfo;
import com.gzai.zhongjiang.digitalmovement.bean.HighestStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.KeepRecordListBean;
import com.gzai.zhongjiang.digitalmovement.bean.LabelBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyCourse;
import com.gzai.zhongjiang.digitalmovement.bean.MyStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyUserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NewsInfo;
import com.gzai.zhongjiang.digitalmovement.bean.NewsListBean;
import com.gzai.zhongjiang.digitalmovement.bean.NoticeListBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipBean;
import com.gzai.zhongjiang.digitalmovement.bean.PersonalVipInfoBean;
import com.gzai.zhongjiang.digitalmovement.bean.PraiseNoticeListBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanCourseBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderByCoachBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderListBean;
import com.gzai.zhongjiang.digitalmovement.bean.StepBean;
import com.gzai.zhongjiang.digitalmovement.bean.StepListBean;
import com.gzai.zhongjiang.digitalmovement.bean.StudioBean;
import com.gzai.zhongjiang.digitalmovement.bean.TopBean;
import com.gzai.zhongjiang.digitalmovement.bean.UploadImageBean;
import com.gzai.zhongjiang.digitalmovement.bean.UserInfo;
import com.gzai.zhongjiang.digitalmovement.bean.VersionInfo;
import com.gzai.zhongjiang.digitalmovement.bean.VipInfoBean;
import com.gzai.zhongjiang.digitalmovement.http.list.CommListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.CountListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBaseResponse;
import com.gzai.zhongjiang.digitalmovement.http.list.ListBean;
import com.gzai.zhongjiang.digitalmovement.http.list.ListMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.list.NewListBean;
import com.gzai.zhongjiang.digitalmovement.message.chat.MessageBean;

import java.util.List;

import okhttp3.MultipartBody;

public class RequestUtils {
    //获取验证码
    public static void getCode(String mobile, String type, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .getCode(mobile, type).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //登陆
    public static void login(String user_name, String verification, MyObserver<UserInfo> observer) {
        RetrofitUtils.getApiUrl()
                .login(user_name, verification).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //安卓更新信息获取
    public static void getAppVersion(MyObserver<VersionInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getAppVersion("").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //上传图片
    public static void uploadImage(String token, String url, MultipartBody.Part body, MyObserver<UploadImageBean> observer) {
        RetrofitUtils.getApiUrl()
                .uploadImage(token, url, body).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //发布动态(圈子)
    public static void addPost(String token, String type, String circle_id, String title, String content, String image_list, String label_id, String dy_sync, String is_open, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addPost(token, type, circle_id, title, content, image_list, label_id, dy_sync, is_open).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    // 广场/关注/圈子列表
    public static void getPostList(String token, int page, int pageSize, String post_type, ListMyObserver<ListBean<DyBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getPostList(token, page, pageSize, post_type).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 广场/关注/圈子列表
    public static void getPostList_cir(String token, int page, int pageSize, String post_type, String circle_id, String sortby, ListMyObserver<ListBean<DyBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getPostList_cir(token, page, pageSize, post_type, circle_id, sortby).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //关注(用户)
    public static void addFollow(String token, String to_user_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addFollow(token, to_user_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //取消关注(用户)
    public static void cancelFollow(String token, String to_user_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .cancelFollow(token, to_user_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //标签列表
    public static void getLabelList(String token, MyObserver<List<LabelBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getLabelList(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 健身房列表
    public static void getGymList(String token, int page, int pageSize, String lng, String lat, ListMyObserver<ListBean<GymBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getGymList(token, page, pageSize, lng, lat).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //健身房信息
    public static void getGymInfo(String id, MyObserver<GymInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getGymInfo(id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 教练列表
    public static void getCoachList(String token, int page, int pageSize, String gym_id, ListMyObserver<ListBean<CoachListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getCoachList(token, page, pageSize, gym_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //教练详情
    public static void getCoachInfo(String token, String id, MyObserver<CoachDetailInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getCoachInfo(token, id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 圈子列表
    public static void getCircleList(String token, int page, int pageSize, ListMyObserver<ListBean<CircleListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getCircleList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //加入圈子
    public static void joinCircle(String token, String circle_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .joinCircle(token, circle_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //取消关注(用户)
    public static void cancelCircle(String token, String circle_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .cancelCircle(token, circle_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //动态/圈子详情
    public static void getPostInfo(String token, String post_id, MyObserver<DyBean> observer) {
        RetrofitUtils.getApiUrl()
                .getPostInfo(token, post_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //圈子详情
    public static void getCircleInfo(String token, String circle_id, MyObserver<CircleInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getCircleInfo(token, circle_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //动态(圈子)评论
    public static void addPostComment(String token, String post_id, String content, String parent_id, String to_user_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addPostComment(token, post_id, content, parent_id, to_user_id, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    // 动态评论列表
    public static void getPostComment(String token, String post_id, int page, int pageSize, ListMyObserver<ListBean<CommentBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getPostComment(token, post_id, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //动态/评论点赞
    public static void addPraise(String token, String post_id, String type, String comment_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addPraise(token, post_id, type, comment_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //动态/评论取消赞
    public static void cancelPraise(String token, String post_id, String type, String comment_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .cancelPraise(token, post_id, type, comment_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    // 我的点赞列表
    public static void getPraiseNoticeList(String token, int page, int pageSize, ListMyObserver<ListBean<PraiseNoticeListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getPraiseNoticeList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 关注/粉丝列表
    public static void getFollowList(String token, int page, int pageSize, String type, ListMyObserver<ListBean<FollowBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getFollowList(token, page, pageSize, type).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //获取用户信息
    public static void getUserInfo(String token, MyObserver<MyUserInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getUserInfo(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //修改用户信息
    public static void saveUserInfo(String token, String avatar, String sex, String nick_name, String birthday, String intro, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .saveUserInfo(token, avatar, sex, nick_name, birthday, intro).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //获取用户信息
    public static void getOtherUserInfo(String token, String user_id, MyObserver<MyUserInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getOtherUserInfo(token, user_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    public static void getMyPostList(String token, int page, int pageSize, String user_id, ListMyObserver<ListBean<DyBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getMyPostList(token, page, pageSize, user_id, "dynamic").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //子评论列表
    public static void getChildCommentList(String token, int page, int pageSize, String post_id, String parent_id, ListMyObserver<CommListBean<ComDasilogBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getChildCommentList(token, page, pageSize, post_id, parent_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //举报内容（用户）
    public static void accusation(String token, String to_user_id, String content_id, String type, String accusation_type, String memo, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .accusation(token, to_user_id, content_id, type, accusation_type, memo).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //获取健康数据
    public static void getHealthInfo(String token, MyObserver<HealthInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getHealthInfo(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //保存健康数据
    public static void saveHealth(String token,String is_coach,String to_user_id, String weight, String height, String bmi, String fat_rate, String fat,
                                  String metabolic_rate, String whr_rate, String humidity_rate, String muscle_rate, String upperarm_left, String upperarm_right,
                                  String hipline, String bust, String waist, String thigh_left, String thigh_right,
                                  String shank_left, String shank_right, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .saveHealth(token,is_coach,to_user_id, weight, height, bmi, fat_rate, fat, metabolic_rate, whr_rate,
                        humidity_rate, muscle_rate, upperarm_left, upperarm_right, hipline, bust, waist, thigh_left, thigh_right, shank_left, shank_right).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //步数上报
    public static void stepReport(String token, String record_date, String step_count, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .stepReport(token, record_date, step_count).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //健身房区域
    public static void getGymRegion(String token, String gym_id, ListMyObserver<NewListBean<GymRegionBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getGymRegion(token, gym_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //好友步数排行
    public static void getStepRank(String token, String record_date, ListMyObserver<NewListBean<StepBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getStepRank(token, record_date).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //单日最高步数
    public static void getHighestStep(String token, MyObserver<HighestStepBean> observer) {
        RetrofitUtils.getApiUrl()
                .getHighestStep(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //消息通知
    public static void getNoticeList(String token, int page, int pageSize, ListMyObserver<ListBean<NoticeListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getNoticeList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //资讯列表
    public static void getNewsList(String token, int page, int pageSize, ListMyObserver<ListBean<NewsListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getNewsList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //资讯详情
    public static void getNewsInfo(String token, String id, MyObserver<NewsInfo> observer) {
        RetrofitUtils.getApiUrl()
                .getNewsInfo(token, id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //私聊-聊天记录
    public static void getMyMsgList(String token, int page, int pageSize, String send_uid, String create_time, ListMyObserver<ListBean<MessageBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getMyMsgList(token, page, pageSize, send_uid, create_time).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    // 我的评论列表
    public static void getCommentNoticeList(String token, int page, int pageSize, ListMyObserver<ListBean<CommentListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getCommentNoticeList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //圈子置顶动态
    public static void getCircleTopPostList(String token, String circle_id, ListMyObserver<NewListBean<TopBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getCircleTopPostList(token, circle_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //步数点赞
    public static void addStepPraise(String token, String step_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addStepPraise(token, step_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //步数曲线图
    public static void getStepList(String token, String start_date, String end_date, MyObserver<List<StepListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getStepList(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //我的步数排名
    public static void getMyStepRank(String token, MyObserver<MyStepBean> observer) {
        RetrofitUtils.getApiUrl()
                .getMyStepRank(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //评论详情
    public static void getCommentInfo(String token, String id, MyObserver<CommDetailBean> observer) {
        RetrofitUtils.getApiUrl()
                .getCommentInfo(token, id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //评论详情
    public static void getCommentInfo1(String token, String id, MyObserver<CommDetailBean1> observer) {
        RetrofitUtils.getApiUrl()
                .getCommentInfo1(token, id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //会员卡信息
    public static void getVipInfo(String token, MyObserver<List<VipInfoBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getVipInfo(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //私教卡信息
    public static void getPersonalVipInfo(String token, MyObserver<List<PersonalVipInfoBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getPersonalVipInfo(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //意见反馈
    public static void feedQuestion(String token, String content, String image_list, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .feedQuestion(token, content, image_list).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    // 意见反馈记录
    public static void getFeedQuestionList(String token, int page, int pageSize, ListMyObserver<ListBean<FeedBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getFeedQuestionList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //我的预约课程记录
    public static void getMySubCourse(String token, String record_date, String coach_id, MyObserver<List<MyCourse>> observer) {
        RetrofitUtils.getApiUrl()
                .getMySubCourse(token, record_date, coach_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //私教课程预约
    public static void subscribeCourse(String token, String start_time, String end_time, String coach_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .subscribeCourse(token, start_time, end_time, coach_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //教练-获取销课二维码信息
    public static void getCoachQrcode(String token, MyObserver<CoachQrcodeBean> observer) {
        RetrofitUtils.getApiUrl()
                .getCoachQrcode(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //教练-当前私教对应学员列表
    public static void getPersonalList(String token, String search_name, MyObserver<List<StudioBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getPersonalList(token, search_name).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 该教练已被预约课程列表
    public static void getSubCourseList(String token, String record_date, String coach_id, String status, ListMyObserver<CountListBean<MyCourse>> observer) {
        RetrofitUtils.getApiUrl()
                .getSubCourseList(token, record_date, coach_id, status).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    // 会员-健身记录
    public static void getKeepRecordList(String token, int page, int pageSize, ListMyObserver<ListBean<KeepRecordListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getKeepRecordList(token, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    // 会员-健身记录
    public static void getNewKeepRecordList(String token, int page, int pageSize, String user_id, ListMyObserver<ListBean<KeepRecordListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getNewKeepRecordList(token, page, pageSize, user_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //教练-获取销课记录
    public static void getScanOrderByCoach(String token, String month, int page, int pageSize, ListMyObserver<ListBean<ScanOrderByCoachBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getScanOrderByCoach(token, month, page, pageSize).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //教练-销课点评内容获取
    public static void getCoachComment(String token, String order_id, String user_id, MyObserver<CoachCommentBean> observer) {
        RetrofitUtils.getApiUrl()
                .getCoachComment(token, order_id, user_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //教练-对私教课点评
    public static void addCoachComment(String token, String order_id, String user_id, String content, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addCoachComment(token, order_id, user_id, content).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //会员-扫码销课
    public static void scanCourse(String token, String qr_data, MyObserver<ScanCourseBean> observer) {
        RetrofitUtils.getApiUrl()
                .scanCourse(token, qr_data).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //会员-私教卡列表
    public static void getMyPersonalVip(String token, MyObserver<List<PersonalVipBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getMyPersonalVip(token, "").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //会员-确认销课
    public static void updateScanOrder(String token, String order_id, String card_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .updateScanOrder(token, order_id, card_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //会员-订单销课确认评价
    public static void addOrderComment(String token, String order_id, String stars, String content, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addOrderComment(token, order_id, stars, content).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //会员-销课记录
    public static void getScanOrderList(String token, int page, int pageSize, String user_id, String card_id, ListMyObserver<ListBean<ScanOrderListBean>> observer) {
        RetrofitUtils.getApiUrl()
                .getScanOrderList(token, page, pageSize, user_id, card_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }

    //教练-添加预约课程
    public static void addSubCourse(String token, String start_time, String end_time, String user_id, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .addSubCourse(token, start_time, end_time, user_id).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //教练-确认/取消预约课
    public static void setSubCourse(String token, String id, String start_time, String end_time, String status, NollDataMyObserver<NullData> observer) {
        RetrofitUtils.getApiUrl()
                .setSubCourse(token, id, start_time, end_time, status).compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }


    //用户-人脸录入(注册)
    public static void faceAdd(String token, String image, MyObserver<FaceBean> observer) {
        RetrofitUtils.getApiUrl()
                .faceAdd(token, image,"URL").compose(RxHelper.observableIO2Main())
                .subscribe(observer);
    }
}
