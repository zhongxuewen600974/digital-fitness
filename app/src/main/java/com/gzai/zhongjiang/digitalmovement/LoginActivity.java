package com.gzai.zhongjiang.digitalmovement;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.UserInfo;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.AgreementActivity;
import com.gzai.zhongjiang.digitalmovement.util.PrivacyActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.hjq.toast.ToastUtils;


import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.login_input_phone)
    EditText input_phone;
    @BindView(R.id.input_code)
    EditText input_code;
    @BindView(R.id.getcode)
    TextView getcode;
    @BindView(R.id.login_button)
    Button login_button;
    @BindView(R.id.agreement)
    TextView agreement;
    @BindView(R.id.privacy)
    TextView privacy;


    private static int isExit = 0;
    private static final int TIME_EXIT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        if (SharePreUtil.getString(LoginActivity.this, "is_login", "").equals("true")) {
            Intent Stingintent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(Stingintent);
        }
        getcode.setOnClickListener(this);
        login_button.setOnClickListener(this);
        agreement.setOnClickListener(this);
        privacy.setOnClickListener(this);
    }
    @Override
    protected void onResume() {
        if (SharePreUtil.getString(LoginActivity.this, "is_login", "").equals("true")) {
            Intent Stingintent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(Stingintent);
        }
        super.onResume();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            exit();
        }
        return false;
    }

    //设置主页延迟推出
    private void exit() {
        if (isExit < 1) {
            Toast.makeText(getApplicationContext(), "再点击一次返回退出程序", Toast.LENGTH_SHORT).show();
            mmCountDownTimer.start();
        } else {
            isExit = 0;
            Toast.makeText(getApplicationContext(), "退出程序", Toast.LENGTH_SHORT).show();
            mmCountDownTimer.cancel();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
            System.exit(0);
        }
    }
    private CountDownTimer mmCountDownTimer = new CountDownTimer(TIME_EXIT, 1000) {
        @Override
        public void onTick(long l) {
            isExit++;
        }

        @Override
        public void onFinish() {
            isExit = 0;
        }
    };

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.getcode:
                String phone = input_phone.getText().toString();
                if (phone.length() == 11) {
                    mCountDownTimer.start();
                    getLoginCode(phone);
                } else if (phone.length() == 0) {
                    ToastUtils.show("请输入手机号");
                } else {
                    ToastUtils.show("请输入完整手机号");
                }
                break;
            case R.id.login_button:
                String myphone = input_phone.getText().toString();
                String mycode = input_code.getText().toString();
                if(myphone.isEmpty()){
                    ToastUtils.show("请输入手机号");
                    return;
                }else if(mycode.isEmpty()){
                    ToastUtils.show("请输入验证码");
                    return;
                }else {
                    Login(myphone,mycode);
                }
                break;
            case R.id.privacy:
                Intent intent = new Intent(LoginActivity.this, PrivacyActivity.class);
                startActivity(intent);
                break;
            case R.id.agreement:
                Intent intent2 = new Intent(LoginActivity.this, AgreementActivity.class);
                startActivity(intent2);
                break;
        }
    }
    private void Login(String myphone, String mycode) {
        RequestUtils.login(myphone, mycode, new MyObserver<UserInfo>(this) {
            @Override
            public void onSuccess(UserInfo result) {
                SharePreUtil.putString(LoginActivity.this,"is_login","true");
                SharePreUtil.putString(LoginActivity.this,"user_id",result.getUser_id());
                SharePreUtil.putString(LoginActivity.this,"token",result.getToken());


                if(result.getFirst_login().equals("0")){
                    Intent Stingintent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(Stingintent);
                }else {
                    Intent Stingintent = new Intent(LoginActivity.this, ImproveInformationActivity.class);
                    Stingintent.putExtra("nick_name",result.getNick_name());
                    startActivity(Stingintent);
                }

            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }



    private void getLoginCode(String myphone) {
        RequestUtils.getCode(myphone,"0", new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result ) {

            }
            @Override
            public void onFailure(Throwable e, String errorMsg) {
                ToastUtils.show(errorMsg);
            }
        });
    }

    private CountDownTimer mCountDownTimer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long l) {
            getcode.setEnabled(false);
            getcode.setText((l / 1000) + "秒后可重发");
        }

        @Override
        public void onFinish() {
            getcode.setEnabled(true);
            getcode.setText("获取验证码");
        }
    };
}