package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.azhon.appupdate.config.UpdateConfiguration;
import com.azhon.appupdate.manager.DownloadManager;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.VersionInfo;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.hjq.toast.ToastUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsActivity extends BaseActivity {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.cherk_version)
    LinearLayout cherk_version;

    private String version;
    private DownloadManager manager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        ButterKnife.bind(this);
        actionBarView.setTitle("关于");
        version = getVersionCode(this);
        cherk_version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cherVersion(version);
            }
        });
    }
    private void cherVersion(String version) {
        RequestUtils.getAppVersion(new MyObserver<VersionInfo>(this) {
            @Override
            public void onSuccess(VersionInfo result) {
                if (version.equals(result.getInfo().getApp_ver())) {
                    ToastUtils.show("当前已是最新版本");
                } else {
                    startUpdate(result.getInfo().getApp_file(), result.getInfo().getApp_ver(), result.getInfo().getApp_desc());
                    Log.e("123456", result.getInfo().getApp_file());
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void startUpdate(String app_file, String app_ver, String app_desc) {
        UpdateConfiguration configuration = new UpdateConfiguration()
                .setEnableLog(true)
                .setJumpInstallPage(true)
                .setDialogButtonTextColor(Color.WHITE)
                .setShowNotification(true)
                .setShowBgdToast(false)
                .setForcedUpgrade(false);
        manager = DownloadManager.getInstance(this);
        manager.setApkName("数字运动.apk")
                .setApkUrl(app_file)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setShowNewerToast(true)
                .setConfiguration(configuration)
                .setApkVersionCode(2)
                .setApkVersionName(app_ver)
                .setApkSize("15")
                .setApkDescription(app_desc)
                .download();
    }

    public static synchronized String getVersionCode(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(
                    context.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}