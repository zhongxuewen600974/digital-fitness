package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.adapter.StepTextAdapter;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.bean.HighestStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.MyStepBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.bean.StepListBean;
import com.gzai.zhongjiang.digitalmovement.http.MyObserver;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.util.step.ISportStepInterface;
import com.gzai.zhongjiang.digitalmovement.util.step.TodayStepManager;
import com.gzai.zhongjiang.digitalmovement.util.step.TodayStepService;
import com.hjq.toast.ToastUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StepsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.today_time)
    TextView today_time;
    @BindView(R.id.ranking)
    TextView ranking;
    @BindView(R.id.today_step)
    TextView today_step;
    @BindView(R.id.linear_step)
    LinearLayout linear_step;
    @BindView(R.id.max_step)
    TextView max_step;
    @BindView(R.id.max_step_day)
    TextView max_step_day;
    @BindView(R.id.cash_chartline)
    LineChart chart1;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    SimpleDateFormat simpleDateFormat;
    int step = 0;
    private Handler mDelayHandler = new Handler(new TodayStepCounterCall());
    private int mStepSum;
    private ISportStepInterface iSportStepInterface;
    private static final int REFRESH_STEP_WHAT = 0;
    private long TIME_INTERVAL_REFRESH = 3000;
    String datedate;

    StepListBean stepListBean;
    List<StepListBean> beanList = new ArrayList<>();
    StepTextAdapter stepTextAdapter;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            "android.hardware.sensor.stepcounter",
            "feature:android.hardware.sensor.stepdetector",
            "android.permission.ACTIVITY_RECOGNITION"
    };

    public static void verifyStoragePermissions(Activity activity) {

        try {
            //检测是否有写的权限
            int permission = ActivityCompat.checkSelfPermission(activity,
                    "android.permission.ACTIVITY_RECOGNITION");
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有写的权限，去申请写的权限，会弹出对话框
                ActivityCompat.requestPermissions(activity, PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_steps);
        ButterKnife.bind(this);
        back.setOnClickListener(this);
        simpleDateFormat = new SimpleDateFormat("MM月dd");
        Date date2 = new Date(System.currentTimeMillis());
        today_time.setText(simpleDateFormat.format(date2) + " 总步数");
        linear_step.setOnClickListener(this);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        datedate = sdf.format(new Date());

//        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
//        recyclerView.setLayoutManager(layoutManager);


        mhandmhandlele.post(timeRunable);
        getHighestStep();
        getStepList();
        getMyStepRank();
        verifyStoragePermissions(this);


        TodayStepManager.startTodayStepService(getApplication());
        //开启计步Service，同时绑定Activity进行aidl通信
        Intent intent = new Intent(this, TodayStepService.class);
        intent.setAction("com.today.step.lib.TodayStepService");
        startService(intent);
        bindService(new Intent(this, TodayStepService.class), mConnection, Context.BIND_AUTO_CREATE);


    }

    private void getMyStepRank() {
        RequestUtils.getMyStepRank(SharePreUtil.getString(this, "token", ""),  new MyObserver<MyStepBean>(this) {
            @Override
            public void onSuccess(MyStepBean result) {
                if(today_step.getText().toString().equals("0")){
                    today_step.setText(result.getStep_count());
                }
                ranking.setText(result.getRownum());
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            //Activity和Service通过aidl进行通信
            iSportStepInterface = ISportStepInterface.Stub.asInterface(service);
            try {
                mStepSum = iSportStepInterface.getCurrentTimeSportStep();
                updateStepCount();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            mDelayHandler.sendEmptyMessageDelayed(REFRESH_STEP_WHAT, TIME_INTERVAL_REFRESH);

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };


    private void getStepList() {
        beanList.clear();
        RequestUtils.getStepList(SharePreUtil.getString(this, "token", ""), "", "", new MyObserver<List<StepListBean>>(this) {
            @Override
            public void onSuccess(List<StepListBean> result) {
                List<StepListBean> list = result;
                if (list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        String step_count = list.get(i).getStep_count();
                        String record_date = list.get(i).getRecord_date();
                        stepListBean = new StepListBean(step_count, record_date);
                        beanList.add(stepListBean);
                    }
                    stepTextAdapter = new StepTextAdapter(beanList);
                    recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(), list.size()));
                    recyclerView.setAdapter(stepTextAdapter);
                    getchart1();
                } else {
                    ToastUtils.show("暂无数据");
                }
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void getchart1() {
        Typeface tf = Typeface.createFromAsset(this.getAssets(), "OpenSans-Light.ttf");
        //  chart1.setViewPortOffsets(50, 0, 50, 0);
        chart1.getDescription().setEnabled(false);
        chart1.setTouchEnabled(true);
        chart1.setDrawBorders(false);
        chart1.setDragEnabled(true);
        chart1.setScaleEnabled(false);
        chart1.setDrawGridBackground(false);
        XAxis x = chart1.getXAxis();
        x.setTypeface(tf);
        x.setDrawGridLines(false);
        x.setDrawAxisLine(true);
        // x.setLabelCount(cashBeanList.size(), true);
        x.setEnabled(false);


        YAxis y = chart1.getAxisLeft();
        y.setEnabled(false);
        y.setTypeface(tf);
        y.setTextColor(Color.BLACK);
        y.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        y.setAxisLineColor(Color.BLACK);
        y.setDrawAxisLine(true);
        y.setAxisMinimum(0f);
        YAxis yy = chart1.getAxisRight();
        yy.setAxisMinimum(0f);
        yy.setEnabled(false);


//        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
//        mv.setChartView(chart1); // For bounds control
//        chart1.setMarker(mv);


        chart1.setDragXEnabled(true);
        chart1.setPinchZoom(true);
        chart1.animateX(750);
        chart1.getAxisLeft().setDrawGridLines(true);
        chart1.getXAxis().setDrawGridLines(false);
        chart1.getXAxis().setDrawAxisLine(false);
        chart1.invalidate();
        setData1();
    }

    private void setData1() {
        ArrayList<Entry> values = new ArrayList<>();
        LineDataSet set1;

        for (int i = 0; i < beanList.size(); i++) {
            int val = Integer.parseInt(beanList.get(i).getStep_count());
            values.add(new Entry(i, val));
        }

        if (chart1.getData() != null &&
                chart1.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) chart1.getData().getDataSetByIndex(0);
            set1.setValues(values);
            chart1.getData().notifyDataChanged();
            chart1.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "");
            set1.setMode(LineDataSet.Mode.HORIZONTAL_BEZIER);
            set1.setDrawCircles(true);

            set1.setCircleColor(Color.parseColor("#FFFFFF"));
            set1.setLineWidth(1.8f);
            set1.setValueTextSize(10f);
            set1.setCircleRadius(4f);
            set1.setHighLightColor(Color.WHITE);
            set1.setHighlightLineWidth(0f);
            set1.setColor(Color.parseColor("#FFFFFF"));
            LineData data = new LineData(set1);
            data.setDrawValues(true);
            // set data
            chart1.setData(data);
        }


    }


    private void getHighestStep() {
        RequestUtils.getHighestStep(SharePreUtil.getString(this, "token", ""), new MyObserver<HighestStepBean>(this) {
            @Override
            public void onSuccess(HighestStepBean result) {
                max_step_day.setText(result.getRecord_date());
                max_step.setText(result.getStep_count());
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.linear_step:
                if (step > 0) {
                    stepReport(datedate, step + "");
                }
                Intent Stingintent = new Intent(StepsActivity.this, StepRankingActivity.class);
                startActivity(Stingintent);
                break;
        }
    }

    @Override
    protected void onResume() {
        if (step > 0) {
            stepReport(datedate, step + "");
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        if (step > 0) {
            stepReport(datedate, step + "");
        }
        super.onPause();
    }

    private void stepReport(String record_date, String step_count) {
        RequestUtils.stepReport(SharePreUtil.getString(this, "token", ""), record_date, step_count, new NollDataMyObserver<NullData>(this) {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {
            }
        });
    }


    class TodayStepCounterCall implements Handler.Callback {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case REFRESH_STEP_WHAT: {
                    //每隔500毫秒获取一次计步数据刷新UI
                    if (null != iSportStepInterface) {

                        try {
                            step = iSportStepInterface.getCurrentTimeSportStep();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        if (mStepSum != step) {
                            mStepSum = step;
                            updateStepCount();
                        }
                    }
                    mDelayHandler.sendEmptyMessageDelayed(REFRESH_STEP_WHAT, TIME_INTERVAL_REFRESH);

                    break;
                }
            }
            return false;
        }
    }

    private void updateStepCount() {
        today_step.setText(mStepSum + "");
    }

    /*****************计时器*******************/
    private Runnable timeRunable = new Runnable() {
        @Override
        public void run() {
            currentSecond = currentSecond + 1000;
            if (!isPause) {
                //递归调用本runable对象，实现每隔一秒一次执行任务
                mhandmhandlele.postDelayed(this, 1000);
            }
        }
    };
    //计时器
    private Handler mhandmhandlele = new Handler();
    private boolean isPause = false;//是否暂停
    private long currentSecond = 0;//当前毫秒数
/*****************计时器*******************/

    /**
     * 根据毫秒返回时分秒
     *
     * @param time
     * @return
     */
    public static String getFormatHMS(long time) {
        time = time / 1000;//总秒数
        int s = (int) (time % 60);//秒
        int m = (int) (time / 60);//分
        int h = (int) (time / 3600);//秒
        return String.format("%02d:%02d:%02d", h, m, s);
    }
}