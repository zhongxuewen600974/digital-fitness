package com.gzai.zhongjiang.digitalmovement.base;

import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;


public class BaseActivity extends AppCompatActivity {

    public  BaseActivity mActivity;
    private FinishBroadcast broadcast;
    private LoadingDialog loadingDialog;



    /**
     * 设置优先级，注册广播。 用于程序退出时，关闭所有页面。优先级值越小，执行的越后面。
     *
     * @param prior 广播的优先级
     */
    public void registerBoradcastReceiver(int prior) {
        broadcast = new FinishBroadcast();
        IntentFilter filter = new IntentFilter();
        filter.setPriority(prior);
        filter.addAction(FinishBroadcast.mAction);
        registerReceiver(broadcast, filter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = this;
        registerBoradcastReceiver(100);
        APPManager.getInstance().addActivity(this);

        setStatusBar();
    }

    /**
     * 修改状态栏颜色，支持4.4以上版本
     * @param activity
     * @param colorId
     */
    protected boolean useThemestatusBarColor = false;//是否使用特殊的标题栏背景颜色，android5.0以上可以设置状态栏背景色，如果不使用则使用透明色值
    protected void setStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//5.0及以上
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            //根据上面设置是否对状态栏单独设置颜色
            if (useThemestatusBarColor) {
                // getWindow().setStatusBarColor(getResources().getColor(R.color.colorTheme));
            } else {
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4到5.0
            WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
            localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | localLayoutParams.flags);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {//android6.0以后可以对状态栏文字颜色和图标进行修改
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcast);
        APPManager.getInstance().removeActivity(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    public void showLoadingProgressDialog() {
        this.showProgressDialog("加载中...");
    }

    public void showuploadProgressDialog() {
        this.showProgressDialog("上传中...");
    }


    public void showProgressDialog(String msg) {
        if (this.loadingDialog == null) {
            this.loadingDialog = new LoadingDialog(this);
        }
        this.loadingDialog.setCanceledOnTouchOutside(false);
        this.loadingDialog.showLoadDialog(msg);
    }

    public void dismissProgressDialog() {
        if (this.loadingDialog != null) {
            this.loadingDialog.dismiss();
        }
    }

}
