package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.CommentChild;
import com.gzai.zhongjiang.digitalmovement.bean.FollowBean;
import com.gzai.zhongjiang.digitalmovement.bean.NullData;
import com.gzai.zhongjiang.digitalmovement.home.DynamicDetailsActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.http.NollDataMyObserver;
import com.gzai.zhongjiang.digitalmovement.http.RequestUtils;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;
import com.gzai.zhongjiang.digitalmovement.view.CircleImageView;
import com.hjq.toast.ToastUtils;
import com.lxj.xpopup.XPopup;
import com.lxj.xpopup.core.BottomPopupView;
import com.lxj.xpopup.core.CenterPopupView;

import java.util.List;

public class FollowAdapter extends RecyclerView.Adapter<FollowAdapter.ViewHolder> {
    private Context mContext;
    private List<FollowBean> dataBean;
    private String uid;
    private int pos;

    public FollowAdapter(List<FollowBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_follow, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            if (dataBean.get(position).getAvatar().length() > 0) {
                Glide.with(mContext).load(dataBean.get(position).getAvatar()).into(holder.circleImageView);
            } else {
                holder.circleImageView.setImageResource(R.drawable.mine_head_icon);
            }
            if (dataBean.get(position).getIsmutual().equals("0")) {
                holder.isfollow.setText("已关注");
            } else {
                holder.isfollow.setText("相互关注");
            }
            holder.name.setText(dataBean.get(position).getNick_name());
        } catch (Exception e) {
        }
        holder.isfollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uid = dataBean.get(position).getUser_id();
                pos = position;
                ShowDailg();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Stingintent = new Intent(mContext, OthersPageActivity.class);
                Stingintent.putExtra("user_id", dataBean.get(position).getUser_id());
                mContext.startActivity(Stingintent);
            }
        });
    }


    private void cancelFollow(String to_user_id) {
        RequestUtils.cancelFollow(SharePreUtil.getString(mContext, "token", ""), to_user_id, new NollDataMyObserver<NullData>(mContext) {
            @Override
            public void onSuccess(String result) {
                dataBean.remove(pos);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable e, String errorMsg) {

            }
        });
    }

    private void ShowDailg() {
        new XPopup.Builder(mContext)
                .hasStatusBarShadow(true)
                .autoOpenSoftInput(true)
                .asCustom(new ToCommentPopup(mContext))
                .show();
    }

    public class ToCommentPopup extends CenterPopupView {
        public ToCommentPopup(@NonNull Context context) {
            super(context);
        }

        @Override
        protected int getImplLayoutId() {
            return R.layout.dailog_follow;
        }

        @Override
        protected void onCreate() {
            super.onCreate();
            findViewById(R.id.again).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
            findViewById(R.id.determine).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelFollow(uid);
                    dismiss();
                }
            });
        }

        @Override
        protected void onShow() {
            super.onShow();
        }

        @Override
        protected void onDismiss() {
            super.onDismiss();
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView circleImageView;
        TextView name, isfollow;
        LinearLayout linearLayout;
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.circleImageView);
            name = (TextView) itemView.findViewById(R.id.name);
            linearLayout = (LinearLayout) itemView.findViewById(R.id.isfollow_linear);
            imageView = (ImageView) itemView.findViewById(R.id.isfollow_icon);
            isfollow = (TextView) itemView.findViewById(R.id.isfollow);
        }
    }

}
