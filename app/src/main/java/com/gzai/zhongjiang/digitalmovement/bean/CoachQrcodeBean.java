package com.gzai.zhongjiang.digitalmovement.bean;

public class CoachQrcodeBean {
    private String qr_data;
    private String order_id;

    public String getQr_data() {
        return qr_data;
    }

    public void setQr_data(String qr_data) {
        this.qr_data = qr_data;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
