package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.home.OthersPageActivity;
import com.gzai.zhongjiang.digitalmovement.util.CacheUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.setting_linear1)
    LinearLayout l1;
    @BindView(R.id.setting_linear2)
    LinearLayout l2;
    @BindView(R.id.setting_linear3)
    LinearLayout l3;
    @BindView(R.id.setting_linear4)
    LinearLayout l4;
    @BindView(R.id.show_cache)
    TextView show_cache;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        actionBarView.setTitle("设置");
        l1.setOnClickListener(this);
        l2.setOnClickListener(this);
        l3.setOnClickListener(this);
        l4.setOnClickListener(this);

        try {
            show_cache.setText(CacheUtil.getTotalCacheSize(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        int id=v.getId();
        switch (id){
            case R.id.setting_linear1:
                Intent intent = new Intent(SettingActivity.this, ModInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.setting_linear2:
                Intent intent2 = new Intent(SettingActivity.this, SafeActivity.class);
                startActivity(intent2);
                break;
            case R.id.setting_linear3:
                CacheUtil.clearAllCache(mActivity);
                new Handler().postAtTime(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            show_cache.setText(CacheUtil.getTotalCacheSize(mActivity));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, 2000);
                break;
            case R.id.setting_linear4:
                Intent intent3 = new Intent(SettingActivity.this, AboutUsActivity.class);
                startActivity(intent3);
                break;
        }
    }
}