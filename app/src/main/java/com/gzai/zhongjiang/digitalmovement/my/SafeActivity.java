package com.gzai.zhongjiang.digitalmovement.my;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.util.FileUtils;
import com.gzai.zhongjiang.digitalmovement.LoginActivity;
import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.base.ActionBarView;
import com.gzai.zhongjiang.digitalmovement.base.BaseActivity;
import com.gzai.zhongjiang.digitalmovement.util.SharePreUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SafeActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.actionBarRoot)
    ActionBarView actionBarView;
    @BindView(R.id.show_phone)
    TextView show_phone;
    @BindView(R.id.exit)
    TextView exit;
    @BindView(R.id.mod_phone_Linear)
    LinearLayout mod_phone_Linear;
    @BindView(R.id.face_cd)
    LinearLayout face_cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe);
        ButterKnife.bind(this);
        actionBarView.setTitle("账号与安全");
        mod_phone_Linear.setOnClickListener(this);
        exit.setOnClickListener(this);
        face_cd.setOnClickListener(this);
        String phone= SharePreUtil.getString(this,"mobile","");
        show_phone.setText(phone.substring(0,4)+"****"+phone.substring(7));
    }

    @Override
    public void onClick(View v) {
       int id=v.getId();
       switch (id){
           case R.id.mod_phone_Linear:
               Intent intent1=new Intent(SafeActivity.this,ModPhoneActivity.class);
               startActivity(intent1);
               break;
           case R.id.exit:
               SharePreUtil.putString(SafeActivity.this,"is_login","fase");
               Intent intent=new Intent(SafeActivity.this,LoginActivity.class);
               startActivity(intent);
               break;
           case R.id.face_cd:
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);
               }
               Intent intent2=new Intent(SafeActivity.this,RecordVideoActivity.class);
               startActivity(intent2);


               break;
       }
    }


}