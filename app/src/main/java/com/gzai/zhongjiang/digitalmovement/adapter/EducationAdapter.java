package com.gzai.zhongjiang.digitalmovement.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gzai.zhongjiang.digitalmovement.R;
import com.gzai.zhongjiang.digitalmovement.bean.KeepRecordListBean;
import com.gzai.zhongjiang.digitalmovement.bean.ScanOrderListBean;
import com.gzai.zhongjiang.digitalmovement.coach.CoachCommentsActivity;
import com.gzai.zhongjiang.digitalmovement.coach.EliminateClassActivity;
import com.gzai.zhongjiang.digitalmovement.coach.StudyOrderCommentActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class EducationAdapter extends RecyclerView.Adapter<EducationAdapter.ViewHolder> {
    private Context mContext;
    private List<ScanOrderListBean> dataBean;

    public EducationAdapter(List<ScanOrderListBean> list) {
        this.dataBean = list;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        int s = 0;
        try {
            s = dataBean.size();
        } catch (Exception e) {

        }
        return s;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_education, parent, false);
        if (mContext == null) {
            mContext = parent.getContext();
        }
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }


    public static String stampToDate(String s) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(Long.parseLong(s + "000"));
        res = simpleDateFormat.format(date);
        return res;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            holder.create_time.setText(stampToDate(dataBean.get(position).getCreate_time()));
            holder.coach_name.setText("教练："+dataBean.get(position).getCoach_name());
            if(dataBean.get(position).getIs_comment().equals("0")){
                holder.is_comment.setVisibility(View.VISIBLE);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent Stingintent = new Intent(mContext, StudyOrderCommentActivity.class);
                        Stingintent.putExtra("avatar","");
                        Stingintent.putExtra("coach_id",dataBean.get(position).getCoach_id());
                        Stingintent.putExtra("name", dataBean.get(position).getCoach_name());
                        Stingintent.putExtra("order_id", dataBean.get(position).getOrder_id());
                        mContext.startActivity(Stingintent);
                    }
                });

            }else {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.is_comment.setVisibility(View.GONE);
                        Intent Stingintent = new Intent(mContext, CoachCommentsActivity.class);
                        Stingintent.putExtra("user_id",dataBean.get(position).getUser_id());
                        Stingintent.putExtra("order_id", dataBean.get(position).getOrder_id());
                        mContext.startActivity(Stingintent);
                    }
                });
            }
        } catch (Exception e) {
        }
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView create_time,coach_name,is_comment;
        ImageView go_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            create_time = (TextView) itemView.findViewById(R.id.create_time);
            coach_name = (TextView) itemView.findViewById(R.id.coach_name);
            is_comment = (TextView) itemView.findViewById(R.id.is_comment);
            go_image = (ImageView) itemView.findViewById(R.id.go_image);
        }
    }

}
